<?php
/*
Plugin Name: Jigoshop Official UPS shipping
Plugin URI: https://www.jigoshop.com/product/jigoshop-ups-shipping/
Description: An official UPS Extension Module for Jigoshop.
Version: 3.0.6
Author: @vasilguruli | JigoLtd
*/
// Define plugin name
define('JIGOSHOP_UPS_SHIPPING_NAME', 'Jigoshop Official UPS shipping');

add_action('plugins_loaded', function () {
	load_plugin_textdomain('jigoshop_ups', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_UPS_SHIPPING_DIR', dirname(__FILE__));
		// Define plugin URL for assets
		define('JIGOSHOP_UPS_SHIPPING_URL', plugins_url('', __FILE__));
		//Check version.
		if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_UPS_SHIPPING_NAME, '2.0')) {
			return;
		}
		//Check license.
		$licence = new \Jigoshop\Licence(__FILE__, 'add_licence_number', 'http://www.jigoshop.com');
		if (!$licence->isActive()) {
			return;
		}
		//Init components.
		require_once(JIGOSHOP_UPS_SHIPPING_DIR . '/src/Jigoshop/Extension/Ups/Common.php');
		if (is_admin()) {
			require_once(JIGOSHOP_UPS_SHIPPING_DIR . '/src/Jigoshop/Extension/Ups/Admin.php');
		}
	}else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.', 'jigoshop_ups'),
				JIGOSHOP_UPS_SHIPPING_NAME, JIGOSHOP_UPS_SHIPPING_NAME);
			echo '</p></div>';
		});
	}
});
