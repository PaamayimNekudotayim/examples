jQuery(function (t) {
    return t("div.ups_shipping_rate_countries_field").show(), t("#ups_shipping_available_for").on("change", function () {
        return "specific" === t(this).val() ? t("#ups_shipping_rate_countries").closest("tr").show() : t("#ups_shipping_rate_countries").closest("tr").hide()
    })
});