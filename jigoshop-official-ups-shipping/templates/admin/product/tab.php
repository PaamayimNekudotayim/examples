<fieldset>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
                <label for="_ups_ships_separate" class="col-xs-12 col-sm-2 margin-top-bottom-9"><?php _e('Ships Separately?', 'jigoshop_ups');?></label>
                <div class="col-xs-8 col-sm-9" style="margin-top: 6px; margin-left: -40px;">
                    <input name="ID" value="<?php echo $ID; ?>" type="hidden">
                    <input name="_ups_ships_separate" class="checkbox" type="checkbox" <?php echo $checked; ?>/>
                </div>
            </div>
        </div>
    </div>
</fieldset>