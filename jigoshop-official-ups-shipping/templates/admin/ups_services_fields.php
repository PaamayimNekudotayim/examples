<?php
/**
 * @var $services array List of available services
 * @var $methods array List of selected methods
 */
?>
<ul style="margin-left:30px;">
    <?php foreach($services as $key=>$value): ?>
        <li><input type="checkbox" name="jigoshop_ups_services[<?php echo $key; ?>]" style="min-width:50px;" <?php checked(isset($methods[$key]) && $methods[$key] == 'on'); ?> /><?php echo $value; ?></li>
    <?php endforeach; ?>
</ul>
