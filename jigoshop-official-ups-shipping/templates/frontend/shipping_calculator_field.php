<p><?php _e('Destination Type', 'jigoshop_ups'); ?></p>
<p class="form-row ups-destination-type-row">
	<label  for="ups_calc_shipping_destination_type" id="ups-destination-type-label"></label>
	<select style="cursor:pointer;" name="ups_calc_shipping_destination_type"  id="ups-calc-shipping-destination-type" class="jigoshop2_is_awesome">
		<option value="residential" <?php selected('residential', esc_attr($_SESSION['ups_calc_shipping_destination_type']), true); ?>><?php _e('Residential Address', 'jigoshop_ups'); ?></option>
		<option value="commercial" <?php selected('commercial', esc_attr($_SESSION['ups_calc_shipping_destination_type']), true); ?>><?php _e('Commercial Address', 'jigoshop_ups'); ?></option>
	</select>
</p>