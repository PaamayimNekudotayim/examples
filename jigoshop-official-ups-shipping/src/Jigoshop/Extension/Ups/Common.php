<?php

namespace Jigoshop\Extension\Ups;

use Jigoshop\Container;
use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;

class Common
{
    private $options;

    public function __construct()
    {
        Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
        Integration\Helper\Render::addLocation('ups_shipping', JIGOSHOP_UPS_SHIPPING_DIR);
        /** @var Container $di */
        $di = Integration::getService('di');
        $di->services->setDetails('jigoshop.shipping.ups_shipping', __NAMESPACE__ . '\\Common\\UpsShipping', array(
            'jigoshop.options',
            'jigoshop.service.cart',
            'jigoshop.service.order',
            'jigoshop.messages',
            'jigoshop.service.customer',
            'jigoshop.service.shipping',
        ));
        $di->triggers->add('jigoshop.service.shipping', 'jigoshop.service.shipping', 'addMethod', array('jigoshop.shipping.ups_shipping'));
        add_action('init', function () {
            $this->options = Integration::getOptions()->get('shipping.jigoshop_ups_shipping');

            if ($this->options['jigoshop_ups_destination_type'] == 'both') {
                add_action('jigoshop\templates\shop\cart\after_calculator', array($this, 'shippingCalculatorField'));
                add_filter('jigoshop\checkout\billing_fields', array($this, 'billingFields'));
            }
        });

    }

    public function shippingCalculatorField()
    {
        Render::output('ups_shipping', 'frontend/shipping_calculator_field', array());
    }

    public function billingFields($billingFields)
    {
        $billingFields[] = array(
            'type' => 'select',
            'name' => 'ups_shipping_destination_type',
            'label' => __('Destination Type', 'jigoshop_ups'),
            'required' => false,
            'options' => array(
                'residential' => __('Residential Address', 'jigoshop_ups'),
                'commercial' => __('Commercial Address', 'jigoshop_ups')
            ),
            'id' => array('ups_shipping_destination_type'),
            'selected' => isset($_SESSION['ups_calc_shipping_destination_type']) ? $_SESSION['ups_calc_shipping_destination_type'] : ''
        );

        return $billingFields;
    }
}

new Common();