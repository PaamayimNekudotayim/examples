<?php
/**
 * Created by PhpStorm.
 * User: wicia
 * Date: 15.07.16
 * Time: 11:46
 */

namespace Jigoshop\Extension\Ups\Common;

use Jigoshop\Integration\Helper\Render;

class Tab
{
	/**
	 * Product constructor.
	 */
	public function __construct()
	{
		add_filter('jigoshop\admin\product\menu', array($this, 'addMenu'));
		add_filter('jigoshop\admin\product\tabs', array($this, 'addTabs'), 10, 2);
		add_action('jigoshop\service\product\save', array($this, 'saveProduct'), 20);
		//add_action('admin_enqueue_scripts', array($this, 'adminAssets'));
	}
	
	/**
	 * @param array $menu
	 * @return array mixed
	 */
	public function addMenu($menu)
	{
		$menu['ups_shipping'] = array(
			'label' => __('Ups Extension', 'jigoshop_ups'),
			'visible' => true,
		);
		
		return $menu;
	}
	
	/**
	 * @param array $tabs
	 * @param \Jigoshop\Entity\Product $product
	 * @return array mixed
	 */
	public function addTabs($tab, $product)
	{
		$ID = $product->getId();
		$ups_settings = get_post_meta($ID, '_ups_ships_separate', true);
		$checked = (isset($ups_settings) && $ups_settings == 'yes') ? 'checked="checked"' : '';
		
		$tab['ups_shipping'] = Render::get('ups_shipping', 'admin/product/tab', array(
			'checked' => $checked,
			'ID' => $ID,
		));
		
		return $tab;
	}

	/**
	 * @param \Jigoshop\Entity\Product $product
	 */
	public function saveProduct($product)
	{
		$setting = (isset($_POST['_ups_ships_separate'])) ? 'yes' : 'no';
		update_post_meta($product->getId(), '_ups_ships_separate', $setting);
	}
}