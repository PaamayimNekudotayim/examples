<?php

namespace Jigoshop\Extension\Ups\Common;

use Exception;
use Jigoshop\Core\Messages;
use Jigoshop\Core\Options;
use Jigoshop\Core\Types;
use Jigoshop\Entity\Order;
use Jigoshop\Entity\OrderInterface;
use Jigoshop\Entity\Product\Variable;
use Jigoshop\Helper\Country;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;
use Jigoshop\Service\CartServiceInterface;
use Jigoshop\Service\OrderServiceInterface;
use Jigoshop\Shipping\Method3;
use Jigoshop\Shipping\MultipleMethod;
use Jigoshop\Shipping\Rate;
use Jigoshop\Shipping\Method;
use Jigoshop\Entity\Product;
use Jigoshop\Frontend\Pages;
use Jigoshop\Entity\Order\Item;

class UpsShipping implements Method3, MultipleMethod
{
    const ID = 'jigoshop_ups_shipping';
    private $settings;
    private $messages;
    private $orderService;
    private $rate;
    private $rates;

    var $weight;
    var $destination;
    const HOME_SHOP_URL = 'http://www.jigoshop.com/';
    const IDENTIFIER = '9444';
    private $packageCount;
    private $separate;
    private $destZip;
    private $destCountry;
    private $combined;
    private $shippingLabel;
    private $weightUnit;
    private $length;
    private $width;
    private $height;
    private $indLength;
    private $indWidth;
    private $indHeight;
    private $insuredValue;
    private $indInsuredValue;
    private $countryCode;
    private $dimensionUnit;
    private $url;
    private $unit;
    private $cartService;
    private $fee;
    private $shippingTotal;
    private $shippingTax;

    public function __construct(Options $options, CartServiceInterface $cartService, OrderServiceInterface $orderService, Messages $messages)
    {
        $baseCountry = $options->get('general.country');
        $countryCode = explode(':', $baseCountry);
        $this->unit = Integration::getOptions()->getAll();

        $this->messages = $messages;
        $this->orderService = $orderService;
        $this->cartService = $cartService;

        $this->url = 'https://onlinetools.ups.com/ups.app/xml/Rate';
        $this->shippingLabel = '';
        $this->weightUnit = '';
        $this->length = false;
        $this->width = false;
        $this->height = false;
        $this->indWidth = false;
        $this->indLength = false;
        $this->indHeight = false;
        $this->insuredValue = 0;
        $this->indInsuredValue = 0;
        $this->countryCode = $countryCode[0];


        $this->settings = $options->get('shipping.' . self::ID, array(
            'enabled' => false,
            'title' => __('UPS Shipping', 'jigoshop_ups'),
            'ups_username' => '',
            'ups_password' => '',
            'ups_api_key' => '',
            'ups_shipper_number' => '',
            'ups_source_postal_code' => '',
            'is_taxable' => false,
            'fee' => '',
            'jigoshop_ups_destination_type' => '',
            'available_for' => '',
            'countries' => '',
            'disable_destination_type' => '',
            'ups_methods' => '',
            'adminOnly' => false,
        ));

        if (!isset($_GET['tab']) || $_GET['tab'] !== 'shipping') {
            return;
        }

        add_action('admin_enqueue_scripts', array($this, 'adminScripts'));
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_action('admin_notices', array($this, 'adminNotices'));
    }

    public function adminNotices()
    {
        if ($this->weightUnit == 'kg') {
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            $this->messages->addError(sprintf(__('%s - It seems you have enabled "Kilograms" in
			<a href="' . admin_url('admin.php?page=jigoshop_settings&tab=products') . '">' . __('Products', 'jigoshop_ups') . '</a> settings tab,
			<strong>UPS Shipping will not work</strong> until you change the weight unit to "Pounds"', 'jigoshop_ups'), $methodName));
        }
        if ($this->dimensionUnit == 'in') {
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            $this->messages->addError(sprintf(__('%s - It seems you have enabled "Inches" in
			<a href="' . admin_url('admin.php?page=jigoshop_settings&tab=products') . '">' . __('Products', 'jigoshop_ups') .'</a> settings tab,
			<strong>UPS Shipping will not work</strong> until you change the dimension unit to "Centimeters"', 'jigoshop_ups'), $methodName));
        }
    }

    /**
     * Convert array to object
     * @var $array
     * @return \stdClass|bool
     * @noinspection Jigoshop2 is awesome!!!
     */
    private static function arrayToObject($array)
    {
        if (!is_array($array)) {
            return $array;
        }
        if (is_array($array) && sizeof($array) > 0) {
            if (class_exists('stdClass')) {
                $object = new \stdClass();
                foreach ($array as $name => $value) {
                    $name = strtolower(trim($name));
                    if (!empty($name)) {
                        $object->$name = self::arrayToObject($value);
                    }
                }
                return $object;
            }
        } else {
            return false;
        }

        return true;
    }

    private static function getUnit($unit)
    {
        switch ($unit) {
            // Weight Unit
            case 'lbs':
                $unit = 'LBS';
                break;
            case 'kg':
                $unit = 'KGS';
                break;
        }
        return self::arrayToObject($unit);
    }

    /*
     * Admin script
     */
    public function adminScripts()
    {
        Scripts::add('jigoshop.admin.scripts.bring_shipping_admin', JIGOSHOP_UPS_SHIPPING_URL . '/assets/js/admin.js', ['jquery']);
    }

    /*
     * Frontend Script/Style
     */
    public function scripts()
    {
        Styles::add('ups_style', JIGOSHOP_UPS_SHIPPING_URL . '/assets/css/style.css', [], ['page' => [Pages::CHECKOUT, Pages::CART]]);
        Scripts::add('ups_script', JIGOSHOP_UPS_SHIPPING_URL . '/assets/js/script.js', ['jquery'], ['page' => [Pages::CHECKOUT, Pages::CART]]);
    }

    /**
     * @return string ID of shipping method.
     */
    public function getId()
    {
        return self::ID;
    }

    /**
     * @return string Name of method.
     */
    public function getName()
    {
        return is_admin() ? $this->getLogoImage() . ' ' . __('UPS Shipping', 'jigoshop_ups') : $this->settings['title'];
    }

    private function getLogoImage()
    {
        return '<img src="' . JIGOSHOP_UPS_SHIPPING_URL . '/assets/images/ups_logo.gif' . '" alt="UPS logo" width="60" height="60" style="border:none !important;" class="shipping-logo" />';
    }

    /**
     * @return string Customizable title of method.
     */
    public function getTitle()
    {
        return $this->settings['title'];
    }

    /**
     * @return bool Whether current method is enabled and able to work.
     */
    public function isEnabled()
    {
        return $this->settings['enabled'];
    }

    /**
     * @return bool Whether current method is taxable.
     */
    public function isTaxable()
    {
        return $this->settings['is_taxable'];
    }

    /**
     * @return array List of options to display on Shipping settings page.
     */
    public function getOptions()
    {
        return [
            [
                'name' => sprintf('[%s][enabled]', self::ID),
                'title' => __('Enable', 'jigoshop_ups'),
                'type' => 'checkbox',
                'checked' => $this->settings['enabled'],
                'classes' => ['switch-medium'],
            ],
            [
                'name' => sprintf('[%s][title]', self::ID),
                'title' => __('Method title', 'jigoshop_ups'),
                'tip' => __('This controls the title which users sees during the checkout', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->getTitle(),
            ],
            [
                'name' => sprintf('[%s][ups_username]', self::ID),
                'title' => __('UPS Username', 'jigoshop_ups'),
                'tip' => __('You\'ll need to sign-up for a free API key. Once you have that, enter it here.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['ups_username'],
            ],
            [
                'name' => sprintf('[%s][ups_password]', self::ID),
                'title' => __('UPS Password', 'jigoshop_ups'),
                'tip' => __('Enter your www.ups.com password.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['ups_password'],
            ],
            [
                'name' => sprintf('[%s][ups_api_key]', self::ID),
                'title' => __('UPS API Key', 'jigoshop_ups'),
                'tip' => __('You\'ll need to sign-up for a free API key. Once you have that, enter it here.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['ups_api_key']
            ],
            [
                'name' => sprintf('[%s][ups_shipper_number]', self::ID),
                'title' => __('UPS Shipper Number', 'jigoshop_ups'),
                'tip' => __('Enter your UPS Account Number. If you don\'t have a UPS Account Number, you can get one at htp://www.ups.com.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['ups_shipper_number'],
            ],
            [
                'name' => sprintf('[%s][ups_source_postal_code]', self::ID),
                'title' => __('UPS Source Postal Code', 'jigoshop_ups'),
                'tip' => __('Enter your postal code.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['ups_source_postal_code'],
            ],
            [
                'name' => sprintf('[%s][is_taxable]', self::ID),
                'title' => __('Is taxable?', 'jigoshop_ups'),
                'type' => 'checkbox',
                'checked' => $this->settings['is_taxable'],
                'classes' => ['switch-medium'],
            ],
            [
                'name' => sprintf('[%s][ups_methods]', self::ID),
                'id' => 'ups_methods',
                'title' => __('UPS Methods', 'jigoshop_ups'),
                'type' => 'user_defined',
                'multiple' => true,
                'display' => [$this, 'adminDisplayMethods'],
            ],
            [
                'name' => sprintf('[%s][fee]', self::ID),
                'title' => __('Handling Fee', 'jigoshop_ups'),
                'tip' => __('Fee excluding tax. Enter an amount, e.g. 2.50, or a percentage, e.g. 5%. Leave blank to disable.', 'jigoshop_ups'),
                'type' => 'text',
                'value' => $this->settings['fee'],
            ],
            [
                'name' => sprintf('[%s][jigoshop_ups_destination_type]', self::ID),
                'title' => __('Destination Type', 'jigoshop_ups'),
                'tip' => __('This field allows to select permanent destination type.', 'jigoshop_ups'),
                'type' => 'select',
                'options' => $this->destinationType(),
                'value' => $this->settings['jigoshop_ups_destination_type'],
            ],
            [
                'name' => sprintf('[%s][available_for]', self::ID),
                'id' => 'ups_shipping_available_for',
                'title' => __('Available for', 'jigoshop_ups'),
                'type' => 'select',
                'value' => $this->settings['available_for'],
                'options' => self::availableCountries(),
            ],
            [
                'name' => sprintf('[%s][countries]', self::ID),
                'id' => 'ups_shipping_rate_countries',
                'title' => __('Select countries', 'jigoshop'),
                'type' => 'select',
                'value' => $this->settings['countries'],
                'options' => Country::getAllowed(),
                'multiple' => true,
                'hidden' => $this->settings['available_for'] == 'all',
            ],
            [
                'name' => sprintf('[%s][adminOnly]', self::ID),
                'title' => __('Enable Only for Admin', 'jigoshop_ups'),
                'type' => 'checkbox',
                'description' => __('Enable this if you would like to test it only for Site Admin', 'jigoshop_ups'),
                'checked' => $this->settings['adminOnly'],
                'classes' => ['switch-medium'],
            ],
        ];
    }

    public function adminDisplayMethods()
    {
        $methods = maybe_unserialize($this->settings['ups_methods']);
        $services = $this->getAvailableServices();


        return Render::get('ups_shipping', 'admin/ups_services_fields', [
            'methods' => $methods,
            'services' => $services
        ]);
    }

    protected function destinationType()
    {
        return [
            'res' => __('Residential', 'jigoshop_ups'),
            'com' => __('Commercial', 'jigoshop_ups'),
            'both' => __('Allow to choose', 'jigoshop_ups'),
        ];
    }

    /*
     * Available countries
     */
    private static function availableCountries()
    {
        return [
            'all' => __('All allowed countries', 'jigoshop_bring_shipping'),
            'specific' => __('Selected countries', 'jigoshop_bring_shipping'),
        ];
    }

    /**
     * @return array List of applicable tax classes.
     */
    public function getTaxClasses()
    {
        return ['standard'];
    }

    /**
     * Validates and returns properly sanitized options.
     *
     * @param $settings array Input options.
     *
     * @return array Sanitized result.
     */
    public function validateOptions($settings)
    {
        $return = null;
        $settings['enabled'] = $settings['enabled'] == 'on';
        $settings['is_taxable'] = $settings['is_taxable'] == 'on';
        $settings['adminOnly'] = $settings['adminOnly'] == 'on';
        $settings['title'] = trim(htmlspecialchars(strip_tags($settings['title'])));
        $settings['ups_username'] = trim(strip_tags($settings['ups_username']));
        $settings['ups_password'] = trim(strip_tags($settings['ups_password']));
        $settings['ups_api_key'] = trim(strip_tags($settings['ups_api_key']));
        $settings['ups_shipper_number'] = trim(strip_tags($settings['ups_shipper_number']));

        $settings['jigoshop_ups_destination_type'] = $this->validateArrayData($settings['jigoshop_ups_destination_type']);
        $settings['ups_methods'] = isset($_POST['jigoshop_ups_services']) ? serialize($_POST['jigoshop_ups_services']) : $settings['ups_methods'];


        // Get digits in zip code.
        preg_match_all('/[0-9]/', $settings['ups_source_postal_code'], $fromZip);

        // Search digits in zip code
        $fromZipValidate = count($fromZip[0]);

        // If digits not fount return null
        if (!empty($settings['ups_source_postal_code']) && !$fromZipValidate) {
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            $this->messages->addWarning(sprintf(__('%s - Zip code must contain digit', 'jigoshop_stripe'), $methodName));
            $settings['ups_source_postal_code'] = $return;
        }

        if (!is_numeric($settings['fee']) && !empty($settings['fee'])) {
            $settings['fee'] = $this->settings['fee'];
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            $this->messages->addWarning(sprintf(__('%s - Fee was invalid - value is left unchanged.', 'jigoshop_stripe'), $methodName));
        }

        if ($settings['fee'] >= 0) {
            $settings['fee'] = (float)$settings['fee'];
        } else {
            $settings['fee'] = $this->settings['fee'];
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            $this->messages->addWarning(sprintf(__('%s - Fee was below 0 - value is left unchanged.', 'jigoshop_stripe'), $methodName));
        }

        if ($settings['available_for'] === 'specific') {
            $settings['countries'] = array_filter($settings['countries'], function ($item) {
                return Country::exists($item);
            });
        } else {
            $settings['countries'] = [];
        }

        return $settings;
    }

    private function validateArrayData($data)
    {
        if (is_array($data)) {
            filter_var_array($data, FILTER_SANITIZE_STRING);
        }

        return $data;
    }

    /**
     * Checks whether current method is the one specified with selected rule.
     *
     * @param \Jigoshop\Shipping\Method $method Method to check.
     * @param Rate $rate Rate to check.
     *
     * @return boolean Is this the method?
     */
    public function is(Method $method, $rate = null)
    {
        return $method->getId() == $this->getId() && $rate instanceof Rate && $rate->getId() == $this->getShippingRate();
    }

    /**
     * @param OrderInterface $order Order to calculate shipping for.
     *
     * @return float Calculates value of shipping for the order.
     * @throws Exception On error.
     */
    public function calculate(OrderInterface $order)
    {
        if ($this->rate) {
            $rates = $this->getRates($order);
            if (empty($rates)) {
                $methodName = $this->getName();
                $methodName = str_replace($this->getLogoImage(), '', $methodName);
                throw new Exception(sprintf(__('%s - There are no rates to calculate, rate is empty', 'jigoshop_ups'), $methodName));
            }
            if (!isset($rates[$this->rate])) {
                $methodName = $this->getName();
                $methodName = str_replace($this->getLogoImage(), '', $methodName);
                throw new Exception(sprintf(__('%s - No rates have been chosen', 'jigoshop_ups'), $methodName));
            }
            return $rates[$this->rate]->getPrice();
        } else {
            $methodName = $this->getName();
            $methodName = str_replace($this->getLogoImage(), '', $methodName);
            throw new Exception(sprintf(__('%s - There was an error during rate calculation, please try again later'), $methodName));
        }
    }

    /**
     * @return array Minimal state to fully identify shipping method.
     */
    public function getState()
    {
        return array(
            'id' => $this->getId(),
            'rate' => $this->getShippingRate(),
        );
    }

    /**
     * Restores shipping method state.
     *
     * @param array $state State to restore.
     */
    public function restoreState(array $state)
    {
        if (isset($state['rate'])) {
            $this->setShippingRate($state['rate']);
        }
    }

    /**
     * Returns list of available shipping rates.
     *
     * @param OrderInterface $order
     *
     * @return array List of available shipping rates.
     */
    public function getRates($order)
    {
        if ($this->rates) {
            return $this->rates;
        }
        $this->shippingTotal = 0;
        $this->shippingTax = 0;
        $this->insuredValue = 0;
        $this->length = false;
        $this->width = false;
        $this->height = false;
        $weight = 0;
        $this->combined = false;
        $this->separate = false;
        $hasDimensions = false;
        $package = array();

        $weightUnit = self::getUnit($this->unit)->products->weightUnit;

        switch ($weightUnit) {
            case 'lbs':
                $this->weightUnit = 'LBS';
                break;
            case 'kg':
                $this->weightUnit = 'KGS';
                break;
        }
        foreach ($order->getItems() as $item) {
            /**@var Item $item */
            $product = $item->getProduct();
            if ($product instanceof Variable) {
                /** 
                 * @var Product $product 
                 * @dzieki Kasa, tak jest duzo lepiej niz Ja mialem. zaoszczedzilem kilka liniek kodu.
                 */
                $product = $product->getVariation($item->getMeta('variation_id')->getValue())->getProduct();
            }
            $data = array(
                'price' => $item->getPrice(),
                'width' => $product->getSize()->getWidth(),
                'height' => $product->getSize()->getHeight(),
                'weight' => $product->getSize()->getWeight(),
                'length' => $product->getSize()->getLength(),
                'qty' => $item->getQuantity(),
            );

            $dimensions = $product->getSize();

            if (true == $dimensions) {
                $hasDimensions = true;
            }

            $shipsSeparate = get_post_meta($product->getId(), '_ups_ships_separate', true);
            $this->insuredValue += $data['price'] * $data['qty'];
            $this->packageCount = 1;
            if ($shipsSeparate !== "yes") {

                $this->combined = true;
                $weight = $weight + ($data['weight'] * $data['qty']);
                if (false !== $hasDimensions) {


                    // determine longest product in cart
                    if (!$this->length || $this->length < $data['length']) {
                        $this->length = $data['length'];
                    }
                    // determine widest product in cart
                    if (!$this->width || $this->width < $data['width']) {
                        $this->width = $data['width'];
                    }
                    // determine overall height of products in the cart
                    $this->height += $data['height'];
                }
            } else {
                $separate_weight = $data['weight'];
                if (false !== $hasDimensions) {
                    $this->indLength = $data['length']; // determine longest product in cart
                    $this->indWidth = $data['width']; // determine widest product in cart
                    $this->indHeight = $data['height']; // determine overall height of products in the cart
                    $this->indInsuredValue = $item->getCost();
                }

                $indDimensions = false;
                if ($this->indLength > 0 && $this->indWidth > 0 && $this->indHeight > 0) {
                    $indDimensions = true;
                }

                $this->packageCount += $data['qty'];
                for ($i = 0; $i < $data['qty']; $i++) {
                    $package['weightUnit'] = strtoupper($weightUnit);
                    $package['separate_weight'] = $separate_weight;
                    $package['indInsuredValue'] = $this->indInsuredValue;
                    $package['ind_dimensions'] = $indDimensions;
                    if ($indDimensions) {
                        $package['dimensionUnit'] = strtoupper($this->dimensionUnit);
                        $package['indLength'] = $this->indLength;
                        $package['indWidth'] = $this->indWidth;
                        $package['indHeight'] = $this->indHeight;
                    }
                    $this->separate[] = $package;
                }
            }

            $address = $order->getCustomer()->getShippingAddress();
            $this->weight = (0 < $weight && $weight < 1) ? 1 : $weight;
            $this->destZip = $address->getPostcode();
            $this->destCountry = $address->getCountry();
            if (!$this->destZip) {
                $this->messages->addError('Please enter a postal code to get UPS rates');
            }
            $this->rates = [];

            $_product = $item->getProduct();
            if ($_product instanceof Variable) {
                $cheapestPrice = $_product->getLowestPrice();

                if (!empty($cheapestPrice) && $cheapestPrice > 0 && $cheapestPrice != null) {
                    $this->shippingTotal = $cheapestPrice;
                } else {
                    $this->rates = (array)$this->rates;
                }
            }
        }
        $services = $this->filter_services();

        if (!is_array($services)) {
            $services = array();
        }

        // Replace image width in method title. Jigoshop2 is awesome!!!
        $find = "60";
        $replace = "20";
        $logoImage = str_replace($find, $replace, $this->getLogoImage());

        foreach ($services as $service) {

            $request = $this->getResponseFromRequest($order, $service);
            if ($request) {
                $response = $this->sendToShippingServer($request);

                $xmlResponse = $this->convertMmlToArray($response);

                $rate = $this->retrieveRateFromResponse($xmlResponse);
                $code = $this->getRateIdFromResponse($xmlResponse);
                $serviceName = $service . ' ' . $logoImage;

                if ($rate == -1) {
                    continue;
                }
                if ($code == null) {
                    continue;
                }
                $rates = new Rate();
                $rates->setId((string)$code);
                $rates->setName($serviceName);
                $rates->setPrice((float)($rate));
                $rates->setMethod($this);
                $this->rates[$rates->getId()] = $rates;
            }
        }

        return $this->rates;
    }

    /*
     * Send XML to Shipping Server
     */
    protected function sendToShippingServer($xml)
    {
        $request = curl_init($this->url);

        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);

        $post_response = curl_exec($request);
        curl_close($request);

        return $post_response;
    }

    /**
     * Convert XML to an array
     * @source Somewhere in Stackoverflow. 
     */
    protected function convertMmlToArray($xml)
    {
        $xml = strstr($xml, "<?");

        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $xml, $vals, $index);
        xml_parser_free($xml_parser);

        $params = array();
        $level = array();

        foreach ($vals as $xml_elem) {
            if ($xml_elem['type'] == 'open') {
                if (array_key_exists('attributes', $xml_elem)) {
                    list($level[$xml_elem['level']]) = array_values($xml_elem['attributes']);
                } else {
                    $level[$xml_elem['level']] = $xml_elem['tag'];
                }
            }

            if ($xml_elem['type'] == 'complete') {
                $start_level = 1;
                $php_stmt = '$params';

                while ($start_level < $xml_elem['level']) {
                    $php_stmt .= '[$level[' . $start_level . ']]';
                    $start_level++;
                }

                $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';

                if (isset($xml_elem['tag']) && isset($xml_elem['value'])) :
                    eval($php_stmt);
                endif;
            }
        }

        return $params;
    }

    public function filter_services()
    {
        $rates = maybe_unserialize($this->settings['ups_methods']);
        $services = $this->getAvailableServices();

        $filtered = array();
        if (isset($rates) && !empty($rates)) {
            foreach ($rates as $key => $value) {
                if (isset($services[$key])) {
                    $filtered[] = $services[$key];
                }
            }
        }
        return $services;
    }

    public function getResponseFromRequest($order, $service = '')
    {
        $services = apply_filters('jigoshop_ups_available_service', $this->getAvailableServices());
        $code = $this->getCode($service, $services);
        $request = $this->rate_request($code, $order);

        return $request;
    }

    public function retrieveRateFromResponse($array_response)
    {
        $shipping_amount = -1;
        $this->fee = false;
        if ($array_response && array_key_exists('RATEDSHIPMENT', $array_response['RATINGSERVICESELECTIONRESPONSE'])) {
            $array_response = apply_filters('jigoshop_ups_response', $array_response);
            $field = array_key_exists('TOTALCHARGESWITHTAXES', $array_response['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']) ? 'TOTALCHARGESWITHTAXES' : 'TOTALCHARGES';
            $shipping_amount = $array_response['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT'][$field]['MONETARYVALUE'];
            if ($shipping_amount) {
                $this->fee = $this->settings['fee'];
            } else {
                $shipping_amount = -1;
            }
        }

        return $shipping_amount;
    }

    public function getRateIdFromResponse($rateID)
    {
        $code = null;
        if ($rateID && array_key_exists('RATEDSHIPMENT', $rateID['RATINGSERVICESELECTIONRESPONSE'])) {
            $code = $rateID['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['SERVICE']['CODE'];
        }

        return $code;
    }

    public function rate_request($service, $order)
    {
        /**@var Order $order */
        $address = $order->getCustomer()->getShippingAddress();

        if (isset($_POST['post_data'])) {
            parse_str($_POST['post_data']);
        }
        if ($this->settings['jigoshop_ups_destination_type'] == 'both') {
            if (isset($_POST['ups_calc_shipping_destination_type']) && !empty($_POST['ups_calc_shipping_destination_type'])) {
                $_SESSION['ups_calc_shipping_destination_type'] = $_POST['ups_calc_shipping_destination_type'];
            }
            if (isset($_SESSION['ups_calc_shipping_destination_type'])) {
                $upsShippingDestinationType = $_SESSION['ups_calc_shipping_destination_type'];
            }
        } else {
            $upsShippingDestinationType = $this->settings['jigoshop_ups_destination_type'] == 'res' ? 'residential' : 'commercial';
        }

        /*
         * XML Access Request
         */
        $xmlPre = new \SimpleXMLElement('<AccessRequest xml:lang="en-US"/>');
        $xmlPre->addChild('AccessLicenseNumber', $this->settings['ups_api_key']);
        $xmlPre->addChild('UserId', htmlentities($this->settings['ups_username']));
        $xmlPre->addChild('Password', htmlentities($this->settings['ups_password']));
        /*
         * XML Extension Information
         */
        $xml = new \SimpleXMLElement('<RatingServiceSelectionRequest xml:lang="en-US"/>');
        $xmlRequest = $xml->addChild('Request');
        $xmlRequest->addChild('RequestAction', 'Rate');
        $xmlRequest->addChild('RequestOption', 'Rate');
        $xmlRequest_trans_ref = $xmlRequest->addChild('TransactionReference');
        // @TODO check below two lines
        $xmlRequest_trans_ref->addChild('CustomerContext', 'Bare Bones Rate Request');
        $xmlRequest_trans_ref->addChild('XpciVersion', '1.0');
        $xml->addChild('PickupType')->addChild('Code', '01');
        $xmlShipment = $xml->addChild('Shipment');
        $xmlShipper = $xmlShipment->addChild('Shipper');
        $xmlShipper->addChild('ShipperNumber', $this->settings['ups_shipper_number']);
        $xmlShipperAddress = $xmlShipper->addChild('Address');
        $xmlShipperAddress->addChild('PostalCode', $this->settings['ups_source_postal_code']);
        $xmlShipperAddress->addChild('CountryCode', $this->countryCode);
        $xmlShipTo = $xmlShipment->addChild('ShipTo');
        $xmlShipTo_address = $xmlShipTo->addChild('Address');
        $xmlShipTo_address->addChild('PostalCode', $address->getPostcode());
        $xmlShipTo_address->addChild('CountryCode', $address->getCountry());
        if (isset($upsShippingDestinationType) && $upsShippingDestinationType == 'residential') {
            $xmlShipTo_address->addChild('ResidentialAddressIndicator');
        }
        $xmlShipFrom = $xmlShipment->addChild('ShipFrom');
        $xmlShipFrom_address = $xmlShipFrom->addChild('Address');
        $xmlShipFrom_address->addChild('PostalCode', $this->settings['ups_source_postal_code']);
        $xmlShipFrom_address->addChild('CountryCode', $this->countryCode);
        if ($this->settings['is_taxable'] == 'Taxable') {
            $xmlShipment->addChild('TaxInformationIndicator', $this->settings['is_taxable']);
        }
        $xmlShipment->addChild('Service')->addChild('Code', $service);
       
        if ($this->combined == true) {
            $xmlPackage = $xmlShipment->addChild('Package');
            $xmlPackage->addChild('PackagingType')->addChild('Code', '02');
            if ($this->length > 0 && $this->width > 0 && $this->height > 0) {
                $xmlPackageDimensions = $xmlPackage->addChild('Dimensions');
                $xmlPackageDimensions->addChild('UnitOfMeasurement')->addChild('Code', $this->dimensionUnit);
                $xmlPackageDimensions->addChild('Length', $this->length);
                $xmlPackageDimensions->addChild('Width', $this->width);
                $xmlPackageDimensions->addChild('Height', $this->height);
            }
            $xmlPackageWeight = $xmlPackage->addChild('PackageWeight');
            $xmlPackageWeight->addChild('UnitOfMeasurement')->addChild('Code', $this->weightUnit);
            $xmlPackageWeight->addChild('Weight', $this->weight);
            $xmlPackageService = $xmlPackage->addChild('PackageServiceOptions');
            $xmlInsuredValue = $xmlPackageService->addChild('InsuredValue');
            $xmlInsuredValue->addChild('CurrencyCode', 'USD');
            $xmlInsuredValue->addChild('MonetaryValue', $this->insuredValue);
        }
        
        if ($this->separate) {
            foreach ($this->separate as $package) {
                $xmlPackage = $xmlShipment->addChild('Package');
                $xmlPackage->addChild('PackagingType')->addChild('Code', '02');
                if ($package['ind_dimensions']) {
                    $xmlPackageDimensions = $xmlPackage->addChild('Dimensions');
                    $xmlPackageDimensions->addChild('UnitOfMeasurement')->addChild('Code', $package['dimensionUnit']);
                    $xmlPackageDimensions->addChild('Length', $package['indLength']);
                    $xmlPackageDimensions->addChild('Width', $package['indWidth']);
                    $xmlPackageDimensions->addChild('Height', $package['indHeight']);
                }
                $xmlPackageWeight = $xmlPackage->addChild('PackageWeight');
                $xmlPackageWeight->addChild('UnitOfMeasurement')->addChild('Code', $package['weightUnit']);
                $xmlPackageWeight->addChild('Weight', $package['separate_weight']);
                $xmlPackageService = $xmlPackage->addChild('PackageServiceOptions');
                $xmlInsuredValue = $xmlPackageService->addChild('InsuredValue');
                $xmlInsuredValue->addChild('CurrencyCode', 'USD');
                $xmlInsuredValue->addChild('MonetaryValue', $package['indInsuredValue']);
            }
        }

        $data = $xmlPre->asXML() . $xml->asXML();
        $data = apply_filters('jigoshop_ups_raw_request', $data);
        return $data;
    }

    public function getAvailableServices()
    {
        $countryCode = $this->countryCode;
        $euArray = [
            'FR',
            'BE',
            'LU',
            'NL',
            'DE',
            'IT',
            'GB',
            'IE',
            'DK',
            'GR',
            'PT',
            'ES',
            'SE',
            'FI',
            'AT',
            'MT',
            'HU',
            'EE',
            'CZ',
            'LV',
            'CY',
            "LT",
            'SI',
            'SK',
            'BG',
            'RO'
        ];
        if (in_array($countryCode, $euArray)) {
            $countryCode = "EU";
        }
        switch ($countryCode) {
            case 'EU':
                $services = [
                    '07' => 'UPS Express',
                    '08' => 'UPS ExpeditedSM',
                    '11' => 'UPS Standard',
                    '54' => 'UPS Worldwide Express PlusSM',
                    '65' => 'UPS Saver'
                ];
                break;
            case 'US':
                $services = [
                    '14' => 'Next Day Air Early AM',
                    '01' => 'Next Day Air',
                    '13' => 'Next Day Air Saver',
                    '02' => '2nd Day Air',
                    '59' => '2nd Day Air AM',
                    '12' => '3 Day Select',
                    '03' => 'Ground',
                    '07' => 'UPS Worldwide ExpressSM',
                    '08' => 'UPS Worldwide ExpeditedSM',
                    '11' => 'UPS Standard',
                    '65' => 'UPS Saver'
                ];
                break;
            case 'CA':
                $services = [
                    '01' => 'UPS Express',
                    '02' => 'UPS Worldwide ExpeditedSM',
                    '11' => 'UPS Standard',
                    '12' => 'UPS Three-Day Select?',
                    '13' => 'UPS Saver SM',
                    '14' => 'UPS Express Early A.M. SM',
                    '07' => 'UPS Express (to US)'
                ];
                break;
            case 'MX':
                $services = [
                    '07' => 'UPS Express',
                    '08' => 'UPS ExpeditedSM',
                    '11' => 'UPS Standard',
                    '54' => 'UPS Express Plus',
                    '65' => 'UPS Saver'
                ];
                break;
            case 'PL':
                $services = [
                    '07' => 'UPS Express',
                    '08' => 'UPS ExpeditedSM',
                    '11' => 'UPS Standard',
                    '54' => 'UPS Worldwide Express PlusSM',
                    '65' => 'UPS Saver',
                    '82' => 'UPS Today StandardSM',
                    '83' => 'UPS Today Dedicated CourierSM',
                    '85' => 'UPS Today Express',
                    '86' => 'UPS Today Express Saver'
                ];
                break;
            default:
                $services = [
                    '07' => 'UPS Express',
                    '08' => 'UPS ExpeditedSM',
                    '11' => 'UPS Standard',
                    '54' => 'UPS Worldwide Express PlusSM',
                    '65' => 'UPS Saver'
                ];
                break;
        }

        return $services;
    }

    private function getCode($selectedService, $services)
    {
        return array_search($selectedService, $services);
    }

    /**
     * @param $rate int Rate to use.
     */
    public function setShippingRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return int Currently used rate.
     */
    public function getShippingRate()
    {
        return $this->rate;
    }

    /**
     * Whenever method was enabled by the user.
     *
     * @return boolean Method enable state.
     */
    public function isActive()
    {
        if (isset($this->settings['enabled'])) {
            return $this->settings['enabled'];
        }
        return false;
    }

    /**
     * Set method enable state.
     *
     * @param boolean $state Method enable state.
     *
     * @return array Method current settings (after enable state change).
     */
    public function setActive($state)
    {
        $this->settings['enabled'] = $state;

        return $this->settings;
    }

    /**
     * Whenever method was configured by the user (all required data was filled for current scenario).
     *
     * @return boolean Method config state.
     */
    public function isConfigured()
    {
        if (isset($this->settings['ups_username']) && $this->settings['ups_username']
            && isset($this->settings['ups_password']) && $this->settings['ups_password']
            && isset($this->settings['ups_api_key']) && $this->settings['ups_api_key']
            && isset($this->settings['ups_shipper_number']) && $this->settings['ups_shipper_number']
            && isset($this->settings['ups_source_postal_code']) && $this->settings['ups_source_postal_code']
        ) {
            return true;
        }

        return false;
    }

    /**
     * Whenever method has some sort of test mode.
     *
     * @return boolean Method test mode presence.
     */
    public function hasTestMode()
    {
        return true;
    }

    /**
     * Whenever method test mode was enabled by the user.
     *
     * @return boolean Method test mode state.
     */
    public function isTestModeEnabled()
    {
        return true;
    }

    /**
     * Set Method test mode state.
     *
     * @param boolean $state Method test mode state.
     *
     * @return array Method current settings (after test mode state change).
     */
    public function setTestMode($state)
    {
        return $this->settings;
    }

    /**
     * Whenever method requires SSL to be enabled to function properly.
     *
     * @return boolean Method SSL requirment.
     */
    public function isSSLRequired()
    {
        return false;
    }

    /**
     * Whenever method is set to enabled for admin only.
     *
     * @return boolean Method admin only state.
     */
    public function isAdminOnly()
    {
        if (true == $this->settings['adminOnly']) {
            return true;
        }

        return false;
    }

    /**
     * Sets admin only state for the method and returns complete method options.
     *
     * @param boolean $state Method admin only state.
     *
     * @return array Complete method options after change was applied.
     */
    public function setAdminOnly($state)
    {
        $this->settings['adminOnly'] = $state;

        return $this->settings;
    }
}