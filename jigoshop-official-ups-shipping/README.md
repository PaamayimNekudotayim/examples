The Jigoshop official UPS Shipping module allows you to gets in real-time UPS shipping rates to your Jigoshop. In order to use 
this extension, you need to have a UPS Account, ups.com username and password and a UPS Rates API key. Each of
these items can be obtained from UPS through the ups.com website. 

###Installation:
1. Upload the plugin folder to the '/wp-content/plugins/' directory
2. Activate plugin through the 'Plugins' menu in WordPress
3. Go to 'Jigoshop/Manage Licenses' to enter license key for your plugin
