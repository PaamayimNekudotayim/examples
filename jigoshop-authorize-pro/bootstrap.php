<?php
/**
 * Plugin Name: Jigoshop Authorize.net PRO Gateway
 * Plugin URI: https://www.jigoshop.com/product/authorize-net-pro/
 * Description: Extends Jigoshop with an <a href="https://www.authorize.net" target="_blank">Authorize.net</a>  gateway. An Authorize.net Merchant account -is- required. Although not required, a server with SSL support and an SSL certificate is needed for security reasons for full PCI compliance and the Authorize.net AIM integration method will be used if the Jigoshop SSL setting is enabled.  Without Jigoshop SSL enabled, then the Authorize.net SIM or DPM integration methods can be used used to ensure maximum PCI complicance for those servers without SSL.
 * Version: 2.0
 * Author: @vasilguruli | JigoLtd
 */
// Define plugin name
define('JIGOSHOP_AUTHORIZE_PRO_NAME', '   Jigoshop Authorize.net PRO Gateway');
add_action('plugins_loaded', function () {
    load_plugin_textdomain('jigoshop_authorize_pro', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    if (class_exists('\Jigoshop\Core')) {
        //Check version.
        if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_AUTHORIZE_PRO_NAME, '2.0')) {
            return;
        }
        //Check license.
       /* $licence = new \Jigoshop\Licence(__FILE__, 'add_licence_number', 'http://www.jigoshop.com');
        if (!$licence->isActive()) {
           return;
        }*/
        // Define plugin directory for inclusions
        define('JIGOSHOP_AUTHORIZE_PRO_DIR', dirname(__FILE__));
        // Define plugin URL for assets
        define('JIGOSHOP_AUTHORIZE_PRO_URL', plugins_url('', __FILE__));
        //Init components.
        require_once(JIGOSHOP_AUTHORIZE_PRO_DIR . '/src/Jigoshop/Extension/AuthorizePro/Common.php');
        if (is_admin()) {
            require_once(JIGOSHOP_AUTHORIZE_PRO_DIR . '/src/Jigoshop/Extension/AuthorizePro/Admin.php');
        } //else {
        //require_once(JIGOSHOP_AUTHORIZE_PRO_DIR . '/src/Jigoshop/Extension/AuthorizePro/Frontend.php');
        //}
    }else {
        add_action('admin_notices', function () {
            echo '<div class="error"><p>';
            printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.',
                'jigoshop_authorize_pro'), JIGOSHOP_AUTHORIZE_PRO_NAME, JIGOSHOP_AUTHORIZE_PRO_NAME);
            echo '</p></div>';
        });
    }
});
