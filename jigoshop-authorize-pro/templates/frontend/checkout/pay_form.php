<?php
/**
 * @var string $title
 * @var array $cards
 * @var array $cardAccept
 * @var array $months
 * @var array $years
 * @var string $ssl
 */
?>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $description; ?></h3>
        <small><?php echo $ssl; ?></small>
	</div>
	<div class="row clearfix">
		<div class="form-group col-sm-6 clearfix">
		<!--	--><?php /*\Jigoshop\Helper\Forms::text(array(
				'name' => 'authorize_pro_card_holder_name',
				'label' => __('Card Holder Name', 'jigoshop_authorize_pro'),
			)); */?>
			<div class="form-group col-xs-12 clearfix">
				
				<label for="cc-card-type"><?php echo __("Cart Type", 'jigoshop_authorize_pro') ?></label>
				<?php foreach ($icons as $icon) : ?>
					<img src="<?php echo esc_url($icon); ?>" alt="" width="" height="" class="fatzebra-accepted-cards"/>
				<?php endforeach; ?>
				<div class="clear"></div>
				
				<select name="authorizePro[cardType]" id="cc-card-type" class="col-xs-12">
					<option value=""><?php _e('Choose', 'jigoshop_authorize_pro') ?></option>
					<?php foreach ($cards as $card): ?>
						<option value="<?php echo $card; ?>"><?php echo $card; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group col-xs-12 clearfix">
				<label for="cc-expire-month"><?php echo __("Expiration Date", 'jigoshop_authorize_pro') ?></label>
				<div class="clear"></div>
				<select name="authorizePro[expiryDateMonth]" id="cc-expire-month" class="col-xs-6">
					<option value=""><?php _e('Month', 'jigoshop_authorize_pro') ?></option>
					<?php foreach ($months as $num => $name): ?>
						<option value="<?php echo $num; ?>"><?php echo $name; ?></option>
					<?php endforeach; ?>
				</select> <select name="authorizePro[expiryDateYear]" id="cc-expire-year" class="col-xs-6">
					<option value=""><?php _e('Year', 'jigoshop_authorize_pro') ?></option>
					<?php foreach ($years as $year): ?>
						<option value="<?php echo $year ?>"><?php echo $year; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group col-sm-6 clearfix">
			<?php \Jigoshop\Helper\Forms::text(array(
				'name' => 'authorizePro[cardNumber]',
				'label' => __('Credit Card Number', 'jigoshop_authorize_pro'),
			)); ?>
			<?php \Jigoshop\Helper\Forms::text(array(
				'name' => 'authorizePro[Cv2]',
				'label' => __('Card Security Code', 'jigoshop_authorize_pro'),
			)); ?>
			<div class="clear"></div>
		</div>
	
	</div>
</div>