<div class="panel panel-primary">
    <div class="row clearfix">
        <div class="form-group col-sm-6 clearfix">
            <div class="form-group col-xs-12 clearfix">

                <label for="cc-card-type"><?php echo __("Cart Type: ", 'jigoshop_authorize_pro') ?></label>
				<?php foreach ($icons as $icon) : ?>
                    <img src="<?php echo esc_url($icon); ?>" alt="" width="" height=""
                         class="authorize-pro-accepted-cards"/>
				<?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>