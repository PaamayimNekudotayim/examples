<?php
    /**
     * @var $url string
     * @var $hiddenFields string
     */
?>
<form method="POST" name="authorize-pro-sim-form" id="authorize-pro-sim-form" action="<?php echo $url ?>">
    <?php echo  $hiddenFields ; ?>
    <input type="submit" class="button-alt" id="authorize-pro-dpm-form-submit" value="<?php _e('Pay via Authorize.net', 'jigoshop_authorize_pro') ?>" />
</form>
<script type="text/javascript">
    jQuery(function($){
        $("body").block(
            {
                message: '<img src="<?php echo \JigoshopInit::getUrl() . '/assets/images/loading.gif'?>" alt="Redirecting..." /><?php _e('One moment ... contacting Authorize.net to process your order ...', 'jigoshop_authorize_pro')?>',
                overlayCSS:
                    {
                        background: "#fff",
                        opacity: 0.6
                    },
                css: {
                    padding:		20,
                    textAlign:	  "center",
                    color:		  "#555",
                    border:		 "3px solid #aaa",
                    backgroundColor:"#fff",
                    cursor:		 "wait"
                }
            });
        $("#authorize-pro-sim-form").submit();
    });
</script>

