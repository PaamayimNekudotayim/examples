<?php
	/**
	 * @var $tinyMce string
	 * @var $allowedHtmlOut array
	 * @var $tinyMceSettings array
	 */
?>
<div class="form-group">
	<div class="row">
		<div class="col-sm-12">
			<div class="col-xs-12 col-sm-12 clearfix">
				<div class="tooltip-inline-badge">
					<span data-original-title="Use limited HTML markup for links, images and javascript code to display your site's SSL certification image." data-toggle="tooltip" class="badge margin-top-bottom-9" data-placement="top" title="">?</span>
				</div>
				<div class="tooltip-inline-input">
				<?php
					wp_editor(wp_kses($tinyMce, $allowedHtmlOut), 'ssl_certificate', $tinyMceSettings);
				?>
				</div>
				<span class="help-block">Leave blank to display nothing if you want to display it elsewhere on the site or you are not using SSL and AIM.</span>
			</div>
		</div>
	</div>
</div>