<?php

namespace Jigoshop\Extension\AuthorizePro\Common;


use Jigoshop\Api\Processable;
use Jigoshop\Core\Messages;
use Jigoshop\Core\Options;
use Jigoshop\Entity\Order\Status;
use Jigoshop\Helper\Order;
use Jigoshop\Service\OrderServiceInterface;
use Jigoshop\Service\OrderService;

class Api implements Processable
{
	private $options;
	/** @var OrderService */
	private $orderService;
	/** @var Messages */
	private $messages;
	private $settings;
	
	public function __construct(Options $options, OrderServiceInterface $orderService, Messages $messages)
	{
		$this->options = $options;
		$this->orderService = $orderService;
		$this->messages = $messages;
		$this->settings = $options->get('payment.' . Method::ID, array());
	}
	
	public function processResponse()
	{
		require_once (JIGOSHOP_AUTHORIZE_PRO_DIR. "/vendor/anet_php_sdk/AuthorizeNet.php");
		if (!empty($_POST['x_response_code'])) {
			$response = new \AuthorizeNetSIM($this->settings['api_login_id'], $this->settings['md5']);
			$order = $this->orderService->find($response->response['x_invoice_num']);
			
			if(!$response->isAuthorizeNet()){
				$note = __('The server-generated fingerprint does not match the merchant-specified fingerprint for the "MD5 Hash" ([md5_hash][md5_setting]).  Check the MD5 Hash Setting for the Authorize PRO Gateway and in your Authorize Merchant Account.', 'jigoshop_authorize_pro');
				$order->setStatus(sprintf(__('Authorize.net PRO Payment Warning: %s', 'jigoshop_authorize_pro'), $note));
				if ($this->settings['debug_log'] == true) {
					$this->addToLog('AUTHORIZE.NET ERROR:'.PHP_EOL.
						'response_code: '.'3'.PHP_EOL.
						'response_reason_code: '.'99'.PHP_EOL.
						'response_reason_text: '.$note.PHP_EOL);
				}
				
				// notify the admin
				$this->emailErrorLog($note.PHP_EOL.print_r($response, true).PHP_EOL, $_POST, $order->getId());
			}
			if ($response->isAuthorizeNet()) {
				$order = $this->orderService->find($response->response['x_invoice_num']);
				
				if ($response->approved) {
					if ($response->response['x_order_key'] == $order->getKey()) {
						$order->setStatus(Order::getStatusAfterCompletePayment($order),
							__("Authorize.Net DPM payment completed successfully", "jigoshop_authorize_pro"));
						$this->orderService->save($order);
						echo \AuthorizeNetDPM::getRelayResponseSnippet(Order::getThankYouLink($order));
						exit;
					} else {
						$this->orderService->addNote($order,
							sprintf(__('Something went wrong. Response from Authorize.net invalidated this order. Reason: %s - %s.',
								'jigoshop_authorize_pro'), $response->response_code, $response->response_reason_text));
						$this->orderService->save($order);
						$this->messages->addError(sprintf(__('Something went wrong. Response from Authorize.net invalidated this order. Reason: %s - %s.',
							'jigoshop_authorize_pro'), $response->response_code, $response->response_reason_text));
						echo \AuthorizeNetDPM::getRelayResponseSnippet(Order::getPayLink($order));
						exit;
					}
					
				} else {
					if ($this->settings['debug_log'] == true) {
						$this->addToLog('AUTHORIZE.NET ERROR:'.PHP_EOL.
							'response_code: '.$response->response_code.PHP_EOL.
							'response_reason_code: '.$response->response_reason_code.PHP_EOL.
							'response_reason_text: '.$response->response_reason_text);
						$this->addToLog('RESPONSE: '.print_r($response, true));
					}
					
					if ($this->settings['email_to_log'] == true) {
						$this->emailErrorLog('AUTHORIZE.NET ERROR:'.PHP_EOL.
							'response_code: '.$response->response_code.PHP_EOL.
							'response_reason_code: '.$response->response_reason_code.PHP_EOL.
							'response_reason_text: '.$response->response_reason_text.PHP_EOL.print_r($response, true).PHP_EOL, $_POST, $order->getId());
					}
					$order->setStatus(Status::ON_HOLD,
						sprintf(__('Payment failed. Response from Authorize.net invalidated this order. Reason: %s - %s.',
							'jigoshop_authorize_pro'), $response->response_code, $response->response_reason_text));
					$this->orderService->save($order);
					$this->messages->addError(sprintf(__('Payment failed. Response from Authorize.net invalidated this order. Reason: %s - %s.',
						'jigoshop_authorize_pro'), $response->response_code, $response->response_reason_text));
					echo \AuthorizeNetDPM::getRelayResponseSnippet(Order::getPayLink($order));
					exit;
				}
			} else {
				$this->messages->addError("Error -- not AuthorizeNet. Check your MD5 Setting.", true);
				echo \AuthorizeNetDPM::getRelayResponseSnippet(home_url());
				exit;
			}
		}
	}
	
	/**
	 *  Dump a message into the logging facility for debugging
	 *
	 * @param $message
	 */
	private function addToLog($message)
	{
		$path = JIGOSHOP_AUTHORIZE_PRO_DIR . '/log/authorize_pro_debug.log';
		
		$string = '-----Log Entry-----' . PHP_EOL;
		$string .= 'Log Date: ' . date('r') . PHP_EOL . $message . PHP_EOL;
		
		if (file_exists($path)) {
			if ($log = fopen($path, 'a')) {
				fwrite($log, $string, strlen($string));
				fclose($log);
			}
		} else {
			if ($log = fopen($path, 'c')) {
				fwrite($log, $string, strlen($string));
				fclose($log);
			}
		}
	}
	
	private function getEmail()
	{
		$email = null;
		if(!empty($this->settings['email_to'])){
			$email = $this->settings['email_to'];
		}else{
			$email = $this->options->get('general.company_email');
		}
		
		return $email;
	}
	
	/**
	 * Email the error logs
	 *
	 * @param $error
	 * @param array $posted
	 * @param string $orderID
	 */
	private function emailErrorLog($error, $posted = array(), $orderID = '')
	{
		$subject = sprintf(__('[%s] Authorize.net PRO Error Log for Order #%s', 'jigoshop'),
										html_entity_decode(get_bloginfo('name'), ENT_QUOTES), $orderID);
		$message = __('Order #', 'jigoshop_authorize_pro').$orderID.PHP_EOL;
		$message .= '======================================================='.PHP_EOL;
		if (!empty($error)) {
			$message .= __('Errors logged during the Jigoshop Authorize.net PRO Checkout process:',
																					'jigoshop_authorize_pro').PHP_EOL;
			$message .= $error.PHP_EOL;
		} else {
			$message .= __('No error message received.', 'jigoshop_authorize_pro').PHP_EOL;
		}
		$message .= '======================================================='.PHP_EOL;
		if (!empty($posted)) {
			$message .= 'All POST variables are:'.PHP_EOL;
			foreach ($posted as $key => $value) {
				$message .= $key.' = '.$value.PHP_EOL;
			}
			$message .= '======================================================='.PHP_EOL;
		}
		
		wp_mail($this->getEmail(), $subject, $message);
	}
}