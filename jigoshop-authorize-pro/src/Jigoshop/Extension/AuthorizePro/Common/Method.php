<?php

namespace Jigoshop\Extension\AuthorizePro\Common;

use Jigoshop\Core\Messages;
use Jigoshop\Core\Options;
use Jigoshop\Entity\Customer\CompanyAddress;
use Jigoshop\Entity\Order;
use Jigoshop\Exception;
use Jigoshop\Frontend\Pages;
use Jigoshop\Helper\Api;
use Jigoshop\Helper\Currency;
use Jigoshop\Helper\Options as OptionsHelper;
use Jigoshop\Integration\Helper\Render;
use Jigoshop\Payment\Method3;
use Jigoshop\Service\CartServiceInterface;
use Jigoshop\Service\OrderServiceInterface;

class Method implements Method3
{
	const ID = 'authorize_pro';
	const LIVE_URL = 'https://secure2.authorize.net/gateway/transact.dll';
	const SANDBOX_URL = 'https://test.authorize.net/gateway/transact.dll';
	/** @var  Options */
	private $options;
	/** @var  OrderService */
	private $orderService;
	/** @var  Messages */
	private $messages;
	/**@var CardService */
	private $cartService;
	/** @var array */
	private $settings;
	/**@var $apiLoginId string */
	private $apiLoginId;
	/**@var $transactionKey string */
	private $transactionKey;
	
	/**@var $serverUrl string */
	private $serverUrl;
	
	/**@var $merchantCountries array */
	private $merchantCountries = ['US', 'CA', 'GB'];
	/**@var $currency array */
	private $allowedCurrency = ['USD', 'CAD', 'GBP'];
	/**@var $currency string */
	private $currency;
	/**@var $country string */
	private $country;
	
	/**@tinyMce ********* */
	protected $tinyMce;
	/** HTML OUTPUT */
	public static $htmlOut = [];
	/**@DefaultSettings for TinyMce */
	public $tinyMceSettings = [
		'wpautop' => false,
		'media_buttons' => true,
		'textarea_name' => 'ssl_certificate',
		'textarea_rows' => 20,
		'tabindex' => null,
		'editor_css' => '',
		'editor_class' => 'ssl_certificate',
		'teeny' => 0,
		'dfw' => 0,
		'tinymce' => 1,
		'quicktags' => 1,
		'drag_drop_upload' => true
	];
	
	protected static $allowedHtml = [
		'a' => [
			'id' => [],
			'href' => [],
			'title' => [],
			'style' => [],
			'class' => [],
		],
		'b' => [
			'id' => [],
			'class' => [],
		],
		'br' => [],
		'em' => [
			'id' => [],
			'class' => []
		],
		'blockquote' => [
			'id' => [],
			'class' => [],
		],
		'i' => [
			'id' => [],
			'class' => [],
		],
		'ins' => [],
		'img' => [
			'title' => [],
			'src' => [],
			'class' => [],
			'id' => [],
		],
		'ol' => [
			'class' => [],
			'id' => [],
		],
		'ul' => [
			'class' => [],
			'id' => [],
		],
		'li' => [
			'class' => [],
			'id' => [],
		],
		'strong' => [
			'id' => [],
			'class' => [],
		],
		'script' => [],
	];
	
	
	/**
	 * Method constructor.
	 * @param Options $options
	 * @param CartServiceInterface $cartService
	 * @param OrderServiceInterface $orderService
	 * @param Messages $messages
	 */
	public function __construct(Options $options, CartServiceInterface $cartService, OrderServiceInterface $orderService, Messages $messages)
	{
		$this->options = $options;
		$this->orderService = $orderService;
		$this->messages = $messages;
		$this->cartService = $cartService;
		$this->currency = Currency::code();
		$this->country = $this->options->get('general.country');
		OptionsHelper::setDefaults('payment.' . self::ID, $this->defaultSettings());
		
		$this->settings = OptionsHelper::getOptions('payment.' . self::ID);
		add_filter('jigoshop\pay\render', [$this, 'renderPay'], 10, 2);
	}
	
	private function getUrl()
	{
		$this->serverUrl = ($this->settings['test_mode'] == false) ? self::LIVE_URL : self::SANDBOX_URL;
		
		return $this->serverUrl;
	}
	
	/**
	 * @return array
	 */
	private function defaultSettings()
	{
		return [
			'enabled' => false,
			'title' => __('AuthorizeNet Pro', 'jigoshop_authorize_pro'),
			'description' => __('Pay via AuthorizeNet Pro', 'jigoshop_authorize_pro'),
			'api_login_id' => '',
			'transaction_key' => '',
			'md5' => '',
			'accepted_cards' => [],
			'transaction_type' => [],
			'connection_method' => [],
			'receipt_email' => false,
			'debug_log' => '',
			'email_to_log' => false,
			'email_to' => '',
			'test_mode' => false,
			'ssl_certificate' => '',
			'adminOnly' => false,
		];
	}
	
	/**
	 * @return string ID of payment method.
	 */
	public function getId()
	{
		return self::ID;
	}
	
	/**
	 * @return string Human readable name of method.
	 */
	public function getName()
	{
		return is_admin() ? $this->getLogoImage() . ' ' . __('AuthorizeNet Pro', 'jigoshop_authorize_pro') : $this->settings['title'];
	}
	
	private function getLogoImage()
	{
		return '<img src="' . JIGOSHOP_AUTHORIZE_PRO_URL . '/assets/images/authorizedreseller.gif' . '" alt="AuthorizePro" width="" height="" />';
	}
	
	/**
	 * @return bool Whether current method is enabled and able to work.
	 */
	public function isEnabled()
	{
		return $this->settings['enabled'];
	}
	
	/**
	 * @return array List of options to display on Payment settings page.
	 */
	public function getOptions()
	{
		return [
			[
				'name' => sprintf(('[%s][enabled]'), self::ID),
				'title' => __('Enable AuthorizeNet Pro', 'jigoshop_authorize_pro'),
				'id' => 'jigoshop_authorize_pro_enable',
				'type' => 'checkbox',
				'classes' => ['switch-medium'],
				'checked' => $this->settings['enabled'],
			],
			[
				'name' => sprintf('[%s][title]', self::ID),
				'title' => __('Method Title', 'jigoshop_authorize_pro'),
				'tip' => __('This controls the title on checkout page', 'jigoshop_authorize_pro'),
				'type' => 'text',
				'value' => $this->settings['title'],
			],
			[
				'name' => sprintf('[%s][description]', self::ID),
				'title' => __('Description', 'jigoshop_authorize_pro'),
				'tip' => __('This controls the description on checkout page', 'jigoshop_authorize_pro'),
				'type' => 'text',
				'value' => $this->settings['description'],
			],
			[
				'name' => sprintf('[%s][api_login_id]', self::ID),
				'title' => __('API Login ID', 'jigoshop_authorize_pro'),
				'tip' => __('Your merchant\'s valid API login ID. Submit the API login ID used to submit transactions.', 'jigoshop_authorize_pro'),
				'type' => 'text',
				'value' => $this->settings['api_login_id'],
			],
			[
				'name' => sprintf('[%s][transaction_key]', self::ID),
				'title' => __('Transaction key', self::ID),
				'tip' => __('Your merchant\'s valid transaction key', 'jigoshop_authorize_pro'),
				'type' => 'text',
				'value' => $this->settings['transaction_key'],
			],
			[
				'name' => sprintf('[%s][md5]', self::ID),
				'title' => __('MD5 Hash', 'jigoshop_authorize_pro'),
				'desc' => __('Optional - not used if using SSL and AIM.', 'jigoshop_authorize_pro'),
				'tip' => __('This is the optional MD5 Hash you may have entered on your Authorize.net Merchant Account for additional security with DPM and SIM transactions.', 'jigoshop_authorize_pro'),
				'id' => 'jigoshop_authorize_md5hash',
				'type' => 'text',
				'value' => $this->settings['md5'],
			],
			[
				'name' => sprintf('[%s][accepted_cards]', self::ID),
				'title' => __('Accepted Cards', 'jigoshop_authorize_pro'),
				'tip' => __('Select which credit card types to accept and display logos for on the Checkout.  These should match the settings within your Authorize.net Merchant account.', 'jigoshop_authorize_pro'),
				'type' => 'select',
				'multiple' => true,
				'options' => $this->acceptedCards(),
				'value' => $this->settings['accepted_cards'],
			],
			[
				'name' => sprintf('[%s][transaction_type]', self::ID),
				'title' => __('Transaction Type', 'jigoshop_authorize_pro'),
				'tip' => __('', 'jigoshop_authorize_pro'),
				'type' => 'select',
				'options' => $this->transactionType(),
				'value' => $this->settings['transaction_type'],
			],
			[
				'name' => sprintf('[%s][connection_method]', self::ID),
				'title' => __('Connection Method', 'jigoshop_authorize_pro'),
				'tip' => $this->tip(),
				'description' => $this->tip(),
				'type' => 'select',
				'options' => $this->connectionMethod(),
				'value' => $this->settings['connection_method'],
			],
			[
				'name' => sprintf('[%s][receipt_email]', self::ID),
				'title' => __('Email Authorize.net Receipt', 'jigoshop_authorize_pro'),
				'desc' => __('', 'jigoshop_authorize_pro'),
				'tip' => __('In addition to Jigoshop emails, allow Authorize.net to also email successful payment receipts to customers.', 'jigoshop_authorize_pro'),
				'id' => 'jigoshop_authorize_email_receipt',
				'type' => 'checkbox',
				'checked' => $this->settings['receipt_email'],
				'classes' => ['switch-medium'],
			],
			[
				'name' => sprintf('[%s][debug_log]', self::ID),
				'title' => __('Debug Logging', 'jigoshop_authorize_pro'),
				'desc' => '',
				'tip' => __('Transactions between the Shop and Authorize.net are logged into a file here: wp-content/plugins/jigoshop-authorize-net-pro/log/authorize_pro_debug.log.  This file must have server write permissions.', 'jigoshop_authorize_pro'),
				'type' => 'checkbox',
				'checked' => $this->settings['debug_log'],
				'classes' => ['switch-medium'],
			],
			[
				'name' => sprintf('[%s][email_to_log]', self::ID),
				'title' => __('Receive Error Log Emails?:', 'jigoshop_authorize_pro'),
				'tip' => __('Do you want to receive emails for the error logs if a problem occurs during a transaction?', 'jigoshop_authorize_pro'),
				'id' => 'jigoshop_authorize_logs_emailto',
				'type' => 'checkbox',
				'checked' => $this->settings['email_to_log'],
			],
			[
				'name' => sprintf('[%s][email_to]', self::ID),
				'title' => __('Email Error Logs to:', 'jigoshop_authorize_pro'),
				'tip' => __('Email address you want to use to send error logs to. If email field is empty, the Jigoshop email address will be used.', 'jigoshop_authorize_pro'),
				'id' => 'jigoshop_authorize_logs_emailto',
				'std' => '',
				'type' => 'text',
				'value' => $this->settings['email_to'],
			],
			[
				'name' => sprintf('[%s][test_mode]', self::ID),
				'title' => __('Test Mode', 'jigoshop_authorize_pro'),
				'description' => __('Requires a <a href="https://developer.authorize.net/testaccount/">developer account</a> on the Authorize.net testing servers.', 'dti-autorize-pro'),
				'type' => 'checkbox',
				'checked' => $this->settings['test_mode'],
				'classes' => ['switch-medium'],
			],
			[
				'name' => sprintf('[%s][ssl_certificate]', self::ID),
				'title' => __('SSL Certification', 'jigoshop_authorize_pro'),
				'id' => 'ssl_certificate',
				'tinymce' => true,
				'type' => 'user_defined',
				'display' => [$this, 'displayTinyMce'],
				'value' => $this->settings['ssl_certificate'],
			],
			[
				'name' => sprintf('[%s][adminOnly]', self::ID),
				'title' => __('Enable Only for Admin', 'jigoshop_authorize_pro'),
				'type' => 'checkbox',
				'description' => __('Enable this if you would like to test it only for Site Admin', 'jigoshop_authorize_pro'),
				'checked' => $this->settings['adminOnly'],
				'classes' => ['switch-medium'],
			],
		];
	}
	
	private function sslTitle()
	{
		return __('<h3>SSL Certification</h3><small>Leave blank to display nothing if you want to display it elsewhere on the site or you are not using SSL and AIM.</small>', 'jigoshop_authorize_pro');
	}
	
	public static function allowedHtmlOutput()
	{
		return array_merge(self::$htmlOut, self::$allowedHtml);
	}
	
	/**
	 * @return string
	 */
	public function displayTinyMce()
	{
		ob_start();
		$this->tinyMce = empty($this->settings['ssl_certificate']) ? '' : $this->settings['ssl_certificate'];
		$this->tinyMce = str_replace('\\', '', $this->tinyMce);
		Render::output('authorize_pro', 'admin/tip', [
			'tinyMce' => $this->tinyMce,
			'allowedHtmlOut' => self::allowedHtmlOutput(),
			'tinyMceSettings' => $this->tinyMceSettings,
		]);
		return ob_get_clean();
	}
	
	/**
	 * @return string
	 */
	private function tip()
	{
		return __('SIM - No SSL required, transfers customer to Authorize.net secured SSL servers to accept credit card Information.
		DPM - No SSL required.  Uses unique transaction "fingerprint" for security.  Customers stay on your Server, but credit card info is sent directly from the customer to the secured Authorize servers.
		AIM - SSL IS required, maximum security and full PCI compliance.  Customers stay on your Server.', 'jigoshop_authorize_pro');
	}
	
	private function validateArrayData($data)
	{
		if (is_array($data)) {
			filter_var_array($data, FILTER_SANITIZE_STRING);
		}
		
		return $data;
	}
	
	private function isEmail($email)
	{
		if (!empty($email)) {
			return preg_match('/^[A-Za-z0-9!#$%&\'*+-\/=?^_`{|}~]+@[A-Za-z0-9-]+(\.[AZa-z0-9-]+)+[A-Za-z]$/', $email);
		}
		
		return true;
	}
	
	private function acceptedCards()
	{
		return array(
			'VSA' => __('Visa', 'jigoshop_authorize_pro'),
			'MSC' => __('Master Card', 'jigoshop_authorize_pro'),
			'JCB' => __('JCB', 'jigoshop_authorize_pro'),
			'AMX' => __('American Express', 'jigoshop_authorize_pro'),
			'DNC' => __('Diners Club', 'jigoshop_authorize_pro'),
			'DIS' => __('Discover', 'jigoshop_authorize_pro'),
		);
	}
	
	private function transactionType()
	{
		return array(
			'capture' => __('Authorize and Capture', 'jigoshop_authorize_pro'),
			'authorize' => __('Authorize Only', 'jigoshop_authorize_pro'),
		);
	}
	
	private function connectionMethod()
	{
		return array(
			'sim' => __('Server Integration Method (SIM)', 'jigoshop_authorize_pro'),
			'dpm' => __('Direct Post Method (DPM)', 'jigoshop_authorize_pro'),
			'aim' => __('Advanced Integration Method (AIM)', 'jigoshop_authorize_pro'),
		);
	}
	
	/**
	 * @param $cardAccept
	 * @return array
	 */
	protected function getCardIcon($cardAccept)
	{
		$icons = array();
		
		if (is_array($cardAccept)) {
			foreach ($cardAccept as $card) {
				$icons[] = JIGOSHOP_AUTHORIZE_PRO_URL . '/assets/images/cards/' . $card . '.png';
			}
		}
		
		return $icons;
	}
	
	/**
	 * @param $cardAccept
	 * @param $allCards
	 * @return array
	 */
	protected function allowedCards($cardAccept, $allCards)
	{
		$cards = array();
		if (is_array($cardAccept)) {
			foreach ($cardAccept as $card) {
				$cards[] = $allCards[$card];
			}
		}
		
		return $cards;
	}
	
	/**
	 * Validates and returns properly sanitized options.
	 *
	 * @param $settings array Input options.
	 *
	 * @return array Sanitized result.
	 */
	public function validateOptions($settings)
	{
		$disabled = false;
		$settings['enabled'] = $settings['enabled'] == 'on';
		$settings['test_mode'] = $settings['test_mode'] == 'on';
		$settings['receipt_email'] = $settings['receipt_email'] == 'on';
		$settings['email_to_log'] = $settings['email_to_log'] == 'on';
		$settings['debug_log'] = $settings['debug_log'] == 'on';
		$settings['adminOnly'] = $settings['adminOnly'] == 'on';
		$settings['title'] = trim(htmlspecialchars(strip_tags($settings['title'])));
		$settings['description'] = trim(htmlspecialchars(strip_tags($settings['description'],
			'<p><a><strong><em><b><i>')));
		
		$settings['api_login_id'] = trim(strip_tags(esc_attr($settings['api_login_id'])));
		$settings['transaction_key'] = trim(strip_tags(esc_attr($settings['transaction_key'])));
		$settings['md5'] = trim(strip_tags(esc_attr($settings['md5'])));
		$settings['email_to'] = trim(strip_tags(esc_attr($settings['email_to'])));
		$settings['receipt_email'] = trim(strip_tags(esc_attr($settings['receipt_email'])));
		$settings['accepted_cards'] = $this->validateArrayData($settings['accepted_cards']);
		$settings['connection_method'] = $this->validateArrayData($settings['connection_method']);
		$settings['transaction_type'] = $this->validateArrayData($settings['transaction_type']);
		
		$settings['ssl_certificate'] = wp_kses($_POST['ssl_certificate'], self::allowedHtmlOutput());
		
		if ($settings['enabled'] == 'on') {
			if (!$this->isEmail($settings['email_to']) && !empty($settings['email_to'])) {
				$this->messages->addError('Please enter a valid debug email. <strong>Gateway is disabled</strong>', 'jigoshop_authorize_pro');
				$settings['enabled'] = $disabled;
			}
		}
		
		$currentCurrency = $this->currency;
		if (!in_array($currentCurrency, $this->allowedCurrency)) {
			$message = sprintf(__('AuthorizePro for Jigoshop accepts payments in: %s. Your current currency is %s. AuthorizePro won\'t work until you change the Jigoshop currency to one accepted.', 'jigoshop_authorize_pro'), implode(' | ', $this->allowedCurrency), $this->currency);
			$this->messages->addError($message);
			$settings['enabled'] = $disabled;
		}
		
		$currentCountry = $this->country;
		if (!in_array($currentCountry, $this->merchantCountries)) {
			$message = sprintf(__('AuthorizePro work for countries: %s. Your current currency is %s. AuthorizePro won\'t work until you change the country to one accepted.', 'jigoshop_authorize_pro'), implode(' | ', $this->merchantCountries), $this->country);
			$this->messages->addError($message);
			$settings['enabled'] = $disabled;
		}
		
		$ssl = $this->options->get('shopping.force_ssl');
		
		if ($settings['connection_method'] == 'aim' && $ssl == false) {
			$message = sprintf(__('You have enabled AIM method but "<a href="%s">Force SSL Option</a>" on checkout is disabled, please enable it to work with AIM method'), admin_url('admin.php?page=jigoshop_settings&tab=shopping'));
			$this->messages->addError($message);
			$settings['enabled'] = $disabled;
		}
		
		if ($ssl == true && $settings['connection_method'] !== 'aim') {
			$message = sprintf(__('The Authorize.net PRO gateway will <strong>always</strong> use the AIM connection method if the Jigoshop settings for SSL are enabled.  The connection method has been <strong>reset back to AIM</strong> in the gateway settings.  If you don\'t want this you must disable Jigoshop\'s SSL setting on the General Tab.', 'jigoshop_authorize_pro'));
			$this->messages->addError($message);
			$settings['connection_method'] = 'aim';
		}
		if ($settings['enabled'] == 'on') {
			if (empty($settings['accepted_cards'])) {
				$message = sprintf(__('The Authorize.net PRO gateway does not have any Credit Card Types enabled.  Please enable the Credit Cards your Authorize.net Merchant account is set up to process or the gateway <strong>will not</strong> be available on the Checkout.  Disable the gateway to remove this warning.', 'jigoshop_authorize_pro'));
				$this->messages->addError($message);
				$settings['enabled'] = $disabled;
			}
		}
		
		return $settings;
	}
	
	/**
	 * Renders method fields and data in Checkout page.
	 */
	public function render()
	{
		if ($this->settings['description']) {
			echo wpautop($this->settings['description']);
		}
		$months = [];
		for ($i = 1; $i <= 12; $i++) {
			$timestamp = mktime(0, 0, 0, $i, 1);
			$months[date('m', $timestamp)] = date('F', $timestamp);
		}
		
		$years = range(date('Y'), date('Y') + 15);
		$cardAccept = $this->settings['accepted_cards'];
		$macarena = '';
		if ($this->settings['connection_method'] == 'aim' && !empty($this->settings['ssl_certificate'])) {
			$macarena = $this->settings['ssl_certificate'];
		}
		$allCards = $this->acceptedCards();
		$icons = $this->getCardIcon($cardAccept);
		$cards = $this->allowedCards($cardAccept, $allCards);
		if ($this->settings['connection_method'] == 'dpm' || $this->settings['connection_method'] == 'aim') {
			Render::output('authorize_pro', 'frontend/checkout/pay_form', [
				'title' => $this->settings['title'],
				'description' => $this->settings['description'],
				'ssl' => $macarena,
				'months' => $months,
				'years' => $years,
				'cards' => $cards,
				'icons' => $icons,
			]);
		} else {
			Render::output('authorize_pro', 'frontend/checkout/card_types', [
				'title' => $this->settings['title'],
				'cards' => $cards,
				'icons' => $icons,
			]);
		}
	}
	
	/**
	 * @var Order $order
	 * @return \AuthorizeNetSIM_Form
	 */
	public function getFormFields($order)
	{
		$this->apiLoginId = $this->settings['api_login_id'];
		$this->transactionKey = $this->settings['transaction_key'];
		$time = time();
		require_once(JIGOSHOP_AUTHORIZE_PRO_DIR . "/vendor/anet_php_sdk/AuthorizeNet.php");
		$fingerprint = \AuthorizeNetDPM::getFingerprint($this->apiLoginId, $this->transactionKey, $order->getTotal(), $order->getId(), $time);
		$transtype = $this->settings['transaction_type'] == 'capture' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';
		
		$billing = $order->getCustomer()->getBillingAddress();
		$shipping = $order->getCustomer()->getShippingAddress();
		
		$bCompany = $billing instanceof CompanyAddress ? $billing->getCompany() : '';
		$sCompany = $shipping instanceof CompanyAddress ? $shipping->getCompany() : '';
		$cancelUrl = \Jigoshop\Helper\Order::getCancelLink($order);
		
		$data = [
			'x_type' => $transtype,
			'x_amount' => $order->getTotal(),
			'x_fp_sequence' => $order->getId(),
			'x_fp_hash' => $fingerprint,
			'x_fp_timestamp' => $time,
			'x_relay_response' => true,
			'x_relay_url' => Api::getUrl(self::ID, home_url()),
			'x_cancel_url' => $cancelUrl,
			'x_login' => $this->apiLoginId,
			'x_first_name' => $billing->getFirstName(),
			'x_last_name' => $billing->getLastName(),
			'x_company' => $bCompany,
			'x_address' => $billing->getAddress(),
			'x_city' => $billing->getCity(),
			'x_state' => $billing->getState(),
			'x_zip' => $billing->getPostcode(),
			'x_country' => $billing->getCountry(),
			'x_phone' => $billing->getPhone(),
			'x_email' => $billing->getEmail(),
			'x_ship_to_first_name' => $shipping->getFirstName(),
			'x_ship_to_last_name' => $shipping->getLastName(),
			'x_ship_to_company' => $sCompany,
			'x_ship_to_address' => $shipping->getAddress(),
			'x_ship_to_city' => $shipping->getCity(),
			'x_ship_to_state' => $shipping->getState(),
			'x_ship_to_zip' => $shipping->getPostcode(),
			'x_ship_to_country' => $shipping->getCountry(),
			'x_cust_id' => $order->getCustomer()->getId(),
			'x_invoice_num' => $order->getId(),
			'x_email_customer' => 'false',
			'x_order_key' => $order->getKey(),
			'x_description' => $order->getId(),
		];
		
		if ($this->settings['receipt_email'] == true) {
			$data['x_email_customer'] = 'true';
		}
		if ($this->settings['connection_method'] == 'dpm') {
			$data = $this->addFields($data);
		}
		if ($this->settings['connection_method'] == 'sim') {
			$data['x_show_form'] = 'PAYMENT_FORM';
		}
		return $data;
	}
	
	/**
	 * @param $data
	 * @return mixed
	 */
	protected function addFields($data)
	{
		if (is_array($data)) {
			$data['x_card_num'] = $this->getPost('authorizePro', 'cardNumber');
			$data['x_exp_date'] = $this->getPost('authorizePro', 'expiryDateMonth') . $this->getPost('authorizePro', 'expiryDateYear');
			$data['x_card_code'] = $this->getPost('authorizePro', 'Cv2');
			$data['x_card_type'] = $this->getPost('authorizePro', 'cardType');
		}
		return (array)$data;
	}
	
	/**
	 * @param $order
	 * @return \AuthorizeNetSIM_Form
	 */
	private function processDpmPayment($order)
	{
		$form = $this->getFormFields($order);
		
		$additionalString = '';
		
		$sim = new \AuthorizeNetSIM_Form($form);
		$hiddenFields = $sim->getHiddenFieldString() . $additionalString;
		
		$transaction = curl_init($this->getUrl());
		curl_setopt($transaction, CURLOPT_POST, 1);
		curl_setopt($transaction, CURLOPT_POSTFIELDS, http_build_query($form));
		curl_setopt($transaction, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($transaction, CURLOPT_TIMEOUT, 30);
		curl_setopt($transaction, CURLOPT_SSL_VERIFYHOST, 2);
		$response = curl_exec($transaction);
		preg_match("/<location>(.+)<\/location>/", $response, $matches);
		
		return $matches[1];
	}
	
	/**
	 * @param $order
	 * @return string $content HTML
	 */
	private function processSimPayment($order)
	{
		$form = $this->getFormFields($order);
		
		$additionalString = '';
		
		$sim = new \AuthorizeNetSIM_Form($form);
		$hiddenFields = $sim->getHiddenFieldString() . $additionalString;
		$url = $this->getUrl();
		
		$content = Render::get('authorize_pro', '/frontend/checkout/pay_order', array(
			'url' => $url,
			'hiddenFields' => $hiddenFields,
		));
		
		return $content;
	}
	
	/**
	 * @var Order $order
	 * @return string
	 */
	private function processAimPayment($order)
	{
		$transtype = $this->settings['transaction_type'] == 'capture' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';
		require_once(JIGOSHOP_AUTHORIZE_PRO_DIR . "/vendor/anet_php_sdk/AuthorizeNet.php");
		$this->apiLoginId = $this->settings['api_login_id'];
		$this->transactionKey = $this->settings['transaction_key'];
		
		$billing = $order->getCustomer()->getBillingAddress();
		$shipping = $order->getCustomer()->getShippingAddress();
		
		$bCompany = $billing instanceof CompanyAddress ? $billing->getCompany() : '';
		$sCompany = $shipping instanceof CompanyAddress ? $shipping->getCompany() : '';
		
		$dti_authorize_request = array(
			'type' => $transtype,
			'amount' => $order->getTotal(),
			'freight' => $order->getShippingPrice(),
			'tax' => $order->getTotalTax(),
			'login' => $this->apiLoginId,
			'tran_key' => $this->transactionKey,
			'cust_id' => $order->getCustomer()->getId(),
			'customer_ip' => $_SERVER['REMOTE_ADDR'],
			'invoice_num' => $order->getNumber(),
			'description' => $order->getId(),
			'first_name' => $billing->getFirstName(),
			'last_name' => $billing->getLastName(),
			'company' => $bCompany,
			'address' => $billing->getAddress(),
			'city' => $billing->getCity(),
			'state' => $billing->getState(),
			'zip' => $billing->getPostcode(),
			'country' => $billing->getCountry(),
			'phone' => $billing->getPhone(),
			'email' => $billing->getEmail(),
			'ship_to_first_name' => $shipping->getFirstName(),
			'ship_to_last_name' => $shipping->getLastName(),
			'ship_to_company' => $sCompany,
			'ship_to_address' => $shipping->getAddress(),
			'ship_to_city' => $shipping->getCity(),
			'ship_to_state' => $shipping->getState(),
			'ship_to_zip' => $shipping->getPostcode(),
			'ship_to_country' => $shipping->getCountry(),
			'method' => 'CC',
			'card_num' => $this->getPost('authorizePro', 'cardNumber'),
			'card_code' => $this->getPost('authorizePro', 'Cv2'),
			'exp_date' => $this->getPost('authorizePro', 'expiryDateMonth') . $this->getPost('authorizePro', 'expiryDateYear'),
			//'card_type' => $this->getPost('authorizePro', 'cardType'),
		);
		
		$request_sale = new \AuthorizeNetAIM($this->apiLoginId, $this->transactionKey);
		$request_sale->setFields($dti_authorize_request);
		
		
		if ($this->settings['receipt_email'] == true) {
			$request_sale->setFields([
				'email_customer' => 'true'
			]);
		}
		if ($this->settings['transaction_type'] == 'capture') {
			$response = $request_sale->authorizeAndCapture();
		} else {
			$response = $request_sale->authorizeOnly();
		}
		/** @var $response \AuthorizeNetAIM_Response */
		if ($response->approved) {
			$orderStatus = \Jigoshop\Helper\Order::getStatusAfterCompletePayment($order);
			$order->setStatus($orderStatus, sprintf(sprintf(__('Authorize.net PRO Payment completed via %s. (Response: %s - Transaction Type: %s with Authorization Code: %s)', 'jigoshop_authorize_pro'), strtoupper($this->settings['connection_method']), $response->response_reason_text, $transtype, $response->authorization_code)));
			$this->orderService->save($order);
			$redirect = add_query_arg('authorize_pro', 'success', add_query_arg('key', $order->getKey(),
				add_query_arg('order', $order->getId(),
					get_permalink($this->options->getPageId(Pages::THANK_YOU)))));
			wp_safe_redirect($redirect);
			exit;
		} elseif ($response->error) {
			if ($this->settings['debug_log'] == true) {
				$this->addToLog($response->error_message . PHP_EOL);
				$this->addToLog('RESPONSE: ' . print_r($response, true));
			}
			if ($this->settings['email_to'] == true) {
				$this->emailErrorLog('AUTHORIZE.NET ERROR:' . PHP_EOL .
					'response_code: ' . $response->response_code . PHP_EOL .
					'response_reason_text: ' . $response->response_reason_text . PHP_EOL . print_r($response, true) . PHP_EOL, array(), $order->getId());
			}
			$order->setStatus(sprintf(__('Payment was declined via Authorize: %s', 'jigoshop_authorize_pro'), strip_tags($response->error_message)));
			$this->orderService->save($order);
			$redirect = \Jigoshop\Helper\Order::getPayLink($order);
			$this->messages->addError(sprintf(__('%s Please try again or choose another gateway for your Order.', 'jigoshop_authorize_pro'), strip_tags($response->response_reason_text)));
			wp_safe_redirect($redirect);
			exit;
		} else {
			if ($this->settings['debug_log']) {
				$this->addToLog('AUTHORIZE.NET ERROR:' . PHP_EOL .
					'response_code: ' . $response->response_code . PHP_EOL .
					'response_reason_text: ' . $response->response_reason_text);
				$this->addToLog('RESPONSE: ' . print_r($response, true));
			}
			if ($this->settings['email_to'] == true) {
				$this->emailErrorLog('AUTHORIZE.NET ERROR:' . PHP_EOL .
					'response_code: ' . $response->response_code . PHP_EOL .
					'response_reason_text: ' . $response->response_reason_text . PHP_EOL . print_r($response, true) . PHP_EOL, array(), $order->getId());
			}
			
			$cancelNote = __('Authorize.net PRO Payment failed', 'jigoshop_authorize_pro') . ' (Response Code: ' . $response->response_code . '). ' . __('Payment was rejected due to an error', 'jigoshop_authorize_pro') . ': "' . $response->response_reason_text . '". ';
			$order->setStatus($cancelNote);
			$this->orderService->save($order);
			
			$this->messages->addError(sprintf(__('Authorize.net PRO Payment failed: %s Please try again or choose another gateway for your Order.', 'jigoshop_authorize_pro'), $response->response_reason_text));
			
		}
		
		return \Jigoshop\Helper\Order::getPayLink($order);
	}
	
	/**
	 * @param $content
	 * @var Order $order
	 * @return string $order
	 */
	public function renderPay($content, $order)
	{
		if ($this->settings['connection_method'] == 'sim') {
			return $this->processSimPayment($order);
		}
		
		return $content;
	}
	
	/**
	 * @param Order $order Order to process payment for.
	 *
	 * @return string URL to redirect to.
	 * @throws Exception On any payment error.
	 */
	public function process($order)
	{
		if ($this->settings['connection_method'] == 'dpm') {
			return $this->processDpmPayment($order);
		} elseif ($this->settings['connection_method'] == 'aim') {
			$this->validateFields();
			return $this->processAimPayment($order);
		} else {
			return \Jigoshop\Helper\Order::getPayLink($order);
		}
	}
	
	/**
	 *  Validate payment form fields on the Checkout, called from parent payment gateway class
	 */
	public function validateFields()
	{
		// SIM and DPM doesn't need card validations
		if ($this->settings['connection_method'] !== 'aim') {
			return true;
		}
		
		
		$cardNumber = $this->getPost('authorizePro', 'cardNumber');
		$cardCVC = $this->getPost('authorizePro', 'Cv2');
		$cardExpirationMonth = $this->getPost('authorizePro', 'expiryDateMonth');
		$cardExpirationYear = $this->getPost('authorizePro', 'expiryDateYear');
		$cardType = $this->getPost('authorizePro', 'cardType');
		
		if (!empty($cardType)) {
			return trim(strip_tags(htmlspecialchars($cardType)));
		}
		
		// check credit card number
		$cardNumber = str_replace(array(' ', '-'), '', $cardNumber);
		// this will validate (Visa, MasterCard, Discover, American Express) for length and format
		$valid = preg_match('^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$^', $cardNumber);
		if ($valid == 0) {
			throw new Exception(__('The Authorize.net Credit Card number entered is invalid.', 'jigoshop_authorize_pro'));
		} elseif ($valid === false) {
			throw new Exception(__('There was an error validating your Authorize.net credit card number.', 'jigoshop_authorize_pro'));
		}
		
		// check CVC
		if (!ctype_digit($cardCVC)) {
			throw new Exception(__('Authorize.net Credit Card security code entered is invalid.', 'jigoshop_authorize_pro'));
		}
		
		// check expiration data
		$currentYear = date('Y');
		if (!ctype_digit($cardExpirationMonth) || !ctype_digit($cardExpirationYear)
			|| $cardExpirationMonth > 12
			|| $cardExpirationMonth < 1
			|| $cardExpirationYear < $currentYear
			|| $cardExpirationYear > $currentYear + 20
		) {
			throw new Exception(__('Authorize.net Credit Card expiration date is invalid.', 'jigoshop_authorize_pro'));
		}
	}
	
	/**
	 *  Clean and return requested $_POST data
	 *
	 * @param $name
	 * @return null|string
	 */
	private function getPost($name, $key)
	{
		$value = null;
		
		if (isset($_POST[$name][$key])) {
			$value = strip_tags(stripslashes(trim($_POST[$name][$key])));
		}
		
		return $value;
	}
	
	/**
	 *  Dump a message into the logging facility for debugging
	 *
	 * @param $message
	 */
	private function addToLog($message)
	{
		$path = JIGOSHOP_AUTHORIZE_PRO_DIR . '/log/authorize_pro_debug.log';
		
		$string = '-----Log Entry-----' . PHP_EOL;
		$string .= 'Log Date: ' . date('r') . PHP_EOL . $message . PHP_EOL;
		
		if (file_exists($path)) {
			if ($log = fopen($path, 'a')) {
				fwrite($log, $string, strlen($string));
				fclose($log);
			}
		} else {
			if ($log = fopen($path, 'c')) {
				fwrite($log, $string, strlen($string));
				fclose($log);
			}
		}
	}
	
	private function getEmail()
	{
		$email = null;
		if (!empty($this->settings['email_to'])) {
			$email = $this->settings['email_to'];
		} else {
			$email = $this->options->get('general.company_email');
		}
		
		return $email;
	}
	
	/**
	 * Email the error logs
	 *
	 * @param $error
	 * @param array $posted
	 * @param string $order_id
	 */
	private function emailErrorLog($error, $posted = [], $order_id = '')
	{
		$subject = sprintf(__('[%s] Authorize.net PRO Error Log for Order #%s', 'jigoshop'), html_entity_decode(get_bloginfo('name'), ENT_QUOTES), $order_id);
		$message = __('Order #', 'jigoshop_authorize_pro') . $order_id . PHP_EOL;
		$message .= '=======================================================' . PHP_EOL;
		if (!empty($error)) {
			$message .= __('Errors logged during the Jigoshop Authorize.net PRO Checkout process:', 'jigoshop_authorize_pro') . PHP_EOL;
			$message .= $error . PHP_EOL;
		} else {
			$message .= __('No error message received.', 'jigoshop_authorize_pro') . PHP_EOL;
		}
		$message .= '=======================================================' . PHP_EOL;
		if (!empty($posted)) {
			$message .= 'All POST variables are:' . PHP_EOL;
			foreach ($posted as $key => $value) {
				$message .= $key . ' = ' . $value . PHP_EOL;
			}
			$message .= '=======================================================' . PHP_EOL;
		}
		wp_mail($this->getEmail(), $subject, $message);
	}
	
	/**
	 * Whenever method was enabled by the user.
	 *
	 * @return boolean Method enable state.
	 */
	public function isActive()
	{
		if (isset($this->settings['enabled'])) {
			return $this->settings['enabled'];
		}
	}
	
	/**
	 * Set method enable state.
	 *
	 * @param boolean $state Method enable state.
	 *
	 * @return array Method current settings (after enable state change).
	 */
	public function setActive($state)
	{
		$this->settings['enabled'] = $state;
		
		return $this->settings['enabled'];
	}
	
	/**
	 * Whenever method was configured by the user (all required data was filled for current scenario).
	 *
	 * @return boolean Method config state.
	 */
	public function isConfigured()
	{
		if (isset($this->settings['api_login_id']) && $this->settings['api_login_id']
			&& isset($this->settings['transaction_key']) && $this->settings['transaction_key']
			&& isset($this->settings['md5']) && $this->settings['md5']
		) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Whenever method has some sort of test mode.
	 *
	 * @return boolean Method test mode presence.
	 */
	public function hasTestMode()
	{
		return true;
	}
	
	/**
	 * Whenever method test mode was enabled by the user.
	 *
	 * @return boolean Method test mode state.
	 */
	public function isTestModeEnabled()
	{
		if (isset($this->settings['test_mode'])) {
			return $this->settings['test_mode'];
		}
	}
	
	/**
	 * Set Method test mode state.
	 *
	 * @param boolean $state Method test mode state.
	 *
	 * @return array Method current settings (after test mode state change).
	 */
	public function setTestMode($state)
	{
		$this->settings['test_mode'] = $state;
		
		return $this->settings;
	}
	
	/**
	 * Whenever method requires SSL to be enabled to function properly.
	 *
	 * @return boolean Method SSL requirment.
	 */
	public function isSSLRequired()
	{
		return true;
	}
	
	/**
	 * Whenever method is set to enabled for admin only.
	 *
	 * @return boolean Method admin only state.
	 */
	public function isAdminOnly()
	{
		if (true == $this->settings['adminOnly']) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Sets admin only state for the method and returns complete method options.
	 *
	 * @param boolean $state Method admin only state.
	 *
	 * @return array Complete method options after change was applied.
	 */
	public function setAdminOnly($state)
	{
		$this->settings['adminOnly'] = $state;
		
		return $this->settings;
	}
}