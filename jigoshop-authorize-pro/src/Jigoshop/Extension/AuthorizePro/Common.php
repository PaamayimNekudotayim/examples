<?php

namespace Jigoshop\Extension\AuthorizePro;


use Jigoshop\Integration\Helper\Render;
use Jigoshop\Integration;
use Jigoshop\Container;

class Common
{
	public function __construct()
	{
		Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
		Integration\Helper\Render::addLocation('authorize_pro', JIGOSHOP_AUTHORIZE_PRO_DIR);
		
		/**@var Container $di*/
		$di = Integration::getService('di');
		$di->services->setDetails('jigoshop.payment.authorize_pro', __NAMESPACE__ . '\\Common\\Method', [
			'jigoshop.options',
			'jigoshop.service.cart',
			'jigoshop.service.order',
			'jigoshop.messages',
		]);
		$di->services->setDetails('jigoshop.api.authorize_pro', __NAMESPACE__ . '\\Common\\Api', [
			'jigoshop.options',
			'jigoshop.service.order',
			'jigoshop.messages'
		]);
		$di->triggers->add('jigoshop.service.payment','jigoshop.service.payment', 'addMethod', ['jigoshop.payment.authorize_pro']);
	}
}
new Common();