<?php
/**
 * Plugin name: Jigoshop RitamForms Integration
 * Description: Extends Jigoshop. Import products from RitamForms to your Jigoshop.
 * Author:  @vasilguruli | JigoLtd
 * Version: 2.0.1
 * Plugin URI: http://jigoshop.com/product/jigoshop-ritamforms-integration/
 * Author URI: http://jigoshop.com
 * Init File Version: 2.0.1
 * Init File Date: 01.04.2016
 */

// Define plugin name
define('JIGOSHOP_RITAM_FORMS_NAME', 'Jigoshop Ritam Forms');

add_action('plugins_loaded', function () {
	load_plugin_textdomain('jigoshop_ritam_forms', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_RITAM_FORMS_DIR', dirname(__FILE__));
		// Define plugin URL for assets
		define('JIGOSHOP_RITAM_FORMS_URL', plugins_url('', __FILE__));
		//Check version.
		/*if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_RITAM_FORMS_NAME, '2.0')) {
			return;
		}
		//Check license.
		$licence = new \Jigoshop\Licence(__FILE__, '', 'http://www.jigoshop.com');
		if (!$licence->isActive()) {
			return;
		}*/
		//Init components.
		if (is_admin()) {
			require_once(JIGOSHOP_RITAM_FORMS_DIR . '/src/Jigoshop/Extension/RitamForms/Admin.php');
		}
	} elseif (class_exists('jigoshop')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_RITAM_FORMS_DIR', dirname(__FILE__) . '/Jigoshop1x');
		// Define plugin URL for assets
		define('JIGOSHOP_RITAM_FORMS_URL', plugins_url('', __FILE__) . '/Jigoshop1x');
		//Check version.
		if (jigoshop_add_required_version_notice(JIGOSHOP_RITAM_FORMS_NAME, '1.17')) {
			return;
		}
		//Check license.
		$licence = new jigoshop_licence_validator(__FILE__, '42579', 'http://www.jigoshop.com');
		if (!$licence->is_licence_active()) {
			return;
		}
		//Init components.
		require_once(JIGOSHOP_RITAM_FORMS_DIR . '/jigoshop-ritam-forms.php');
	} else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.', 'jigoshop_ritam_forms'),
				JIGOSHOP_RITAM_FORMS_NAME, JIGOSHOP_RITAM_FORMS_NAME);
			echo '</p></div>';
		});
	}
}); 