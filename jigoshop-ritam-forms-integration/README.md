Jigoshop RitamForms Integration
===============================

This plugin allows you to synchronise your products from RitamForms ERP with your Jigoshop store.

### Description

A plugin that allows you to extend your Jigoshop installation with the RitamForms ERP Integration.

This plugin was created by: Jigoshop

### Installation

1. Upload the `jigoshop-ritam-forms-integration` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Navigate to `Import products from RitamForms` to configure the RitamForms Integration settings.

### Steps for configuration of your RitamForms Integration installation

1. Login to your Jigoshop store
2. Navigate to `Import products from RitamForms->Options`.
3. Set your "Main API link" - production link is "http://webservice.ritam.hr:8920/".
4. Enter your RitamForms "API User".
5. Enter your RitamForms "API Secret Key".
7. Save Changes.

### Options

+ Import all your products to Jigoshop

+ Update stock of all your products

+ Search products by name or product code

+ Update quantity

+ Product preview