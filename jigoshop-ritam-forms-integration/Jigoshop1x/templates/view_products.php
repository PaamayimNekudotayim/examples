<?php echo $this->showLog(); ?>

<form method="post" action="" class="ritam_forms">
	<span class="submit"><input type="submit" class="option_submit" name="add_all_product" value="<?php _e('Import All Your Products to Jigoshop'); ?>" /></span>
	<span class="submit"><input type="submit" class="option_submit" name="update_all_stocks" value="<?php _e('Update Stocks of All Your Imported Products to Jigoshop'); ?>" /></span>
</form>

<form method="post" action="" class="ritam_forms">
	<input type="text" name="ritam_code" value="<?php echo stripslashes($this->_s_code) ?>" placeholder="<?php _e('Product code'); ?>" />
	<input type="text" name="ritam_name" value="<?php echo stripslashes($this->_s_name) ?>" placeholder="<?php _e('Product name'); ?>" />
	<span class="submit"><input type="submit" class="option_submit" name="option_search" value="<?php _e('Search'); ?>" /></span>
</form>
<div class="ritam_info"><?php _e('We found ');
	echo '<strong>'.count($this->_products).'</strong>';
	echo _e(' items'); ?></div>
<table class="view" style="border-collapse: collapse;">
	<tr>
		<th style="width: 30px;"></th>
		<th><?php _e('RF ID') ?></th>
		<th><?php _e('Code') ?></th>
		<th><?php _e('Name') ?></th>
		<th><?php _e('RF stock') ?></th>
		<th><?php _e('JigoS stock') ?></th>
		<th><?php _e('Action') ?></th>
		<th><?php _e('Product page') ?></th>
	</tr>
	<?php if ($this->_engine_ready == true && count($this->_products) > 0):
		foreach ($this->_products as $k => $v) :
			$rf_st = $this->_qty[$v->item_id];
			$differences_style = '';
			if (isset($this->_added_prod_id_to_jigoshop[$v->item_id])) {
				$imported = ' class="imported"';
				$checkbox = '';
				$button_add_update = '<input type="submit" class="" name="update_qty" value="'.__('Update quantity').'"/>';
				$preview_link = '<a href="'.get_the_guid($this->_added_prod_id_to_jigoshop[$v->item_id]).'">'.__('Preview').'</a>';
				if (($this->_qty[$v->item_id]) != get_post_meta($this->_added_prod_id_to_jigoshop[$v->item_id], 'stock', true)) {
					$differences_style = ' style="background-color: lightcoral;"';
				}
				$js_st = get_post_meta($this->_added_prod_id_to_jigoshop[$v->item_id], 'stock', true);
			} else {
				$imported = '';
				$checkbox = '<input type="checkbox" class="product" name="product[]" value="'.$v->item_id.'">';
				$button_add_update = '<input type="submit" class="" name="add_product" value="'.__('Add to Jigoshop').'"/>';
				$preview_link = 'none';
				$js_st = 'none';
			};
			?>
			<tr<?php echo $imported; ?>>
				<td class="td_standard prod"><?php echo $checkbox; ?></td>
				<td class="td_standard prod"><?php echo $v->item_id ?></td>
				<td class="td_standard prod"><?php echo $v->item_code ?></td>
				<td class="td_left prod"><?php echo $v->item_name ?></td>
				<td class="td_right prod"<?php echo $differences_style; ?>><?php echo $rf_st; ?></td>
				<td class="td_right prod"<?php echo $differences_style; ?>><?php echo $js_st; ?></td>
				<td class="td_standard prod">
					<form method="post" action="" class="">
						<input type="hidden" name="id_product" value="<?php echo $v->item_id ?>" />
						<?php echo $button_add_update; ?></form>
				</td>
				<td class="td_standard prod"><?php echo $preview_link; ?></td>
			</tr>
		<?php endforeach; endif; ?>
</table>
<br />
<div class="div_button" id="check_all"><?php _e('Check All'); ?></div>
<div class="div_button" id="uncheck_all"><?php _e('Uncheck All'); ?></div>
<div class="div_button" id="revers_sel"><?php _e('Reverse Select'); ?></div>
<br /><br /><br />
<form class="clear" id="ritam_import" action="" method="post">
	<input type="hidden" id="id_prod_box" name="id_prod_box" value="" />
	<input type="submit" id="import_submit" class="" name="import_submit" value="<?php _e('Import selected') ?>">
</form>
<br /><br />
