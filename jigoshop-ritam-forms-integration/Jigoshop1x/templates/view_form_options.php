<h1><?php _e('Options'); ?></h1>
<?php echo $this->_notice ?>
<form method="post" action="">
	<table class="form-table">
		<tbody>
		<tr valign="top">
			<td scope="row">
				<label for="api_link"><?php _e('Main API Link'); ?>:</label>
			</td>
			<td>
				<input class="option_text" type="text" name="api_link" id="api_link" value="<?php echo $this->_api_link ?>" />
			</td>
		</tr>
		<tr valign="top">
			<td scope="row">
				<label for="user"><?php _e('API User'); ?>:</label>
			</td>
			<td>
				<input class="option_text" type="text" name="user" id="user" value="<?php echo $this->_user ?>" />
			</td>
		</tr>
		<tr valign="top">
			<td scope="row">
				<label for="secret_key"><?php _e('API SecretKey'); ?>:</label>
			</td>
			<td>
				<input class="option_text" type="text" name="secret_key" id="secret_key" value="<?php echo $this->_secret_key ?>" />
			</td>
		</tr>
		</tbody>
	</table>
	<span class="submit"><input type="submit" class="option_submit" name="save_option" value="<?php _e('Save'); ?>" /></span>
</form>