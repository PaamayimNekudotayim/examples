<?php

if (!defined('JIGOSHOP_RITAM_FORMS_TABLE_NAME')) {
	define('JIGOSHOP_RITAM_FORMS_TABLE_NAME', 'ritam_forms_relationships');
}

add_action('plugins_loaded', function (){
	
	require_once(JIGOSHOP_RITAM_FORMS_DIR.'/src/Jigoshop/Integration/RitamForms.php');
},11);

register_activation_hook(__FILE__, function (){
	add_option('_ritam_api_link', __('like http://linkToWebServis:Port/'));
	add_option('_ritam_user', '');
	add_option('_ritam_secret_key', '');
	global $wpdb;
	$wpdb->query('
	CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.JIGOSHOP_RITAM_FORMS_TABLE_NAME.'(
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ritam_id INT NOT NULL,
	jigoshop_id INT NOT NULL
	) ENGINE = InnoDB;');
});
if(is_admin()){
	add_filter('plugin_action_links_' . plugin_basename(dirname(JIGOSHOP_RITAM_FORMS_DIR) . '/bootstrap.php'), function($links){
		$links[] = '<a href="https://www.jigoshop.com/documentation/jigoshop-ritamforms-integration/" target="_blank">Documentation</a>';
		$links[] = '<a href="https://www.jigoshop.com/support/" target="_blank">Support</a>';
		$links[] = '<a href="https://wordpress.org/support/view/plugin-reviews/jigoshop#postform" target="_blank">Rate Us</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">More plugins for Jigoshop</a>';
		
		return $links;
	});
}