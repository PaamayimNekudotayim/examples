jQuery(document).ready(function($){
	$('.get_product_data_from_ritam').on('click', function(){
		$(this).prop('disabled', true);
		var productID = $(this).closest('.jigoshop_variation').attr('rel');
		var sku = $('input[name="variations[' + productID + '][sku]"]').val();
		if(sku == '') {
			alert('SKU field is necessary to get variation data from Ritam, please fill it.');
			$(this).prop('disabled', false);
			return;
		}

		$.ajax({
			type: "POST",
			url: jigoshop_params.ajax_url,
			dataType: "json",
			data: {
				action: 'get_product_data_from_ritam',
				sku: sku
			},
			success: function(response){
				if(response != ''){
					$('input[name="variations[' + productID + '][stock]"]').val(response['stock']);
					$('input[name="variations[' + productID + '][regular_price]"]').val(response['regular_price']);
				} else {
					alert('There is no product with that SKU.');
				}
			}
		});

		$(this).prop('disabled', false);
	});
});
