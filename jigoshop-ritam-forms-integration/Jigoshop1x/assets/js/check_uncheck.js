jQuery(function($){
	$('#check_all').click(function(){
		$('.product').prop('checked', true);
	});
	$('#uncheck_all').click(function(){
		$('.product').prop('checked', false);
	});
	$('#revers_sel').click(function(){
		$(":checkbox[name='product[]']").each(function(){
			$(this).prop('checked', !$(this).prop('checked'));
		});
	});
	$('#import_submit').click(function(){
		var inCheck = new Array();
		$(":checkbox[name='product[]']").each(function(){
			if($(this).prop('checked')){
				inCheck.push($(this).val());
			}
		});
		$('#id_prod_box').val(inCheck.join('|'));
		$('#ritam_import').submit();
	});
});