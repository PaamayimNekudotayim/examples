<?php

if (!defined('WP_UNINSTALL_PLUGIN')) {
	exit();
}

delete_option('_ritam_api_link');
delete_option('_ritam_user');
delete_option('_ritam_secret_key');

global $wpdb;
$wpdb->query('DROP TABLE IF EXISTS '.$wpdb->prefix.'ritam_forms_relationships');
