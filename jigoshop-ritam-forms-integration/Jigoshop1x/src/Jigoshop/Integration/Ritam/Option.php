<?php
namespace Jigoshop\Integration\Ritam;

class Option
{
	protected $_user = '';
	protected $_secret_key = '';
	protected $_api_link = '';
	protected $_notice = '';

	function __construct()
	{
		$this->getConfig();
		jigoshop_add_style('ritam_css_handler', JIGOSHOP_RITAM_FORMS_URL.'/assets/css/ritam_forms_style.css');
		if (isset($_POST['user']) && isset($_POST['secret_key']) && isset($_POST['api_link'])) {
			$this->saveOption();
			$this->getConfig();
		}

		$this->viewForm();
	}

	protected function getConfig()
	{
		$this->_user = get_option('_ritam_user');
		$this->_secret_key = get_option('_ritam_secret_key');
		$this->_api_link = get_option('_ritam_api_link');
	}

	protected function viewForm()
	{
		include_once JIGOSHOP_RITAM_FORMS_DIR.'/templates/view_form_options.php';
	}

	protected function saveOption()
	{
		$user = trim($_POST['user']);
		$sk = trim($_POST['secret_key']);
		$al = trim($_POST['api_link']);
		update_option('_ritam_user', $user);
		update_option('_ritam_secret_key', $sk);
		update_option('_ritam_api_link', $al);
		$this->_notice = '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"><p>'.__('<strong>Good!</strong> Your settings have been saved correctly').'</p></div>';
	}
}

new Option();