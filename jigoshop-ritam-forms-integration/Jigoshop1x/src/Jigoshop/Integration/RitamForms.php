<?php
namespace Jigoshop\Integration;

class RitamForms
{
	protected $_user = '';
	protected $_secret_key = '';
	protected $_api_link = '';
	protected $_sql_table = '';
	protected $_db;
	protected $_log_to_notice = array();
	protected $_s_name = '';
	protected $_s_code = '';
	protected $_info_about = '';
	protected $_products = array();
	protected $_qty = array();
	protected $_engine_ready = false;
	protected $_added_prod_id_to_jigoshop = array();

	protected $_links_api = array(
		'product_list' => '/rest/api/v1/products/list',
		'qty' => '/rest/api/v1/products/instock',
		'price' => '/rest/api/v1/products/refreshprices',
		'organization' => '/rest/api/v1/workingunits/list',
		'prod_full_li' => '/rest/api/v1/stock/full/lis'
	);

	public function __construct()
	{
		global $wpdb;
		$this->_db = $wpdb;
		$this->getRitamOptions();
		$this->_sql_table = $this->_db->prefix.JIGOSHOP_RITAM_FORMS_TABLE_NAME;

		add_action('admin_menu', array($this, 'adminMenu'));
		add_action('current_screen', array($this, 'scripts'));

		add_action('jigoshop_variable_product_table_end', array($this, 'displaySynchronizeButton'));
		add_action('wp_ajax_get_product_data_from_ritam', array($this, 'getProductDataFromRitam'));
		add_action('wp_ajax_nopriv_get_product_data_from_ritam', array($this, 'getProductDataFromRitam'));
	}

	public function scripts()
	{
		if(is_admin()){
			global $current_screen;

			if($current_screen->base == 'toplevel_page_ritam-forms'){
				jigoshop_add_style('ritam_css_handler', JIGOSHOP_RITAM_FORMS_URL.'/assets/css/ritam_forms_style.css');
				jigoshop_add_script('ritam_forms_script', JIGOSHOP_RITAM_FORMS_URL.'/assets/js/check_uncheck.js', array('jquery'));
			}

			jigoshop_add_script('sync_with_ritam_forms_script', JIGOSHOP_RITAM_FORMS_URL.'/assets/js/sync_variation.js', array('jquery'));
		}
	}

	public function adminMenu()
	{
		add_menu_page(__('RitamForms - Import Product'), __('Import products from RitamForms'), 'manage_options', 'ritam-forms', array($this, 'start'));
		add_submenu_page('ritam-forms', __('RitamForms - Options'), __('Options'), 'manage_options', 'ritam-forms-option', array($this, 'showOption'));
	}

	public function showOption()
	{
		require_once(JIGOSHOP_RITAM_FORMS_DIR.'/src/Jigoshop/Integration/Ritam/Option.php');
	}

	public function displaySynchronizeButton() {
		echo '<tr><td colspan="7"><button type="button" class="get_product_data_from_ritam button">Sync with Ritam</button></td></tr>';
	}

	public function getProductDataFromRitam()
	{
		if(isset($_POST['sku']) && !empty($_POST['sku'])){
			$sku = $_POST['sku'];
			if ($this->_engine_ready) {
				$this->_products = $this->getSourceByType();
				$this->prepareQty();
			}

			$count_products = count($this->_products);
			for ($i = 0; $i < $count_products; $i++) {
				if ($sku == $this->_products[$i]->item_code) {
					 $productData = array(
						 'regular_price' => str_replace(',', '.', $this->_products[$i]->item_mpc),
						 'stock' => $this->_qty[$this->_products[$i]->item_id]
					 );

					echo json_encode($productData);
				}
			}
		}

		exit;
	}

	protected function getRitamOptions()
	{
		$this->_user = get_option('_ritam_user');
		$this->_secret_key = get_option('_ritam_secret_key');
		$this->_api_link = rtrim(get_option('_ritam_api_link'), '/');
		if (empty($this->_user) || empty($this->_secret_key) || empty($this->_api_link)) {
			$this->saveLog('empty_option', array());
		} else {
			$this->_engine_ready = true;
		}
	}

	public function start()
	{
		if ($this->_engine_ready) {
			$this->_products = $this->getSourceByType();

			$this->prepareQty();
		}
		$this->_added_prod_id_to_jigoshop = $this->getAddedProdIdToJigoshop();

		if (isset($_POST['add_all_product'])) {
			$this->addAllProductToJigoshop();
		} elseif (isset($_POST['add_product']) && isset($_POST['id_product'])) {
			$product = $this->getInfoProductById((int)$_POST['id_product']);
			$this->addProductToJigoshop($product);
		} elseif (isset($_POST['id_prod_box']) && isset($_POST['import_submit'])) {
			$_POST['id_prod_box'] = trim($_POST['id_prod_box']);
			$id_prod_box = explode('|', $_POST['id_prod_box']);

			for ($aa = 0; $aa < count($id_prod_box); $aa++) {
				$product = $this->getInfoProductById($id_prod_box[$aa]);
				$this->addProductToJigoshop($product);
			}
		} elseif (isset($_POST['update_qty']) && isset($_POST['id_product'])) {
			$this->updateQty($_POST['id_product']);
		} elseif (isset($_POST['update_all_stocks'])) {
			$this->updateAllQty();
		}

		if (isset($_POST['option_search'])) {
			$count_p = count($this->_products);
			$this->_s_name = preg_quote(esc_attr(trim($_POST['ritam_name'])), '#');
			$this->_s_code = preg_quote(esc_attr(trim($_POST['ritam_code'])), '#');
			for ($aa = 0; $aa < $count_p; $aa++) {
				if (!(preg_match("#.*".strtolower($this->_s_name).".*#", strtolower($this->_products[$aa]->item_name)) == 1 && preg_match("#.*".strtolower($this->_s_code).".*#", strtolower($this->_products[$aa]->item_code)) == 1)) {
					unset($this->_products[$aa]);
				}
			}
		}

		include_once JIGOSHOP_RITAM_FORMS_DIR.'/templates/view_products.php';
	}

	protected function addAllProductToJigoshop()
	{
		$count_products = count($this->_products);
		for ($aa = 0; $aa < $count_products; $aa++) {
			$this->addProductToJigoshop($this->_products[$aa]);
		}
	}

	protected function getAddedProdIdToJigoshop()
	{
		$ids = $this->_db->get_results('SELECT ritam_id, jigoshop_id FROM '.$this->_sql_table);
		$return = array();
//		ritam_id i jigoshop_id (post_id)
		foreach ($ids as $k => $v) {
			$return[$v->ritam_id] = $v->jigoshop_id;
		}

		return $return;
	}

	protected function generateSignature($type, $date)
	{
		$str = 'GET'."\r\n".$this->_links_api[$type]."\r\n".$date;

		return '&signature='.hash_hmac('sha1', $str, $this->_secret_key);
	}

	protected function getSourceByType($type = 'product_list')
	{
		$date = date("d.m.Y H:i:s");

		$api = $this->_api_link.$this->_links_api[$type].'?username='.$this->_user.'&date='.$date.$this->generateSignature($type, $date);
		$api = str_replace(' ', '%20', $api);

		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $api);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		$json = curl_exec($c);

		if (curl_errno($c)) {
			$this->ritamError(curl_error($c));
		}
		$httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
		curl_close($c);

		if ($httpcode >= 200 && $httpcode < 300) {
			return json_decode($json)->List;
		} else {
			$this->ritamError('httpcode: '.$httpcode.' '.__('Probably entered the wrong data on option menu, or REST API not responsible'));
		}

	}

	protected function getInfoProductById($id)
	{
		$count_products = count($this->_products);
		for ($aa = 0; $aa < $count_products; $aa++) {
			if ($id == $this->_products[$aa]->item_id) {
				return $this->_products[$aa];
			}
		}
	}

	protected function addProductToJigoshop($product)
	{
		if (isset($this->_added_prod_id_to_jigoshop[$product->item_id])) {
			$this->saveLog('not_import', array('name' => $product->item_name));

			return;
		}

		$category = array();
		$sub_category = array();

		if (empty($product->item_group)) {
			$category = $this->checkCreateCategory(0, '');
		} else {
			$category = $this->checkCreateCategory(0, $product->item_group);
		}
		if (!empty($product->item_subgroup)) {
			$sub_category = $this->checkCreateCategory($category['term_id'], $product->item_subgroup);
		}

		$data = date("Y-m-d H:i:s");
		$gmdata = gmdate("Y-m-d H:i:s");

		$post_id = wp_insert_post(array(
			'post_author' => get_current_user_id(),
			'post_date' => $data,
			'post_date_gmt' => $gmdata,
			'post_content' => $product->item_description,
			'post_title' => $product->item_name,
			'post_excerpt' => '',
			'post_status' => 'publish',
			'comment_status' => 'open',
			'ping_status' => get_option('default_ping_status'),
			'post_password' => '',
			'post_name' => $product->item_name,
			'to_ping' => '',
			'pinged' => '',
			'post_modified' => $data,
			'post_modified_gmt' => $gmdata,
			'post_content_filtered' => '',
			'post_parent' => 0,
			'guid' => '',
			'menu_order' => 0,
			'post_type' => 'product',
			'post_mime_type' => '',
			'comment_count' => 0,
		), $wp_error = false);

		$meta = array(
			'_edit_last' => '1',
			'_edit_lock' => time().':1',
			'regular_price' => str_replace(',', '.', $product->item_mpc),
			'sale_price' => '',
			'weight' => '',
			'length' => '',
			'width' => '',
			'height' => '',
			'tax_status' => 'taxable',
			'tax_classes' => ($product->item_tax_rate == 0) ? 'a:1:{i:0;s:9:"zero-rate";}' : 'a:1:{i:0;s:1:"*";}',
			'visibility' => 'visible',
			'featured' => '',
			'customizable' => 'no',
			'customized_length' => '',
			'product_attributes' => '',
			'manage_stock' => '1',
			'stock_status' => '-1',
			'stock' => $this->_qty[$product->item_id],
			'backorders' => 'no',
			'sale_price_dates_from' => '',
			'sale_price_dates_to' => '',
			'_product_addons' => 'a:0:{}',
			'sku' => $product->item_code,
		);

		foreach ($meta as $k => $v) {
			add_post_meta($post_id, $k, $v);
		}

		wp_set_object_terms($post_id, 2, 'product_type');

		$all_category = array((int)$category['term_id']);
		if (isset($sub_category['term_id'])) {
			$all_category[] = (int)$sub_category['term_id'];
		}

		wp_set_object_terms($post_id, $all_category, 'product_cat');

		$this->_db->query($this->_db->prepare('INSERT INTO '.$this->_sql_table.'(ritam_id, jigoshop_id)
			    VALUES (%d, %d )', array(
			$product->item_id,
			$post_id
		)));

		$this->_added_prod_id_to_jigoshop[$product->item_id] = $post_id;

		$this->saveLog('import', array('name' => $product->item_name, 'qty' => $this->_qty[$product->item_id]));
	}

	protected function saveLog($type, $param)
	{
		if ($type == 'import') {
			$this->_log_to_notice[] = __('Import item:').' <strong>'.$param['name'].'</strong> '.__('with quantity:').' <strong>'.$param['qty'].'</strong>';
		} elseif ($type == 'qty') {
			$this->_log_to_notice[] = __('Update stock for item :').' <strong>'.$param['name'].'</strong> '.__('with quantity:').' <strong>'.$param['qty'].'</strong>';
		} elseif ($type == 'not_import') {
			$this->_log_to_notice[] = __('Item:').' <strong>'.$param['name'].'</strong> '.__('is not imported because it already exists in the Jigoshop');
		} elseif ($type == 'empty_option') {
			$this->_log_to_notice[] = __('You need in first set your option on option page');
		}
	}

	protected function showLog()
	{
		if (count($this->_log_to_notice) > 0) {
			return '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"><p>'.__('<strong>Information!</strong>').'<br/>'.join('<br/>', $this->_log_to_notice).'</p></div>';

		}
	}

	protected function ritamError($er)
	{
		die(__('Error no: ').$er);
	}

	protected function checkCreateCategory($parent_id, $cat_name)
	{
		if (empty($cat_name)) {
			$cat_name = __('Uncategorized');
		}
		if (!($category = term_exists($cat_name, 'product_cat'))) {
			$category = wp_insert_term($cat_name, 'product_cat', array(
				'description' => $cat_name,
				'parent' => $parent_id
			));
			if (isset($category->errors)) {
				$this->ritamError(10);
			}
		}

		return $category;
	}

	protected function prepareQty()
	{
		$this->getSourceByType('qty');
		if ($this->_engine_ready) {
			foreach ($this->getSourceByType('qty') as $k => $v) {
				$this->_qty[$v->item_id] = $v->item_qty;
			}
		}
	}

	protected function updateQty($ritam_id)
	{
		$ritam_id = (int)$ritam_id;
		$post_product_id = $this->_db->get_var($this->_db->prepare("SELECT jigoshop_id
				FROM $this->_sql_table
				WHERE ritam_id = %d
				", $ritam_id));

		update_post_meta($post_product_id, 'stock', $this->_qty[$ritam_id]);

		$this->saveLog('qty', array('name' => $this->_products[$ritam_id]->item_name, 'qty' => $this->_qty[$ritam_id]));
	}

	protected function updateAllQty()
	{
		$related_ids = $this->_db->get_results('SELECT ritam_id, jigoshop_id FROM '.$this->_sql_table);

		foreach ($related_ids as $k => $v) {
			update_post_meta($v->jigoshop_id, 'stock', $this->_qty[$v->ritam_id]);
			$this->saveLog('qty', array('name' => $this->_products[$v->ritam_id]->item_name, 'qty' => $this->_qty[$v->ritam_id]));
		}
	}
}

new RitamForms();
