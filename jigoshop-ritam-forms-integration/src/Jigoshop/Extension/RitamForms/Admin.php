<?php
namespace Jigoshop\Extension\RitamForms;

/**
 * Class Admin
 */
class Admin
{
	public function __construct()
	{
		add_filter('plugin_action_links_' . plugin_basename(JIGOSHOP_RITAM_FORMS_DIR . '/bootstrap.php'), array($this, 'actionLinks'));
		//DO NOT FORGET TO DELETE THIS!
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s -This plugin is not Jigoshop 2.0 compatible at the moment, please wait for it\'s next update ',
				'jigoshop_ritam_forms'), JIGOSHOP_RITAM_FORMS_NAME, JIGOSHOP_RITAM_FORMS_NAME);
			echo '</p></div>';
		});
	}
	
	/**
	 * @param array $links
	 * @return array
	 */
	public function actionLinks($links)
	{

		$links[] = '<a href="https://wordpress.org/support/plugin/jigoshop-ecommerce/#new-post" target="_blank">Support</a>';
		$links[] = '<a href="https://wordpress.org/support/plugin/jigoshop-ecommerce/reviews/#new-post" target="_blank">Rate Us</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">More plugins for Jigoshop</a>';
		
		return $links;
	}
}

new Admin;