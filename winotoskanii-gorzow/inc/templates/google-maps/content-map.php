<section class="location-wtg clear container" style="border-top: 1px solid #eee; padding-top:25px;">
	<div class="container-grid">
		<h2 class="primary-text text-center"><?php echo 'Odwiedź nas'; ?></h2>
		<div class="columns2-4" style="width:100%">
			<div id="map">
				<?php _e('Map', '') ?>
			</div>
		</div>
	</div>
</section>