<?php get_header(); ?>
	<section class="ingredients">
		<div class="container">
			<div class="container-grid">
				<h3><?php _e('Wygląda na to że tutaj nie ma tego czego szukasz :(') ?></h3>
				<p><a href="<?php echo esc_url(home_url()) ?>"><?php _e('Wruć na głuwną stronę') ?></a></p>
			</div>
		</div>
	</section>
<?php get_footer(); ?>