<?php get_header(); ?>


    <div id="slider">
        <ul class="bxslider">
			<?php
			$banArgs = array(
				'posts_per_page' => 4,
				'post_type' => 'banery',
				'category_name' => 'banery',
				'orderby' => 'DESC',
			);
			?>
			<?php $banners = new WP_Query($banArgs); ?>
			<?php while ($banners->have_posts()) : ?>
				<?php $banners->the_post(); ?>
                <li><?php the_post_thumbnail('slider'); ?></li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
        </ul>
    </div>

    <section class="ingredients">
        <div class="container">
            <div class="container-grid">
				<?php while (have_posts()): the_post(); ?>
                    <!--REMOVE THIS HARDCODED CSS AFTER-->
                    <div class="columns0" style="border-bottom: 1px solid #eee; padding-bottom: 20px; width:100%">
                        <h3><?php the_field('aboutus'); ?></h3>
						<?php the_field('about_us_text'); ?>
						<?php $url = get_page_by_title('O Nas'); ?>
                        <a href="<?php echo get_permalink($url->ID); ?>"
                           class="button primary"
                           style="color:#000;"><?php _e('Więcej', WTGFunctions::TEXT_DOMAIN) ?></a>
                    </div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
    <div class="main-content container">
        <main class="container-grid content-text">
            <h2 class="primary-text text-center"><?php _e('', WTGFunctions::TEXT_DOMAIN) ?></h2>
            <div id="sgb">
                <ul class="bxslider">
					<?php
					$args = [
						'posts_per_page' => 4,
						'post_type' => 'strona_glowna_banery',
						'category_name' => 'sgb',
						'orderby' => 'rand'
					];
					?>
					<?php $frontPageGal = new WP_Query($args); ?>
					
					<?php while ($frontPageGal->have_posts()) : ?>
						<?php $frontPageGal->the_post(); ?>
                        <li class="sgb-cont"><?php the_post_thumbnail('wtg-front'); ?>
                            <ul class="wiecej">
                                <li class="1"><?php the_field('opis') ?></li>
                                <li class="1"><a href="<?php the_field('link') ?>"><?php the_field('nazwa') ?></a></li>
                            </ul>
                        </li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
                </ul>
            </div>
        </main>
    </div>
<?php get_template_part('inc/templates/google-maps/content', 'map'); ?>
    <div class="clear"></div>
<?php get_footer(); ?>