<?php
	/**
	 * Template Name: About Us
	 */
?>
<?php get_header(); ?>
<section class="ingredients">
	<div class="container">
		<div class="container-grid">
			<?php while (have_posts()): the_post(); ?>
				<!--REMOVE THIS HARDCODED CSS AFTER-->
				<div class="columns0" style="border-bottom: 1px solid #eee; padding-bottom: 20px; width:100%">
					<h3><?php the_field('about_us'); ?></h3>
					<?php the_field('about_text'); ?>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>
<?php get_template_part('inc/templates/google-maps/content', 'map'); ?>
<div class="clear"></div>
<?php get_footer(); ?>
