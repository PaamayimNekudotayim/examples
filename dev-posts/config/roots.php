<?php
$siteUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http' ). "://" . $_SERVER['SERVER_NAME'];
$currentUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http' ). "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if (!defined('ROOT_DIR')) {
	define('ROOT_DIR', str_replace('\\', '/', dirname(__DIR__)));
}

if (!defined('APP_DIR')) {
	define('APP_DIR', ROOT_DIR . '/src/app');
}

if (!defined('APP_VIEW_DIR')) {
	define('APP_VIEW_DIR', ROOT_DIR . '/public/templates');
}

if (!defined('DEFAULT_TEMPLATE')) {
	define('DEFAULT_TEMPLATE', 'index');
}

if (!defined('HOME_URL')) {
	define('HOME_URL', $siteUrl);
}

if (!defined('APP_ERROR_REPORTING')) {
	define('APP_ERROR_REPORTING', error_reporting(-1));
}

if (!defined('LOG_DIR')) {
	define('LOG_DIR', ROOT_DIR . '/tmp/log');
}

if (!defined('APP_DEBUG')) {
	define('APP_DEBUG', true);
}

if (!defined('TEMPLATE_URL')) {
	define('TEMPLATE_URL', $siteUrl . '/public/templates');
}

if (!defined('CURRENT_URL')) {
	define('CURRENT_URL', $currentUrl);
}

if (!defined('CACHE_DIR')) {
	define('CACHE_DIR', ROOT_DIR . '/tmp/cache');
}

if (!defined('ADMIN_URL')) {
	define('ADMIN_URL', $siteUrl . '/admin');
}

