$ = jQuery.noConflict();
$(document).ready(function(){
    $(document).on('click', '#admin-delete-user', function (){
        var authorId = $(this).data('id');
        swal({
            title: deletePostTitle,
            text: deletePostText,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmText,
            showLoaderOnConfirm: false,
            preConfirm: function() {
                $.ajax({
                    url: deletePost,
                    type: 'POST',
                    data: {'delete_author': authorId, 'redirect': postRedirect},
                    success: function (res) {
                       result =JSON.parse(res);
                       //console.log(result);
                       $("[data-id='"+ authorId + "']").parent().parent().remove();
                       if (result.status == 'success') {
                            swal({
                                title: "Success",
                                text: "Author was deleted",
                                type: 'success',
                                showConfirmButton: false,
                                showCancelButton: false

                            });
                           window.setTimeout(function (){
                                window.location.href = result.redirect;
                            }, 1000);
                        }
                    },
                    error: function (){
                        swal({
                            title: "Error",
                            text: "Author was not deleted!",
                            type: 'error',
                            showConfirmButton: false,
                            showCancelButton: false,
                        });
                    }
                })

            },
            allowOutsideClick: false
        });//
    });

    $(document).on('click', '#admin-edit-user', function (){
        /*var authorId = $(this).data('id');
        $.ajax({
            url: 'admin/user/edit',
            type: 'POST',
            data: {
                'authorId' : authorId
            },
            success: function (res) {
                console.log(res);
            }
        })*/
    });
});
