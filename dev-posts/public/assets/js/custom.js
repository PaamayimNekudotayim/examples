$ = jQuery.noConflict();
$(document).ready(function(){
    // News Delete
    $(document).on('click', '.delete_post', function (){
        var postId = $(this).data('id');
        swal({
            title: deletePostTitle,
            text: deletePostText,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmText,
            showLoaderOnConfirm: false,
            preConfirm: function() {
                    $.ajax({
                        url: deletePost,
                        type: 'POST',
                        data: {'delete_post': postId, 'redirect': postRedirect},
                        success: function (res) {
                            $("[data-id='"+ postId + "']").parent().parent().parent().remove();
                           if (res.status == 'success') {
                               swal({
                                   title: "Success",
                                   text: "Post deleted!",
                                   type: 'success',
                                   showConfirmButton: false,
                                   showCancelButton: false

                               });
                               window.setTimeout(function (){
                                     window.location.href = res.redirect;
                                   }, 1000);
                            } else {
                                swal({
                                    title: "Error",
                                    text: "Something went wrong",
                                    type: 'error',
                                    showConfirmButton: false
                                });
                            }
                        },
                        error: function (){
                            swal({
                                title: "Error",
                                text: "Post was not deleted!",
                                type: 'error',
                                showConfirmButton: false,
                                showCancelButton: false,
                            });
                        }
                    })

            },
            allowOutsideClick: false
        });//
   });
});

$(document).ready(function(){
    setTimeout(function() {
        $('#alert').fadeOut();
    }, 4000);
});