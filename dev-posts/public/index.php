<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/lib/routes.php';
require __DIR__ . '/../src/lib/functions.php';
require __DIR__ . '/../config/roots.php';


/**
 * @throws Exception
 */
new \App\Run();