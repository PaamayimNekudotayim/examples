<?php
    /**
     * @var $errorNum
     * @var $errorMessage
     * @var $errorFile
     * @var $errorLine
     */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/assets/css/custom.css"/>
	<title>Dev</title>
</head>
<body>
<div class="container">
	<div class="copy-container center-xy">
		<p><?php echo $errorNum; ?></p>
		<p>Message: <?php echo $errorMessage; ?></p>
		<p>File: <?php echo $errorFile; ?></p>
		<p>Line: <?php echo $errorLine; ?></p>
        <?php
        echo '<pre>Code:&nbsp;';
        var_dump($errorCode);
        echo '</pre>';
        ?>
	</div>
</div>

</body>
</html>