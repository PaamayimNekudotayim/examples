<?php
/**
 * @var string $headerAttributes
 * @var string $template
 * @var string $theHeader
 */
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <base href="/"/>
		<?php $theHeader(); ?>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/assets/dist/css/skins/_all-skins.min.css">
        <link type="text/css" rel="stylesheet" href="/assets/css/sweetalert2.min.css">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <span class="logo-lg logo"><b>Admin</b></span>
            <nav class="navbar navbar-static-top">
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs"><?php htmlout('Welcome: ' . $_SESSION['user']['first_name']) ?></span>
                            </a>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="/">
                                <?php htmlout('Go to posts'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    <?php htmlout('Data Tables') ?>
                    <small><?php htmlout('Preview') ?></small>
                </h1>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
						<?php if (isset($_SESSION['error'])): ?>
                            <div class="alert alert-danger" id="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <p class="text-center"><strong><?php htmlout($_SESSION['error']); ?></strong></p>
								<?php unset($_SESSION['error']); ?>
                            </div>
						<?php endif; ?>
						<?php if (isset($_SESSION['success'])): ?>
                            <div class="alert alert-success alert-dismissible" id="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <p class="text-center"><strong><?php htmlout($_SESSION['success']); ?></strong></p>
								<?php unset($_SESSION['success']); ?>
                            </div>
						<?php endif; ?>
                    </div>
                </div>
				<?php template($template); ?>
            </section>
        </div>
        <div class="control-sidebar-bg"></div>
    </div>
    <script src="/assets/js/jquery-1.11.0.min.js"></script>
    <script src="/assets/js/bootstrap.js"></script>
    <script src="/assets/js/sweetalert2.min.js"></script>
    <script src="/assets/js/admin/custom-admin.js"></script>
    <script type="text/javascript">
        deletePost = '/admin/user/delete';
        deletePostTitle = 'Are you sure?';
        deletePostText = 'All posts related to this author will be deleted as well!';
        confirmText =  'Delete Author';
        postRedirect = "<?php echo CURRENT_URL; ?>"
    </script>
    </body>
    </html>
<?php exit(); ?>