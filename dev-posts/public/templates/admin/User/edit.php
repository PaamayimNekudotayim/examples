<?php
/**
 * @var $data array
 */
?>

<div class="box box-primary">
	<?php if (isset($data['editUser'])) : ?>
            <div class="box-header with-border">
              <h3 class="box-title"><?php htmlout('Edit User Data') ?></h3>
	            <p><b>User Role (Type): <?php echo  ($data['editUser']->type == '2' ? 'User' : 'Admin');  ?></b></p>
            </div>
	<form method="post" action="admin/user/update" role="form">
		<div class="box-body">
		
		<?php foreach ($data['editUser'] as $user => $value) : ?>
				<div class="form-group">
					<label for="<?php htmlout($user) ?>">
						<?php htmlout(ucwords(str_replace('_', ' ', $user))); ?>
					</label>
					<input type="text" class="form-control" name="<?php htmlout($user) ?>"
					       id="<?php htmlout($user) ?>" value="<?php htmlout($value); ?>">
				</div>
		<?php endforeach; ?>
		
		
		
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</form>
	<?php else : ?>
	Nothing to view :(
	<?php endif; ?>
</div>
