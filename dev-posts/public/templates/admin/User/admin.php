<?php
/**
 * @var $data array
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php htmlout('All Users'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th><?php htmlout('First Name'); ?></th>
                        <th><?php htmlout('Last Name'); ?></th>
                        <th><?php htmlout('Email'); ?></th>
                        <th><?php htmlout('Created At'); ?></th>
                        <th><?php htmlout('Type'); ?></th>
                        <th><?php htmlout('Actions'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($data['allUsers'])) : ?>
	                    <?php foreach ($data['allUsers'] as $user) : ?>
                            <tr>
                                <td><strong><?php htmlout($user->first_name); ?></strong></td>
                                <td><strong><?php htmlout($user->last_name); ?></strong></td>
                                <td><a href="mailto:<?php htmlout($user->email); ?>"><?php htmlout($user->email); ?></td>
                                <td><?php htmlout($user->created_at); ?></td>
                                <td><?php htmlout(($user->type == '1' ? 'Admin' : 'User')); ?></td>
                                <td><a id="admin-delete-user" data-id="<?php htmlout($user->id) ?>"
                                       href="javascript:void(0)">
                                                                                    <?php htmlout('Delete') ?></a> |
                                    <a id="admin-edit-user" href="admin/user/edit?userId=<?php htmlout($user->id) ?>""><?php htmlout('Edit') ?></a></td>
                            </tr>
	                    <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>