<form class="form-horizontal" action="user/signup" method="post" data-toggle="validator" role="form" >
	<div class="row form">
		<h1 class="text-center"><span><?php htmlout('Welcome') ?></span></h1>
		<p class="text-center"><?php htmlout('Please register to view the page :)') ?></p>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="text" class="form-control" id="name" name="first_name" placeholder="First Name*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="text" class="form-control" id="lastname" name="last_name" placeholder="Last Name*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="text" class="form-control" id="login" name="login" placeholder="Login*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="email" class="form-control" id="email" name="email" placeholder="Email*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
        <div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
                <label for="birthdate" style="color:#fff;">Birth Date*</label>
				<input type="date" class="form-control" id="birthdate" name="birth_date" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<label for="gender" style="color:#fff;">Gender</label>
				<select class="form-control" name="gender" id="gender">
					<option value="male">Male</option>
					<option value="female">Female</option>
				</select>
			</div>
		</div>
		
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="password" class="form-control" id="password"
				       name="password" placeholder="Password*" data-error="Password should be at least 6 characters long"
				       data-minlength="6" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
			</div>
		</div>
		
		<div class="form-group has-feedback">
			<div class="col-sm-offset-2 col-sm-8 col-sm-offset-2">
				<button type="submit" class="btn btn-default"><?php htmlout('Register') ?></button>
			</div>
		</div>
	</div>
</form>
<?php if(isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>