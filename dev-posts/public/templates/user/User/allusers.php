<?php
	/**
	 * @var $data array
	 */
?>
<div class="container">
	<h1 class="text-center"><?php htmlout('All Registered Users') ?></h1>
	<p class="text-center"><a href="/" class="btn btn-default navbar-btn" role="button">
			<?php htmlout('Go to posts') ?></a></p>
	<table class="table">
		<thead>
		<tr>
			<th><?php htmlout('First Name'); ?></th>
			<th><?php htmlout('Last Name'); ?></th>
			<th><?php htmlout('Email'); ?></th>
		</tr>
		</thead>
		<?php if (isset($data['allUsers'])) : ?>
			<?php foreach ($data['allUsers'] as $user) : ?>
				<tr>
					<td><strong><?php htmlout($user->first_name); ?></strong></td>
					<td><strong><?php htmlout($user->last_name); ?></strong></td>
					<td><a href="mailto:<?php htmlout($user->email); ?>"><?php htmlout($user->email); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</table>
</div>