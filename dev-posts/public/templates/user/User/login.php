<form class="form-horizontal" action="user/login" method="post" data-toggle="validator" role="form">
	<div class="row form">
		<h1 class="text-center"><span><?php htmlout('Welcome') ?></span></h1>
		<p class="text-center"><?php htmlout('Please login to view the page :)') ?></p>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="text" class="form-control" id="login" name="login" placeholder="Login*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="password" class="form-control" id="password" name="password" placeholder="Password*" required />
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8 col-sm-offset-2">
				<input type="submit" class="btn btn-default" value="<?php htmlout('Login') ?>" />
			</div>
		</div>
	</div>
</form>