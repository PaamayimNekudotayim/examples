<?php
/**
 * @var $data array
 */
?>
<form class="form-horizontal" action="user/update" method="post" data-toggle="validator" role="form" >
	<div class="row form">
		<h1 class="text-center"><span><?php htmlout('Welcome') ?></span></h1>
		<p class="text-center"><?php htmlout('Here you cn update your data') ?></p>
        <p class="text-center"><a href="/"><?php htmlout('Go to posts') ?></a></p>
		<?php if (isset($data['user'])) : ?>
        <?php if (!empty($data['user'])) : ?>
            <?php foreach ($data['user'] as $name => $value) :?>
                    <?php if ($name == 'id' || $name == 'type') : ?>
                        <?php continue; ?>
                    <?php endif; ?>
                    <div class="form-group has-feedback">
                        <div class="col-sm-8 col-sm-offset-2">
                            <label class="is-active settings" for="<?php htmlout($name) ?>"><?php htmlout(ucwords(str_replace('_', ' ', $name)));  ?></label>
                            <input type="text" class="form-control" id="<?php htmlout($name) ?>" name="<?php htmlout($name) ?>"
                                   placeholder="<?php htmlout(ucwords(str_replace('_', ' ', $name))); ?>"
                                   value="<?php htmlout($value); ?>" required />
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php endif; ?>
        <input type="hidden" name="id" value="<?php htmlout($data['user']->id) ?>" />
		<div class="form-group has-feedback">
			<div class="col-sm-offset-2 col-sm-8 col-sm-offset-2">
				<button type="submit" class="btn btn-default"><?php htmlout('Update Data') ?></button>
			</div>
		</div>
	</div>
</form>