<?php
/**
 * @var array $data
 */
?>
<div class="container">
	<div class="row">
		<h1 class="text-center"><?php htmlout('All Inactive Posts') ?></h1>
        <p class="text-center"><a href="/" class="btn btn-primary" role="button"><?php htmlout(' Go to posts') ?></a></p>
		<?php if (isset($data['inactive'])) : ?>
        <?php if (!empty($data['inactive'])) : ?>
	        <?php if (is_array($data['inactive'])) :?>
		        <?php foreach ($data['inactive'] as $news) : ?>
			        <?php if ($_SESSION['user']['id'] !== $news->author_id) : continue; ?>
			        <?php endif; ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3 class="modal-title text-center"><strong><?php echo htmlspecialchars_decode($news->name) ?></strong></h3>
                                <p><?php echo htmlspecialchars_decode($news->description, ENT_NOQUOTES) ?></p>
						        <?php if (isset($_SESSION['user'])) : ?>
							        <?php if (!empty($_SESSION['user']['id'])) : ?>
								        <?php if ($news->author_id == $_SESSION['user']['id']) :  ?>
                                            <p>
                                                <a href="news/edit/?post_id=<?php htmlout($news->id) ?>" class="btn btn-primary" role="button">
                                                    <?php htmlout('Edit') ?></a>
                                                <a href="javascript:void(0)"
                                                   class="btn btn-default delete_post" id="delete-post" data-id="<?php htmlout($news->id) ?>"
                                                   role="button"><?php htmlout('Delete') ?></a>
                                            </p>
								        <?php endif; ?>
							        <?php endif; ?>
						        <?php endif;?>
                            </div>
                        </div>
                    </div>
		        <?php endforeach; ?>
        <?php endif; ?>
			<?php else: ?>
                <h1 class="text-center"><?php htmlout('No posts found :(') ?></h1>
			<?php endif; ?>
		
		<?php endif; ?>
	
	</div>
</div>