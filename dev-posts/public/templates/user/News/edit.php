<form class="form-horizontal email" method="post" action="news/update" role="form">
	<div class="row">
		<h1 class="text-center edit-post">Edit Post</h1>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="text" class="form-control single-post"
				       id="name" name="name" value="<?php echo $data['singlePost']->name ?>" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<textarea class="form-control" id="message" name="description" rows="5">
					<?php echo $data['singlePost']->description ?>
				</textarea>
			</div>
		</div>
        <div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
                <label class="is-active" for="is-active"><?php htmlout('Is Active?') ?></label>
                <select name="is_active" id="is-active">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8 col-sm-offset-2">
				<input type="hidden" name="id" class="btn btn-default" value="<?php echo $_GET['post_id'] ?>" />
				<input type="submit" class="btn btn-default" value="Update" />
			</div>
		</div>
	</div>
</form>