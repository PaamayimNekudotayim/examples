<?php
header('Content-type: application/json; charset=UTF-8');

if(isset($_POST['delete_post'])) {
	$res = [
		'status' => 'success',
		'message' => 'Post was deleted',
		'redirect' => (isset($_POST['redirect']) ? $_POST['redirect'] : '/'),
	];
	
	echo json_encode($res);
}
exit();