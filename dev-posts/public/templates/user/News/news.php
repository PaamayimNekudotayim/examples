<?php
/**
 * @var array $data
 */
?>
<?php if (isset($_SESSION['user'])) : ?>
	<?php if ($data['user']->login !== $_SESSION['user']['login']) : ?>
		<?php unset($_SESSION['user']);
		redirect('/'); ?>
	<?php endif; ?>
<?php endif; ?>
    <div class="row">
<?php if (isset($data['news'])) : ?>
	<?php if (is_array($data['news'])) : ?>
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".user">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse  user">
				<?php if (isset($_SESSION['user'])) : ?>
					<?php if (!empty($_SESSION['user'])) : ?>
                        <ul class="nav navbar-nav">
                            <li>
                                <h3 class="text-center"><?php htmlout('Welcome: ' . $_SESSION['user']['first_name']) ?></h3>
                            </li>
							<?php $authorId = []; ?>
							<?php foreach ($data['news'] as $post) : ?>
								<?php $authorId[] = $post->author_id; ?>
							<?php endforeach; ?>
							<?php if (in_array($_SESSION['user']['id'], $authorId)) : ?>
                                <li><a href="news/add" class="btn btn-default navbar-btn"
                                       role="button"><?php htmlout('Add Post') ?></a></li>
                                <li><a href="news/inactive" class="btn btn-default navbar-btn" role="button">
										<?php htmlout('See inactive posts') ?> ( <?php echo $data['count'][0]->posts ?>
                                        )</a></li>
							<?php else : ?>
                                <li><a href="news/add" class="btn btn-default navbar-btn"
                                       role="button"><?php htmlout('Add your first Post') ?></a></li>
							<?php endif; ?>
                            <li><a class="btn btn-default navbar-btn"
                                   href="user/settings"><?php htmlout('Settings') ?></a></li>
                            <li><a class="btn btn-default navbar-btn"
                                   href="admin"><?php htmlout('Admin Login') ?></a></li>
                            <li><a class="btn btn-default navbar-btn"
                                   href="user/allusers"><?php htmlout('View All Registered Users') ?></a></li>
                            <li><a class="btn btn-default navbar-btn" href="user/logout"><?php htmlout('Logout') ?></a>
                            </li>
                        </ul>
					<?php endif; ?>
				<?php else : ?>
                    <h3 class="text-center"><?php htmlout('To edit post or add new please login or register') ?></h3>
                    <ul class="nav navbar-nav">
                        <li><a class="btn btn-default navbar-btn" href="user/login"><?php htmlout('Login') ?></a></li>
                        <li><a class="btn btn-default navbar-btn" href="user/signup"><?php htmlout('Sign Up') ?></a>
                        </li>
                    </ul>
				<?php endif; ?>

            </div>
        </nav>
        <h1 class="text-center"><?php htmlout('All Posts') ?></h1>
		<?php if (!empty($data['news'])) : ?>
			<?php foreach ($data['news'] as $news) : ?>
				<?php if ($news->active_news == 'no') : continue; ?><?php endif; ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="modal-title text-center">
                                <strong><?php echo htmlspecialchars_decode($news->name) ?></strong></h3>
                            <p><?php echo htmlspecialchars_decode($news->description, ENT_NOQUOTES) ?></p>
							<?php if (isset($_SESSION['user'])) : ?>
								<?php if (!empty($_SESSION['user']['id'])) : ?>
									<?php if ($news->author_id == $_SESSION['user']['id']) : ?>
                                        <p>
                                            <a href="news/edit/?post_id=<?php htmlout($news->id) ?>"
                                               class="btn btn-primary" role="button"><?php htmlout('Edit Post') ?></a>
                                            <a href="javascript:void(0)"
                                               class="btn btn-default delete_post" id="delete-post"
                                               data-id="<?php htmlout($news->id) ?>"
                                               role="button"><?php htmlout('Delete'); ?></a>
                                            <span data-id="<?php htmlout($news->id) ?>" class="glyphicon glyphicon-remove delete_post text-danger del-item" aria-hidden="true"></span>
                                        </p>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>
                            <p><strong><?php htmlout('Date: ') ?><span
                                            class="date"><?php htmlout($news->news_date) ?></span></strong></p>
                            <p>
                                <strong><?php htmlout('Author: ') ?><?php echo htmlspecialchars_decode($news->first_name . ' ' . $news->last_name, ENT_NOQUOTES) ?></strong>
                            </p>

                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
		<?php else : ?>
            </div>
            <h1 class="text-center"><?php htmlout('Dummy Post') ?></h1>
            <div class="row">
                <div class="col-sm-4 col-md-12">
                    <div class="thumbnail">
                        <div class="caption"
                        ">
                        <h2 class="text-center"><?php htmlout($data['dummyText']->title) ?></h2>
                        <p><?php htmlout(htmlspecialchars_decode($data['dummyText']->content)) ?></p>
                    </div>
                </div>
            </div>
            
		<?php endif; ?>
	<?php endif; ?>
<?php else: ?>
    no posts :(
<?php endif; ?>