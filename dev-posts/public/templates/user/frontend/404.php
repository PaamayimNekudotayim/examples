<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/assets/css/custom.css"/>
	<title>404</title>
</head>
<body>

<div class="container">
	<div class="copy-container center-xy">
		<h1 class="text-center"><?php htmlout('The requested page not found :(') ?></h1>
	</div>
</div>
</body>
</html>