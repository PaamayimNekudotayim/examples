<?php
/**
 * @var \PDOException $e
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.css"/>
	<link type="text/css" rel="stylesheet" href="/assets/css/custom.css"/>
</head>
<body>
<div class="container">
	
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">There was a problem to connect database, please check config.php file</h3>
			<p>Message: <strong><?php echo $e->getMessage(); ?></strong></p>
			<p>File: <strong><?php echo $e->getFile(); ?></strong></p>
			<p>Line: <strong><?php echo $e->getLine(); ?></strong></p>
			<p>Code: <strong><?php echo $e->getCode(); ?></strong></p>
			<p>Trace: <strong><?php echo '<pre>';
					var_dump($e->getTrace()[0]);
					echo '</pre>'; ?></strong></p>
		</div>
	</div>
</div>
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<script src="/assets/js/bootstrap.js"></script>
<script src="/assets/js/validator.js"></script>
</body>
</html>