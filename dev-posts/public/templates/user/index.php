<?php
/**
 * @var string $headerAttributes
 * @var string $template
 * @var string $theHeader
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/"/>
	<?php $theHeader(); ?>
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="/assets/css/custom.css"/>
    <link type="text/css" rel="stylesheet" href="/assets/css/sweetalert2.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<?php if (isset($_SESSION['error'])): ?>
                <div class="alert alert-danger" id="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-center"><strong><?php htmlout($_SESSION['error']);?></strong></p>
                    <?php unset($_SESSION['error']); ?>
                </div>
			<?php endif; ?>
			<?php if (isset($_SESSION['success'])): ?>
                <div class="alert alert-success alert-dismissible" id="alert" >
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-center"><strong><?php htmlout($_SESSION['success']);?></strong></p>
                    <?php unset($_SESSION['success']);  ?>
                </div>
			<?php endif; ?>
        </div>
    </div>
	<?php template($template); ?>
</div>
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<script src="/assets/js/bootstrap.js"></script>
<script src="/assets/js/sweetalert2.min.js"></script>
<script src="/assets/js/validator.js"></script>
<script type="text/javascript">
    deletePost = '/news/delete';
    deletePostTitle = 'Are you sure?';
    deletePostText = 'You will delete this Post permanently!';
    confirmText =  'Delete Post';
    postRedirect = "<?php echo CURRENT_URL; ?>"
</script>
<script src="/assets/js/custom.js"></script>
</body>
</html>
