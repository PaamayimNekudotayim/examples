<?php

namespace App;


use App\singleton\Singleton;

class SQL
{
	use Singleton;
	
	/**
	 * @return string
	 */
	public function getAllNews()
	{
		$sql = "SELECT news.id, news.name, news.description, news.is_active AS active_news, news.news_date, news.created_at AS
				news_create_date, news.updated_at AS news_up_date, author_id,
				a.first_name, a.last_name, a.email, a.gender, a.type AS active_author, a.created_at AS user_created_at, a.updated_at
				AS user_updated_at
				FROM news INNER JOIN author a ON author_id = a.id
				ORDER BY news.created_at DESC";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function selectAuthors()
	{
		$sql = "SELECT * FROM author";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function selectRegisteredUsers()
	{
		$sql = "SELECT id, first_name, last_name, email, gender, type, created_at FROM author ORDER BY created_at DESC";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function selectAuthorToEdit()
	{
		$sql = "SELECT id, first_name, last_name, email, type, login FROM author WHERE id=:id ORDER BY created_at DESC";
		
		return $sql;
	}
	
	/**
	 * @param $type
	 * @return string
	 */
	public function updateAuthor($type = '2')
	{
		if ($type == '1') {
			$sql = "UPDATE author SET first_name=:first_name, last_name=:last_name, email=:email, login=:login, type=:type,
                					updated_at = CURRENT_TIMESTAMP() WHERE id=:id";
		} else {
			$sql = "UPDATE author SET first_name=:first_name, last_name=:last_name, email=:email, login=:login,
                					updated_at = CURRENT_TIMESTAMP() WHERE id=:id";
		}
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function checkLoggedInUser()
	{
		$sql = 'SELECT * FROM author WHERE email=:email AND login=:login LIMIT 1';
		
		return $sql;
	}
	
	/**
	 * @param $type
	 * @return string
	 */
	public function getLoggedInUser($type = false)
	{
		if (true == $type) {
			$sql = "SELECT * FROM author WHERE login=:login AND type='1' LIMIT 1";
		} else {
			$sql = 'SELECT * FROM author WHERE login=:login LIMIT 1';
		}
		
		return $sql;
	}
	
	/**
	 * @param $table
	 * @return string
	 */
	public function insertUser($table)
	{
		$sql = "INSERT INTO {$table} SET first_name=:first_name, last_name=:last_name, login=:login,
                            email=:email, birth_date=:birth_date, gender=:gender, password=:password, type = '2', created_at = CURRENT_TIMESTAMP(),
                            updated_at = CURRENT_TIMESTAMP()";
		
		return $sql;
	}
	
	/**
	 * @return array
	 */
	public function deleteUser()
	{
		$sql = [
			"SELECT id FROM news WHERE author_id =:id", "DELETE FROM news WHERE author_id =:id",
			"DELETE FROM author WHERE id = :id",
		];
		
		return $sql;
	}
	
	/**
	 * @param $table
	 * @return string
	 */
	public function insertNews($table)
	{
		$sql = "INSERT INTO {$table} SET name=:name, description=:description, is_active=:is_active,
                            news_date = CURDATE(), created_at = CURRENT_TIMESTAMP(), updated_at = CURRENT_TIMESTAMP(),
                            																		author_id=:author_id";
		
		return $sql;
	}
	
	
	/**
	 * @return string
	 */
	public function update()
	{
		$sql = "UPDATE news SET name=:name, description=:description, is_active=:is_active,
                					created_at = CURRENT_TIMESTAMP(), updated_at = CURRENT_TIMESTAMP() WHERE id=:id";
		
		return $sql;
	}
	
	/**
	 * @param $id
	 * @return string
	 */
	public function delete()
	{
		$sql = "DELETE FROM news WHERE id=:id";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function selectOnePost()
	{
		$sql = "SELECT * FROM news WHERE id=:id";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function getInactivePosts()
	{
		$sql = "SELECT * FROM news WHERE is_active = 'no' AND author_id=:author_id";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function countPosts()
	{
		$sql = "SELECT  COUNT(*) posts FROM news WHERE is_active = 'no' AND author_id=:author_id";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function createTableNews()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `news` (
			  `id` INT(11) NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `name` VARCHAR(255) NOT NULL,
			  `description` TEXT,
			  `is_active` VARCHAR(10) DEFAULT 'no',
			  `news_date` DATE NOT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  `author_id` INT(11) DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function createTableUsers()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `author` (
			  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `first_name` VARCHAR(255) DEFAULT NULL,
			  `last_name` VARCHAR(255) DEFAULT NULL,
			  `login` VARCHAR(100) NOT NULL,
			  `email` VARCHAR(255) DEFAULT NULL,
			  `birth_date` DATE NOT NULL,
			  `gender` VARCHAR(50) DEFAULT NULL,
			  `type` SMALLINT(10) DEFAULT '2',
			  `password` VARCHAR(255) NOT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			";
		
		$sql .= "INSERT INTO `author` (`id`, `first_name`, `last_name`, `login`, `email`, `birth_date`, `gender`, `type`,
		                      `password`, `created_at`, `updated_at`) VALUES
			(1, 'Admin', 'User', 'admin', 'admin@mail.com', '1990-12-05', 'male', 1,
			 '".password_hash('admin1234', PASSWORD_DEFAULT)."', '2019-02-27 19:28:00', '2019-02-27 19:28:00'
			 );";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function dummyText()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `dummy_text` (
			  `id` int(11) NOT NULL,
			  `title` varchar(150) NOT NULL,
			  `content` text
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		$sql .= "INSERT INTO `dummy_text` (`id`, `title`, `content`) VALUES (1, 'Awesome dummy post',
								'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
								when an unknown printer took a galley of type and scrambled it to make a type specimen book.
								It has survived not only five centuries, but also the leap into electronic typesetting,
								remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
								sheets  containing Lorem Ipsum passages, and more recently with desktop publishing software like
								Aldus PageMaker including versions of Lorem Ipsum.'
								)";
		
		return $sql;
	}
	
	/**
	 * @return string
	 */
	public function getDummyText()
	{
		$sql = "SELECT title, content FROM dummy_text";
		
		return $sql;
	}
}