<?php

namespace App\singleton;


trait Singleton
{
	private static $instance;
	
	/**
	 * @param $binding
	 * @return Singleton|static
	 */
	public static function getInstance($binding)
	{
		switch ($binding) {
			case 'late' :
				if (null === self::$instance) {
					self::$instance = new static();
				}
				break;
			case 'early' :
				if (null === self::$instance) {
					self::$instance = new self();
				}
				break;
			default :
				if (null === self::$instance) {
					self::$instance = new self();
				}
				break;
		}
		
		
		return self::$instance;
	}
}