<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace App\views;

use App\template\Template;

class View
{
	/**
	 * @var array $route
	 */
	private $route;
	
	/**
	 * @var string $template
	 */
	private $template;
	
	/**
	 * @var string $view
	 */
	private $view;
	
	/**
	 * @var $controller
	 */
	private $controller;
	
	/**
	 * @var array $meta
	 */
	public $meta = [];
	
	/**
	 * @var $prefix string
	 */
	private $prefix;
	
	
	/**
	 * View constructor.
	 * @param $route
	 * @param $template
	 * @param $view
	 * @param $meta
	 */
	public function __construct($route, $template = '', $view = '', $meta = [])
	{
		$this->route = $route;
		$this->template = $template;
		$this->view = $view;
		$this->controller = $route['class'];
		$this->prefix = $route['prefix'];
		$this->meta = $meta;
		
		if (false === $template) {
			$this->template = false;
		} else {
			$this->template = $template ?: DEFAULT_TEMPLATE;
		}
	}
	
	/**
	 * @param array $data
	 * @throws \Exception
	 */
	public function render(array $data)
	{
		if (!empty($data)) {
			extract($data);
		}
		$this->prefix = str_replace('\\', '/', $this->prefix);
		$from = $this->prefix != '' ? 'admin' : 'user';
		$file = $from == 'admin' ? APP_VIEW_DIR . "/{$this->prefix}{$this->controller}/{$this->view}.php" :
											APP_VIEW_DIR . "/$from/{$this->prefix}{$this->controller}/{$this->view}.php";
		if (is_file($file)) {
			$template = Template::showTemplate()->outputTemplate("{$from}",'run', "{$this->controller}",
				"{$this->view}", [
					'data' => $data,
				]);
		} else {
			throw new \Exception("Template {$file} could not be found", 500);
		}
		if (false !== $this->template) {
			$templateFile = APP_VIEW_DIR . "/$from/{$this->template}.php";
			if (is_file($templateFile)) {
				Template::showTemplate()->renderTemplate("{$from}",'default', "",
					"{$this->template}", [
						'template' => $template,
						'theHeader' => function(){
							$this->headerTitle();
						}
					]
				);
			} else {
				throw new \Exception("File {$templateFile} could not be found", 500);
			}
		}
	}
	
	public function headerTitle()
	{
		$html = '<title>' . $this->meta['title'] . '</title>'. PHP_EOL;
		$html .= '<meta charset="utf-8" />'. PHP_EOL;
		
		echo $html;
	}
}