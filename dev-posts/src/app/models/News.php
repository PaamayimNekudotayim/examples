<?php

namespace App\models;


class News extends Model
{
	/**
	 * @param $data
	 * @return array
	 */
	public function getAllNews($data)
	{
		return $this->getAll($data);
	}
	
	/**
	 * @param $id
	 * @return mixed
	 */
	public function getOneNews($id)
	{
		return $this->selectOneNews($id);
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	public function update($data)
	{
		return $this->updateNews($data);
	}
	
	/**
	 * @param $table
	 * @param $data
	 * @return bool
	 */
	public function add($table, $data)
	{
		return $this->addNewPost($table, $data);
	}
	
	/**
	 * @param $id
	 * @return array
	 */
	public function getCountInactiveNews($id)
	{
		return $this->getCountInactive($id);
	}
	
	/**
	 * @return bool
	 */
	public function createNews()
	{
		return parent::createNews();
	}
	
	/**
	 * @return bool
	 */
	public function dummyText()
	{
		return $this->createAndShowDummyText();
	}
	
	/**
	 * @return mixed
	 */
	public function getDummyText()
	{
		return parent::getDummyText();
	}
}