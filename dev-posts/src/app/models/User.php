<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace App\models;


use App\singleton\Singleton;
use App\SQL;

class User extends Model
{
	use Singleton;
	
	public $attributes = [
		'first_name' => '',
		'last_name' => '',
		'login' => '',
		'email' => '',
		'birth_date' => '',
		'gender' => '',
		'password' => '',
	];
	
	public $rules = [
		'required' => [
			['first_name'],
			['last_name'],
			['login'],
			['email'],
			['password'],
		],
		'email' => [
			['email'],
		],
		'lengthMin' => [
			['password', 6],
		]
	];
	
	/**
	 * @return bool
	 */
	public function checkUnique()
	{
		$user = User::getInstance('late')->checkOneUser();
		if ($user) {
			if ($user->login == $this->attributes['login']) {
				$this->errors['unique'][] = 'This login is taken';
			}
			if ($user->email == $this->attributes['email']) {
				$this->errors['unique'][] = 'This email is taken';
			}
			return false;
		}
		return true;
	}
	
	
	/**
	 * @param $table
	 * @return bool
	 */
	public function save($table)
	{
		$sql = SQL::getInstance('late')->insertUser($table);
		$stmt = $this->dbh->prepare($sql);
		$data = [];
		$data[] = $this->attributes;
		foreach ($data as $attribute) {
			return $stmt->execute($attribute);
		}
		
		return false;
	}
	
	/**
	 * @param $type
	 * @return bool
	 */
	public function login($type = false)
	{
		$login = null;
		$password = null;
		if (isset($_POST['login']) && isset($_POST['password'])) {
			$login = !empty(trim($_POST['login'])) ? trim($_POST['login']) : null;
			$password = !empty(trim($_POST['password'])) ? trim($_POST['password']) : null;
		}
		
		if ($login && $password) {
			$user = User::getInstance('late')->getOneUser($type);
			if ($user) {
				if (password_verify($password, $user->password)) {
					foreach ($user as $key => $value) {
						if ($key != 'password') {
							$_SESSION['user'][$key] = $value;
						}
					}
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @return array
	 */
	public function getRegisteredUsersIfAny()
	{
		//TODO why this is not working?? ^__^ return $this->getRegisteredUsers();
		return parent::getRegisteredUsers();
	}
	
	/**
	 * @param $id
	 * @return mixed
	 */
	public function getAuthorToEdit($id)
	{
		return $this->selectAuthorToEdit($id);
	}
	
	/**
	 * @return bool
	 */
	public function createUsers()
	{
		return parent::createUsers();
	}
	
	/**
	 * @param $type
	 * @param $data
	 * @return bool
	 */
	public function updateUser($type = '2', $data)
	{
		return $this->updateAuthor($type, $data);
	}
	
	/**
	 * @return bool
	 */
	public function checkAuth()
	{
		return isset($_SESSION['user']);
	}
	
	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		return (isset($_SESSION['user']) && $_SESSION['user']['type'] == '1');
	}
	
	public function deleteUser($id)
	{
		parent::deleteUser($id);
	}
}