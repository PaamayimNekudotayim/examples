<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace App\models;


use App\controllers\DB;
use App\SQL;
use Valitron\Validator;

class Model extends DB
{
	
	public $attributes = [];
	
	public $errors = [];
	
	public $rules = [];
	
	/**
	 * @param $data
	 */
	public function load($data)
	{
		foreach ($this->attributes as $key => $value) {
			if (isset($data[$key])) {
				$this->attributes[$key] = $data[$key];
			}
		}
	}
	
	/**
	 * @return bool
	 */
	public function createUsers()
	{
		$sql = SQL::getInstance('late')->createTableUsers();
		
		return $this->dbh->prepare($sql)->execute();
	}
	
	/**
	 * @return bool
	 */
	public function createNews()
	{
		$sql = SQL::getInstance('late')->createTableNews();
		
		return $this->dbh->prepare($sql)->execute();
	}
	
	/**
	 * @return bool
	 */
	public function createAndShowDummyText()
	{
		$sql = SQL::getInstance('late')->dummyText();
		
		return $this->dbh->prepare($sql)->execute();
	}
	
	/**
	 * @param $sql
	 * @param $data
	 * @return array
	 */
	public function getAll($data)
	{
		$sql = SQL::getInstance('early')->getAllNews();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute($data);
		$data = $stmt->fetchAll(\PDO::FETCH_OBJ);
		
		return $data;
	}
	
	/**
	 * @return array
	 */
	public function getUsers()
	{
		$sql = SQL::getInstance('late')->selectAuthors();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute();
		
		return $stmt->fetchAll(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $id
	 */
	public function deleteUser($id)
	{
		$sqls = SQL::getInstance('late')->deleteUser();
		$stmt = null;
		foreach ($sqls as $sql) {
			$stmt = $this->dbh->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
		}
	}
	
	/**
	 * @return array
	 */
	public function getRegisteredUsers()
	{
		$sql = SQL::getInstance('late')->selectRegisteredUsers();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute();
		
		return $stmt->fetchAll(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @return mixed
	 */
	public function getDummyText()
	{
		$sql = SQL::getInstance('late')->getDummyText();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute();
		
		return $stmt->fetch(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @return mixed
	 */
	public function checkOneUser()
	{
		$sql = SQL::getInstance('late')->checkLoggedInUser();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute(['email' => $_POST['email'], 'login' => $_POST['login']]);
		
		return $stmt->fetch(\PDO::FETCH_OBJ);
	}
	
	
	/**
	 * @param bool $type
	 * @return mixed
	 */
	public function getOneUser($type = false)
	{
		$sql = SQL::getInstance('late')->getLoggedInUser($type);
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute(['login' => $_POST['login']]);
		
		return $stmt->fetch(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $id
	 * @return mixed
	 */
	public function selectOneNews($id)
	{
		$sql = SQL::getInstance('late')->selectOnePost();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute(['id' => $id]);
		
		return $stmt->fetch(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $id
	 * @return mixed
	 */
	public function selectAuthorToEdit($id)
	{
		$sql = SQL::getInstance('late')->selectAuthorToEdit();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute(['id' => $id]);
		
		return $stmt->fetch(\PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	public function updateNews($data)
	{
		$sql = $sql = SQL::getInstance('late')->update();
		$stmt = $this->dbh->prepare($sql);
		foreach ($data as $key => $value) {
			$stmt->bindValue(':' . $key .'', $value);
			
			return $stmt->execute($data);
		}
		
		return false;
	}
	
	/**
	 * @param $type
	 * @param $data
	 * @return bool
	 */
	public function updateAuthor($type = '2',$data)
	{
		$sql = $sql = SQL::getInstance('late')->updateAuthor($type);
		$stmt = $this->dbh->prepare($sql);
		foreach ($data as $key => $value) {
			$stmt->bindValue(':' . $key .'', $value);
			
			return $stmt->execute($data);
		}
		
		return false;
	}
	
	/**
	 * @param $id
	 * @return bool
	 */
	public function deleteNews($id)
	{
		$sql = SQL::getInstance('late')->delete();
		
		$stmt = $this->dbh->prepare($sql);
		return $stmt->execute($id);
	}
	
	/**
	 * @param $table
	 * @param $data
	 * @return bool
	 */
	public function addNewPost($table, $data)
	{
		$sql = SQL::getInstance('late')->insertNews($table);
		$stmt = $this->dbh->prepare($sql);
		foreach ($data as $key => $value) {
			$stmt->bindValue(':' . $key .'', $value);
			
			return $stmt->execute($data);
		}
		
		return false;
	}
	
	/**
	 * @param $data
	 * @return bool
	 */
	public function validate($data)
	{
		$validate = new Validator($data);
		$validate->rules($this->rules);
		
		if ($validate->validate()) {
			return true;
		}
		
		$this->errors = $validate->errors();
		
		return false;
	}
	
	/**
	 * @errors
	 */
	public function getErrors()
	{
		if (!empty($this->errors)) {
			
			$htmlError = '<ul>';
			
			foreach ($this->errors as $errors) {
				foreach ($errors as $error) {
					$htmlError .= "<li>{$error}</li>";
				}
			}
			
			$htmlError .= '</ul>';
			
			$_SESSION['error'] = $htmlError;
		}
	}
	
	/**
	 * @param $data
	 * @return array
	 */
	public function getInactiveNews($data)
	{
		$sql = SQL::getInstance('late')->getInactivePosts();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute($data);
		$data = $stmt->fetchAll(\PDO::FETCH_OBJ);
		
		return $data;
	}
	
	/**
	 * @param $id
	 * @return array
	 */
	public function getCountInactive($id)
	{
		$sql = SQL::getInstance('late')->countPosts();
		$stmt = $this->dbh->prepare($sql);
		$stmt->execute([':author_id' => $id]);
		$data = $stmt->fetchAll(\PDO::FETCH_OBJ);
		
		return $data;
	}
}