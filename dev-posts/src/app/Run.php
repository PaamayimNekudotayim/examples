<?php

namespace App;


use App\messages\Messages;

class Run
{
	/**
	 * Run constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		new Messages();
		session_start();
		addRoute('^admin$', ['class' => 'User', 'method' => 'admin', 'prefix' => 'admin']);
		addRoute('^admin/?(?P<class>[a-z-]+)/?(?P<method>[a-z-]+)?$', ['prefix' => 'admin']);
		addRoute('^$', ['class' => 'News', 'method' => '']);
		addRoute('^(?P<class>[a-z-]+)/?(?P<method>[a-z-]+)?$');
		
	}
}