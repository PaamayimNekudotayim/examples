<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace App\messages;


use App\template\Template;

class Messages
{
	/**
	 * ErrorHandler constructor.
	 */
	public function __construct()
	{
		APP_ERROR_REPORTING;
		set_exception_handler([$this, 'handleErrors']);
	}
	
	/**
	 * @param $e
	 * @throws \Exception
	 */
	public function handleErrors($e)
	{
		/**@var \Exception $e*/
		$this->_log(
			$e->getMessage(),
			$e->getFile(),
			$e->getLine()
		);
		$this->displayError(
			"Exception: {$e->getCode()}",
			$e->getMessage(),
			$e->getFile(),
			$e->getLine(),
			$e->getCode()
		);
	}
	
	/**
	 * @param $message
	 * @param $file
	 * @param $line
	 */
	public function _log($message, $file, $line)
	{
		$output = '';
		if (function_exists('error_log')) {
			$output .= "Error reporting: \n
			Date: " . date('Y-m-d H:i:s') . "\n" .
				"Message: {$message}\n
			File: {$file} | \n
			Line: {$line}\n------------------------------\n";
			
			error_log($output, 3, LOG_DIR . '/error.log');
		}
	}
	
	
	/**
	 * @param $errorNum
	 * @param $errorMessage
	 * @param $errorFile
	 * @param $errorLine
	 * @param int $responseCode
	 * @throws \Exception
	 */
	public function displayError($errorNum, $errorMessage, $errorFile, $errorLine, $responseCode = 404)
	{
		http_response_code((intval($responseCode)));
		$template = null;
		if ($responseCode == 404 && !APP_DEBUG) {
			$template = Template::showTemplate()->outputTemplate(
				'user','404', 'frontend', '404', []
			);
			echo $template;
			exit;
		} elseif (APP_DEBUG) {
			$template = Template::showTemplate()->outputTemplate(
				'admin','development', 'dev', 'dev', [
					'errorNum' => $errorNum,
					'errorMessage' => $errorMessage,
					'errorFile' => $errorFile,
					'errorLine' => $errorLine,
					'errorCode' => $responseCode,
				]
			);
			echo $template;
			exit;
		} else {
			$template = Template::showTemplate()->outputTemplate(
				'user','oops', 'frontend', 'oops', []
			);
			echo $template;
			exit;
		}
	}
}