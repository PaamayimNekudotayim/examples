<?php

namespace App\controllers;


use App\singleton\Singleton;
use App\template\Template;


class DB
{
	use Singleton;
	
	public $dbh;
	
	/**
	 * DB constructor.
	 */
	public function __construct()
	{
		$config = (include ROOT_DIR . '/config/config.php')['db'];
		
		try {
			$this->dbh = new \PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'], $config['db_user'],
																								$config['db_password']);
			$this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$this->dbh->exec('SET NAMES "utf8"');
		} catch (\PDOException $e) {
			Template::showTemplate()->renderTemplate('user','error', 'DB-404', '404', [
				'e' => $e,
			]);
			
			die();
		}
		
		
		
	}
}