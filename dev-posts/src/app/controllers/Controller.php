<?php

namespace App\controllers;


use App\views\View;

abstract class Controller
{
	/**
	 * @var array current routing
	 */
	public $route = [];
	
	/**
	 * @var $controller
	 */
	public $controller;
	
	/**
	 * @var $method
	 */
	public $view;
	
	/**
	 * @var $template
	 */
	public $template;
	
	/**
	 * @var array
	 */
	public $data = [];
	
	/**
	 * @var array
	 */
	public $meta = [
		'title' => ''
	];
	
	/**
	 * Controller constructor.
	 * @param $route
	 */
	public function __construct($route)
	{
		$this->route = $route;
		$this->controller = $route['class'];
		$this->view = $route['method'];
	}
	
	/**
	 * @param $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
	
	/**
	 * @param string $title
	 */
	public function setHeaderMeta($title = '')
	{
		$this->meta = [
			'title' => $title,
		];
	}
	
	/**
	 * @throws \Exception
	 */
	public function getTemplate()
	{
		$obj = new View($this->route, $this->template, $this->view, $this->meta);
		
		$obj->render($this->data);
	}
	
	/**
	 * @return bool
	 */
	public function isAjax()
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}
	
}