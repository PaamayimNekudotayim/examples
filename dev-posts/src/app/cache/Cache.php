<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace App\cache;


use App\singleton\Singleton;

class Cache
{
	use Singleton;
	
	/**
	 * @param $key
	 * @param $data
	 * @param int $time
	 * @return bool
	 */
	public function setCache($key, $data, $time = 3600)
	{
		$content = [];
		
		if ($time) {
			$content['data'] = $data;
			$content['created_at'] = time() + $time;
			
			if (file_put_contents(CACHE_DIR . '/' . md5($key) . '.txt', serialize($content))) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param $key
	 * @return bool|mixed
	 */
	public function getCache($key)
	{
		// Get Cache file
		$file = CACHE_DIR . '/' . md5($key) . '.txt';
		// Check if cache exists
		if (file_exists($file)) {
			// Unserialize contnet.
			$content = unserialize(file_get_contents($file));
			if (!is_array($content)) {
				return false;
			}
			// Check if current time is less or equal then the cache file was created
			if (time() <= $content['created_at']) {
				// Return content
				return $content;
			}
			
			// Delete cache
			unlink($file);
		}
		
		return false;
	}
	
	/**
	 * @param $key
	 */
	public function deleteCache($key)
	{
		$file = CACHE_DIR . '/' . md5($key) . '.txt';
		// Check if cache exists
		if (file_exists($file)) {
			unlink($file);
		}
	}
}