<?php

namespace App\template;


class Template
{

	private $prop = [];
	private static $template;
	
	public static function showTemplate()
	{
		if (null == self::$template) {
			self::$template = new self();
		}
		
		return self::$template;
	}
	
	/**
	 * @param $key
	 * @param $where
	 * @param $template
	 * @param $from
	 * @return string/null
	 */
	public function getTemplate($from, $key, $where, $template)
	{
		if (!isset($this->prop[$key])) {
			$this->__setProp($key, ucfirst(str_replace('_', ' ', $key)));
			
			switch ($from) {
				case strtolower('user') :
					$file = APP_VIEW_DIR . '/user/' . $where . '/' . $template . '.php';
				break;
				case strtolower('admin') :
					$file = APP_VIEW_DIR . '/admin/' . $where . '/' . $template . '.php';
				break;
				default :
					$file = APP_VIEW_DIR . '/user/' . $where . '/' . $template . '.php';
				break;
			}
			
			
			if (!empty($file)) {
				return $file;
			} else {
				die("File {$template} could not be found");
			}
		} else {
			die("The key {$key} already exists");
		}
	}
	
	/**
	 * @param $key
	 * @param $from
	 * @param $where
	 * @param $template
	 * @param array $env
	 */
	public function renderTemplate($from, $key, $where, $template, array $env)
	{
		$file = $this->getTemplate($from, $key, $where, $template);
	
		extract($env);
		
		/** @noinspection PhpIncludeInspection */
		require ($file);
	}
	
	
	/**
	 * @param $key
	 * @param $from
	 * @param $where
	 * @param $template
	 * @param array $env
	 * @return string
	 */
	public function outputTemplate($from, $key, $where, $template, array $env)
	{
		$file = $this->getTemplate($from, $key, $where, $template);
		extract($env);
		
		ob_start();
		/** @noinspection PhpIncludeInspection */
		require_once ($file);
		
		
		return ob_get_clean();
	}
	
	public function __setProp($key, $val)
	{
		$this->prop[$key] = $val;
	}
	
	public function __getProp($key)
	{
		return $this->prop[$key];
	}
}