<?php
/**
 * @var $reg
 * @var $route array
 * @return bool
 * @throws Exception
 */

function addRoute($reg, $route = [])
{
	$routes[$reg] = $route;
	
	$url = removeQueryString(queryString());
	
	foreach ($routes as $pattern => $route) {
		if (preg_match("#{$pattern}#", $url, $matches)) {
			foreach ($matches as $key => $value) {
				if (is_int($key)) {
					continue;
				}
				$route[$key] = $value;
			}
			if (empty($route['method'])) {
				$route['method'] = 'news';
			}
			
			if (!isset($route['prefix'])) {
				$route['prefix'] = '';
			} else {
				$route['prefix'] .= '\\';
			}
			$route['class'] = handleControllerName($route['class']);
			
			$_route = $route;
			if (!empty($_route)) {
					$obj = null;
					$class = 'Hmf\controllers\\' . $route['prefix'] . $_route['class'] . 'Controller';
					if (class_exists($class)) {
						$obj = new $class($_route);
						$method = handleMethodName($_route['method']);
						if (method_exists($obj, $method)) {
							/**@var \App\controllers\Controller $obj*/
							$obj->$method();
							$obj->getTemplate();
						} else {
							
							throw new \Exception("Method {$method} Not Found :(", 404);
						}
					} else {
						throw new \Exception("Object {$class} Not Found", 404);
					}
				}else {
					throw new \Exception('Page not found', 500);
				}
			
		}
	}
	
	return false;
}

/**
 * @return string
 */
function queryString()
{
	return trim($_SERVER['QUERY_STRING'], '/');
}

/**
 * @param $controller
 * @return null|string
 */
function handleControllerName($controller)
{
	
	if (!empty($controller)) {
		return str_replace(' ', '', ucwords(str_replace('-', ' ', $controller)));
	}
	
	return null;
}

/**
 * @param $method
 * @return null|string
 */
function handleMethodName($method)
{
	if (!empty($method)) {
		return lcfirst(handleControllerName($method));
	}
	
	return null;
}

/**
 * @param $url
 * @return string
 */
function removeQueryString($url)
{
	
	if (false !== $url) {
		$params = explode('&', $url, 2);
		if (false === strpos($params[0], '=')) {
			return rtrim($params[0], '/');
		} else {
			return '';
		}
	}
	
	return '';
}