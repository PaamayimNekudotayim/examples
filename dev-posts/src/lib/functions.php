<?php

/**
 * @param $text
 * @return string
 */
function html($text)
{
	return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

/**
 * @param $text
 */
function htmlout($text)
{
	echo html($text);
}

/**
 * @param bool $http
 */
function redirect($http = false)
{
	if ($http) {
		$redirect = $http;
	} else {
		$redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOME_URL;
	}
	header("Location: $redirect");
	
	exit();
}

/**
 * @param $template
 */
function template($template)
{
	$output = '';
	
	if ('' !== $template) {
		$output .= $template;
	}
	
	echo $output;
}