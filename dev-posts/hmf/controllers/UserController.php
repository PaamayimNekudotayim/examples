<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace Hmf\controllers;

use App\controllers\Controller;
use App\models\User;

class UserController extends Controller
{
	/**
	 *
	 */
	public function settings()
	{
		$this->setHeaderMeta('Settings');
		if (isset($_SESSION['user'])) {
			$id = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : null;
			
			if (null !== $id) {
				$this->setData(
					[
						'user' => User::getInstance('late')->selectAuthorToEdit((int)$id),
					]
				);
			} else {
				$_SESSION['error'] = 'No user found to edit :(';
				redirect('/');
			}
			
		} else {
			exit('Something went wrong :(');
		}
	}
	
	public function signup()
	{
		$this->setHeaderMeta('Register');
		if (!empty($_POST)) {
			$data = $_POST;
			$user = User::getInstance('late');
			$user->load($data);
			
			if (!$user->validate($data) || !$user->checkUnique()) {
				$user->getErrors();
				$_SESSION['form_data'] = $data;
			} else {
				$user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
				if ($user->save('author')) {
					$_SESSION['success'] = 'Successful register';
					redirect('/user/login');
					exit;
				} else {
					$_SESSION['error'] = 'Error during registration';
				}
			}
		}
	}
	
	public function login()
	{
		if (!empty($_POST)) {
			$user = new User();
			if ($user->login()) {
				$_SESSION['success'] = 'Successful login';
				redirect('/');
				exit();
			} else {
				$_SESSION['error'] = 'Login or Password are incorrect';
			}
		}
		$this->setHeaderMeta('Login');
	}
	
	public function logout()
	{
		if (isset($_SESSION['user'])) {
			unset($_SESSION['user']);
		}
		redirect('/');
		
		exit;
	}
	
	public function update()
	{
		if (isset($_POST)) {
			if (!empty($_POST['id'])) {
				$email = $_POST['email'];
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$_SESSION['error'] = 'Email is incorrect';
					redirect('settings/');
				}
				$data = [
					'id' => strip_tags(htmlspecialchars($_POST['id'])),
					'first_name' => strip_tags(htmlspecialchars($_POST['first_name'], ENT_QUOTES, 'UTF-8')),
					'last_name' => strip_tags(htmlspecialchars($_POST['last_name'], ENT_QUOTES, 'UTF-8')),
					'email' => strip_tags($email),
					'login' => strip_tags(htmlspecialchars($_POST['login'], ENT_QUOTES, 'UTF-8')),
				];
				
				if (User::getInstance('late')->updateUser('2', $data)) {
					$_SESSION['success'] = 'User has been updated';
				} else {
					$_SESSION['error'] = 'Something went wrong :(';
				}
				
				redirect();
				
				exit();
			}
		}
	}
	
	public function allUsers()
	{
		$this->setHeaderMeta('All Users');
		
		$allUsers = User::getInstance('late')->getRegisteredUsersIfAny();
		
		if (!empty($allUsers)) {
			$this->setData(
				[
					'allUsers' => $allUsers
				]
			);
		}
	}
}