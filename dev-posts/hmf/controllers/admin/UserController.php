<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace Hmf\controllers\admin;


use App\models\User;

class UserController extends AdminController
{
	public function admin()
	{
		$this->setHeaderMeta('Admin');
		
		$allUsers = User::getInstance('late')->getRegisteredUsersIfAny();
		
		if (!empty($allUsers)) {
			$this->setData(
				[
					'allUsers' => $allUsers
				]
			);
		}
	}
	
	public function login()
	{
		
		if (!empty($_POST)) {
			$user = new User();
			if (!$user->login(true)) {
				$_SESSION['error'] = 'Error! You have no permissions to access admin area.';
			}
			
			if (User::getInstance('late')->isAdmin()) {
				redirect(ADMIN_URL);
			} else {
				redirect(ADMIN_URL . '/user/login');
			}
		}
		$this->template = 'login';
	}
	
	public function delete()
	{
		if(isset($_POST['delete_author'])) {
			$id = intval($_POST['delete_author']);
			
			$res = [
				'status' => 'success',
				'message' => 'Author was deleted',
				'redirect' => filter_var($_POST['redirect'], FILTER_VALIDATE_URL),
			];
			User::getInstance('late')->deleteUser($id);
			echo  json_encode($res);
		}
			exit();
	}
	
	public function edit()
	{
		$this->setHeaderMeta('Edit Users');
		
		if (isset($_GET['userId'])) {
			$id = filter_var($_GET['userId'], FILTER_SANITIZE_STRING);
			$this->setData(
				[
					'editUser' => User::getInstance('late')->selectAuthorToEdit((int)$id),
				]
			);
		}
	}
	
	public function update()
	{
		if (isset($_POST)) {
			if (!empty($_POST['id'])) {
				$email = $_POST['email'];
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$_SESSION['error'] = 'Email is incorrect';
					redirect();
				}
				$data = [
					'id' => strip_tags(htmlspecialchars($_POST['id'])),
					'first_name' => strip_tags(htmlspecialchars($_POST['first_name'], ENT_QUOTES, 'UTF-8')),
					'last_name' => strip_tags(htmlspecialchars($_POST['last_name'], ENT_QUOTES, 'UTF-8')),
					'email' => strip_tags($email),
					'login' => strip_tags(htmlspecialchars($_POST['login'], ENT_QUOTES, 'UTF-8')),
					'type' => strip_tags(htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8')),
				];
				
				if (!ctype_digit($data['id']) || !ctype_digit($data['type'])) {
					$_SESSION['error'] = 'Id or Type is not in correct format. Only digits allowed.';
					redirect();
				}
				
				if ($data['type'] > 2 || $data['type'] < 1) {
					$_SESSION['error'] = 'Type should be 1 or 2';
					redirect();
				}
				
				if (User::getInstance('late')->updateUser('1', $data)) {
					$_SESSION['success'] = 'User has been updated';
				} else {
					$_SESSION['error'] = 'Something went wrong :(';
				}
				
				redirect();
				
				exit();
			}
		}
		
	}
}