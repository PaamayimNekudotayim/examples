<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace Hmf\controllers\admin;


use App\controllers\Controller;
use App\models\User;

class AdminController extends Controller
{
	public function __construct($route)
	{
		parent::__construct($route);
		
		if (!User::getInstance('late')->isAdmin() && $route['method'] != 'login') {
			redirect(ADMIN_URL . '/user/login');
		}
	}
}