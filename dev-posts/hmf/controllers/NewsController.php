<?php

namespace Hmf\controllers;

use App\controllers\AppController;
use App\models\News;
use App\models\User;

class NewsController extends AppController
{
	/**
	 *News
	 */
	public function news()
	{
		$this->setHeaderMeta('Awesome News');
		if (!User::getInstance('late')->createUsers()) {
			User::getInstance('late')->createUsers();
		}
		
		if (!News::getInstance('late')->createNews()) {
			News::getInstance('late')->createNews();
		}
		
		if(!News::getInstance('late')->dummyText()) {
			News::getInstance('late')->dummyText();
		}
		$id = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : null;
		
		$this->setData(
			[
				'news' => News::getInstance('late')->getAllNews([]),
				'count' => News::getInstance('late')->getCountInactiveNews($id),
				'dummyText' => News::getInstance('late')->getDummyText(),
				'user' => User::getInstance('late')->getAuthorToEdit($id),
			]
		);
	}
	
	/**
	 *
	 */
	public function edit()
	{
		$this->setHeaderMeta('Edit Post');
		if (isset($_GET['post_id'])) {
			if (!empty($_GET['post_id'])) {
				$singlePost = News::getInstance('late')->getOneNews(strip_tags((int)$_GET['post_id']));
				
				$this->setData(['singlePost' => $singlePost]);
			}
		}
	}
	
	/**
	 *
	 */
	public function update()
	{
		$this->setHeaderMeta('Update');
		if (isset($_POST)) {
			if (!empty($_POST['id'])) {
				$data = [
					'id' => strip_tags(htmlspecialchars($_POST['id'])),
					'name' => strip_tags(htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8')),
					'description' => strip_tags(htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8')),
					'is_active' => strip_tags($_POST['is_active']),
				];
				
				News::getInstance('late')->update($data);
				
				redirect('/');
				
				exit();
			}
		}
		
		exit();
	}
	
	
	/**
	 * @return array|false|string
	 */
	public function delete()
	{
		if(isset($_POST['delete_post'])) {
			$id = [
				'id' => intval($_POST['delete_post']),
			];
			return News::getInstance('late')->deleteNews($id);
		}
		
		exit();
	}
	
	/**
	 *
	 */
	public function add()
	{
		$this->setHeaderMeta('Add Awesome Post');
		if (isset($_POST)) {
			if (isset($_POST['author_id'])) {
				$data = [
					'name' => strip_tags(htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8')),
					'description' => strip_tags(htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8')),
					'author_id' => strip_tags(htmlspecialchars($_POST['author_id'])),
					'is_active' => strip_tags(htmlspecialchars($_POST['is_active'], ENT_QUOTES, 'UTF-8')),
				];
				
				News::getInstance('late')->add('news', $data);
				
				redirect('/');
				
				exit();
				
			}
		}
	}
	
	/**
	 *
	 */
	public function inactive()
	{
		if (!empty($_SESSION['user']['id'])) {
			$id = [
				'author_id' => strip_tags($_SESSION['user']['id']),
			];
			
			$inactive = News::getInstance('late')->getInactiveNews($id);
			
			$this->setData(['inactive' => $inactive]);
		}
	}
}