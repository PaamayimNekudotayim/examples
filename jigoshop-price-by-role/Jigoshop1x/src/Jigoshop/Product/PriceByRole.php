<?php
namespace Jigoshop\Product;

class PriceByRole
{
	public function __construct()
	{
		add_action('jigoshop_product_write_panel_tabs', array($this, 'writePanelTabs'));
		add_action('product_write_panels', array($this, 'productWritePanel'));
		add_filter('jigoshop_process_product_meta', array($this, 'productSaveData'));

		add_filter('jigoshop_product_get_regular_price', array($this, 'getPrice'), 0, 2);
		add_filter('jigoshop_get_sale_price',array($this, 'getSalePrice'), 0, 2);

		add_filter('jigoshop_product_price_display_in_cart', array($this, 'savingsDisplayInCart'), 0, 10);

		add_action('jigoshop_variable_product_table_end', array($this, 'variationPriceOnRole'), 0, 2);
		add_action('jigoshop_variable_product_table_data_save', array($this, 'productVariationSaveData'), 10, 2);

		require_once(JIGOSHOP_PRICE_BY_ROLE_DIR.'/src/Jigoshop/Product/PriceByRole/Helper.php');
	}

	public function variationPriceOnRole($variation, $attributes)
	{
		global $wp_roles;
		$productType = 'variable';
		$roles = $wp_roles->get_names();
		$defaults = array();

		$defaults[] = array(
			'name' => __('Enable Price by Role', 'jigoshop_price_by_role'),
			'id' => 'jigoshop_price_by_role_enable',
			'type' => 'enable',
			'checked' => $this->getMeta('enable', '', $variation->ID)
		);

		$defaults[] = array(
			'name' => __('Price by role', 'jigoshop_price_by_role'),
			'type' => 'title'
		);
		foreach ($roles as $id => $name) {
			$defaults[] = array(
				'name' => $name,
				'id' => $id,
				'type' => 'label',
				'checked' => $this->getMeta($id, 'enable', $variation->ID),
				'value' => $this->getMeta($id, 'value', $variation->ID),
				'placeholder' => __('Price isn\'t set', 'jigoshop_price_by_role')
			);
		}

		echo '<tr><td colspan="7">';
		\Jigoshop\Product\PriceByRole\Helper::displayWritePanel($variation->ID, $defaults, $productType);
		echo '</td></tr>';
	}

	public function getPrice($price, $productId)
	{
		$enabled = $this->getMeta('enable', '', $productId);
				if (empty($enabled)) {
			return $price;
		}

		$roles = $this->getUserRole();
		foreach ($roles as $role) {
			$role = $this->getMeta($role, '', $productId);
			if (empty($role['enable'])) {
				continue;
			}

			if ($price > $role['value']) {
				$price = $role['value'];
			}
		}

		return $price;
	}


	public function getSalePrice($price, $product)
	{
		return $this->getPrice($price,$product->ID);
	}

	public function savingsDisplayInCart($price, $product, $values)
	{
		$product = $values['data'];

		return jigoshop_price($this->getPrice($price, $product->id));
	}

	public function getUserRole()
	{
		$user_ID = get_current_user_id();

		return get_userdata($user_ID)->roles;
	}

	private function getProductType($post)
	{
		$terms = get_the_terms($post, 'product_type');
		if ($terms) {
			$productType = current($terms)->slug;
		} else {
			$productType = 'simple';
		}

		return $productType;
	}

	public function writePanelTabs()
	{
		global $post;

		$productType = $this->getProductType($post);

		if ($productType == 'grouped' || $productType == 'simple') {
			\Jigoshop\Product\PriceByRole\Helper::displayPanelTab();
		}
	}

	public function productWritePanel()
	{
		global $wp_roles, $post;
		$roles = $wp_roles->get_names();
		$defaults = array();

		$productType = $this->getProductType($post);

		if ($productType == 'grouped' || $productType == 'simple') {
			$defaults[] = array(
				'name' => __('Enable Price by Role', 'jigoshop_price_by_role'),
				'id' => 'jigoshop_price_by_role_enable',
				'type' => 'enable',
				'checked' => $this->getMeta('enable')
			);

			$defaults[] = array(
				'name' => __('Select Roles', 'jigoshop_price_by_role'),
				'type' => 'title'
			);
			foreach ($roles as $id => $name) {
				$defaults[] = array(
					'name' => $name,
					'id' => $id,
					'type' => 'label',
					'checked' => $this->getMeta($id, 'enable'),
					'value' => $this->getMeta($id, 'value'),
					'placeholder' => __('Price isn\'t set', 'jigoshop_price_by_role' )
				);
			}
			\Jigoshop\Product\PriceByRole\Helper::displayWritePanel($post->ID, $defaults, $productType);
		}
	}

	public function productSaveData($post_id)
	{
		if ($_POST['product-type'] != 'variable') {
			$this->updatePostMeta($post_id, $_POST);
		}
	}

	private function updatePostMeta($postId, $data)
	{
		global $wp_roles;
		$roles = $wp_roles->get_names();
		$priceByRole = array();
		if (isset($data['jigoshop_price_by_role_enable'])) {
			$priceByRole['enable'] = $data['jigoshop_price_by_role_enable'];
		} else {
			$priceByRole['enable'] = null;
		}

		foreach ($roles as $id => $name) {
			if(isset($data['jigoshop_price_by_role_check_'.$id])) {
				$enable = $data['jigoshop_price_by_role_check_'.$id];
			} else {
				$enable = null;
			}
			$priceByRole[$id] = array(
				'enable' => $enable,
				'value' => $data['jigoshop_price_by_role_set_'.$id]
			);
		}

		update_post_meta($postId, 'price_by_role', $priceByRole);
	}

	public function productVariationSaveData($ID, $meta)
	{
		if (isset($meta['variation_prices_by_roles']) && $meta['variation_prices_by_roles'] == 'on') {
			$this->updatePostMeta($ID, $meta);
		}
	}

	public function getMeta($id, $subid = '', $product_id = '')
	{
		$priceByRole = '';
		if (empty($product_id)) {
			global $post;
			$priceByRole = get_post_meta($post->ID, 'price_by_role', true);
		} else {
			$priceByRole = get_post_meta($product_id, 'price_by_role', true);
		}
		if (empty($priceByRole)) {
			return '';
		}
		if (empty($subid)) {
			return $priceByRole[$id];
		}

		return $priceByRole[$id][$subid];
	}
}
