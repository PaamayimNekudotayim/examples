<?php

namespace Jigoshop\Product\PriceByRole;

class Helper
{

	public static function displayPanelTab()
	{
		echo '<li><a href="#price_by_role">'.__('Price by Role', 'jigoshop_price_by_role').'</a></li>';
	}

	public static function displayWritePanel($postId, $defaults, $productType)
	{
		if ($productType == 'variable') {
			echo '<div id="price_by_role" class="jigoshop_options_panel">';
			echo Helper::createVariationInput($postId, $defaults).'</div>';
		} else {
			echo '<div id="price_by_role" class="panel jigoshop_options_panel">';
			echo Helper::createInput($defaults).'</div>';
		}
	}

	public static function createVariationInput($variationId, $defaults)
	{
		$title = '';
		$enable = '';
		$labels = '';
		$status = '';

		foreach ($defaults as $default) {

			switch ($default['type']) {

				case 'title':
					$title .= '<h3>'.$default['name'].'</h3>';
					break;

				case 'label':
					$checkName = '['.$variationId.'][jigoshop_price_by_role_check_'.$default['id'].']';
					$setName = '['.$variationId.'][jigoshop_price_by_role_set_'.$default['id'].']';
					$uniqueId = $variationId.'_'.$default['id'];
					$labels .= '<p class="form-field">';
					$labels .= '<label for="jigoshop_price_by_role_check_'.$uniqueId.'">'.$default['name'].'</label>';

					if (!empty($default['checked'])) {
						$labels .= '<input style"margin-right: 10px;" type="checkbox" class="checkbox" name="variations'.$checkName.'" id="jigoshop_price_by_role_check_'.$uniqueId.'" checked="'.$default['checked'].'" />';
						$labels .= '<input style="display: block" type="number" class="short" step="any" name="variations'.$setName.'" id="jigoshop_price_by_role_set_'.$uniqueId.'" value="'.$default['value'].'"placeholder="'.$default['placeholder'].'"/>';
					} else {
						$labels .= '<input type="checkbox" class="checkbox" name="variations'.$checkName.'" id="jigoshop_price_by_role_check_'.$uniqueId.'" />';
						$labels .= '<input style="display: none" type="number" class="short" step="any" name="variations'.$setName.'" id="jigoshop_price_by_role_set_'.$uniqueId.'" placeholder="'.$default['placeholder'].'"/>';
					}

					$labels .= '</p>';
					$labels .= '<script>jQuery("#jigoshop_price_by_role_check_'.$uniqueId.'").click(function(){ jQuery("#jigoshop_price_by_role_set_'.$uniqueId.'").toggle(500);});</script>';
					break;

				case 'enable':
					$uniqueId = $variationId.'_'.$default['id'];
					$uniqueName = '['.$variationId.']['.$default['id'].']';
					$enable .= '<input type="hidden" name="variations['.$variationId.'][variation_prices_by_roles]" value="on">';
					$enable .= '<p class="form-field">';
					$enable .= '<label for="'.$uniqueId.'">'.$default['name'].'</label>';

					if (!empty($default['checked'])) {
						$enable .= '<input type="checkbox" class="checkbox" name="variations'.$uniqueName.'" id="'.$uniqueId.'" checked="'.$default['checked'].'" />';
						$status = 'show';
					} else {
						$enable .= '<input type="checkbox" class="checkbox" name="variations'.$uniqueName.'" id="'.$uniqueId.'" />';
						$status = 'hide';
					}

					$enable .= '</p>';
					$enable .= '<script>jQuery("#'.$uniqueId.'").click(function(){ jQuery("#jigoshop_price_by_role_labels_'.$variationId.'").toggle(500);});</script>';
					break;
			}
		}
		echo $title;
		echo '<fieldset>'.$enable.'</fieldset>';
		if ($status == 'show') {
			echo '<div id="jigoshop_price_by_role_labels_'.$variationId.'" style="display: block">';
		} else {
			echo '<div id="jigoshop_price_by_role_labels_'.$variationId.'" style="display: none">';
		}
		echo '<fieldset>'.$labels.'</fieldset>';
		echo '</div>';
	}

	public static function createInput($defaults)
	{
		$title = '';
		$enable = '';
		$labels = '';
		$status = '';

		foreach ($defaults as $default) {
			switch ($default['type']) {

				case 'title':
					$title .= '<h3>'.$default['name'].'</h3>';
					break;

				case 'label':
					$labels .= '<p class="form-field">';
					$labels .= '<label for="'.$default['id'].'">'.$default['name'].'</label>';

					if (!empty($default['checked'])) {
						$labels .= '<input style"margin-right: 10px;" type="checkbox" class="checkbox" name="jigoshop_price_by_role_check_'.$default['id'].'" id="jigoshop_price_by_role_check_'.$default['id'].'" checked="'.$default['checked'].'" />';
						$labels .= '<input style="display: block" type="number" class="short" step="any" name="jigoshop_price_by_role_set_'.$default['id'].'" id="jigoshop_price_by_role_set_'.$default['id'].'" value="'.$default['value'].'"placeholder="'.$default['placeholder'].'"/>';
					} else {
						$labels .= '<input type="checkbox" class="checkbox" name="jigoshop_price_by_role_check_'.$default['id'].'" id="jigoshop_price_by_role_check_'.$default['id'].'" />';
						$labels .= '<input style="display: none" type="number" class="short" step="any" name="jigoshop_price_by_role_set_'.$default['id'].'" id="jigoshop_price_by_role_set_'.$default['id'].'" placeholder="'.$default['placeholder'].'"/>';
					}

					$labels .= '</p>';
					$labels .= '<script>jQuery("#jigoshop_price_by_role_check_'.$default['id'].'").click(function(){ jQuery("#jigoshop_price_by_role_set_'.$default['id'].'").toggle(500);});</script>';
					break;

				case 'enable':
					$enable .= '<p class="form-field">';
					$enable .= '<label for="'.$default['id'].'">'.$default['name'].'</label>';

					if (!empty($default['checked'])) {
						$enable .= '<input type="checkbox" class="checkbox" name="'.$default['id'].'" id="'.$default['id'].'" checked="'.$default['checked'].'" />';
						$status = 'show';
					} else {
						$enable .= '<input type="checkbox" class="checkbox" name="'.$default['id'].'" id="'.$default['id'].'" />';
						$status = 'hide';
					}

					$enable .= '</p>';
					$enable .= '<script>jQuery("#'.$default['id'].'").click(function(){ jQuery("#jigoshop_price_by_role_labels").toggle(500);});</script>';
					break;
			}
		}
		echo $title;
		echo '<fieldset>'.$enable.'</fieldset>';
		if ($status == 'show') {
			echo '<div id="jigoshop_price_by_role_labels" style="display: block">';
		} else {
			echo '<div id="jigoshop_price_by_role_labels" style="display: none">';
		}
		echo '<fieldset>'.$labels.'</fieldset>';
		echo '</div>';
	}
}
