<?php

add_action('plugins_loaded', function(){

	if(is_user_logged_in()) {

		require_once(JIGOSHOP_PRICE_BY_ROLE_DIR.'/src/Jigoshop/Product/PriceByRole.php');

		new Jigoshop\Product\PriceByRole();
	}
}, 11);
if(is_admin()){
	add_filter('plugin_action_links_' . plugin_basename(dirname(JIGOSHOP_PRICE_BY_ROLE_DIR) . '/bootstrap.php'), function($links) {
		$links[] = '<a href="https://www.jigoshop.com/documentation/price-role/" target="_blank">Documentation</a>';
		$links[] = '<a href="https://www.jigoshop.com/support/" target="_blank">Support</a>';
		$links[] = '<a href="https://wordpress.org/support/view/plugin-reviews/jigoshop#postform" target="_blank">Rate Us</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">More plugins for Jigoshop</a>';
		return $links;
	});
}
