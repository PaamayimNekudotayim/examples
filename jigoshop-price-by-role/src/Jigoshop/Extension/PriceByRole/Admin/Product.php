<?php

namespace Jigoshop\Extension\PriceByRole\Admin;

use Jigoshop\Core\Types;
use Jigoshop\Entity\Product\Downloadable;
use Jigoshop\Entity\Product\External;
use Jigoshop\Entity\Product\Purchasable;
use Jigoshop\Entity\Product\Simple;
use Jigoshop\Entity\Product\Virtual;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration\Helper\Render;

class Product
{
	private $wpRoles;
	
	/**
	 * Product constructor.
	 */
	public function __construct()
	{
		global $wp_roles;
		$this->wpRoles = &$wp_roles;
		
		add_action('jigoshop\service\product\save', array($this, 'save'), 20);
		add_action('jigoshop\admin\product_variation\save', array($this, 'save'), 10);
		add_filter('jigoshop\admin\product\tabs', array($this, 'addTab'), 10, 2);
		add_filter('jigoshop\admin\product\menu', array($this, 'addMenu'), 10, 2);
		
		add_action('admin_enqueue_scripts', function () {
			$screen = get_current_screen();
			Styles::add('jigoshop.vendors.bs_switchd', JIGOSHOP_URL . '/assets/css/vendors/bs_switch.css',
				array('jigoshop.admin'));
			Scripts::add('jigoshop.vendors.bs_tab_trans_tooltip_collapsek',
				JIGOSHOP_URL . '/assets/js/vendors/bs_tab_trans_tooltip_collapse.js');
			Scripts::add('jigoshop.vendors.bs_switchk', JIGOSHOP_URL . '/assets/js/vendors/bs_switch.js');
			Scripts::add('jigoshop.admin.settings', JIGOSHOP_URL . '/assets/js/admin/settings.js');
			//Scripts::add('jigoshop.price_by_role.admin.product');
			
			Scripts::add('jigoshop.price_by_role.admin.product',
				JIGOSHOP_PRICE_BY_ROLE_URL . '/assets/js/admin/select.js',
				array('jquery', 'wp-util')
			);
			Scripts::localize('jigoshop.price_by_role.admin.product', 'jigoshop_price_by_role',
				$this->wpRoles->get_names());
		});
		
		add_action('jigoshop\product\tabs\variations', [$this, 'addRolePrototype']);
		add_action('jigoshop\admin\variation', [$this, 'addVariationForm'], 10, 2);
	}
	
	/**
	 * @param $menu
	 * @return array
	 */
	public function addMenu($menu)
	{
		$menu['price_by_role'] = array(
			'label' => __('Price by role', 'jigoshop_price_by_role'),
			'visible' => array(Simple::TYPE, Virtual::TYPE, External::TYPE, Downloadable::TYPE)
		);
		
		return $menu;
	}
	
	public function getDefaultData()
	{
		return [];
	}
	
	/**
	 * @param array $tabs
	 * @param \Jigoshop\Entity\Product $product
	 * @param bool $isVariable
	 * @return array
	 */
	public function addTab($tabs, $product, $isVariable = false)
	{
		$disable = get_post_meta($product->getId(), 'jigoshop_price_by_role_disable', true);
		$disable = $disable === '1';
		
		$data = get_post_meta($product->getId(), 'jigoshop_price_by_role_data', true);
		if (!is_array($data)) {
			$data = [];
		}
		$data = array_merge($this->getDefaultData(), $data);
		
		$roles = array();
		foreach ($this->wpRoles->get_names() as $key => $value) {
			$roles[$key] = array(
				'label' => $value,
				'disabled' => (isset($data[$key]))
			);
		}
		
		$tabs['price_by_role'] = Render::get('price_by_role', 'admin/product/tab', array(
			'product' => $product,
			'saved' => $data,
			'roles' => $roles,
			'howManyRoles' => sizeof($this->wpRoles->get_names()),
			'disable' => $disable,
			'namePrefix' => $isVariable ? sprintf('product[variation][%d][product]', $product->getId()) : 'product',
		));
		
		return $tabs;
	}
	
	/**
	 * Injects variation fields into form.
	 *
	 * @param \Jigoshop\Entity\Product\Variable\Variation $variation
	 * @param \Jigoshop\Entity\Product $product
	 */
	public function addVariationForm($variation, $product)
	{
		echo $this->addTab([], $product, true)['price_by_role'];
	}
	
	/**
	 * Injects role prototype into output.
	 */
	public function addRolePrototype($product)
	{
		if ($product->getType() == 'variable') {
			$namePrefix = 'product[variation][{{{data.id}}}][product]';
		} else {
			$namePrefix = 'product';
		}
		
		Render::output('price_by_role', 'admin/product/rolePrototype', [
			'namePrefix' => $namePrefix
		]);
	}
	
	public function validate($product)
	{
		return array_diff($product, array(""));
	}
	
	/**
	 * @param \Jigoshop\Entity\Product $product
	 */
	public function save($product)
	{
		if (isset($_POST['product']['type']) && $_POST['product']['type'] == 'variable') {
			$products = $_POST['product']['variation'];
		} else {
			if (!isset($_POST['product'])) {
				return;
			}
			
			$products[$product->getId()] = $_POST;
		}
		
		foreach ($products as $productId => $params) {
			if (isset($params['product']['price_by_role']) && is_array($params['product']['price_by_role'])) {
				$params = $params['product']['price_by_role'];
				
				if ($params['disable'] === 'on' || $params['disable'] === true || $params['disable'] === '1') {
					update_post_meta($productId, 'jigoshop_price_by_role_disable', 1);
					update_post_meta($productId, 'jigoshop_price_by_role_data', []);
				} else {
					$roles = [];
					foreach ($params as $param => $value) {
						if ($param == 'disable') {
							continue;
						}
						
						$roles[$param] = $value;
					}
					
					update_post_meta($productId, 'jigoshop_price_by_role_disable', 0);
					update_post_meta($productId, 'jigoshop_price_by_role_data', $roles);
				}
			}
		}
	}
}