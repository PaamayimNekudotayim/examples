<?php

namespace Jigoshop\Extension\PriceByRole\Admin;

use Jigoshop\Integration\Render;

class User
{
	/**
	 * User constructor.
	 */
	public function __construct()
	{
		add_action('show_user_profile', [$this, 'render']);
		add_action('edit_user_profile', [$this, 'render']);
		add_action('personal_options_update', [$this, 'save']);
		add_action('edit_user_profile_update', [$this, 'save']);
	}
	
	/**
	 * @param \WP_User $user
	 */
	public function render($user)
	{
		if (current_user_can('manage_jigoshop')) {
			$value = esc_attr(get_user_meta($user->ID, 'user_discount', true));
			Render::output('price_by_role', 'admin/user/table', ['value' => $value]);
		}
	}
	
	/**
	 * @param int $userId
	 */
	public function save($userId)
	{
		if (current_user_can('manage_jigoshop')) {
			$value = isset($_POST['user_discount']) ? (float)$_POST['user_discount'] : '';
			update_user_meta($userId, 'user_discount', $value);
		}
	}
}