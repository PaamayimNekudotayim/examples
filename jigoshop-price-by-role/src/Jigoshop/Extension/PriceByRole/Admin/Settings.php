<?php

namespace Jigoshop\Extension\PriceByRole\Admin;

use Jigoshop\Admin\Settings\TabInterface;
use Jigoshop\Helper\Scripts;
use Jigoshop\Integration;
use Jigoshop\Integration\Render;
use WPAL\Wordpress;

class Settings implements TabInterface
{
	const SLUG = 'price_by_role';
	
	private $wp;
	private $options;
	private $messages;
	
	public function __construct()
	{
		$this->wp = new Wordpress();
		
		$this->wp->addFilter('jigoshop\admin\settings', [$this, 'addSettingsTab']);
		$this->options = $this->getOptions();
		$this->messages = Integration::getMessages();
		
		$this->wp->addAction('admin_enqueue_scripts', function () {
			Scripts::add('jigoshop_price_by_role_settings', JIGOSHOP_PRICE_BY_ROLE_URL . '/assets/js/admin/settings.js', ['jquery']);
		});
		
		$this->wp->addAction('wp_ajax_jigoshop_price_by_role_global_add_rule', [$this, 'ajaxAddGlobalRule']);
		$this->wp->addAction('wp_ajax_jigoshop_price_by_role_global_remove_rule', [$this, 'ajaxRemoveGlobalRule']);
	}
	
	public function addSettingsTab($tabs)
	{
		$tabs[] = $this;
		
		return $tabs;
	}
	
	public function getSlug()
	{
		return self::SLUG;
	}
	
	public function getTitle()
	{
		return __('Price by role', 'jigoshop_price_by_role');
	}
	
	public function getOptions()
	{
		return array_merge($this->getDefaultOptions(), Integration::getOptions()->get($this->getSlug(), []));
	}
	
	private function getDefaultOptions()
	{
		return [
			'rules' => []
		];
	}
	
	private function getAvailableRoles($disabledRoles = [])
	{
		$roles = [];
		foreach ($this->wp->getRoles()->roles as $roleKey => $roleParams) {
			$roles[$roleKey] = [
				'label' => $roleParams['name'],
				'disabled' => in_array($roleKey, $disabledRoles)
			];
		}
		
		return $roles;
	}
	
	public function getSections()
	{
		return [
			[
				'id' => 'rules',
				'title' => __('Global rules', 'jigoshop_price_by_role'),
				'display' => [$this, 'renderRulesTable']
			]
		];
	}
	
	public function renderRulesTable()
	{
		return Render::get('price_by_role', 'admin/settings/table', [
			'availableRoles' => $this->getAvailableRoles(),
			'rules' => $this->options['rules']
		]);
	}
	
	public function validate($settings)
	{
		return $this->options;
	}
	
	private function sendJsonResponse($success, $error = '')
	{
		echo json_encode([
			'success' => $success,
			'error' => $error
		]);
		
		exit;
	}
	
	public function ajaxAddGlobalRule()
	{
		if (!isset($this->getAvailableRoles()[$_POST['role']])) {
			$this->sendJsonResponse(0, __('Invalid role selected.', 'jigoshop_price_by_role'));
		}
		
		if (isset($this->options['rules'][$_POST['role']])) {
			$this->sendJsonResponse(0, __('Rule with selected role already exists.', 'jigoshop_price_by_role'));
		}
		
		if (!preg_match('/^[\d\.]*%?$/', $_POST['value'])) {
			$this->sendJsonResponse(0, __('Invalid discount value.', 'jigoshop_price_by_role'));
		}
		
		$value = str_replace('%', '', $_POST['value']);
		if ($value <= 0) {
			$this->sendJsonResponse(0, __('Discount value cannot be 0 or less.', 'jigoshop_price_by_role'));
		}
		if ($value > 100) {
			$this->sendJsonResponse(0, __('Discount value cannot be more than 100%.', 'jigoshop_price_by_role'));
		}
		
		$this->options['rules'][$_POST['role']] = $value;
		
		$optionsService = Integration::getOptions();
		$optionsService->update($this->getSlug(), $this->options);
		$optionsService->saveOptions();
		
		$this->messages->addNotice(__('Rule added successfully.', 'jigoshop_price_by_role'));
		
		$this->sendJsonResponse(1);
		
		exit;
	}
	
	public function ajaxRemoveGlobalRule()
	{
		unset($this->options['rules'][$_POST['role']]);
		
		$optionsService = Integration::getOptions();
		$optionsService->update($this->getSlug(), $this->options);
		$optionsService->saveOptions();
		
		$this->messages->addNotice(__('Rule deleted successfully.', 'jigoshop_price_by_role'));
		
		$this->sendJsonResponse(1);
		
		exit;
	}
}