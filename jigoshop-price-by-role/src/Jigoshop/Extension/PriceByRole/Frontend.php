<?php

namespace Jigoshop\Extension\PriceByRole;

use Jigoshop\Entity\Product;
use Jigoshop\Integration;
use Jigoshop\Integration\Render;

class Frontend
{
	private $userRoles;
	
	public function __construct()
	{
		add_filter('jigoshop\product\get_price', array($this, 'updatePrice'), 10, 2);
	}
	
	public function setPriceByRole($price)
	{
		return Render::get('price_by_role', 'shop/product/price', array('price' => $price));
	}
	
	public function getUserRoles()
	{
		if ($this->userRoles == null) {
			global $current_user;
			$this->userRoles = $current_user->roles;
		}
		return $this->userRoles;
	}
	
	/**
	 * @param int $price
	 * @param Product $product
	 *
	 * @return float Price for product.
	 */
	public function updatePrice($price, $product)
	{
		if (!is_user_logged_in()) {
			return $price;
		}
		
		$enable = get_post_meta($product->getId(), 'jigoshop_price_by_role_disable', true);
		if ($enable === '1') {
			return $price;
		}
		
		$userRoles = $this->getUserRoles();
		$userDiscounts = [];
		$resultPrice = $price;
		if ($price !== '' || $price > 0) {
			$options = Integration::getOptions()->get('price_by_role', []);
			foreach ($userRoles as $userRole) {
				if (isset($options['rules'][$userRole])) {
					$userDiscounts[$userRole] = (float)$options['rules'][$userRole];
				}
			}
			
			if (!empty($userDiscounts)) {
				$resultPrice = $price - ($price * (max($userDiscounts) / 100));
			}
			
			$currentUserId = get_current_user_id();
			$userDiscount = (float)get_user_meta($currentUserId, 'user_discount', true);
			if ($userDiscount > 0) {
				$resultPrice = $price - ($price * ($userDiscount / 100));
			}
		}
		
		$data = get_post_meta($product->getId(), 'jigoshop_price_by_role_data', true);
		if (!is_array($data)) {
			$data = [];
		}
		
		$userPrices = [];
		foreach ($userRoles as $userRole) {
			if (isset($data[$userRole])) {
				$userPrices[] = $data[$userRole];
			}
		}
		
		if (!empty($userPrices)) {
			$resultPrice = min($userPrices);
		}
		
		if ($resultPrice < 0) {
			$resultPrice = 0.00;
		}
		
		return $resultPrice;
	}
}

new Frontend();