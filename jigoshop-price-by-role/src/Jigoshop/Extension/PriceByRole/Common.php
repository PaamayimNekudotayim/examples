<?php

namespace Jigoshop\Extension\PriceByRole;

use Jigoshop\Integration;

class Common
{
    /**
     * Common constructor.
     */
    public function __construct()
    {
        Integration::addPsr4Autoload(__NAMESPACE__.'\\', __DIR__);
    }
}

new Common();