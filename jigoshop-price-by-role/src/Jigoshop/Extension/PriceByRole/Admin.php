<?php
namespace Jigoshop\Extension\PriceByRole;

use Jigoshop\Integration\Render;

/**
 * Class Admin
 */
class Admin
{
	/**
	 * Admin constructor.
	 */
	public function __construct()
	{
		add_action('init', array($this, 'init'), 0);
		add_filter('plugin_action_links_' . plugin_basename(JIGOSHOP_PRICE_BY_ROLE_DIR . '/bootstrap.php'), array($this, 'actionLinks'));
        Render::addLocation('price_by_role', JIGOSHOP_PRICE_BY_ROLE_DIR);
	}

	/**
	 * Init Admin Component
	 */
	public function init()
	{
		new Admin\Settings();
		new Admin\Product();
		new Admin\User();
	}

	/**
	 * @param array $links
	 * @return array
	 */
	public function actionLinks($links)
	{

		$links[] = '<a href="https://wordpress.org/support/plugin/jigoshop-ecommerce/#new-post" target="_blank">Support</a>';
		$links[] = '<a href="https://wordpress.org/support/plugin/jigoshop-ecommerce/reviews/#new-post" target="_blank">Rate Us</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">More plugins for Jigoshop</a>';
		return $links;
	}
}

new Admin;