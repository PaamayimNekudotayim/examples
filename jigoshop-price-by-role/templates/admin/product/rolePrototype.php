<?php
 /**
  * @var string $namePrefix
  */
 ?>
<script type="text/template" id="tmpl-jigoshop_price_by_role_role_prototype">
    <div class="row clearfix jigoshop_price_by_role_role_container" data-role="{{{data.role}}}">
        <div class="col-xs-9 col-sm-6 col-md-10">
            <?php \Jigoshop\Admin\Helper\Forms::text(array(
                'label' => 'Price for {{{data.name}}}',
                'name' => '{{{data.namePrefix}}}[price_by_role][{{{data.role}}}]'
            )); ?>
        </div>
        <div class="col-xs-3 col-sm-6 col-md-2">
            <a class="btn btn-primary jigoshop_price_by_role_role_delete_role">Delete</a>
        </div>
    </div>
</script>