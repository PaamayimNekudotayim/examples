<?php
/**
 * @var $roles ,
 * @var $howManyRoles ,
 * @var $saved
 * @var string $namePrefix
 */

//\Jigoshop\Helper\Scripts::add();
?>
<h4><?= __('Price by roles settings', 'jigoshop_price_by_role'); ?></h4>
<small><?= __('Prices set below will override the global discounts set for the user roles as well as user individual discounts.', 'jigoshop_price_by_role'); ?></small>
<div class="col-md-12">
    <div class="row clearfix">
        <div class="col-xs-9 col-sm-8 col-md-10">
            <div class="form-group roles_field ">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="roles" class="col-xs-12 col-sm-2 margin-top-bottom-9"><?= __('Choose preffered group', 'jigoshop_price_by_role'); ?></label>
                        <div class="col-xs-12 col-sm-10 clearfix">
                            <div class="tooltip-inline-badge">
                            </div>
                            <div class="tooltip-inline-input">
                                <select id="roles" name="roles" title="Choose preferred group" style="width:100%;">
                                    <?php foreach($roles as $key => $value): ?>
                                        <option value="<?= $key ?>"<?php print $value['disabled'] and 'disabled' ?>><?= $value['label'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                /*<![CDATA[*/
                jQuery(function ($) {
                    $("select#roles").select2();
                    $("label[for='roles']").click(function () {
                        if (!$("#roles").select2("open")) {
                            $("#roles").select2("close");
                        }
                    });
                });
                /*]]>*/
            </script>
        </div>
        <div class="col-xs-3 col-sm-4 col-md-2">
            <a class="btn btn-primary jigoshop_price_by_role_add_role"><?= __('Add', 'jigoshop_price_by_role'); ?></a>
        </div>
    </div>
</div>
<div class="col-md-12">
    <?php foreach ($saved as $key => $save): ?>
        <div class="row clearfix jigoshop_price_by_role_role_container" data-role="<?php echo $key; ?>">
            <div class="col-xs-9 col-sm-6 col-md-10">
                <?php \Jigoshop\Admin\Helper\Forms::text(array(
                    'label' => 'Price for ' . $roles[$key]['label'],
                    'name' => sprintf($namePrefix.'[price_by_role][%s]', $key),
                    'value' => $save
                )); ?>
            </div>
            <div class="col-xs-3 col-sm-6 col-md-2">
                <a class="btn btn-primary jigoshop_price_by_role_role_delete_role"><?= __('Delete', 'jigoshop_price_by_role'); ?></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class="col-md-12 jigoshop_price_by_role_roles_container"></div>
<div class="clear"></div>