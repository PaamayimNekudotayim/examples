<?php
use Jigoshop\Integration\Helper\Render;

/**
 * @var \Jigoshop\Entity\Product $product
 * @var array $roles
 * @var array $howManyRoles
 * @var array $saved
 * @var string $enable
 * @var string $namePrefix
 */
if(!isset($namePrefix) || empty($namePrefix)) {
    $namePrefix = 'product';
}
?>
<fieldset style="margin-top: 12px" data-id="<?php echo $product->getId(); ?>">
    <?php \Jigoshop\Admin\Helper\Forms::checkbox(array(
        'id' => sprintf('jigoshop_price_by_role_disable_%s', $product->getId()),
        'name' => $namePrefix.'[price_by_role][disable]',
        'classes' => array('switch-medium', 'jigoshop_price_by_role_disable'),
        'label' => __('Disable Price by role', 'jigoshop_price_by_role'),
        'checked' => $disable
    )) ?>
    <div class="jigoshop_price_by_role_container">
        <div class="jigoshop_price_by_role_inner_container">
            <hr>
            <div class="lead"><?= __('Select price preferences for particular clients\' groups', 'jigoshop_price_by_role'); ?></div>
            <hr>
            <?php Render::output('price_by_role', 'admin/product/select',
                array('product' => $product, 'roles' => $roles, 'howManyRoles' => $howManyRoles, 'saved' => $saved, 'namePrefix' => $namePrefix)) ?>
        </div>
    </div>
</fieldset>