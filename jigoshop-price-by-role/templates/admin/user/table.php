<?php
/**
 *
 */
?>
<h3><?= __("User discount", "blank"); ?></h3>

<table class="form-table">
    <tr>
        <th><label for="user_discount"><?= __("Discount in percent", 'jigoshop_price_by_role'); ?></label></th>
        <td>
            <input type="number" name="user_discount" id="user_discount" value="<?= $value; ?>" class="regular-text" /><br />
            <span class="description"><?= __("Entered value will override the global discount set for this user role. This will work only for products with enabled price by role.", 'jigoshop_price_by_role'); ?></span>
        </td>
    </tr>
</table>
