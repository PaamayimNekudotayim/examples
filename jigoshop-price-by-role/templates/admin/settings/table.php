<?php
use Jigoshop\Admin\Helper\Forms;
use Jigoshop\Integration\Render;
?>

<table class="table table-striped">
	<thead>
		<tr>
			<th><?php echo __('Role', 'jigoshop_price_by_role'); ?></th>
			<th><?php echo __('Discount', 'jigoshop_price_by_role'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($rules as $ruleName => $ruleValue) {
			Render::output('price_by_role', 'admin/settings/rule', [
				'ruleName' => $ruleName,
				'ruleValue' => $ruleValue
			]);
		}
		?>
	</tbody>
</table>

<div class="row clearfix">
	<div class="col-sm-4">
		<?php 
		Forms::select([
			'name' => 'jigoshop_price_by_role_role',
			'id' => 'jigoshop_price_by_role_role',
			'label' => __('Role', 'jigoshop_price_by_role'),
			'options' => $availableRoles
		]);
		?>
	</div>
	<div class="col-sm-4">
		<?php
		Forms::text([
			'name' => 'jigoshop_price_by_role_value',
			'id' => 'jigoshop_price_by_role_value',
			'label' => __('Discount (percent)', 'jigoshop_price_by_role')
		]);
		?>
	</div>
	<div class="col-sm-2">
		<a href="#" id="jigoshop_price_by_role_add_rule_button" class="btn btn-default">
			<span class="glyphicon glyphicon-plus"></span>
			<?php echo __('Add', 'jigoshop_price_by_role'); ?>
		</a>
	</div>
</div>
<h4><?= __('Price by role functionality is enabled for all products by default. To disable it, go to Price by Role tab on individual product edit page.', 'jigoshop_pice_by_role'); ?></h4>