<tr data-role="<?php echo $ruleName; ?>">
	<td><?php echo $ruleName; ?></td>
	<td><?php echo $ruleValue; ?>%</td>
	<td>
		<a href="#" class="jigoshop_price_by_role_remove_rule_button btn btn-default">
			<span class="glyphicon glyphicon-remove"></span>

			<?php echo __('Remove', 'jigoshop_price_by_role'); ?>
		</a> 
	</td>
</tr>