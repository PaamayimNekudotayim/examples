<?php
/**
 * Plugin name: Jigoshop Price by Role Premium
 * Description: This plugin allows you to set custom product prices for different user roles.
 * Author:  @vasilguruli | JigoLtd
 * Version: 4.0
 * Plugin URI: https://www.jigoshop.com/product/jigoshop-price-by-role/
 * Author URI: http://jigoshop.com
 */

// Define plugin name
define('JIGOSHOP_PRICE_BY_ROLE_NAME', 'Jigoshop Price by Role');

add_action('plugins_loaded', function () {
	load_plugin_textdomain('jigoshop_price_by_role', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_PRICE_BY_ROLE_DIR', dirname(__FILE__));
		// Define plugin URL for assets
		define('JIGOSHOP_PRICE_BY_ROLE_URL', plugins_url('', __FILE__));
		//Check version.
		if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_PRICE_BY_ROLE_NAME, '2.0')) {
			return;
		}
//		//Check license.
//		$licence = new \Jigoshop\Licence(__FILE__, '', 'http://www.jigoshop.com');
//		if (!$licence->isActive()) {
//			return;
//		}
		//Init components.
			require_once(JIGOSHOP_PRICE_BY_ROLE_DIR . '/src/Jigoshop/Extension/PriceByRole/Common.php');
		if (is_admin()) {
			require_once(JIGOSHOP_PRICE_BY_ROLE_DIR . '/src/Jigoshop/Extension/PriceByRole/Admin.php');
		} else {
			require_once(JIGOSHOP_PRICE_BY_ROLE_DIR . '/src/Jigoshop/Extension/PriceByRole/Frontend.php');
		}
	} elseif (class_exists('jigoshop')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_PRICE_BY_ROLE_DIR', dirname(__FILE__) . '/Jigoshop1x');
		// Define plugin URL for assets
		define('JIGOSHOP_PRICE_BY_ROLE_URL', plugins_url('', __FILE__) . '/Jigoshop1x');
		//Check version.
		if (jigoshop_add_required_version_notice(JIGOSHOP_PRICE_BY_ROLE_NAME, '1.17')) {
			return;
		}
		//Check license.
		$licence = new jigoshop_licence_validator(__FILE__, '14075', 'http://www.jigoshop.com');
		if (!$licence->is_licence_active()) {
			return;
		}
		//Init components.
		require_once(JIGOSHOP_PRICE_BY_ROLE_DIR . '/jigoshop-price-by-role.php');
	} else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.', 'jigoshop_price_by_role'),
				JIGOSHOP_PRICE_BY_ROLE_NAME, JIGOSHOP_PRICE_BY_ROLE_NAME);
			echo '</p></div>';
		});
	}
});
