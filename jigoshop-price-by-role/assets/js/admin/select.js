jQuery(function() {
    jQuery('input[type="checkbox"]').each(function(index, element) {
        if(jQuery(element).hasClass('jigoshop_price_by_role_disable')) {
            if(jQuery(element).bootstrapSwitch('state')) {
                jQuery(element).parents('fieldset').find('.jigoshop_price_by_role_container').hide();
            }

            jQuery(element).on('switchChange.bootstrapSwitch', function(e) {
                let target, container;

                target = e.delegateTarget;
                container = jQuery(target).parents('fieldset');

                if(jQuery(target).is(':checked')) {
                    jQuery(container).find('.jigoshop_price_by_role_container').fadeOut('slow');
                }
                else {
                    jQuery(container).find('.jigoshop_price_by_role_container').fadeIn('slow');
                }
            });
        }
    });

    jQuery('.jigoshop_price_by_role_add_role').click(function(e) {
        let target, container, roleValue, roleName, template, namePrefix;

        e.preventDefault();

        template = wp.template('jigoshop_price_by_role_role_prototype');

        target = e.delegateTarget;
        container = jQuery(target).parents('fieldset');
        roleValue = jQuery(container).find('select').val();
        roleName = jQuery(container).find('option[value="' + roleValue + '"]').text();
        if(!roleName) {
            return;
        }

        jQuery(container).find('select').find('option[value="' + roleValue + '"]').attr('disabled', 'disabled');

        if(jQuery('#product-type').val() == 'variable') {
            namePrefix = 'product[variation][' + jQuery(container).data('id') + '][product]';
        }
        else {
            namePrefix = 'product';
        }

        jQuery(container).find('.jigoshop_price_by_role_roles_container').append(template({
            id: jQuery(container).data('id'),
            role: roleValue,
            name: roleName,
            namePrefix: namePrefix
        }));

        jigoshopPriceByRoleReloadBinds();
    });

    jigoshopPriceByRoleReloadBinds();
});

function jigoshopPriceByRoleReloadBinds() {
    jQuery('.jigoshop_price_by_role_role_delete_role').each(function(index, element) {
        jQuery(element).unbind('click').click(function(e) {
            let roleContainer, roleValue;

            e.preventDefault();

            roleContainer = jQuery(e.delegateTarget).parents('.jigoshop_price_by_role_role_container');
            roleValue = jQuery(roleContainer).data('role');

            jQuery(roleContainer).parents('fieldset').find('option[value="' + roleValue + '"]').removeAttr('disabled');
            jQuery(roleContainer).remove();
        });
    });
}