jQuery(function() {
	jQuery('#jigoshop_price_by_role_add_rule_button').click(function(e) {
		e.preventDefault();

		jQuery('#jigoshop_price_by_role_add_rule_button').attr('disabled', 'disabled');

		jQuery.post(ajaxurl, {
			action: 'jigoshop_price_by_role_global_add_rule',
			role: jQuery('#jigoshop_price_by_role_role').val(),
			value: jQuery('#jigoshop_price_by_role_value').val()
		}, function(data) {
			if(data.success == 1) {
				location.href = document.URL;
			}
			else {
				jigoshop.addMessage('danger', data.error, 6000);
			}

			jQuery('#jigoshop_price_by_role_add_rule_button').removeAttr('disabled');
		}, 'json');
	});

	jQuery('.jigoshop_price_by_role_remove_rule_button').click(function(e) {
		e.preventDefault();

		jQuery.post(ajaxurl, {
			action: 'jigoshop_price_by_role_global_remove_rule',
			role: jQuery(e.delegateTarget).parents('tr').data('role')
		}, function(data) {
			location.href = document.URL;
		}, 'json');
	});
});