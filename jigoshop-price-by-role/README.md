Jigoshop Price By Role
======================

This plugin allows you to set custom product prices for different user roles.
Works with every product type different than variable.
If a price for particular role is higher than regular for everyone,
plugin will show the regular one.

### Installation

1. Download the plugin file
2. Unzip the file into a folder on your hard drive
3. Upload the `/jigoshop-price-by-role/` folder to the `/wp-content/plugins/` folder on your site
4. Visit the plugins page in admin and activate it
