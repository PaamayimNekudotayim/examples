<?php
use TS\Plugin\Settings;
	/**@var $page string*/
?>
<div class="wrap">
	<?php settings_errors('settings_saved'); ?>
    <form method="post" action="options.php">
		<?php
            switch ($page) {
                case Settings::$sections['welcome'] :
	                do_settings_sections(Settings::$sections['welcome']);
	                settings_fields(Settings::$sections['welcome']);
	                break;
                case Settings::$sections['general'] :
                    do_settings_sections(Settings::$sections['general']);
                    settings_fields(Settings::$sections['general']);
                    break;
                case Settings::$sections['json'] :
                    do_settings_sections(Settings::$sections['json']);
                    settings_fields(Settings::$sections['json']);
                    break;
                default :
                    echo '';
                    break;
            }
            
            if (isset($_GET['tab'])) {
                Settings::submitButton();
            }
		?>
    </form>
</div>