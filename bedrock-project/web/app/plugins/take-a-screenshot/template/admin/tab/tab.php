<?php use TS\Plugin\Tabs; ?>
<div class="wrap" style="overflow: hidden">
    <h1 class="settings"><?php echo sprintf(__('Take A Screenshot')); ?></h1>
    <nav class="nav-tab-wrapper">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="settingsBar">
				<?php foreach (Tabs::renderTabs() as $tabs): ?>
                    <a class="<?php echo $tabs['class'] . $tabs['active'] ?>"
                       href="<?php echo esc_url(admin_url($tabs['href'] . $tabs['tab'])); ?>">
						<?php echo esc_html($tabs['page']) ?>
                    </a>
				<?php endforeach; ?>
            </div>
        </div>
    </nav>
    <div class="clear"></div>
    <div class="tab-content">
        <div class="tab-panel">
			<?php
			try {
				Tabs::addTabSettings();
			} catch (Exception $e) {
			    // Tab not found
			}
			?>
        </div>
    </div>
</div>
<div class="clear"></div>
