<?php

namespace TS\Plugin;

class Tabs
{
	/**
	 * @return array tabs
	 */
	public static function renderTabs()
	{
		return [
			[
				'tab' => 'ts',
				'href' => 'admin.php?page=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['page']) && $_GET['page'] == 'ts' && !isset($_GET['tab']) ? 'nav-tab-active' : ''),
				'page' => __('Welcome'),
			],
			[
				'tab' => 'general',
				'href' => 'admin.php?page=ts&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] == 'general' ? 'nav-tab-active' : ''),
				'page' => __('General'),
			],
			[
				'tab' => 'json_files',
				'href' => 'admin.php?page=ts&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] == 'json_files' ? 'nav-tab-active' : ''),
				'page' => __('Json Files'),
			],
		];
	}
	
	
	/**
	 * @throws \Exception
	 */
	public static function addTabSettings()
	{
		if (isset($_GET['page']) && $_GET['page'] == 'ts' && !isset($_GET['tab'])) {
			Forms::getInstance()->renderSettings(Settings::$sections['welcome']);
		}
		if (isset($_GET['tab'])) {
			switch ($_GET['tab']) {
				case 'general' :
					Forms::getInstance()->renderSettings(Settings::$sections['general']);
					break;
					case 'json_files' :
					Forms::getInstance()->renderSettings(Settings::$sections['json']);
					break;
				default :
					Forms::getInstance()->renderSettings('');;
			}
		}
	}
}