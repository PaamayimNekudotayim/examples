<?php

namespace TS\Plugin;


use Exception;
use ReflectionMethod;
use TS\Container\Service;
use TS\Plugin\ErrorException\ErrorException;

/**
 * @borrowed form Krzysztof Kassowski
 * @link https://github.com/jigoshop/Jigoshop-eCommerce/blob/master/src/Jigoshop/Container/Services.php
 * @smallchange @vasilguruli
 */
class Services implements Service
{
	/** @var array $services */
	public $services = [];
	
	/** @var array $services */
	public $args = [];
	
	/**
	 * @param $key
	 * @param $name
	 * @param array $params
	 * @throws Exception
	 */
	public function setDetails($key, $name, array $params)
	{
		if ($this->keyExists($key)) {
			throw new Exception(sprintf('Details info for << %s >> are already set.', $key));
		}
		
		$serviceDetails = [
			'name' => $name,
			'params' => $params,
		];
		
		$this->services[$key] = $serviceDetails;
	}
	
	/**
	 * @param $key
	 * @param $name
	 */
	public function set($key, $name)
	{
		$this->services[$key] = $name;
	}
	
	/**
	 * @param $key
	 * @return mixed
	 */
	public function get($key)
	{
		return $this->services[$key];
	}
	
	/**
	 * @param string $key
	 * @return string
	 */
	public function getClassName($key)
	{
		return $this->services[$key]['name'];
	}
	
	/**
	 * @param $key
	 * @return bool
	 */
	public function keyExists($key)
	{
		return isset($this->services[$key]);
	}
	
	/**
	 * @return mixed|void
	 */
	public function addService()
	{
		$className = '';
		foreach ($this->services as $key => $value) {
			if (!empty($key)) {
				$className = $this->getClassName($key);
			}
			try{
				$instance = new \ReflectionClass($className);
				$instance->newInstanceArgs($value['params']);
				
			} catch(Exception $e) {
				$errorMessage = [];
				/** @noinspection PhpUnusedLocalVariableInspection  */
				$errors = new ErrorException();
				$errorMessage[] = $errors->displayErrors(sprintf(__('Class <strong><< %s >></strong> Could not be found'), $className));
				require_once TS_ERROR_TEMPLATE_DIR . '/error.php';
				exit();
			}
		}
	}
}