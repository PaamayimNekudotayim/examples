<?php

namespace TS\Plugin;


use TS\Plugin\Template\Template;

class Forms
{
	private static $instance;
	
	/**
	 * @disclaimer!
	 * As Singleton pattern is known as "Anti Pattern" for obvious reasons
	 * I DO LIKE to use it in my code. "Matt Zanstra's PHP Objects, Patterns and Practice -> Singleton Pattern"
	 * Pro's are many, read them please before judge!
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new Forms();
		}
		
		return self::$instance;
	}
	
	/**
	 * @param $page
	 * @throws \Exception
	 */
	public function renderSettings($page)
	{
		$this->currentUserCanManageOptions();
		$this->isAdmin();
		$this->settingsUpdated();
		
		Template::output('admin/forms/form', [
			'page' => $page
		]);
	}
	
	public function currentUserCanManageOptions()
	{
		if (!current_user_can('manage_options')) {
			die ('You are not allowed to be here!');
		}
	}
	
	public function isAdmin()
	{
		if (!is_admin()) {
			die ('Hm, it seems you don\'t have permissions to be here');
		}
	}
	
	public function settingsUpdated()
	{
		if (isset($_GET['settings-updated'])) {
			add_settings_error('settings_saved', 'settings_saved', __('Settings Saved'),
				'updated');
		}
	}
}