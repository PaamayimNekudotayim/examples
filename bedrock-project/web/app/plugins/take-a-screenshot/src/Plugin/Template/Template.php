<?php

namespace TS\Plugin\Template;


use Exception;
use TS\Plugin\ErrorException\ErrorException;

class Template
{
	public static function locateTemplate($template)
	{
		$template = str_replace('\\', '/', $template);
		
		$file = str_replace('\\', '/', TS_TEMPLATE_DIR). '/' . $template . '.php';
		if (!file_exists($file)) {
			$errorMessage = [];
			/** @noinspection PhpUnusedLocalVariableInspection  */
			$errors = new ErrorException();
			$errorMessage[] = $errors->displayErrors(sprintf(__('File <strong><< %s >></strong> Could not be found'), $file));
			require_once TS_ERROR_TEMPLATE_DIR . '/error.php';
			exit();
		}
		
		return $file;
	}
	
	/**
	 * @param $template
	 * @param array $env
	 * @throws Exception
	 */
	public static function output($template, array $env)
	{
		$file = self::locateTemplate($template);
		extract($env);
		/** @noinspection PhpIncludeInspection */
		require($file);
	}
	
	/**
	 * @param $template
	 * @param array $env
	 * @return false|string
	 * @throws Exception
	 */
	public static function get($template, array $env)
	{
		ob_start();
		
		self::output($template, $env);
		
		return ob_get_clean();
	}
}