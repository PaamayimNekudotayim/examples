<?php

namespace TS\Plugin\Helper;


trait SanitizeData
{
	/**
	 * @param $key
	 * @param $data
	 * @param string $function
	 * @return array|string
	 */
	public function sanitize($key, $data, $function = 'esc_attr')
	{
		$output = [];
		switch ($function) {
			case 'esc_attr' :
				if (isset($data[$key])) {
					$output = esc_attr($data[$key]);
				}
			break;
			case 'esc_url' :
				if (isset($data[$key])) {
					$output = esc_url($data[$key]);
				}
			break;
			default :
				return 'esc_attr';
		}
		
		return $output;
	}
}