<?php


namespace TS\Plugin\Helper;


class Sanitize
{
	use SanitizeData;
	
	public function __construct()
	{
	
	}
	
	
	/**
	 * @param $key
	 * @param $data
	 * @param $function
	 * @return array|string
	 */
	public function escAttrApiKey($key, $data, $function)
	{
		return $this->sanitize($key, $data, $function);
	}
	
	
	/**
	 * @param $key
	 * @param $data
	 * @param $function
	 * @return array|string
	 */
	public function escUrlAPiUrl($key, $data, $function)
	{
		return $this->sanitize($key, $data, $function);
	}
	
	
	/**
	 * @param $key
	 * @param $data
	 * @param $function
	 * @return array|string
	 */
	public function escAttrExtensionOutput($key, $data, $function)
	{
		return $this->sanitize($key, $data, $function);
	}
	
	
	public function escUrlScreenshotUrl($key, $data, $function)
	{
		return $this->sanitize($key, $data, $function);
	}
}