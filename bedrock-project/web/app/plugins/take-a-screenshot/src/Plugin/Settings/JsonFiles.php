<?php

namespace TS\Plugin\Settings;


class JsonFiles
{
	public function __construct()
	{
	}
	
	/**
	 * @param $args
	 * @param $options
	 */
	public static function jsonFilesSettings($args, $options)
	{
		switch ($args['value']) {
			case 'extension_output' :
				if (!empty($args['options'])) {
					$outputExt = isset($options['extension_output']) ? $options['extension_output'] : '';
					foreach ($args['options'] as $key => $value) {
						echo '<label class="form-control">' . esc_attr($value) . '</label>';
						echo '<input type="radio" class="form-control" name="ts_json_files[' . esc_attr($args['value'])  . ']"
						 value="' . esc_attr($key) . '" ' . checked($outputExt, $key, false) . ' />';
					}
				}
				break;
			case 'screenshot_url' :
				$screenshotUrl = isset($options['screenshot_url']) ? $options['screenshot_url'] : '';
				echo '<input  type="text" size="75" class="form-control" id="' . esc_url($args['value']) . '"
		            name="ts_json_files[' . $args['value'] . ']" value="' . esc_url($screenshotUrl) . '" />';
				echo '<p class="description">
                         Here you can provide URL for screenshot.<a target="_blank" href="' . esc_url('https://phantomjscloud.com/docs/') . '">More info here</a>
                      </p>';
				break;
			default:
				echo '';
		}
	}
}