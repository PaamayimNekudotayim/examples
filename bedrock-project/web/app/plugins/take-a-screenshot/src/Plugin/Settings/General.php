<?php

namespace TS\Plugin\Settings;


class General
{
	private function __construct()
	{
	}
	
	/**
	 * @param $args
	 * @param $options
	 */
	public static function generalSettings($args, $options)
	{
		switch ($args['value']) {
			case 'phantomjs_api_key' :
				$apiKey = isset($options['phantomjs_api_key']) ? $options['phantomjs_api_key'] : '';
				echo '<input  type="text" size="75" class="form-input" id="' . esc_attr($args['value']) . '"
		            name="ts_settings[' . $args['value'] . ']" value="' . esc_attr($apiKey) . '" />';
				echo '<p class="description">
                            API key (e.g ak-012345-abcde-012345-abcde-012345) for PhantomJs Cloud <strong>If it will be empty default API key will be used. </strong><a href="' . esc_url('https://phantomjscloud.com/docs/') . '">More info here</a>
                      </p>';
				break;
			case 'phantom_js_api_url' :
				$apiUrl = isset($options['phantom_js_api_url']) ? $options['phantom_js_api_url'] : '';
				echo '<input  type="text" size="75" class="form-control" id="' . esc_url($args['value']) . '"
		            name="ts_settings[' . $args['value'] . ']" value="' . esc_url($apiUrl) . '" />';
				echo '<p class="description">
                        API Url for PhantomJs Cloud.<a href="' . esc_url('https://phantomjscloud.com/docs/') . '">More info here</a>
                      </p>';
				break;
			default :
				echo '';
		}
	}
}