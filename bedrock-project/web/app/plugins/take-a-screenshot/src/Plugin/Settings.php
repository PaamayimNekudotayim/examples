<?php

namespace TS\Plugin;

use TS\Plugin\Helper\Sanitize;
use TS\Plugin\Settings\General;
use TS\Plugin\Settings\JsonFiles;
use TS\Plugin\Template\Template;

class Settings extends SettingsSection
{
	/**@var array $options */
	private $options;
	/**@var string $url */
	private $url;
	private $phantomJsCloudApiUrl = 'https://phantomjscloud.com/api/browser/v2/';
	
	/**@var Sanitize $sanitize*/
	public $sanitize;
	
	
	
	public function __construct()
	{
		add_action('admin_menu', [$this, 'tsSettingsMenu']);
		add_action('admin_init', [$this, 'tsSettings']);
		
		static::$sections = [
			'welcome' => 'welcome',
			'general' => 'ts_general',
			'json' => 'json_files',
		];
		
		$this->sanitize = new Sanitize();
	}
	
	public function tsSettingsMenu()
	{
		add_menu_page('Take a Screenshot', 'Take a Screenshot',
			'manage_options', 'ts', [$this, 'tsMenu'], 'dashicons-camera');
	}
	
	/**
	 * @throws \Exception
	 */
	public function tsMenu()
	{
		$this->options = get_option('ts_settings');
		Template::output('admin/tab/tab', []);
	}
	
	public function tsSettings()
	{
		// Welcome
		$this->registerSettings(static::$sections['welcome'], 'ts_welcome');
		$this->addSettingsSection('ts_welcome', '', [$this, 'welcome'], static::$sections['welcome']);
		$this->checkOption('ts_welcome');
		
		// General
		$this->registerSettings(static::$sections['general'], 'ts_settings', $this, 'sanitize');
		$this->addSettingsSection('ts_ID', '', [$this, 'tsGeneral'], static::$sections['general']);
		$this->checkOption('ts_settings');
		
		// Json Files
		$this->registerSettings(static::$sections['json'], 'ts_json_files', $this, 'sanitize');
		$this->addSettingsSection('ts_json', '', [$this, 'tsJson'], static::$sections['json']);
		$this->checkOption('ts_json_files');
		
		/***<<GENERAL SETTINGS>>***/
		$this->addSettingsField('phantom_js_api_key', __('PhantomJs API Key'), [$this, 'generalSettings'],
			self::$sections['general'],
			'ts_ID', [
				'value' => 'phantomjs_api_key'
			]);
		$this->addSettingsField('phantom_js_api_url', __('PhantomJs API Url'), [$this, 'generalSettings'],
			self::$sections['general'],
			'ts_ID', [
				'value' => 'phantom_js_api_url'
			]);
		
		/****<<JSON FILES>>*/
		$this->addSettingsField('extension_output', __('Output file extension'), [$this, 'jsonFiles'],
			self::$sections['json'],
			'ts_json', [
				'value' => 'extension_output',
				'output' => 'extensions',
				'id' => 'extensions-list',
				'options' => [
					'jpg' => 'JPG',
					'png' => 'PNG',
					'pdf' => 'PDF',
				]
			]);
		$this->addSettingsField('screenshot_url', __('URL to Screenshot'), [$this, 'jsonFiles'],
			self::$sections['json'],
			'ts_json', [
				'value' => 'screenshot_url',
			]);
		
		/***<<JSON FILES>>***/
	}
	
	/**
	 * @param $data
	 * @return array
	 */
	public function sanitize($data)
	{
		$outputData = [
			// General
			'phantom_js_api_url' => $this->sanitize->escUrlAPiUrl('phantom_js_api_url', $data, 'esc_url'),
			'phantomjs_api_key' => $this->sanitize->escAttrApiKey('phantomjs_api_key', $data, 'esc_attr'),
			// Json files
			'extension_output' => $this->sanitize->escAttrExtensionOutput('extension_output', $data, 'esc_attr'),
			'screenshot_url' => $this->sanitize->escUrlScreenshotUrl('screenshot_url', $data, 'esc_url')
		];
		
		return $outputData;
	}
	
	public function tsGeneral()
	{
		echo '<h3 class="settings-title">' . __('General Settings') . '</h3>';
	}
	
	public function tsJson()
	{
		echo '<h3 class="settings-title">' . __('Json Files') . '</h3>';
		echo '<p class="settings-description">' . __('Here you can generate json files') . '</p>';
	}
	
	/**
	 * @throws \Exception
	 */
	public function welcome()
	{
		Template::output('admin/hello', []);
	}
	
	/**
	 * @param $args
	 */
	public function generalSettings($args)
	{
		$this->options = get_option('ts_settings');
		General::generalSettings($args, $this->options);
	}
	
	/**
	 * @param $args
	 */
	public function jsonFiles($args)
	{
		$this->options = get_option('ts_json_files');
		JsonFiles::jsonFilesSettings($args, $this->options);
	}
}