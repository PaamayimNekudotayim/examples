<?php

namespace TS\Plugin;

use TS\Container\Plugin;
use TS\Plugin\Template\Template;

abstract class TSInit extends Services implements Plugin
{
	/**@var string $phantomJsApiKey*/
	protected $phantomJsApiKey = '';
	
	/**@var string $phantomjsAPiUrl*/
	protected $phantomjsAPiUrl = '';
	
	/**@var string $phantomjsCloudUrl*/
	protected $phantomjsCloudUrl = 'https://phantomjscloud.com/';
	
	/**@var array $errors*/
	public $errors = [];
	
	/**@var array $tsServices*/
	protected $tsServices = [];
	
	public function __construct()
	{
		$this->phantomJsApiKey = get_option('_phantomjs_api_key');
		$this->phantomjsAPiUrl = get_option('_phantomjs_api_url');
		
		$this->tsServices = [
			'settings' => '\\TS\\Plugin\\Settings',
			'tabs' => '\\TS\\Plugin\\Tabs',
			'forms' => '\\TS\\Plugin\\Forms'
		];
	}
	
	public function actions() {
		// TODO: Implement actions() method.
	}
	
	public  function notices()
	{
		add_action('admin_notices', [$this, 'adminNotices']);
		$this->apiKeyNotice();
		$this->minimumPhpVersionNotice();
	}
	
	
	public function deactivate(){}
	
	public function activate()
	{
		$this->options();
	}
	
	public function adminNotices()
	{
		if (!empty($this->errors)) {
			foreach ($this->errors as $error) {
				echo sprintf('%s', $error);
			}
		}
	}
	
	private function options()
	{
		add_option('_phantomjs_api_key');
		add_option('_phantomjs_api_url');
	}
	
	/**
	 * @return array
	 */
	public function apiKeyNotice()
	{
		if ('' === $this->phantomJsApiKey) {
			$this->errors[] = sprintf('<div id="message" class="notice notice-warning is-dismissible"><p>
 				<strong><< %s >></strong> requires PhantomJs APIKey to work properly. You can get it
 				<a href="%s" target="_blank">here</a>. Or go to the <a href="%s">Settings</a> page and set it.</p></div>',
				TS_NAME, esc_url($this->phantomjsCloudUrl), esc_url(admin_url('admin.php?page=ts')));
		}
		
		return $this->errors;
	}
	
	/**
	 * @return array
	 */
	public function minimumPhpVersionNotice()
	{
		if (version_compare(phpversion(), "7.0", "<=")) {
			$this->errors[] = sprintf('<div id="message" class="error notice-error is-dismissible"><p>
				<strong><< %s >> </strong> requires minimum version of PHP <strong>%s</strong>
				 Your current version is: <strong>%s</strong></p></div>', TS_NAME, TS_PHP_MIN_VERSION, PHP_VERSION);
		}
		
		return $this->errors;
	}
}