<?php

namespace TS\Plugin\ErrorException;


use Throwable;

class ErrorException extends \Exception
{
	protected $message;
	protected $code;
	private $errors = [];
	
	public function __construct($message = "", $code = 0, Throwable $previous = null)
	{
		$this->message = $message;
		$this->code = $code;
		
		parent::__construct($message, $code, $previous);
	}
	
	private function showMessage($message)
	{
		$errors[] = [
			'message' => sprintf('%s', $message),
			'file' => sprintf('%s', $this->getFile()),
			'code' => sprintf('%s', $this->getCode()),
			'trace' => $this->getTrace(),
		];
		
		return $errors;
	}
	
	/**
	 * @param $message
	 * @return array|int|mixed|string
	 */
	public function displayErrors($message)
	{
		$error = [];
		if (!empty($this->showMessage($message))) {
			foreach($this->showMessage($message) as $error => $errorMessage) {
				$error = $errorMessage;
			}
		}
		
		return $error;
	}
}