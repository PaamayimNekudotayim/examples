<?php

namespace TS\Plugin;


use ReflectionMethod;
use TS\Plugin\Services;

class TS extends TSInit
{
	
	/**@var $services array */
	public $services = [];
	
	public function register()
	{
		add_action('plugins_loaded', [$this, 'initPlugin']);
	}
	
	/**
	 * @throws \Exception
	 */
	public function initPlugin()
	{
		try {
			$this->setDetails('ts.activate', $this->tsServices['settings'], ['run']);
			$this->setDetails('ts.tabs', $this->tsServices['tabs'], []);
			$this->setDetails('ts.forms', $this->tsServices['forms'], []);
		} catch (\ErrorException $e) {
			//
		}
		
		$this->addService();
	}
	
	/**
	 *
	 */
	public function _links()
	{
		add_filter('plugin_action_links_take-a-screenshot/bootstrap.php',
			function ($links) {
				$links[] = '<a href="' . esc_url(admin_url('admin.php?page=ts')) . '">Settings</a>';
				$links[] = '<a href="#" target="_blank">Documentation</a>';
				$links[] = '<a href="#" target="_blank">Support</a>';
				
				return $links;
			});
	}
}