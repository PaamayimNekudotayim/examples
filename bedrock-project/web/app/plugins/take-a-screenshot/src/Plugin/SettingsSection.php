<?php

namespace TS\Plugin;


use TS\Container\Section;

abstract class SettingsSection implements Section
{
	public static $sections = [];
	
	/**
	 * @param $optionGroup
	 * @param $optionName
	 * @param $object
	 * @param $method
	 */
	public function registerSettings($optionGroup, $optionName, $object = null, $method = null)
	{
		register_setting($optionGroup, $optionName, [$object, $method]);
	}
	
	/**
	 * @param $ID
	 * @param $title
	 * @param array $handler
	 * @param $page
	 */
	public function addSettingsSection($ID, $title, array $handler, $page)
	{
		add_settings_section($ID, $title, $handler, $page);
	}
	
	/**
	 * @param $ID
	 * @param $title
	 * @param array $handler
	 * @param $page
	 * @param $section
	 * @param array $data
	 */
	public function addSettingsField($ID, $title, array $handler, $page, $section, $data = [])
	{
		add_settings_field($ID, $title, $handler, $page, $section, $data);
	}
	
	/**
	 * @param $option
	 */
	protected function checkOption($option)
	{
		if (false === get_option($option)) {
			add_option($option);
		}
	}
	
	/**
	 * @param null $text
	 * @param string $type
	 * @param string $name
	 * @param bool $wrap
	 * @param null $other_attributes
	 */
	public static function submitButton($text = null, $type = 'primary', $name = 'submit', $wrap = true, $other_attributes = null)
	{
		return submit_button($text, $type, $name, $wrap, $other_attributes);
	}
}