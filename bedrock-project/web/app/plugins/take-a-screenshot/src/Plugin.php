<?php

namespace TS;

use TS\Container\Plugin as PluginContainer;
use TS\Plugin\TS;

class Plugin
{
	/**
	 * @return TS
	 */
	public static function create() : PluginContainer
	{
		static $plugin = null;
		
		if (null === $plugin) {
			$plugin = new TS();
		}
		
		return $plugin;
	}
}