<?php

namespace TS\Container;


interface Section
{
	public function registerSettings($optionGroup, $optionName, $object = null, $method = null);
	
	public function addSettingsSection($ID, $title, array $handler, $page);
	
	public function addSettingsField($ID, $title, array $handler, $page, $section, $data = []);
}