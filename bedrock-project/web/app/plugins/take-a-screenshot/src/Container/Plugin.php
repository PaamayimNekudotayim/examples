<?php


namespace TS\Container;


interface Plugin extends Activate, Deactivate, Register
{
	public function actions();
}