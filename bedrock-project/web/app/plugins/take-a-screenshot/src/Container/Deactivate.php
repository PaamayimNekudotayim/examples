<?php

namespace TS\Container;


interface Deactivate
{
	/**
	 * Deactivate Plugin
	 */
	public function deactivate();
}