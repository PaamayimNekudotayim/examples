<?php

namespace TS\Container;


interface Activate
{
	public function activate();
	
	public  function notices();
	
	public function apiKeyNotice();
	
	public function minimumPhpVersionNotice();
}