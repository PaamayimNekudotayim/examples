<?php

namespace TS\Container;


use ReflectionMethod;

interface Service
{
	/**
	 * @param $key
	 * @param $name
	 * @param array $params
	 * @return mixed
	 */
	public function setDetails($key, $name, array $params);
	
	/**
	 * @return mixed
	 */
	public function addService();
}