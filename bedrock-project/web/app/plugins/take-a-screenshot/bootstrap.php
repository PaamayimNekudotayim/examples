<?php
/**
 * @wordpress-plugin
 * Plugin Name: Take a Screenshot
 * Plugin URI:
 * Description: This plugin allows you to take a screenshot of your website pages and generate pdf, jpg, and png extensions to download them
 * Requires: PHP >= 7.0
 * Author: @vasilguruli
 * Version: 1.0
 * Licence: ...
 * Licence URI: ...
 * @shortname TS
 * @namespace TS
 */

if ( !defined( 'ABSPATH' ) ) {
	die();
}

include __DIR__ . '/config/constants.php';
include __DIR__ . '/vendor/autoload.php';

if (is_admin()) {
	$plugin = \TS\Plugin::create();
	
	register_activation_hook(__FILE__, function() use($plugin) {
		$plugin->activate();
	});
	
	$plugin->notices();
	$plugin->register();
	$plugin->_links();
}
