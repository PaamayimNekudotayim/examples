jQuery(document).ready(function ($) {
    $('#update_charts').click(function () {
        if (scm_charts.update_charts_url == '') {
            $('div.wrap').prepend('<div id="message" class="error notice  hide-notice is-dismissible"><p>Url to Charts update is not provided!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text close-notice">Dismiss this notice.</span></button></div>');
            $('.notice-dismiss').on('click', function () {
                $('#message').hide();
            });

            return false;
        }
        $.ajax({
            url: scm_charts.ajax_url,
            type: 'post',
            data: {
                'action': 'update_charts',
                'charts_url': scm_charts.update_charts_url,
                'update_charts': 'Update Charts'
            },
            beforeSend: function () {
                $('#update_charts').attr("disabled", "disabled");
                $('.update_charts_status').addClass('loader');
            },

            success: function (data) {
                var result = JSON.parse(data);

                if (result.status == 'success') {
                    $('div.wrap').prepend('<div id="message" class="updated notice notice-success hide-notice is-dismissible"><p>Charts has been updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text close-notice">Dismiss this notice.</span></button></div>');
                    $('#update_charts').removeAttr("disabled");
                    $('.update_charts_status').removeClass("loader");
                }

                $('.notice-dismiss').on('click', function () {
                    $('#message').hide();
                });
            }
        });
        return false;
    });
});


jQuery(document).ready(function ($) {
    $('#generate_pdf').click(function () {
        $.ajax({
            url: scm_charts.ajax_url,
            type: 'post',
            data: {
                'action': 'generate_pdf',
                'phantomjs_url': scm_charts.phantomjs_url,
                'generate_pdf': 'Generate PDF'
            },
            beforeSend: function () {
                $('#generate_pdf').attr("disabled", "disabled");
                $('.generate_pdf_status').addClass('loader');
            },

            success: function (data) {

                var result = JSON.parse(data);

                if (result.status == 'success') {
                    $('div.wrap').prepend('<div id="message" class="updated notice notice-success hide-notice is-dismissible"><p>Pdf(s) has been generated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text close-notice">Dismiss this notice.</span></button></div>');
                    $('#generate_pdf').removeAttr("disabled");
                    $('.generate_pdf_status').removeClass("loader");
                } else {
                    $('div.wrap').prepend('<div id="message" class="error-message notice notice-error hide-notice is-dismissible"><p>Error!: ' + result.message + '</p><button type="button" class="notice-dismiss"><span class="screen-reader-text close-notice">Dismiss this notice.</span></button></div>');
                    $('#generate_pdf').removeAttr("disabled");
                    $('.generate_pdf_status').removeClass("loader");
                }

                $('.notice-dismiss').on('click', function () {
                    $('#message').hide();
                });
            }
        });
        return false;
    });
});