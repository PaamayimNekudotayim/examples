Generate PDF from Spreadsheet

Usage

 1. PhantomJs Cloud API Key is required for generating PDF documents:
    More info at https://phantomjscloud.com/index.html
    
 2. Url with PhantomJs Cloud API key should be provided GenerateChartsPdf.php class file (line 7)
        example : 	
            private $phantomJsCloudUrl = 'https://phantomjscloud.com/api/browser/v2/a-demo-key-with-low-quota-per-ip-address/';
    
 3. Settings are available in wp-admin/Generate PDF tab.
 
 4. To be able to generate pdf documents the appropriate json files are required
        more info at:  https://phantomjscloud.com/docs/php.html
 
 5. The json files are located at Theme Directory/functions/generate-charts-pdf/json
 
 6. After appropriate json file is created it needs to be added at GenerateChartsPdf.php class file (line 237)
            example $payload = [
                        'key-name' => file_get_contents(__DIR__ . 'json/file.json')
            ];
            
            'key-name' will be also the name of PDF document e.g key-name.pdf
            
            
 7. After generating pdf files the appropriate url for download should be provided in 
    
    portfolios pages e.g "SCM | Bond Reserve" in wp-admin (Factsheet field in page settings)