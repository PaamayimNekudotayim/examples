<?php

if ( !defined( 'TS_DIR' ) ) {
	define('TS_DIR', dirname(__FILE__, 2));
}


if ( !defined( 'TS_ASSETS_URL' ) ) {
	define( 'TS_ASSETS_URL', plugins_url('assets', dirname(__FILE__)) );
}

if ( !defined('TS_PHP_MIN_VERSION' ) ) {
	define( 'TS_PHP_MIN_VERSION', '7.0' );
}

if ( !defined('TS_NAME') ) {
	define ( 'TS_NAME', 'Take a Screenshot' );
}

if ( !defined( 'TS_TEMPLATE_DIR' ) ) {
	define( 'TS_TEMPLATE_DIR', TS_DIR . '/template' );
}

if ( !defined( 'TS_ERROR_TEMPLATE_DIR' ) ) {
	define ( 'TS_ERROR_TEMPLATE_DIR', TS_DIR . '/template/admin/error' );
}


