<?php

if (!defined( 'ABSPATH' )) {
	die();
}

delete_option('_phantomjs_api_key');
delete_option('_phantomjs_api_url');