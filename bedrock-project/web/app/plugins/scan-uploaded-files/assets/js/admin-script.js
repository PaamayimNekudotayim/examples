const {__, _x, _n, _nx} = wp.i18n;
// Bootstrap switch
//phpcs:disable
(function ($) {
    $('#enable-scan').bootstrapToggle({
        on: __('Enabled', 'scan-uploaded-files'),
        off: __('Disabled', 'scan-uploaded-files')
    });
})(jQuery);
// Add new file extension
(function ($) {
    let add_new_extension = $('input.add-new-extension');
    let add_extension_field = $('input#add-extension');
    let extension_container = $('table.extension-container');
    let count_rows = document.getElementsByClassName('extensions');
    let error_message = $('div.error-message');
    let success_message = $('div.success-message');
    $(add_new_extension).on('click', function () {
        let extension_value = $(add_extension_field).val();
        let row_to_add = count_rows.length + 1;
        let extension = {};

        if ( extension_value.length > 5 ) {
            alert(__('Invalid extension. Please use different name', 'scan-uploaded-files'));

            return false;
        }
        $.ajax({
            type: 'POST',
            url: scan_uploaded_files.ajax_url,
            dataType: 'json',
            data: {
                action: 'add_extension',
                add_extension: extension_value,
                saf_nonce: scan_uploaded_files.saf_nonce
            },
            beforeSend: function () {
                $('input.add-new-extension').prop('disabled', true);
                $('strong.display-message').remove();
            },
            success: function (result) {
                if (result.result === 'success') {
                    extension_container.append(
                        '<tr class="extensions">' +
                        '<th scope="row">' + row_to_add + '</th>' +
                        '<td><label for="' + extension_value + '"' +
                        ' class="">' + __(extension_value.toUpperCase(), 'scan-uploaded-files') + '</label></td>' +
                        '<td>' +
                        '<input type="checkbox" class="form-check-input" name="scan_uploaded_settings[' + extension_value + ']" id="enable-scan" data-toggle="toggle">' +
                        '</td>' +
                        '</tr>'
                    );
                    $('.form-check-input').bootstrapToggle();
                    $('input.add-new-extension').prop('disabled', false);
                    success_message.append('<strong>' + __(result.message, 'scan-uploaded-files' + '</strong>'));
                    success_message.show();
                    setTimeout(function () {
                        success_message.hide('slow');
                    }, 2000);
                }
                if (result.result === 'error') {
                    error_message.append('<strong class="display-message">' + __(result.message, 'scan-uploaded-files') + '</strong>');
                    error_message.show();
                    setTimeout(function () {
                        $('input.add-new-extension').prop('disabled', false);
                    }, 1000)
                }
            }
        });
    });
})(jQuery);

// Add file size
(function($){
    let add_new_file_size = $('input.add-file-size');
    let add_size = $('input#file-size');
    let file_size_container = $('ul.file-size-container');

    $(add_new_file_size).on('click', function(){
        $.ajax({
            type: 'POST',
            url: scan_uploaded_files.ajax_url,
            dataType: 'json',
            data: {
                action: 'add_file_size',
                add_file_size: add_size.val(),
                saf_nonce: scan_uploaded_files.saf_nonce
            },
            success: function(result) {
                if (result.result === 'success') {
                    file_size_container.append('<li class="list-group-item">' +
                        '<span class="desc"><a href="'+scan_uploaded_files.remove_file_size_url + "&remove_file_size="+add_size.val()+"" +
                        "&file_nonce="+scan_uploaded_files.file_nonce+'">' +
                        '<i class="fas fa-trash-alt"></i></a>&nbsp;'+add_size.val()+'&nbsp;KB</span></li>');
                }
            }
        })
    })
})(jQuery);

// Check if ClamAV directory is readable
(function($){
    let check_directory = $('input#is-directory-readable');
    let status_readable = $('td.status-readable');
    $(check_directory).on('click', function () {
        $.ajax({
            type: 'POST',
            url: scan_uploaded_files.ajax_url,
            dataType: 'json',
            data: {
                action: 'check_if_directory_is_readable',
                check_directory: 'check',
                saf_nonce: scan_uploaded_files.saf_nonce
            },
            beforeSend: function(){
               $('.readable').remove();

            },
            success: function(result) {
                if (result.result === 'success') {
                    status_readable.append('<i class="fas fa-check readable"></i>&nbsp;');
                }
            }
        })
    });
})(jQuery);


