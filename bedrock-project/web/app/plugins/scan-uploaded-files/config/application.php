<?php
declare( strict_types=1 );

if ( ! defined( 'SCAN_UPLOADED_FILES_DIR' ) ) {
	define( 'SCAN_UPLOADED_FILES_DIR', dirname( __DIR__ ) );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_URL' ) ) {
	define( 'SCAN_UPLOADED_FILES_URL', plugins_url( '', dirname( __FILE__ ) ) );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_NAME' ) ) {
	define( 'SCAN_UPLOADED_FILES_NAME', 'Scan Uploaded Files' );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_TEXT_DOMAIN' ) ) {
	define( 'SCAN_UPLOADED_FILES_TEXT_DOMAIN', 'scan-uploaded-files' );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_SETTINGS_PAGE' ) ) {
	define( 'SCAN_UPLOADED_FILES_SETTINGS_PAGE', 'scan-uploaded-files' );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_VERSION' ) ) {
	define( 'SCAN_UPLOADED_FILES_VERSION', '1.0' );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_ERROR_DIR' ) ) {
	define( 'SCAN_UPLOADED_FILES_ERROR_DIR', dirname( __DIR__ ) . '/templates/error' );
}

if ( ! defined( 'SCAN_UPLOADED_FILES_CLAM_AV_DIR' ) ) {
	define( 'SCAN_UPLOADED_FILES_CLAM_AV_DIR', dirname( __DIR__ ) . '/modules/clamav' );
}

if ( ! defined( 'APPLICATION_ENVIRONMENT' ) ) {
	define( 'APPLICATION_ENVIRONMENT', false );
}