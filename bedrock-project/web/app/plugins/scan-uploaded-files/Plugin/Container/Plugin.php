<?php


namespace ScanUploadedFiles\Container;

interface Plugin extends Activate, Deactivate, Register, Notices {
	/**
	 * Add actions
	 */
	public function actions();
	
}