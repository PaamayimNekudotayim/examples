<?php

namespace ScanUploadedFiles\Container;

interface Register {
	
	/**
	 * Register plugin
	 */
	public function register();
	
}