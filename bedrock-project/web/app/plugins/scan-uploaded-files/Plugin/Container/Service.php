<?php

namespace ScanUploadedFiles\Container;

interface Service {
	
	/**
	 * Set details for registered service
	 *
	 * @param $key
	 * @param $name
	 * @param array $args
	 * @param array $methods
	 *
	 * @return bool
	 */
	public function set_details( $key, $name, array $args, array $methods );
	/**
	 * Add service
	 */
	public function add_service();
	
}