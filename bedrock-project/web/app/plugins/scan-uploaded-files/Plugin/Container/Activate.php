<?php

namespace ScanUploadedFiles\Container;

interface Activate {
	
	/**
	 * Activate plugin
	 */
	public function activate();
}