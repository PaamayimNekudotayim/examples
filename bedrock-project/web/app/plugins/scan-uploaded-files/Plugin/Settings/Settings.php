<?php
declare( strict_types=1 );

namespace ScanUploadedFiles\Settings;

use ScanUploadedFiles\FormFields\FormFields;

/**
 * Class Settings
 *
 * @package ScanUploadedFiles\Settings
 */
class Settings {
	
	/** @var array $suf_settings */
	public $suf_settings;
	/** @var $filter_extension */
	private $filter_extension = [
		//phpcs:disable
		'.',
		'/',
		'\\',
		',',
		'"',
		'|',
		'%',
		'^',
		'@',
		'!',
		'?',
	];
	/** @var array $default_extensions */
	private $default_extensions = [
		'extensions' => [
			'png'  => 'PNG',
			'jpg'  => 'JPG',
			'jpeg' => 'JPEG',
			'gif'  => 'GIF',
			'pdf'  => 'PDF',
		],
	];
	/** @var array $default_file_sizes */
	private $default_file_sizes = [
		'excluded_file_sizes' => [
			'1024' => 1024,
			'50'   => 50,
			'60'   => 60,
			'100'  => 100,
		],
	];
	
	/**
	 * @return \ScanUploadedFiles\Settings\Settings
	 */
	public static function settings_instance() {
		return new self();
	}
	
	/**
	 * Run Settings
	 *
	 * @return void
	 */
	public function run() {
		//phpcs:disable
		add_action( 'admin_menu', [ $this, 'scan_uploaded_files_menu' ], 10 );
		add_action( 'admin_init', [ $this, 'register_settings_for_plugin' ] );
		//update_option( 'saf_extensions', $this->default_extensions );
		//update_option( 'excluded_file_sizes', $this->default_file_sizes );
		if ( false === get_option( 'saf_extensions' ) ) {
			update_option( 'saf_extensions', $this->default_extensions );
		}
		if ( false === get_option( 'excluded_file_sizes' ) ) {
			update_option( 'excluded_file_sizes', $this->default_file_sizes );
		}
		add_action( 'init', [ $this, 'remove_file_size' ], 100 );
	}
	
	/**
	 * Remove custom file sizes
	 *
	 * @return void
	 */
	public function remove_file_size() {
		if ( isset( $_GET['page'] ) && 'scan-uploaded-files' === esc_attr( $_GET['page'] ) ) {
			if ( isset( $_GET['remove_file_size'] ) ) {
				if ( ! wp_verify_nonce( $_GET['file_nonce'], 'file_size_nonce' ) ) {
					die( esc_attr( 'You are not allowed to be here', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) );
				}
				$file_size = (int) esc_attr( $_GET['remove_file_size'] );
				
				$file_sizes = get_option( 'excluded_file_sizes' );
				
				if ( in_array( (int) $file_size, $file_sizes['excluded_file_sizes'], TRUE ) ) {
					unset( $file_sizes['excluded_file_sizes'][ $file_size ] );
					
					update_option( 'excluded_file_sizes', $file_sizes );
					
					wp_safe_redirect( esc_url( admin_url( 'admin.php?page=scan-uploaded-files' ) ) );
					exit();
				}
			}
		}
	}
	
	/**
	 * Display menu.
	 *
	 * @return void
	 */
	public function scan_uploaded_files_menu() {
		add_menu_page(
		//phpcs:disable
			esc_attr( __( 'Scan Uploaded Files', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
			esc_attr( __( 'Scan Uploaded Files', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
			'manage_options',
			esc_attr( SCAN_UPLOADED_FILES_SETTINGS_PAGE ),
			[ $this, 'display_admin_settings' ],
			'dashicons-shield',
			5
		);
	}
	
	/**
	 * Register settings for plugin.
	 *
	 * @return void
	 */
	public function register_settings_for_plugin() {
		register_setting( 'scan_uploaded_files', 'scan_uploaded_settings',
			[ $this, 'sanitize_settings', ] );
		register_setting( 'clam_av', 'clam_av_settings',
			[ $this, 'sanitize_settings', ] );
		register_setting( 'saf_log', 'saf_log_settings',
			[ $this, 'sanitize_settings', ] );
	}
	
	/**
	 * Display admin settings.
	 *
	 * @return void
	 */
	public static function display_admin_settings() {
		include SCAN_UPLOADED_FILES_DIR . '/templates/admin/settings.php';
	}
	
	/**
	 * Sanitize settings
	 *
	 * @param $settings
	 *
	 * @return mixed
	 */
	public function sanitize_settings( $settings ) {
		add_settings_error(
			'scan_uploaded_files',
			'scan_uploaded_files_settings', __( 'Settings saved', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), 'updated' );
		if ( isset( $settings['enable'] ) ) {
			$settings['enable'] = esc_attr( $settings['enable'] );
		}
		
		$settings['add_extension'] = trim( esc_attr( $settings['add_extension'] ) );
		
		if ( '' !== $settings['add_extension'] ) {
			$settings['add_extension'] = str_replace( $this->filter_extension, '', $settings['add_extension'] );
			if ( strlen( $settings['add_extension'] ) > 5 ) {
				add_settings_error(
					'scan_uploaded_files',
					'scan_uploaded_files_settings', __( 'Invalid length for Extension', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), 'error' );
				$settings['add_extension'] = '';
			}
			
			if ( preg_match_all( '!\d+!', $settings['add_extension'], $matches ) ) {
				add_settings_error(
					'scan_uploaded_files',
					'scan_uploaded_files_settings', __( 'Invalid Extension! Extension could not contain any digits!', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), 'error' );
				$settings['add_extension'] = '';
			}
		}
		
		if ( false !== get_option( 'saf_extensions' ) ) {
			$default_extensions = filter_var_array( get_option( 'saf_extensions' ), FILTER_SANITIZE_STRING );
			$settings           = array_merge( $settings, $default_extensions );
		}
		
		if ( false !== get_option( 'excluded_file_sizes' ) ) {
			$file_sizes = filter_var_array( get_option( 'excluded_file_sizes' ), FILTER_SANITIZE_STRING );
			$settings   = array_merge( $settings, $file_sizes );
		}
		
		return $settings;
	}
	
	/**
	 * Check if checkbox is set.
	 *
	 * @param array $settings
	 * @param string $key
	 *
	 * @return string
	 */
	public function is_value_set_for_checkbox( $settings, $key ) {
		$checked = '';
		if ( isset( $settings[ $key ] ) ) {
			if ( 'on' === $settings[ $key ] ) {
				$checked = 'checked=checked';
			}
		}
		
		return esc_attr( $checked );
	}
	
	/**
	 * Set value for input.
	 *
	 * @param array $settings
	 * @param string $key
	 *
	 * @return string
	 */
	public function set_value_for_input( $settings, $key ) {
		$value = '';
		if ( isset( $settings[ $key ] ) ) {
			$value = esc_attr( $settings[ $key ] );
		}
		
		return esc_attr( $value );
	}
}