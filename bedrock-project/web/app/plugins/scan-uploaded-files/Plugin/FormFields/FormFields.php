<?php
declare( strict_types=1 );

namespace ScanUploadedFiles\FormFields;

use ScanUploadedFiles\Errors\Errors;

/**
 * Class FormFields
 *
 * @package ScanUploadedFiles\FormFields
 */
class FormFields {
	/**
	 * @var array $input_attr
	 */
	//phpcs:ignore
	private $input_attr = [
		'type'        => 'type',
		'class'       => 'class',
		'id'          => 'id',
		'name'        => 'name',
		'value'       => 'value',
		'checked'     => 'checked',
		'required'    => 'required',
		'placeholder' => 'placeholder',
		'data-toggle' => 'data-toggle',
	];
	/**
	 * @var array $available_fields
	 */
	//phpcs:ignore
	private static $available_fields = [
		'input',
		'select',
		'textarea',
		'button',
	];
	
	/**
	 * @param $field
	 *
	 * @return \ScanUploadedFiles\FormFields\FormFields|void
	 */
	public static function add_field( $field ) {
		if ( ! isset( $field ) || '' === $field || ! in_array( $field, self::$available_fields, true ) ) {
			$errors = new Errors();
			//phpcs:disable
			$errors->add_errors( sprintf( __( 'Field << %s >> is unknown or incorrectly provided', SCAN_UPLOADED_FILES_TEXT_DOMAIN ), esc_attr( $field ) ), false );
		}
		
		return new self();
	}
	
	/**
	 * Input field.
	 *
	 * @param array $args
	 *
	 * @return void
	 */
	public function field_input( $args ) {
		echo '<input ' . ( isset( $args[ $this->input_attr['type'] ] ) ? 'type="' . esc_attr( $args[ $this->input_attr['type'] ] ) . '"' : '' ) . '
			' . ( isset( $args[ $this->input_attr['class'] ] ) ? 'class="' . esc_attr( implode( ' ', $args[ $this->input_attr['class'] ] ) ) . '"' : '' ) . '
			' . ( isset( $args[ $this->input_attr['name'] ] ) ? 'name="' . esc_attr( $args[ $this->input_attr['name'] ] ) . '"' : '' ) . '
			' . ( isset( $args[ $this->input_attr['id'] ] ) ? 'id="' . esc_attr( implode( ' ', $args[ $this->input_attr['id'] ] ) ) . '"' : '' ) . '
			' . ( isset( $args[ $this->input_attr['value'] ] ) ? 'value="' . esc_attr( $args[ $this->input_attr['value'] ] ) . '"' : '' ) . '
			' . ( isset( $args[ $this->input_attr['checked'] ] ) ? esc_attr( $args[ $this->input_attr['checked'] ] ) : '' ) . '
			' . ( isset( $args[ $this->input_attr['data-toggle'] ] ) ? esc_attr( 'data-toggle=' . esc_attr( $args[ $this->input_attr['data-toggle'] ] ) ) : '' ) . '
			' . ( isset( $args[ $this->input_attr['placeholder'] ] ) ? esc_attr( 'placeholder=' . esc_attr( $args[ $this->input_attr['placeholder'] ] ) ) : '' ) . '>';
	}
	
	/**
	 * Select field.
	 *
	 * @param array args
	 * @param bool $multiple
	 * @param string $text
	 *
	 * @return void
	 */
	public function field_select( $args, $multiple = true, $text = '' ) {
		$select = '<select ' . ( true === $multiple ? 'multiple' : '' ) . ' ' . ( isset( $args['class'] ) ? 'class="' . esc_attr( implode( ' ', $args['class'] ) ) . '"' : '' ) . '
						  ' . ( isset( $args['name'] ) ? 'name="' . esc_attr( $args['name'] ) . '"' : '' ) . '>
						  ' . ( isset( $args['id'] ) ? 'id="' . esc_attr( implode( ' ', $args['id'] ) ) . '"' : '' ) . '';
		if ( isset( $args['options'] ) ) {
			//phpcs:ignore
			$select .= '<optgroup>' . esc_html_e( $text, SCAN_UPLOADED_FILES_TEXT_DOMAIN ) . '</optgroup>';
			foreach ( $args['options'] as $key => $value ) {
				if ( isset( $key ) ) {
					//phpcs:ignore
					$select .= '<option value="' . esc_attr( $key ) . '">' . esc_attr( __( $value, SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ) . '</option>';
				}
			}
		}
		//phpcs:ignore
		echo $select;
	}
}