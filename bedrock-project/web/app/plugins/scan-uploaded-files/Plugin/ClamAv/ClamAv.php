<?php
declare( strict_types=1 );

namespace ScanUploadedFiles\ClamAv;

use Appwrite\ClamAV\Network;
use Appwrite\ClamAV\Pipe;

/**
 * Class AdminAjax.
 *
 * @package ScanUploadedFiles\Ajax
 */
class ClamAv {
	public function __construct() {
	}
	
	/**
	 * Scan file with CLamAv
	 *
	 * @param array $data
	 *
	 * @return bool
	 */
	public function scan_file() {
		$config   = array(
			'driver'     => 'clamscan',
			'executable' => $this->get_clamav_executable()
		);
		
		$clamscan = new \Avasil\ClamAv\Scanner( $config );
		
		echo '<pre style="margin: 0 auto; width: 800px; display:block; outline: 1px solid #f00; padding: 15px;">';
		var_dump( $clamscan->version() );
		echo '</pre>';
		
		var_dump( $clamscan->scan(SCAN_UPLOADED_FILES_CLAM_AV_DIR . '/virus.txt') );
		
	}
	
	/**
	 * Get ClamAv executable file
	 *
	 * @return string
	 */
	private function get_clamav_executable() {
		return SCAN_UPLOADED_FILES_CLAM_AV_DIR . '/clamav-0.102.4/clamscan/clamscan';
	}
}