<?php
declare( strict_types=1 );

namespace ScanUploadedFiles;


use ScanUploadedFiles\Container\Plugin as PluginContainer;
use ScanUploadedFiles\ScanUploadedFilesInit\ScanUploadedFilesInit;

class Init {
	
	/**
	 * Create plugin
	 *
	 * @return Container\Plugin
	 */
	public static function create(): PluginContainer {
		static $plugin = null;
		if ( null === $plugin ) {
			$plugin = new ScanUploadedFilesInit();
		}
		return $plugin;
	}
}