<?php
declare( strict_types=1 );

namespace ScanUploadedFiles\Ajax;


/**
 * Class AdminAjax.
 *
 * @package ScanUploadedFiles\Ajax
 */
class AdminAjax {
	/**
	 * Ajax action
	 *
	 * @return array
	 */
	private function add_ajax_actions() {
		//phpcs:ignore
		return [
			'add_extension'                  => 'saf_add_extension',
			'add_file_size'                  => 'saf_add_file_size',
			'check_if_directory_is_readable' => 'saf_check_if_directory_is_readable',
		];
	}
	/**
	 * Add ajax action
	 *
	 * @return void
	 */
	public function add_ajax() {
		if ( ! is_admin() && ! current_user_can( 'manage_options' ) ) {
			die();
		}
		foreach ( $this->add_ajax_actions() as $action => $method ) {
			//phpcs:ignore
			add_action( "wp_ajax_{$action}", [ $this, $method ] );
		}
	}
	/**
	 * Add new extension option to options table
	 *
	 * @return void
	 */
	public function saf_add_extension() {
		//phpcs:ignore
		$extension = [];
		$this->check_admin();
		//phpcs:ignore
		$this->check_nonce( $_POST['saf_nonce'], 'saf_nonce' );
		if ( isset( $_POST['add_extension'] ) ) {
			$extension[ esc_attr( $_POST['add_extension'] ) ] = strtoupper( esc_attr( $_POST['add_extension'] ) );
			$saf_extensions                                   = get_option( 'saf_extensions' );
			if ( in_array( strtoupper( esc_attr( $_POST['add_extension'] ) ), $saf_extensions['extensions'], true ) ) {
				//phpcs:ignore
				$result = [
					'result'  => 'error',
					//phpcs:ignore
					'message' => esc_attr( __( 'Extension already exists! Please set another one', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
				die();
			}
			$saf_extensions['extensions'] = array_merge( $saf_extensions['extensions'], $extension );
			if ( ! empty( $saf_extensions['extensions'] ) ) {
				update_option( 'saf_extensions', filter_var_array( $saf_extensions, FILTER_SANITIZE_STRING ) );
				//phpcs:ignore
				$result = [
					'result'  => 'success',
					//phpcs:ignore
					'message' => esc_attr( __( 'Extension successfully added. Please save your settings', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
			} else {
				//phpcs:ignore
				$result = [
					'result'  => 'error',
					//phpcs:ignore
					'message' => esc_attr( __( 'There was an error. Please try again later', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
			}
		}
		exit();
	}
	
	/**
	 * Add file size to exclude
	 *
	 * @return void
	 */
	public function saf_add_file_size() {
		//phpcs:ignore
		$file_size = [];
		$this->check_admin();
		//phpcs:ignore
		$this->check_nonce( $_POST['saf_nonce'], 'saf_nonce' );
		//phpcs:ignore
		if ( isset( $_POST['add_file_size'] ) ) {
			//phpcs:ignore
			$file_size[ esc_attr( "{$_POST['add_file_size']}" ) ] = esc_attr( (int) $_POST['add_file_size'] );
			//phpcs:ignore
			$file_sizes = get_option( 'excluded_file_sizes' );
			if ( in_array( (int) $file_size, $file_sizes['excluded_file_sizes'], true ) ) {
				//phpcs:ignore
				$result = [
					'result'  => 'error',
					//phpcs:ignore
					'message' => esc_attr( __( 'File size already exists! Please set another one', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
				die();
			}
			$file_sizes['excluded_file_sizes'] = $file_sizes['excluded_file_sizes'] + $file_size;
			if ( ! empty( $file_sizes['excluded_file_sizes'] ) ) {
				update_option( 'excluded_file_sizes', filter_var_array( $file_sizes, FILTER_VALIDATE_INT ) );
				//phpcs:ignore
				$result = [
					'result'  => 'success',
					//phpcs:ignore
					'message' => esc_attr( __( 'File size successfully added. Please save your settings', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
			} else {
				//phpcs:ignore
				$result = [
					'result'  => 'error',
					//phpcs:ignore
					'message' => esc_attr( __( 'There was an error. Please try again later', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				];
				echo wp_json_encode( $result );
			}
		}
		exit();
	}
	
	/**
	 * Check if directory is readable for ClamAV
	 *
	 * @return void
	 */
	public function saf_check_if_directory_is_readable() {
		$this->check_admin();
		//phpcs:ignore
		$this->check_nonce( $_POST['saf_nonce'], 'saf_nonce' );
		//phpcs:ignore
		if ( isset( $_POST['check_directory'] ) ) {
			//phpcs:ignore
			$check = esc_attr( $_POST['check_directory'] );
			if ( 'check' === $check ) {
				if ( is_dir( SCAN_UPLOADED_FILES_CLAM_AV_DIR ) ) {
					if ( is_writable( SCAN_UPLOADED_FILES_CLAM_AV_DIR ) ) {
						//phpcs:ignore
						$result = [
							'result' => 'success',
						];
						update_option( 'directory_readable', 'yes' );
					} else {
						//phpcs:ignore
						$result = [
							'result' => 'error',
						];
						update_option( 'directory_readable', 'no' );
					}
					echo wp_json_encode( $result );
				}
			}
		}
		exit();
	}
	
	/**
	 * Check if is admin
	 *
	 * @return void
	 */
	private function check_admin() {
		if ( ! is_admin() && ! current_user_can( 'manage_options' ) ) {
			echo wp_json_encode(
			//phpcs:ignore
				[
					'result'  => 'error',
					//phpcs:ignore
					'message' => esc_attr( __( 'You are not allowed to make changes here!', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
				]
			);
			die();
		}
	}
	
	/**
	 * Check nonce
	 *
	 * @param string $nonce
	 * @param string $action
	 *
	 * @return void
	 */
	private function check_nonce( $nonce, $action ) {
		if ( isset( $nonce ) ) {
			if ( ! wp_verify_nonce( esc_attr( $nonce ), $action ) ) {
				echo wp_json_encode(
				//phpcs:ignore
					[
						'result'  => 'error',
						//phpcs:ignore
						'message' => esc_attr( __( 'You are not allowed to make changes here!', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
					]
				);
				die();
			}
		}
	}
	
}