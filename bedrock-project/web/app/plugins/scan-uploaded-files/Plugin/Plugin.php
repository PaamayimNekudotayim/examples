<?php
declare( strict_types=1 );

namespace ScanUploadedFiles;

use ScanUploadedFiles\Ajax\AdminAjax;
use ScanUploadedFiles\Settings\Settings;

/**
 * Class Plugin
 *
 * @package ScanUploadedFiles
 */
class Plugin {
	/** @var string $plugin_url */
	public $plugin_url;
	/** @var string $plugin_dir */
	public $plugin_dir;
	/** @var $seetings_page */
	private $settings_page;
	/** @var string $suf_nonce */
	public static $saf_nonce;
	/**
	 * Plugin constructor.
	 *
	 * @param array $args
	 */
	//phpcs:ignore
	public function __construct( $args = [] ) {
		$this->plugin_url    = $args['plugin_url'];
		$this->plugin_dir    = $args['plugin_dir'];
		$this->settings_page = $args['settings_page'];
	}
	/**
	 * Admin menu for Scan Uploaded files.
	 *
	 * @return void
	 */
	public function actions() {
		//phpcs:ignore
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_scripts', ] );
		//phpcs:ignore
		add_action( 'init', [ $this, 'saf_create_nonce' ] );
	}
	/**
	 * Create nonce fro validation.
	 *
	 * @return void
	 */
	public function saf_create_nonce() {
		self::$saf_nonce = wp_create_nonce( 'saf_nonce' );
	}
	/**
	 * Enqueue admin scripts for plugin.
	 *
	 * @return void
	 */
	public function enqueue_admin_scripts() {
		if ( esc_attr( 'toplevel_page_' . SCAN_UPLOADED_FILES_SETTINGS_PAGE ) === get_current_screen()->base ) {
			//phpcs:ignore
			wp_enqueue_style( 'suf-bootstrap-css', $this->plugin_url . '/assets/css/bootstrap.min.css', [], '4.5.2' );
			//phpcs:ignore
			wp_enqueue_style( 'suf-bootstrap-toggle-css', $this->plugin_url . '/assets/css/bootstrap4-toggle.min.css', [], '3.6.1' );
			//phpcs:ignore
			wp_enqueue_style( 'suf-font-awesome', $this->plugin_url . '/assets/css/font-awesome/css/all.min.css', [], '5.4' );
			//phpcs:ignore
			wp_enqueue_style( 'suf-admin-style', $this->plugin_url . '/assets/css/admin-style.css', [], SCAN_UPLOADED_FILES_VERSION );
			//phpcs:ignore
			wp_enqueue_script( 'suf-bootstrap-js', $this->plugin_url . '/assets/js/bootstrap.min.js', [ 'jquery' ], '4.5.2', true );
			//phpcs:ignore
			wp_enqueue_script( 'suf-bootstrap-toggle-js', $this->plugin_url . '/assets/js/bootstrap4-toggle.min.js', [ 'jquery' ], '3.6.1', true );
			//phpcs:ignore
			wp_enqueue_script( 'suf-admin-js', $this->plugin_url . '/assets/js/admin-script.js',
				//phpcs:ignore
				[
					'jquery',
					'suf-bootstrap-toggle-js',
					'wp-i18n',
				//phpcs:ignore
				], SCAN_UPLOADED_FILES_VERSION, true
			);
			wp_localize_script(
			//phpcs:ignore
				'suf-admin-js', 'scan_uploaded_files', [
					'ajax_url'             => esc_url( admin_url( 'admin-ajax.php' ) ),
					'saf_nonce'            => self::$saf_nonce,
					'remove_file_size_url' => esc_url( 'admin.php?page=scan-uploaded-files' ),
					'file_nonce'           => wp_create_nonce( esc_attr( 'file_size_nonce' ) ),
				]
			);
		}
	}
}