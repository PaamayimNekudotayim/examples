<?php //phpcs:disable ?>
<?php if ( ! empty( $errors ) ) : ?>
    <ul class="ts-error-display">
		<?php foreach ( $errors as $error => $message ) : ?>
            <li class="message"><h2><?php esc_html_e( 'Some error occurred, details below:' ) ?></h2>
            </li>
            <li class="message"><?php echo sprintf( '%s', esc_attr( $message['message'] ) ) ?></li>
            <li class="message"><strong><?php esc_html_e( 'IN FILE' ) ?></strong>
                >>> <?php echo sprintf( '%s', esc_attr( $message['file'] ) ) ?></li>
            <li class="message">
				<?php esc_html_e( 'Code: ' ) ?><?php echo sprintf( '%s', esc_attr( $message['code'] ) ) ?>
            </li>
            <li class="message">
				<?php esc_html_e( 'Line: ' ) ?><?php echo sprintf( '%s', esc_attr( $message['line'] ) ) ?></li>
            <?php if ( APPLICATION_ENVIRONMENT ) : ?>
            <li class="message"><h2><?php esc_html_e( 'Backtrace:' ) ?></h2></li>
                <pre><?php print_r( $message['trace'] ); ?></pre>
            <?php endif; ?>
		<?php endforeach; ?>
    </ul>
<?php endif; ?>