<?php
declare( strict_types=1 );


use ScanUploadedFiles\FormFields\FormFields;
use ScanUploadedFiles\Settings\Settings;

/**
 * @var array $saf_settings
 * @var string $active_tab
 */

$saf_settings        = get_option( 'scan_uploaded_settings' );
$excluded_file_sizes = get_option( 'excluded_file_sizes' );

//echo '<pre>';
//var_dump(  get_option( 'status_connection', 'not_checked' ) );
//echo '</pre>';
?>
<!--Enable Scanning-->
<div class="form-group">
	<?php //phpcs:disable ?>
    <label for="enable-scan">
		<?php esc_html_e( 'Enable Scan for Uploaded files', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></label>
	<?php
	//phpcs:disable
	FormFields::add_field( 'input' )->field_input(
		[
			'type'        => 'checkbox',
			'name'        => 'scan_uploaded_settings[enable]',
			'id'          => [ 'enable-scan' ],
			'checked'     => Settings::settings_instance()
			                         ->is_value_set_for_checkbox( $saf_settings, 'enable' ),
			'class'       => [ 'form-input' ],
			'data-toggle' => 'toggle',
		]
	)
	?>
</div>
<!--End enable scanning-->

<!--Extensions to exclude from scan-->
<div class="form-group">
    <h5><i class="fas fa-file-csv"></i>&nbsp;<?php esc_html_e( 'Extensions to exclude from scan', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></h5>
</div>
<div class="form-group">
    <div class="row">
        <table class="table table-bordered extension-container"
               id="extension-container">
            <thead>
            <tr>
                <th scope="col"><?php esc_html_e( 'All Extensions', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?>
                    &nbsp;( <?php echo count( $saf_settings['extensions'] ) ?> )
                </th>
                <th scope="col"><?php esc_html_e( 'Extension', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?></th>
                <th scope="col"><?php esc_html_e( 'Exclude', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?></th>
            </tr>
            </thead>
			<?php if ( ! empty( $saf_settings['extensions'] ) ) : ?>
				<?php $i = 1; ?>
				<?php foreach ( $saf_settings['extensions'] as $key => $extension ) : ?>
                    <tr class="extensions">
                        <th><?php echo $i ++; ?></th>
                        <td><label for="<?php echo esc_attr( $key ) ?>"
                                   class=""><?php esc_html_e( esc_attr( $extension ), SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></label>
                        </td>
                        <td>
							<?php
							FormFields::add_field( 'input' )->field_input(
								[
									'type'        => 'checkbox',
									'name'        => 'scan_uploaded_settings[' . esc_attr( $key ) . ']',
									'id'          => [ 'enable-scan' ],
									'checked'     => Settings::settings_instance()
									                         ->is_value_set_for_checkbox( $saf_settings, esc_attr( $key ) ),
									'class'       => [
										'form-check-input',
									],
									'data-toggle' => 'toggle',
								]
							);
							?>
                        </td>
                    </tr>
				<?php endforeach; ?>
			<?php endif; ?>
        </table>
    </div>
    <hr/>
    <div style="display:none"
         class="alert alert-danger alert-dismissible fade show error-message"
         role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div style="display:none"
         class="alert alert-success alert-dismissible fade show success-message"
         role="alert">
        <button type="button" class="close" data-dismiss="alert"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="form-group margin-top">
        <button class="btn btn-primary success" type="button"
                data-toggle="collapse"
                data-target="#add-extension"
                aria-expanded="false"
                aria-controls="addExtension">
			<?php esc_html_e( 'Add Extension', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
        </button>
    </div>

    <div class="collapse" id="add-extension">
        <div class="card card-body">
            <div class="form-group mx-sm-3 mb-2">
                <label for="add-extension"><?php esc_html_e( 'Add Extension to exclude', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></label>
				<?php
				FormFields::add_field( 'input' )
				          ->field_input(
					          [
						          'type'  => 'text',
						          'name'  => 'scan_uploaded_settings[add_extension]',
						          'id'    => [ 'add-extension' ],
						          'value' => Settings::settings_instance()
						                             ->set_value_for_input( $saf_settings, 'add_extension' ),
						          'class' => [
							          'form-control',
						          ],
					          ]
				          );
				?>
                <div class="form-group margin-top">
                    <input type="button"
                           class="button add-new-extension"
                           value="<?php esc_html_e( 'Add  new extension', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>"/>
                </div>
            </div>

        </div>
    </div>
</div>
<!--End extensions to exclude from scan-->
<hr/>
<!--File size to exclude-->
<div class="form-group">
    <h5><i class="fas fa-weight-hanging"></i>&nbsp;<?php esc_html_e( 'File size to exclude', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></h5>
</div>
<div class="form-group">
    <div class="card card-body">
        <div class="form-group">
            <label for="add-file"><?php esc_html_e( 'Add file size to exclude from scan (in kb)', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></label>
			<?php
			FormFields::add_field( 'input' )
			          ->field_input(
				          [
					          'type'        => 'number',
					          'name'        => 'scan_uploaded_settings[file_size]',
					          'id'          => [ 'file-size' ],
					          'value'       => Settings::settings_instance()
					                                   ->set_value_for_input( $saf_settings, 'file_size' ),
					          'class'       => [
						          'form-control',
					          ],
					          'placeholder' => __( '1024', SCAN_UPLOADED_FILES_TEXT_DOMAIN ),
				          ]
			          );
			?>
        </div>
        <div class="form-group">
            <h5><?php esc_html_e( 'Excluded file sizes', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></h5>

            <ul class="list-group file-size-container">
				<?php if ( isset( $excluded_file_sizes['excluded_file_sizes'] ) && count( $excluded_file_sizes['excluded_file_sizes'] ) > 0 ) : ?>
					<?php foreach ( $excluded_file_sizes['excluded_file_sizes'] as $file_size => $size ) : ?>
                        <li class="list-group-item"><span
                                    class="desc"><a
                                        title="<?php esc_html_e( 'Remove file size', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>"
                                        href="<?php echo esc_url( 'admin.php?page=scan-uploaded-files&remove_file_size=' . esc_attr( $file_size ) . '&file_nonce=' . wp_create_nonce( esc_attr( 'file_size_nonce' ) ) . '' ) ?>">
                                    <i class="fas fa-trash-alt"></i></a>&nbsp;<?php esc_html_e( $size ); ?>&nbsp;KB
                                    </span>
                        </li>
					<?php endforeach; ?>
				
				<?php else : ?>
                    <p>
                        <span class="description"><?php esc_html_e( 'No file sizes added yet', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
                    </p>
				<?php endif; ?>
            </ul>

            <div class="form-group margin-top">
                <input type="button"
                       class="button add-file-size"
                       value="<?php esc_html_e( 'Add file size', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>"/>
            </div>
        </div>
    </div>
</div>
<!--End file size to exclude-->
<hr/>