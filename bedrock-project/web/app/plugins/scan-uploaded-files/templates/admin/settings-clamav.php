<?php
use ScanUploadedFiles\FormFields\FormFields;
?>
<!--ClamAv status check-->
<?php //phpcs:disable ?>
<div class="form-group">
    <h5><i class="fas fa-shield-alt"></i>&nbsp;<?php esc_html_e( 'Status', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></h5>
    <span class="describe"><?php esc_html_e( 'Check for ClamAv status', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
</div>
<div class="form-group">
    <div class="card card-body">
        <div class="form-group">
            <table class="table table-bordered"
                   id="clam-av-options">
                <thead>
                <tr>
                    <th scope="col"><?php esc_html_e( 'Status', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?></th>
                    <th scope="col"><?php esc_html_e( 'Name', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?></th>
                    <th scope="col"><?php esc_html_e( 'Action', SCAN_UPLOADED_FILES_TEXT_DOMAIN ); ?></th>
                </tr>
                </thead>
                <!--Connection status-->
                <tr>
                    <td class="status-connection">
						<?php if ( 'not_checked' === get_option( 'status_connection', 'not_checked' ) ) : ?>
                            <span class="saf-unchecked"><?php esc_html_e( 'Unchecked', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
						<?php else : ?>
							<?php if( 'yes' === get_option( 'status_connection' ) ) : ?>
                                <span class="saf-success"><?php esc_html_e( 'Ok', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php else : ?>
                                <span class="saf-error"><?php esc_html_e( 'Failed', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php endif; ?>
						<?php endif; ?>
                    </td>
                    <td>
						<?php esc_html_e( 'ClamAV Antivirus connection', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
                        <p>
                            <small><?php esc_html_e( 'Checks if connection with CLamAV Antivirus server is ok', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></small>
                        </p>
                    </td>
                    <td>
						<?php
						FormFields::add_field( 'input' )->field_input(
							[
								'type'  => 'button',
								'id'    => [ 'clamav-connection' ],
								'value' => esc_attr( __( 'Check', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
								'class' => [
									'button',
								],
							]
						);
						?>
                    </td>
                </tr>
                <!--End connection status-->
                <!--Readable status-->
                <tr>
                    <td class="status-readable">
						<?php if ( 'not_checked' === get_option( 'directory_readable', 'not_checked' ) ) : ?>
                            <span class="saf-unchecked"><?php esc_html_e( 'Unchecked', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
						<?php else : ?>
							<?php if ( 'yes' === get_option( 'directory_readable' ) ) : ?>
                                <span class="saf-success"><?php esc_html_e( 'Ok', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php else : ?>
                                <span class="saf-error"><?php esc_html_e( 'Failed', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php endif; ?>
						<?php endif; ?>
                    </td>
                    <td>
						<?php esc_html_e( 'is ClamAV directory writable', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
                        <p>
                            <small><?php esc_html_e( 'Checks if ClamAV directory is writable for database files', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></small>
                        </p>
                    </td>
                    <td>
						<?php
						FormFields::add_field( 'input' )->field_input(
							[
								'type'  => 'button',
								'id'    => [ 'is-directory-readable' ],
								'value' => esc_attr( __( 'Check', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
								'class' => [
									'button',
								],
							]
						);
						?>
                    </td>
                </tr>
                <!--End readable status-->
                <!--Executable status-->
                <tr>
                    <td class="status-executable">
						<?php if ( 'not_checked' === get_option( 'status_executable', 'not_checked' ) ) : ?>
                            <span class="saf-unchecked"><?php esc_html_e( 'Unchecked', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
						<?php else : ?>
							<?php if( 'yes' === get_option( 'status_executable' ) ) : ?>
                                <span class="saf-success"><?php esc_html_e( 'Ok', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php else : ?>
                                <span class="saf-error"><?php esc_html_e( 'Failed', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
							<?php endif; ?>
						<?php endif; ?>
                    </td>
                    <td>
						<?php esc_html_e( ' ClamAV executable status', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
                        <p>
                            <small><?php esc_html_e( 'Checks if ClamAV executable file has a right permission to make scans', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></small>
                        </p>
                    </td>
                    <td>
						<?php
						FormFields::add_field( 'input' )->field_input(
							[
								'type'  => 'button',
								'id'    => [ 'executable-status' ],
								'value' => esc_attr( __( 'Check', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
								'class' => [
									'button',
								],
							]
						);
						?>
                    </td>
                </tr>
                <!--End executable status-->
                <tr>
                    <td class="status-database-update">
			            <?php if ( 'not_checked' === get_option( 'status_database_update', 'not_checked' ) ) : ?>
                            <span class="saf-unchecked"><?php esc_html_e( 'Unchecked', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
			            <?php else : ?>
				            <?php if( 'yes' === get_option( 'status_database_update' ) ) : ?>
                                <span class="saf-success"><?php esc_html_e( 'Ok', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
				            <?php else : ?>
                                <span class="saf-error"><?php esc_html_e( 'Failed', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></span>
				            <?php endif; ?>
			            <?php endif; ?>
                    </td>
                    <td>
			            <?php esc_html_e( 'Update ClamAV Database', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
                        <p>
                            <small><b><?php esc_html_e( 'Important! The directory of ClamAV Antivirus should be writable!', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></b></small>
                        </p>
                        <p><span class="describe"><?php esc_html_e( 'Last Update: ' . date('Y-m-d H:i:s') ) ?></span></p>
                    </td>
                    <td>
			            <?php
			            FormFields::add_field( 'input' )->field_input(
				            [
					            'type'  => 'button',
					            'id'    => [ 'clamav-update' ],
					            'value' => esc_attr( __( 'Update', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ),
					            'class' => [
						            'button',
					            ],
				            ]
			            );
			            ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<!--End ClamAv status check-->