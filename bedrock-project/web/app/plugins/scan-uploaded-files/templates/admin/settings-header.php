<?php
declare( strict_types=1 );

use ScanUploadedFiles\Plugin;
use ScanUploadedFiles\Settings\Settings;

$saf_tab = check_tab();
?>
<?php //phpcs:disable ?>
<h1>
	<i class="fas fa-shield-alt"></i>&nbsp;<?php
	
	esc_html_e( get_admin_page_title(), SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
</h1>
<h3 class="modal-title"><i
		class="fas fa-tools"></i>&nbsp;<?php esc_html_e( 'Settings', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
</h3>
<?php settings_errors(); ?>
<ul class="nav nav-tabs">
	<li class="nav-item">
		<a href="<?php echo esc_attr( '?page=scan-uploaded-files&tab=saf_general_settings&suf_nonce=' . esc_attr( Plugin::$saf_nonce ) ); ?>"
		   class="nav-link
                <?php echo $saf_tab === esc_attr( 'saf_general_settings' ) ? 'active' : ''; ?>">
			<?php esc_html_e( 'General', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>
		</a>
	
	</li>
	<li class="nav-item">
		<a href="<?php echo esc_attr( '?page=scan-uploaded-files&tab=clamav&suf_nonce=' . esc_attr( Plugin::$saf_nonce ) ); ?>"
		   class="nav-link
                <?php echo $saf_tab === esc_attr( 'clamav' ) ? 'active' : ''; ?>"><?php echo __( 'ClamAV', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></a>
	</li>
	<li class="nav-item">
		<a href="<?php echo esc_attr( '?page=scan-uploaded-files&tab=saf_log&suf_nonce=' . esc_attr( Plugin::$saf_nonce ) ); ?>"
		   class="nav-link
                <?php echo $saf_tab === esc_attr( 'saf_log' ) ? 'active' : ''; ?>"><?php echo __( 'Log', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?></a>
	</li>
</ul>
