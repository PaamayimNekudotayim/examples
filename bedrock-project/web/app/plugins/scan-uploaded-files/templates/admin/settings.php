<?php
declare( strict_types=1 );

$saf_tab = check_tab();
?>

<?php //phpcs:disable ?>
<div class="container saf-container saf-row">
    <div class="row">
        <div class="col">
			<?php add_plugin_template( 'header' ) ?>
        </div>

    </div>
    <div class="clear"></div>
    <div class="row">
        <div class="col">
            <form action="options.php" method="post">
				<?php if ( $saf_tab === 'saf_general_settings' ) : ?>
					<?php settings_fields( 'scan_uploaded_files' ); ?>
					<?php do_settings_sections( 'scan_uploaded_files' ); ?>
					<?php add_plugin_template( 'general' ) ?>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary"
                               value="<?php esc_html_e( 'Save Settings', SCAN_UPLOADED_FILES_TEXT_DOMAIN ) ?>"/>
                    </div>
				<?php endif; ?>
                
                <?php if ( $saf_tab === 'clamav' ) : ?>
					<?php settings_fields( 'clam_av' ); ?>
					<?php do_settings_sections( 'clam_av' ); ?>
					<?php add_plugin_template( 'clamav' ) ?>
				<?php endif; ?>
                
                <?php if ( $saf_tab === 'saf_log' ) : ?>
					<?php settings_fields( 'saf_log' ); ?>
					<?php do_settings_sections( 'saf_log' ); ?>
					<?php add_plugin_template( 'log' ) ?>
				<?php endif; ?>
            </form>
        </div>
    </div>
</div>
