<?php

namespace SettingsSection;


use Jigoshop\Frontend\Pages;
use Jigoshop\Integration;
use SettingsOptions\SettingsOptions;

class SettingsSection
{
	const HSC_OPTIONS = 'chronous-header-shopping-cart';
	const HOME_PAGE = 'chronous-home-page';
	const WELCOME = 'chronous-theme';
	const GENERAL_SETTINGS = 'general-settings';
	const CHECKOUT_OPTIONS = 'checkout-settings';
	const IMG_SIZE = 'medium';
	private static $instance;
	
	private $options;
	public static $htmlOut = array();
	protected $message;
	
	public $settings = array(
		'wpautop' => false,
		'media_buttons' => false,
		'textarea_name' => 'jigoshop_chronous_ccm_text',
		'textarea_rows' => 20,
		'tabindex' => null,
		'editor_css' => '',
		'editor_class' => 'jigoshop_chronous_ccm_text',
		'teeny' => 0,
		'dfw' => 0,
		'tinymce' => 1,
		'quicktags' => 1,
		'drag_drop_upload' => false
	);
	protected static $allowedHtml = [
		'a' => [
			'id' => [],
			'href' => [],
			'title' => [],
			'style' => [],
			'class' => [],
		],
		'b' => [
			'id' => [],
			'class' => [],
		],
		'br' => [],
		'em' => [
			'id' => [],
			'class' => []
		],
		'blockquote' => [
			'id' => [],
			'class' => [],
		],
		'i' => [
			'id' => [],
			'class' => [],
		],
		'ins' => [],
		'img' => [
			'title' => [],
			'src' => [],
			'class' => [],
			'id' => [],
		],
		'ol' => [
			'class' => [],
			'id' => [],
		],
		'ul' => [
			'class' => [],
			'id' => [],
		],
		'li' => [
			'class' => [],
			'id' => [],
		],
		'strong' => [
			'id' => [],
			'class' => [],
		],
	];
	
	public static function allowedHtmlOutput()
	{
		return array_merge(self::$htmlOut, self::$allowedHtml);
	}
	
	/**
	 * @return SettingsSection
	 */
	public static function section()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	/**
	 * @param $section
	 */
	public static function addSettings($section)
	{
		switch ($section) {
			case 'section' :
				self::section()->add();
				break;
			default :
				add_settings_error('error', 'ChronousErrorMessage', 'Could not save the request!',
					'error');
				settings_errors('error');
				break;
		}
		
	}
	
	/**
	 *
	 */
	public function add()
	{
		if (false == get_option(self::HSC_OPTIONS)) {
			add_option(self::HSC_OPTIONS);
		}
		add_settings_section(
			'jchsc_settings',
			'Header Shopping Cart Settings',
			[$this, 'jchscSettingsTitle'],
			self::HSC_OPTIONS
		);
		add_settings_field(
			'enable-hsc',
			'Enable Header Shopping Cart',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'value' => 'jigoshop_chronous_hsc_enable'
			]
		);
		add_settings_field(
			'display-hsc-cart-icon',
			'Display Cart Icon',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'value' => 'jigoshop_chronous_hsc_icon_display'
			]
		);
		add_settings_field(
			'display-cart-even-empty',
			'Display Cart even if it\'s empty',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'value' => 'jigoshop_chronous_hsc_display_empty'
			]
		);
		
		add_settings_field(
			'select-menu',
			'Select Menu',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'value' => 'jigoshop_chronous_hsc_select_menu'
			]
		);
		
		add_settings_field(
			'text-on-cart',
			'Text On Cart',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'value' => 'jigoshop_chronous_hsc_text'
			]
		);
		
		add_settings_field(
			'choose-icon',
			'Choose Cart Icon',
			[$this, 'hscMainSettings'],
			self::HSC_OPTIONS,
			'jchsc_settings',
			[
				'iconOptions' => 'jigoshop_chronous_hsc_icon',
				'id' => 'cart-icon',
				'options' => [
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
				]
			]
		);
		
		// Home Page
		add_settings_section(
			'home_settings',
			'Home Page Settings',
			[$this, 'homeSettingsTitle'],
			self::HOME_PAGE
		);
		add_settings_field(
			'home-title',
			'Home Page Title',
			[$this, 'homeMainSettings'],
			self::HOME_PAGE,
			'home_settings',
			[
				'value' => 'jigoshop_chronous_home_title'
			]
		);
		add_settings_field(
			'home-description',
			'Home Page Short Description',
			[$this, 'homeMainSettings'],
			self::HOME_PAGE,
			'home_settings',
			[
				'value' => 'jigoshop_chronous_home_description'
			]
		);
		add_settings_field(
			'home-image',
			'Background Image',
			[$this, 'homeMainSettings'],
			self::HOME_PAGE,
			'home_settings',
			[
				'value' => 'jigoshop_chronous_home_image'
			]
		);
		add_settings_field(
			'home-image-size',
			'Image Size (On Frontend)',
			[$this, 'homeMainSettings'],
			self::HOME_PAGE,
			'home_settings',
			[
				'value' => 'jigoshop_chronous_home_image_size'
			]
		);
		
		// Welcome Tab
		add_settings_section(
			'welcome_tab',
			'Welcome To Chronous Theme',
			[$this, 'welcomeChronousTitle'],
			self::WELCOME
		);
		add_settings_field(
			'welcome-title',
			'About',
			[$this, 'welcomeTheme'],
			self::WELCOME,
			'welcome_tab',
			[
				'value' => 'jigoshop_chronous_welcome_title'
			]
		);
		add_settings_field(
			'contact-theme',
			'Have a question?',
			[$this, 'welcomeTheme'],
			self::WELCOME,
			'welcome_tab',
			[
				'value' => 'jigoshop_chronous_contact_theme'
			]
		);
		add_settings_field(
			'support-theme',
			'Need Help?',
			[$this, 'welcomeTheme'],
			self::WELCOME,
			'welcome_tab',
			[
				'value' => 'jigoshop_chronous_support'
			]
		);
		
		// General settings
		add_settings_section(
			'general_settings',
			'More Settings',
			[$this, 'generalSettingsTitle'],
			self::GENERAL_SETTINGS
		);
		
		add_settings_field(
			'logo_image',
			'Logo Image',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_logo_image'
			]
		);
		add_settings_field(
			'favicon',
			'Favicon',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_favicon'
			]
		);
		
		add_settings_field(
			'background-color',
			'Background Color (Body)',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_background_color'
			]
		);
		add_settings_field(
			'which-page',
			'Where to apply Background?',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_which_page'
			]
		);
		add_settings_field(
			'footer-background',
			'Footer Background',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_footer_background'
			]
		);
		add_settings_field(
			'which-footer',
			'Where to apply background?',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_which_footer'
			]
		);
		add_settings_field(
			'products-per-shop-page',
			__('Items per page', 'jigoshop-chronous'),
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_products_per_shop_page'
			]
		);
		add_settings_field(
			'custom-css-enable',
			'Enable Custom Css',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_custom_css_enable'
			]
		);
		add_settings_field(
			'custom-css',
			'Custom Css',
			[$this, 'generalSettings'],
			self::GENERAL_SETTINGS,
			'general_settings',
			[
				'value' => 'jigoshop_chronous_custom_css'
			]
		);
		
		// Checkout Options
		add_settings_section(
			'ccm_settings',
			'Custom Checkout Message',
			[$this, 'ccmSettingsTitle'],
			self::CHECKOUT_OPTIONS
		);
		
		add_settings_field(
			'enable-ccm',
			'Enable',
			[$this, 'ccmMainSettings'],
			self::CHECKOUT_OPTIONS,
			'ccm_settings',
			[
				'value' => 'jigoshop_chronous_ccm_enable'
			]
		);
		add_settings_field(
			'ccm-text',
			'Custom Message On Checkout',
			[$this, 'ccmMainSettings'],
			self::CHECKOUT_OPTIONS,
			'ccm_settings',
			[
				'value' => 'jigoshop_chronous_ccm_text'
			]
		);
		add_settings_field(
			'disable-checkout-button',
			'Disable Checkout Button',
			[$this, 'ccmMainSettings'],
			self::CHECKOUT_OPTIONS,
			'ccm_settings',
			[
				'value' => 'jigoshop_chronous_disable_checkout'
			]
		);
	}
	
	public function jchscSettingsTitle()
	{
		echo '<p>' . esc_html(__('Header Shopping Cart', 'jigoshop-chronous')) . '</p>';
	}
	
	public function ccmSettingsTitle()
	{
		echo '<p>' . esc_html(__('Custom Checkout Message', 'jigoshop-chronous')) . '</p>';
	}
	
	public function homeSettingsTitle()
	{
		echo '<p>' . esc_html(__('Home Page', 'jigoshop-chronous')) . '</p>';
	}
	
	public function welcomeChronousTitle()
	{
		echo '<p>' . esc_html(__('', 'jigoshop-chronous')) . '</p>';
	}
	
	public function generalSettingsTitle()
	{
		echo '<p>' . esc_html(__('', 'jigoshop-chronous')) . '</p>';
	}
	
	/**
	 * @param $arg
	 */
	public function hscMainSettings($arg)
	{
		$html = '';
		if (!empty($arg['value'])) {
			switch ($arg['value']) {
				case 'jigoshop_chronous_hsc_enable' :
					$enable = esc_attr(get_option($arg['value']));
					if ($enable == 'on') {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<input type="checkbox" class="jigoshop switch-medium" id="' . esc_attr($arg['value']) . '"
					name="' . esc_attr($arg['value']) . '" ' . $checked . ' />';
					break;
				case 'jigoshop_chronous_hsc_icon_display' :
					$cartIcon = esc_attr(get_option($arg['value']));
					if ($cartIcon == 'on') {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<input type="checkbox" class="switch-medium" id="' . esc_attr($arg['value']) . '" name="'
						. esc_attr($arg['value']) . '" ' . $checked . ' />';
					break;
				case 'jigoshop_chronous_hsc_display_empty' :
					$emptyCartDisplay = esc_attr(get_option($arg['value']));
					if ($emptyCartDisplay == 'on') {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<input type="checkbox" class="switch-medium" id="' . esc_attr($arg['value']) . '" name="'
						. esc_attr($arg['value']) . '" ' . $checked . ' />';
					break;
				case 'jigoshop_chronous_hsc_select_menu' :
					$menus = SettingsOptions::init()->selectMenu();
					$selected = esc_attr(get_option($arg['value']));
					$html .= '<select id="' . esc_attr($arg['value']) . '" name="' . esc_attr($arg['value']) . '">';
					if (!empty($menus)) {
						foreach ($menus as $key => $value) {
							$html .= '<option value="' . esc_attr($key) . '" ' . selected($selected, $key, false)
								. '>' . esc_attr($value) . '</option>';
						}
					}
					$html .= '</select>';
					break;
				case 'jigoshop_chronous_hsc_text' :
					$cartText = esc_attr(get_option($arg['value']));
					$html .= '<input type="text" class="" size="35" id="' . esc_attr($arg['value']) . '" name="' .
						esc_attr($arg['value']) . '" value="' . esc_attr($cartText) . '" />';
					break;
				default :
					echo '';
					break;
				
			}
		}
		if (!empty($arg['iconOptions'])) {
			$icons = SettingsOptions::init()->cartIcon(SettingsOptions::init()->icons());
			$iconsInCart = $arg['iconOptions'];
			$option = get_option($iconsInCart);
			if (!is_array($option)) {
				return;
			}
			$id = $arg['id'];
			$current = '';
			
			if (isset($option[$id])) {
				$current = $option[$id];
			}
			$img = null;
			$checked = false;
			if (!empty($icons)) {
				foreach ($arg['options'] as $key => $numValue) {
					$html .= '<img src="' . CHRONOUS_HSC_URI . '/images/cartIcons/cartIcon' . $numValue . '.png' . '"
					id="' . __('jchsc_') . $numValue . '" />';
					$html .= '<input type="radio" name="' . esc_attr($iconsInCart . "[{$id}]") . '" id="'
						. esc_attr($iconsInCart . "[{$id}]" . "[{$key}]") . '" value="' . esc_attr($key) . '" '
						. checked($current, $key, false) . ' />';
				}
				add_option('chronousHscOptions');
				foreach ($icons as $compareNum => $icon) {
					if ($current == $compareNum) {
						$img = $icon;
						$checked = true;
					}
				}
				$cartIconArg = [];
				if (true == $checked) {
					$cartIconArg['ID'] = $current;
					$cartIconArg['imgUrl'] = esc_url($img);
				}
				
				$commonArgs = [
					'enable' => esc_attr(get_option('jigoshop_chronous_hsc_enable')),
					'displayCartIcon' => esc_attr(get_option('jigoshop_chronous_hsc_icon_display')),
					'displayCartEvenEmpty' => esc_attr(get_option('jigoshop_chronous_hsc_display_empty')),
					'selectMenu' => esc_attr(get_option('jigoshop_chronous_hsc_select_menu')),
					'textOnCart' => esc_attr(get_option('jigoshop_chronous_hsc_text')),
				];
				
				$args = array_merge($cartIconArg, $commonArgs);
				
				update_option('chronousHscOptions', $args);
				
			}
		}
		
		echo $html;
	}
	
	/**
	 * @param $arg
	 */
	public function ccmMainSettings($arg)
	{
		$html = '';
		if (!empty($arg['value'])) {
			switch ($arg['value']) {
				case 'jigoshop_chronous_ccm_enable' :
					$enable = esc_attr(get_option($arg['value']));
					if ($enable == 'on') {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<input type="checkbox" class="switch-medium" id="' . esc_attr($arg['value']) . '" name="'
						. esc_attr($arg['value']) . '" ' . $checked . ' />';
					break;
				case 'jigoshop_chronous_disable_checkout' :
					$enable = esc_attr(get_option($arg['value']));
					if ($enable == 'on') {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$html .= '<input type="checkbox" class="switch-medium" id="' . esc_attr($arg['value']) . '" name="'
						. esc_attr($arg['value']) . '" ' . $checked . ' />';
					$html .= '<span class="help-block">' . __('Customers will not be able to Checkout to pay orders',
							'jigoshop-chronous') . '</span>';
					break;
				case 'jigoshop_chronous_ccm_text' :
					$this->message = empty(get_option('jigoshop_chronous_ccm_text')) ? '' :
																				get_option('jigoshop_chronous_ccm_text');
					$this->message = str_replace('\\', '', $this->message);
					$html .= wp_editor(wp_kses($this->message, self::allowedHtmlOutput()),
						'custom_checkout_box_message', $this->settings);
					break;
				default :
					$html .= '';
					break;
			}
		}
		
		echo $html;
	}
	
	/**
	 * @param $arg
	 */
	public function homeMainSettings($arg)
	{
		$html = '';
		if (!empty($arg['value'])) {
			switch ($arg['value']) {
				case 'jigoshop_chronous_home_title' :
					$homeTitle = esc_attr(get_option($arg['value']));
					$html .= '<input type="text" class="c-title form-control" size="45" id="' . esc_attr($arg['value'])
									. '" name="' . esc_attr($arg['value']) . '" value="' . esc_attr($homeTitle) . '" />';
					break;
				case 'jigoshop_chronous_home_description' :
					$homeDescription = esc_attr(strip_tags(get_option($arg['value']), '<p><a><strong><em><b><i>'));
					$html .= '<textarea class="form-control" cols="47" rows="5" id="' . esc_attr($arg['value']) . '"
					name="' . esc_attr($arg['value']) . '">' . esc_attr(strip_tags($homeDescription,
																'<p><a><strong><em><b><i>')) . '</textarea>';
					$html .= '<br />';
					$html .= '<span class="description small">Allowed HTM tags are: (< p >< a >< strong >< em >< b >< i >)
																												</span>';
					break;
				case 'jigoshop_chronous_home_image' :
					$value = esc_attr(get_option($arg['value']));
					$name = 'jigoshop_chronous_home_image';
					$imageAttributes = wp_get_attachment_image_src($value, self::IMG_SIZE);
					$image = null;
					if ($imageAttributes) {
						$image = '<img src="' . esc_url($imageAttributes[0]) . '"  />';
					}
					$html = '';
					$html .= '<div>';
					$html .= '<a href="#" style="display:' . (null == $image ? '' : 'none') . '"
						class="button upload_image">' . __('Upload Image for custom background', 'jigoshop-chronous') . '</a>';
					$html .= $image;
					$html .= '<input type="hidden" name="' . esc_attr($name) . '" id="' . esc_attr($name) . '"
					value="' . esc_attr($value) . '" class="t_image" />';
					
					$editUrl = esc_url(admin_url('post.php?post=' . $value . '&action=edit&image-editor'));
					$html .= '&nbsp;&nbsp;<a href="#" class="remove_image" title="Remove Image" style="display:'
																			. (null == $image ? 'none' : 'inline') . '">'
						. __('Remove', 'jigoshop_chronous') . '</a>&nbsp;&nbsp;';
					$html .= '<a href="' . $editUrl . '" class="edit_image" target="_blank" title="Edit" style="display:'
																			. (null == $image ? 'none' : 'inline') . '">'
						. __('Edit', 'jigoshop_chronous') . '</a>';
					$html .= '</div>';
					break;
				case 'jigoshop_chronous_home_image_size' :
					$size = esc_attr(get_option($arg['value']));
					$html .= '<select class="c-select form-control" id="' . esc_attr($arg['value']) . '" name="'
																						. esc_attr($arg['value']) . '">';
					foreach (SettingsOptions::init()->imageSize() as $key => $value) {
						$html .= '<option class="c-option" value="' . esc_attr($key) . '" ' . selected($size, $key, false) . '>' . esc_attr($value) . '</option>';
					}
					break;
				default :
					$html .= '';
					break;
			}
			
			if (!empty($arg['value'])) {
				$args = [
					'title' => esc_attr(get_option('jigoshop_chronous_home_title')),
					'description' => esc_attr(get_option('jigoshop_chronous_home_description')),
					'imageID' => esc_attr(get_option('jigoshop_chronous_home_image')),
					'imageSize' => esc_attr(get_option('jigoshop_chronous_home_image_size')),
				];
				
				update_option(self::HOME_PAGE, $args);
			}
			
		}
		
		echo $html;
	}
	
	public function welcomeTheme($arg)
	{
		$html = '';
		switch ($arg['value']) {
			case 'jigoshop_chronous_welcome_title' :
				$html .= '<span class="about-chronous">' . __('Jigoshop Chronous Theme. V 1.0', 'jigoshop-chronous') . '</span>';
				break;
			case 'jigoshop_chronous_contact_theme' :
				$html .= '<span class="contact-chronous"><a href="' . esc_url('https://www.jigoshop.com/contact/') . '">' . __('Contact Us', 'jigoshop-chronous') . '</a></span>';
				break;
			case 'jigoshop_chronous_support' :
				$html .= '<span class="contact-chronous"><a href="' . esc_url('https://www.jigoshop.com/support/') . '">' . __('Support', 'jigoshop-chronous') . '</a>
				&nbsp;|&nbsp;<a href="' . esc_url('https://www.jigoshop.com/product-category/themes/jigoshop-chronous/documentation') . '">' . __('Documentation', 'jigoshop-chronous') . '</a></span>';
				break;
			default :
				$html .= '';
		}
		
		echo $html;
	}
	
	/**
	 * @param $arg
	 */
	public function generalSettings($arg)
	{
		$html = '';
		switch ($arg['value']) {
			case 'jigoshop_chronous_logo_image' :
				$value = esc_attr(get_option($arg['value']));
				$name = 'jigoshop_chronous_logo_image';
				$imageAttributes = wp_get_attachment_image_src($value, self::IMG_SIZE);
				$image = null;
				if ($imageAttributes) {
					$image = '<img src="' . esc_url($imageAttributes[0]) . '"  />';
				}
				$html = '';
				$html .= '<div>';
				$html .= '<a href="#" style="display:' . (null == $image ? 'inline-block' : 'none') . '"
						class="c-image button upload_image">' . __('Upload', 'jigoshop-chronous') . '</a>';
				$html .= $image;
				$html .= '<input type="hidden" name="' . esc_attr($name) . '" id="' . esc_attr($name) . '"
					value="' . esc_attr($value) . '" class="t_image" />';
				
				$editUrl = esc_url(admin_url('post.php?post=' . $value . '&action=edit&image-editor'));
				$html .= '&nbsp;&nbsp;<a href="#" class="remove_image" title="Remove Image" style="display:' .
																			(null == $image ? 'none' : 'inline') . '">'
					. __('Remove', 'jigoshop_chronous') . '</a>&nbsp;&nbsp;';
				$html .= '<a href="' . $editUrl . '" class="edit_image" target="_blank" title="Edit" style="display:' .
																			(null == $image ? 'none' : 'inline') . '">'
					. __('Edit', 'jigoshop_chronous') . '</a>';
				$html .= '</div>';
				break;
			case 'jigoshop_chronous_favicon' :
				$value = esc_attr(get_option($arg['value']));
				$name = 'jigoshop_chronous_favicon';
				$imageAttributes = wp_get_attachment_image_src($value, self::IMG_SIZE);
				$image = null;
				if ($imageAttributes) {
					$image = '<img src="' . esc_url($imageAttributes[0]) . '"  />';
				}
				$html = '';
				$html .= '<div>';
				$html .= '<a href="#" style="display:' . (null == $image ? 'inline-block' : 'none') . '"
						class="c-image button upload_favicon">' . __('Upload', 'jigoshop-chronous') . '</a>';
				$html .= $image;
				$html .= '<input type="hidden" name="' . esc_attr($name) . '" id="' . esc_attr($name) . '"
					value="' . esc_attr($value) . '" class="favicon_image" />';
				
				$editUrl = esc_url(admin_url('post.php?post=' . $value . '&action=edit&image-editor'));
				$html .= '&nbsp;&nbsp;<a href="#" class="remove_favicon" title="Remove Image" style="display:' .
																				(null == $image ? 'none' : 'inline') . '">'
					. __('Remove', 'jigoshop_chronous') . '</a>&nbsp;&nbsp;';
				$html .= '<a href="' . $editUrl . '" class="favicon_edit_image" target="_blank" title="Edit" style="display:'
																			. (null == $image ? 'none' : 'inline') . '">'
					. __('Edit', 'jigoshop_chronous') . '</a>';
				$html .= '</div>';
				break;
			case 'jigoshop_chronous_background_color' :
				$cPicker = esc_attr(get_option($arg['value']));
				$cPicker = str_replace('#', '', $cPicker);
				if (!empty($cPicker)) {
					if (strlen($cPicker) < 3 || strlen($cPicker) > 6) {
						add_settings_error('cPickerError', 'ChronousErrorMessage',
																__('Please set the color format correctly(#xxx or #xxxxxx)'
							, 'jigoshop-chronous'), 'error');
						settings_errors('cPickerError');
						$cPicker = null;
					}
				}
				$html .= '<input class="c-color c_picker form-control" type="text" size="10" id="c_picker"
					name="' . esc_attr($arg['value']) . '" value="' . esc_attr($cPicker) . '" placeholder="#" />';
				break;
			case 'jigoshop_chronous_which_page' :
				if ('' == get_option('jigoshop_chronous_background_color') && !empty(get_option('jigoshop_chronous_which_page'))) {
					add_settings_error('pagesError', 'ChronousErrorMessage',
														__('Body background color should not be empty when you select pages'
						, 'jigoshop-chronous'), 'warning');
					settings_errors('pagesError');
					update_option('jigoshop_chronous_which_page', '');
				}
				$whichPage = get_option('jigoshop_chronous_which_page');
				$pages = [];
				if (isset($whichPage['pages']) && !empty($whichPage['pages'])) {
					$pages = filter_var_array($whichPage['pages'], FILTER_SANITIZE_STRING);
					
					foreach ($pages as $k => $value) {
						if ($value == 'all' && count($pages) > 1) {
							add_settings_error('pagesError', 'ChronousErrorMessage',
													__('"All Pages" background on Body should be checked without other pages'
								, 'jigoshop-chronous'), 'error');
							settings_errors('pagesError');
							$pages = [];
							update_option('jigoshop_chronous_which_page', '');
						}
					}
				}
				foreach (SettingsOptions::init()->selectPage() as $key => $value) {
					$html .= '<input type="checkbox" class="switch-medium" name="jigoshop_chronous_which_page[pages][]"
						id="' . esc_attr($key) . '-body" value="' . esc_attr($key) . '"' .
															(in_array(esc_attr($key), $pages) ? 'checked' : '') . '/>';
					$html .= '<label for="' . esc_attr($key) . '-body" style="padding-left:5px;padding-right:15px;">'
																						. esc_attr($value) . '</label>';
					
				}
				break;
			case 'jigoshop_chronous_footer_background':
				$fBPicker = esc_attr(get_option($arg['value']));
				$fBPicker = str_replace('#', '', $fBPicker);
				if (!empty($fBPicker)) {
					if (strlen($fBPicker) < 3 || strlen($fBPicker) > 6) {
						add_settings_error('cPickerError', 'ChronousErrorMessage',
															__('Please set the color format correctly(#xxx or #xxxxxx)'
							, 'jigoshop-chronous'), 'error');
						settings_errors('cPickerError');
						$fBPicker = null;
					}
				}
				$html .= '<input class="c-color f_picker form-control" type="text" size="10" id="f_picker"
					name="' . esc_attr($arg['value']) . '" value="' . esc_attr($fBPicker) . '" placeholder="#" />';
				break;
			case 'jigoshop_chronous_which_footer' :
				if ('' == get_option('jigoshop_chronous_footer_background') &&
																!empty(get_option('jigoshop_chronous_which_footer'))) {
					add_settings_error('pagesError', 'ChronousErrorMessage',
												__('Footer background color should not be empty when you select pages'
						, 'jigoshop-chronous'), 'warning');
					settings_errors('pagesError');
					update_option('jigoshop_chronous_which_footer', '');
				}
				$whichPage = get_option('jigoshop_chronous_which_footer');
				$pages = [];
				if (isset($whichPage['pages']) && !empty($whichPage['pages'])) {
					$pages = filter_var_array($whichPage['pages'], FILTER_SANITIZE_STRING);
					
					foreach ($pages as $k => $value) {
						if ($value == 'all' && count($pages) > 1) {
							add_settings_error('footerError', 'ChronousErrorMessage',
											__('"All Pages" background on Footer should be checked without other pages'
								, 'jigoshop-chronous'), 'error');
							settings_errors('footerError');
							$pages = [];
							update_option('jigoshop_chronous_which_footer', '');
						}
					}
				}
				foreach (SettingsOptions::init()->selectPage() as $key => $value) {
					$html .= '<input type="checkbox" class="switch-medium" name="jigoshop_chronous_which_footer[pages][]"
						id="' . esc_attr($key) . '-footer" value="' . esc_attr($key) . '"' .
															(in_array(esc_attr($key), $pages) ? 'checked' : '') . '/>';
					$html .= '<label for="' . esc_attr($key) . '-footer" style="padding-left:5px;padding-right:15px;">'
																						. esc_attr($value) . '</label>';
					
				}
				break;
			case 'jigoshop_chronous_products_per_shop_page' :
				if ('' != get_option('jigoshop_chronous_products_per_shop_page')) {
					if (!ctype_digit(get_option('jigoshop_chronous_products_per_shop_page')) ||
														get_option('jigoshop_chronous_products_per_shop_page') == 0) {
						add_settings_error('perProError', 'ChronousErrorMessage',
													__('Value for Products Per Page is incorrect (Must be greater then 0)'
							, 'jigoshop-chronous'), 'error');
						settings_errors('perProError');
						update_option('jigoshop_chronous_products_per_shop_page', '');
					}
				}
				$perPage = esc_attr(get_option($arg['value']));
				$html .= '<input type="number" class="c-color c_picker form-control" id="' . esc_attr($arg['value']) . '"
				name="' . esc_attr($arg['value']) . '" value="' . esc_attr($perPage) . '" placeholder="12" />';
				$html .= '<span class="short">' . __('This will overrides the value set in <a href="' .
											esc_url(admin_url('admin.php?page=jigoshop_settings&tab=shopping')) . '">' .
								__('Jigoshop Settings', 'jigoshop-chronous') . '</a>', 'jigoshop-chronous') . '</span>';
				break;
			case 'jigoshop_chronous_custom_css_enable' :
				$enable = esc_attr(get_option($arg['value']));
				if ($enable == 'on') {
					$checked = 'checked';
				} else {
					$checked = '';
				}
				$html .= '<input type="checkbox" class="jigoshop switch-medium" id="' . esc_attr($arg['value']) .
														'" name="' . esc_attr($arg['value']) . '" ' . $checked . ' />';
				break;
			case 'jigoshop_chronous_custom_css' :
				$css = strip_tags(get_option($arg['value']));
				$html .= '<textarea class="form-control" cols="15" rows="5" id="' . esc_attr($arg['value']) .
																			'" name="' . esc_attr($arg['value']) . '">';
				$html .= strip_tags(esc_attr($css));
				$html .= '</textarea>';
				break;
			default :
				$html .= '';
				break;
		}
		
		echo $html;
	}
}