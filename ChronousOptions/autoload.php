<?php

/**
 * @param $class
 * @without Jigoshop this is useless
 */
function chronous($class) {
$class = str_replace('\\', '/', $class);

	$classFile = get_template_directory() . '/ChronousOptions/' . $class . '.php';

	if(file_exists($classFile)) {
		/** @noinspection PhpIncludeInspection */
		require $classFile;
	}
}

spl_autoload_register('chronous');