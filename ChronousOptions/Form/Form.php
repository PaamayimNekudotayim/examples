<?php

namespace Form;


use Jigoshop\Integration;
use SettingsSection\SettingsSection;

class Form
{
	const PLUGIN_NAME = 'Jigoshop Header Shopping Cart';
	const PLUGIN = 'jigoshop-header-shopping-cart/bootstrap.php';
	
	private static $form;
	private static $errors = [];
	private static $errorNum = [];
	
	private function __construct()
	{
		add_action('init', [$this, 'checkIfHscIsInstall']);
	}
	
	private function __clone()
	{
	}
	
	/**
	 * @return Form
	 */
	public static function this()
	{
		if (null == self::$form) {
			self::$form = new self();
		}
		
		return self::$form;
	}
	
	/**
	 *
	 */
	function checkIfHscIsInstall()
	{
		if (is_plugin_active(self::PLUGIN)) {
			echo '<div class="error"><p>' . __('It seems you have active the plugin '
					. self::PLUGIN_NAME . '. Please <a href="' . esc_url($this->deactivatePlugin(self::PLUGIN, 'deactivate')) . '">' . __('disable') . '</a> it before using this option', '') . '</p></div>';
			
			exit();
		}
	}
	
	/**
	 * https://nazmulahsan.me/generate-activation-deactivation-link-wordpress-plugin/
	 */
	private function deactivatePlugin($plugin, $action = 'activate')
	{
		if (strpos($plugin, '/')) {
			$plugin = str_replace('\/', '%2F', $plugin);
		}
		$url = sprintf(admin_url('plugins.php?action=' . $action . '&plugin=%s&plugin_status=all&paged=1&s'), $plugin);
		$_REQUEST['plugin'] = $plugin;
		$url = wp_nonce_url($url, $action . '-plugin_' . $plugin);
		
		return $url;
	}
	
	/**
	 * @param $form
	 * @return Form|string
	 */
	public static function _form($form)
	{
		if (!is_admin()) {
			wp_die('Ops. It seems you are not allowed to be here');
		}
		switch ($form) {
			case 'form' :
				return Form::this();
			case 'no_form' :
				return Form::this();
				break;
			default :
				die(sprintf(__('Option ' . $form . ' could not be found', 'jigoshop_chronous')));
				break;
		}
	}
	
	/**
	 * @HeaderShoppingCart
	 * @param $form
	 */
	public function renderSettings($form)
	
	{
		if (!current_user_can('manage_options')) {
			wp_die('Sorry you are not allowed to edit this page');
		}
		if (!is_admin()) {
			wp_die('Ops. You are not in appropriate page sparky!');
		}
		if (isset($_GET['settings-updated'])) {
			add_settings_error('jigoshopChronousMessage', 'ChronousHscMessage', __('Settings Saved', 'jigoshop-chronous'),
				'updated');
		}
		$this->checkIfHscIsInstall();
		
		settings_errors('jigoshopChronousMessage');
		
		echo '<div class="wrap jigoshop ChronusOption">';
		echo '<form action="options.php" method="post">';
		wp_nonce_field(
			'update-options'
		);
		switch ($form) {
			case SettingsSection::HSC_OPTIONS :
				do_settings_sections(SettingsSection::HSC_OPTIONS);
				settings_fields(SettingsSection::HSC_OPTIONS);
				break;
			case SettingsSection::HOME_PAGE :
				do_settings_sections(SettingsSection::HOME_PAGE);
				settings_fields(SettingsSection::HOME_PAGE);
				break;
			case SettingsSection::WELCOME :
				do_settings_sections(SettingsSection::WELCOME);
				settings_fields(SettingsSection::WELCOME);
				break;
			case SettingsSection::GENERAL_SETTINGS :
				do_settings_sections(SettingsSection::GENERAL_SETTINGS);
				settings_fields(SettingsSection::GENERAL_SETTINGS);
			break;
			case SettingsSection::CHECKOUT_OPTIONS:
				do_settings_sections(SettingsSection::CHECKOUT_OPTIONS);
				settings_fields(SettingsSection::CHECKOUT_OPTIONS);
			break;
			default :
				die('No Settings Found');
				break;
		}
		
		
		submit_button(__('Save Changes', 'jigoshop-chronous'));
		
		echo '</form>';
		echo '</div>';
	}
	/**
	 * @void
	 */
	public function noForm()
	{
		echo '<div class="wrap jigoshop ChronusOption">';
		echo '<h1>' . __('No Form found', 'jigoshop-chronous') . '</h1>';
		echo '</div>';
	}
}