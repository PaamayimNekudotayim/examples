<?php

namespace Form;


use SettingsSection\SettingsSection;

class Tab
{
	/**
	 * @param $tab string
	 * @param $page string
	 * @param string $active
	 */
	public static function randerTab($tab, $page, $active = '')
	{
		$html = '';
		if (!empty($tab)) {
			$html .= '<a href="' . esc_url(admin_url('admin.php?page=chronous-settings&tab=' . esc_attr($tab) . '')) . '"
			class="nav-tab ' . ($_GET['tab'] == esc_attr($tab) ? $active : '') . '">';
			$html .= __($page, 'jigoshop-chronous');
			$html .= '</a>';
		}
		echo $html;
	}
	
	/**
	 * @return array tabs
	 */
	public static function renderTabs()
	{
		return [
			[
				'tab' => 'chronous-settings',
				'href' => 'admin.php?page=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['page']) && $_GET['page'] == 'chronous-settings' && !isset($_GET['tab']) ? 'nav-tab-active' : ''),
				'page' => __('Chronous Theme', 'jigoshop-chronous'),
			],
			[
				'tab' => 'general-options',
				'href' => 'admin.php?page=chronous-settings&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] == 'general-options' ? 'nav-tab-active' : ''),
				'page' => __('General Settings', 'jigoshop-chronous'),
			],
			[
				'tab' => 'home-options',
				'href' => 'admin.php?page=chronous-settings&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] =='home-options' ? 'nav-tab-active' : ''),
				'page' => __('Home Page', 'jigoshop-chronous'),
			],
			[
				'tab' => 'cart-options',
				'href' => 'admin.php?page=chronous-settings&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] =='cart-options' ? 'nav-tab-active' : ''),
				'page' => __('Cart Options', 'jigoshop-chronous'),
			],
			[
				'tab' => 'checkout-options',
				'href' => 'admin.php?page=chronous-settings&tab=',
				'class' => 'nav-tab ',
				'active' => (isset($_GET['tab']) && $_GET['tab'] =='checkout-options' ? 'nav-tab-active' : ''),
				'page' => __('Checkout Options', 'jigoshop-chronous'),
			]
		];
	}
	
	/**
	 * @void
	 */
	public static function addTabSettings()
	{
		if (isset($_GET['page']) && $_GET['page'] == 'chronous-settings' && !isset($_GET['tab'])) {
			Form::_form('form')->renderSettings(SettingsSection::WELCOME);
		}
		if (isset($_GET['tab'])) {
			switch ($_GET['tab']) {
				case 'cart-options' :
					Form::_form('form')->renderSettings(SettingsSection::HSC_OPTIONS);
					break;
				case 'home-options' :
					Form::_form('form')->renderSettings(SettingsSection::HOME_PAGE);
					break;
				case 'general-options' :
					Form::_form('form')->renderSettings(SettingsSection::GENERAL_SETTINGS);
					break;
				case 'checkout-options' :
					Form::_form('form')->renderSettings(SettingsSection::CHECKOUT_OPTIONS);
					break;
				default :
					Form::_form('no_form')->noForm();
			}
		}
	}
	
	/**
	 * @return string
	 */
	public static function activeTab()
	{
		$active = '';
		if(isset($_GET['page']) && $_GET['page'] == 'chronous-settings' && !isset($_GET['tab'])){
			$active = 'nav-tab-active';
		}
		
		return $active;
	}
}