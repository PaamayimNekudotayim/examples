<?php

namespace RegisterSettings;


use SettingsSection\SettingsSection;

class RegisterSettings
{
	private static $instance;
	
	public static function settings()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	/**
	 * @param $settings
	 */
	public static function register($settings)
	{
		switch ($settings) {
			case 'settings' :
				self::settings()->add();
				break;
			default :
				add_settings_error('error', 'ChronousErrorHscMessage', 'Could not save the request!',
					'error');
				settings_errors('error');
				break;
		}
	}
	
	public function add()
	{
		// Jigoshop Chronous Header Shopping Cart -> jchsc
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_enable'
		);
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_icon_display'
		);
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_display_empty'
		);
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_select_menu'
		);
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_text'
		);
		register_setting(
			SettingsSection::HSC_OPTIONS,
			'jigoshop_chronous_hsc_icon'
		);
		
		// Home Page
		register_setting(
			SettingsSection::HOME_PAGE,
			'jigoshop_chronous_home_title'
		);
		register_setting(
			SettingsSection::HOME_PAGE,
			'jigoshop_chronous_home_description'
		);
		register_setting(
			SettingsSection::HOME_PAGE,
			'jigoshop_chronous_home_image'
		);
		register_setting(
			SettingsSection::HOME_PAGE,
			'jigoshop_chronous_home_image_size'
		);
		// Welcome Tab
		register_setting(
		SettingsSection::WELCOME,
			'jigoshop_chronous_welcome_title'
		);
		register_setting(
		SettingsSection::WELCOME,
			'jigoshop_chronous_contact_theme'
		);
		register_setting(
		SettingsSection::WELCOME,
			'jigoshop_chronous_support'
		);
		
		// General Settings
		register_setting(
			SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_background_color'
		);
		register_setting(
			SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_footer_background'
		);
		register_setting(
			SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_which_page'
		);
		register_setting(
			SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_which_footer'
		);
		register_setting(
		SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_logo_image'
		);
		register_setting(
		SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_favicon'
		);
		register_setting(
			SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_products_per_shop_page'
		);
		register_setting(
		SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_custom_css_enable'
		);
		register_setting(
		SettingsSection::GENERAL_SETTINGS,
			'jigoshop_chronous_custom_css'
		);
		
		// Custom Message
		register_setting(
			SettingsSection::CHECKOUT_OPTIONS,
			'jigoshop_chronous_ccm_enable'
		);
		register_setting(
			SettingsSection::CHECKOUT_OPTIONS,
			'jigoshop_chronous_ccm_text'
		);
		register_setting(
			SettingsSection::CHECKOUT_OPTIONS,
			'jigoshop_chronous_disable_checkout'
		);
	}
}