<?php

namespace ChronousOptionsInit;

use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Output\Output;
use RegisterSettings\RegisterSettings;
use SettingsSection\SettingsSection;

if (!defined('CHRONOUS_HSC_URI')) {
	define('CHRONOUS_HSC_URI', get_template_directory_uri());
}
if (!defined('CHRONOUS_HSC_DIRECTORY')) {
	define('CHRONOUS_HSC_DIRECTORY', get_template_directory());
}

class ChronousOptionsInit
{
	public function __construct()
	{
		add_action('admin_menu', [$this, 'chronousThemeOptions']);
		add_action('admin_init', [$this, 'initJigoshopHscSettings']);
		add_action('wp_head', [$this, 'favicon']);
		add_action('wp_head', [$this, 'customCss']);
		add_filter('jigoshop\query\product_list_base', [$this,'productsPerPage'], 10, 2);
	}
	
	/**
	 * @theme page
	 */
	public function chronousThemeOptions()
	{
		$page = add_theme_page('Chronus Options',
			'Chronous Options', 'edit_theme_options',
			'chronous-settings', [$this, 'displayOptions']
		);
		// Niby to jest potepione ale w dokumentacji nie znalazlem :(
		add_action('admin_print_scripts-'.$page, [$this, 'initScripts']);
	}
	
	/**
	 * @tab
	 */
	public function displayOptions()
	{
		Output::output('admin/tab', []);
	}
	
	public function initJigoshopHscSettings()
	{
		SettingsSection::addSettings('section');
		RegisterSettings::register('settings');
	}
	
	/**
	 *
	 */
	public function initScripts()
	{
		if (!is_admin()) {
			return;
		}
		Styles::add('jigoshop.styles.new', CHRONOUS_HSC_URI . '/ChronousOptions/inc/style.css');
		Styles::add('jigoshop.vendors.bs_switch', \JigoshopInit::getUrl() . '/assets/css/vendors/bs_switch.css');
		Scripts::add('jigoshop.vendors.bs_switch', \JigoshopInit::getUrl() . '/assets/js/vendors/bs_switch.js');
		Scripts::add('jigoshop.chronous.script', CHRONOUS_HSC_URI . '/ChronousOptions/inc/script.js');
		Scripts::localize('jigoshop.chronous.script', 'chronus', [
			'title' => __('Upload Image', 'jigoshop-chronous'),
			'description' => __('Upload Image', 'jigoshop-chronous'),
		]);
		//Color Picker
		Scripts::add('chronous.color.picker.js', CHRONOUS_HSC_URI . '/ChronousOptions/inc/colorpicker/js/colorpicker.js', ['jquery'], ['in_footer' => true, 'version' => '1.0']);
		Styles::add('jigoshop.color.picker.css', CHRONOUS_HSC_URI . '/ChronousOptions/inc/colorpicker/css/colorpicker.css');
		if (!did_action('wp_enqueue_media')) {
			wp_enqueue_media();
		}
	}
	
	public function favicon()
	{
		$favicon = get_option('jigoshop_chronous_favicon');
		$url = wp_get_attachment_image_url($favicon, 'full');
		
		if (false <> $url) {
			echo '<link rel="shortcut icon" href="' . esc_url($url) . '" />';
		}
	}
	
	/**
	 * @custom css
	 */
	public function customCss()
	{
		$css = null;
		$html = '';
		$customCss = esc_attr(get_option('jigoshop_chronous_custom_css'));
		$enabled = esc_attr(get_option('jigoshop_chronous_custom_css_enable'));
		
		if('' !== $customCss || null !== $customCss){
			$css = true;
		}
		if('on' == $enabled && $enabled){
			if(true == $css){
				$html .=
					"\n"
						.'<style type="text/css" id="jigoshop-chronous-custom-css">'
							. "\n\t" . stripslashes($customCss) . "\n"
						. '</style>'
					. "\n";
			}
		}
		
		echo $html;
	}
	
	/**
	 * @param $result
	 * @param $request
	 * @return mixed
	 */
	public function productsPerPage($result, $request){
		$productsPerPage = esc_attr((int)get_option('jigoshop_chronous_products_per_shop_page'));
		if(!ctype_digit($productsPerPage)){
			return;
		}
		if(!empty($request)){
			if(!empty($productsPerPage) && '' != $productsPerPage){
				$result['posts_per_page'] = $productsPerPage;
			}
		}
		
		return $result;
	}
}