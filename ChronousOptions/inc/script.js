jQuery(function ($) {
    return $("input[type=checkbox].switch-medium").bootstrapSwitch({
        size: "small",
        onText: "Yes",
        offText: "No"
    }), $("input[type=checkbox].switch-mini").bootstrapSwitch({
        size: "mini",
        onText: "Yes",
        offText: "No"
    }), $("input[type=checkbox].switch-normal").bootstrapSwitch({size: "normal", onText: "Yes", offText: "No"})

});
jQuery(document).ready(function ($) {
    $('.upload_image').on('click', function (event) {
        event.preventDefault();
        var button = $(this),
            frame = wp.media.frames.frame = wp.media({
                title: chronus.title,
                button: {
                    text: chronus.description
                },
                library: {
                    type: 'image'
                },
                multiple: false
            });
        frame.on('select', function () {
            var attachment = frame.state().get('selection').first().toJSON();
            $(button).removeClass('button').html('<img class="ct_image" ' + 'style="max-width: 25%"' +
                'src="' + attachment.url + '"/>').next().val(attachment.id).next().show();
        });
        frame.open();
    });
    $('.remove_image').on('click', function (event) {
        $(this).hide().prev().val('').prev().addClass('button').html(chronus.title);
        $('.upload_image').show();
        $('.edit_image').hide();
        $('.t_image').attr('value','');
        $('img.ct_image').remove();
        $('img.button').remove();
        return false;
    });


    $('.upload_favicon').on('click', function (event) {
        event.preventDefault();
        var button = $(this),
            frame = wp.media.frames.frame = wp.media({
                title: chronus.title,
                button: {
                    text: chronus.description
                },
                library: {
                    type: 'image'
                },
                multiple: false
            });
        frame.on('select', function () {
            var attachment = frame.state().get('selection').first().toJSON();
            $(button).removeClass('button').html('<img class="favicon_image" ' + 'style="max-width: 50%"' +
                'src="' + attachment.url + '"/>').next().val(attachment.id).next().show();
        });
        frame.open();
    });
    $('.remove_favicon').on('click', function (event) {
        $(this).hide().prev().val('').prev().addClass('button').html(chronus.title);
        $('.upload_favicon').show();
        $('.favicon_edit_image').hide();
        $('.favicon_image').attr('value','');
        $('img.favicon_image').remove();
        $('img.button').remove();
        return false;
    });


    $('#c_picker, #f_picker').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
            $(el).val(hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        }
    })
        .bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
});