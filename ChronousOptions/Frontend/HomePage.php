<?php

namespace Frontend;


use Jigoshop\Frontend\Pages;

class HomePage
{
	// Could be extends some Root or Ground but whatever I like this way.
	private static $instance;
	
	private function __construct()
	{
	
	}
	
	private function __clone(){}
	
	public static function home()
	{
		if(null == self::$instance){
			self::$instance = new HomePage();
		}
		
		return self::$instance;
	}
	
	/**
	 * @param $option array
	 */
	public function getHomeImageBackground($option)
	{
		$style = '';
		
		if(!empty($option['imageID'])){
			$style = 'style="background:url(\'' . esc_url(wp_get_attachment_image_url($option['imageID'], $option['imageSize'])) . '\') no-repeat top center"';
		}
		
		echo $style;
	}
	
	/**
	 * @param $option array
	 */
	public function getHomePageTitle($option)
	{
		
		if(!empty($option['title'])){
			$title = esc_attr($option['title']);
		}else{
			$title = 'Gear up for mountains';
		}
		
		echo $title;
	}
	
	/**
	 * @param $option array
	 */
	public function getHomePageShortDescription($option)
	{
		if(!empty($option['description'])){
			$description = esc_attr($option['description']);
		}else{
			$description = 'All you need to travel in one place. Ruined by professional hitchhikers team.';
		}
		
		echo $description;
	}
	
	public function whatPage()
	{
		return [
			'shop' => Pages::isShop(),
			'cart' => Pages::isCart(),
			'account' => Pages::isAccount(),
			'checkout' => Pages::isCheckout(),
			'front' => is_front_page(),
		];
	}
}