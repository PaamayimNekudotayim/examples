<?php

namespace Frontend;

class FrontendOptions
{
	private static $front;
	
	/**
	 * @return FrontendOptions
	 */
	public static function front()
	{
		if (null == self::$front) {
			self::$front = new self();
		}
		
		return self::$front;
	}
	
	/**
	 * @BgColor
	 */
	public function setBgColor()
	{
		$bgColor = esc_attr(get_option('jigoshop_chronous_background_color'));
		$style = '';
		if (!empty($bgColor)) {
			$style = 'style="background: #' . esc_attr($bgColor) . '"';
		}
		echo $style;
	}
	
	public function setFooterColor()
	{
		$footerColor = esc_attr(get_option('jigoshop_chronous_footer_background'));
		$style = '';
		if (!empty($footerColor)) {
			$style = 'style="background: #' . esc_attr($footerColor) . '"';
		}
		echo $style;
	}
	
	/**
	 * @param $whichPages
	 * @param $pages
	 * @return null|bool
	 * @jest w 'header.php'
	 */
	public function getBgColor($whichPages, $pages)
	{
		$style = null;
		if (!empty($whichPages) && is_array($whichPages)) {
			foreach ($whichPages['pages'] as $key => $value) {
				if ($value == 'all') {
					$this->setBgColor();
					break; // Cza?
				} else {
					if (in_array($value, $pages)) {
						foreach ($pages as $k => $v) {
							if ($v == true) {
								if ($k == $value) {
									$style = true;
								}
							}
						}
					}
					if (true == $style) {
						$this->setBgColor();
					}
				}
			}
		}
		return $style;
	}
	
	/**
	 * @param $whichPages
	 * @param $pages
	 * @return null|bool
	 * @mogła być jedna metoda dla obu typu $where = 'footer' lub 'body'
	 * @albo mogłem dać jakiegos trait'a, ale tak tez mozna i mi sie podoba, czy nie??? Pomysle poznej.
	 * @ale prawde mowiac jedna metoda typu ->setBgColor('footer') (Czy Colour?) lub ->setBgColor('body') wyglada duzo lepiej
	 * @no i ten kod tez mozna albo trzeba? poprawic! tak samo above;
	 * @jest w 'footer.php'
	 */
	public function getFooterColor($whichPages, $pages)
	{
		$style = null;
		if (!empty($whichPages) && is_array($whichPages)) {
			foreach ($whichPages['pages'] as $key => $value) {
				if ($value == 'all') {
					$this->setFooterColor();
					break; // Cza?
				} else {
					if (in_array($value, $pages)) {
						foreach ($pages as $k => $v) {
							if ($v == true) {
								if ($k == $value) {
									$style = true;
								}
							}
						}
					}
					if (true == $style) {
						$this->setFooterColor();
					}
				}
			}
		}
		return $style;
	}
	
	/**
	 * @return string
	 */
	public function getLogoImage()
	{
		$logo = get_option('jigoshop_chronous_logo_image');
		
		$img = wp_get_attachment_image_url($logo, 'full');
		
		return esc_url($img);
	}
}