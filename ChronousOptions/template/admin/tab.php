<?php use Form\Tab;?>
<div class="wrap jigoshop" style="overflow: hidden">
    <h1 class="chronous-settings"><?php echo sprintf(__('Chronus Settings', 'jigoshop-chronous')); ?></h1>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="settingsBar">
                <ul class="nav navbar-nav">
		            <?php foreach(Tab::renderTabs() as $tabs): ?>
                        <li class="<?php echo $tabs['class'] . $tabs['active'] ?>">
                            <a class="no-decoration" href="<?php echo esc_url(admin_url($tabs['href'] . $tabs['tab'])); ?>">
                                <?php echo esc_html($tabs['page']) ?>
                            </a>
                        </li>
		            <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="clear"></div>
    <div class="tab-content">
        <div class="tab-panel">
	        <?php Tab::addTabSettings(); ?>
        </div>
    </div>
</div>
<div class="clear"></div>
