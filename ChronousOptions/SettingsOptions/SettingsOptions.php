<?php

namespace SettingsOptions;


class SettingsOptions
{
	private static $init;
	
	public static function init()
	{
		if(null == self::$init){
			self::$init = new self();
		}
		
		return self::$init;
	}
	
	/**
	 * @return array
	 */
	public function selectMenu()
	{
		$adminMenus = get_terms(
			'nav_menu',
			[
				'hide_empty' => false,
			]
		);
		
		$menus = [];
		if (!empty($adminMenus)) {
			foreach ($adminMenus as $navMenu) {
				$menus[$navMenu->slug] = $navMenu->name;
			}
		}
		
		return $menus;
	}
	
	/**
	 * @param $cartIcons
	 * @return array
	 */
	public function cartIcon($cartIcons)
	{
		$icons = array();
		
		if (is_array($cartIcons)) {
			foreach ($cartIcons as $key => $icon) {
				$icons[] = CHRONOUS_HSC_URI . '/images/cartIcons/' . $icon . '.png';
			}
		}
		
		return $icons;
	}
	
	/**
	 * @return array
	 */
	public function icons()
	{
		return [
			// Keys should be 0,1,2,3 but whatever, I like this
			'1' => 'cartIcon0',
			'2' => 'cartIcon1',
			'3' => 'cartIcon2',
			'4' => 'cartIcon3',
		];
	}
	
	/**
	 * @return array
	 */
	public function imageSize()
	{
		return [
			'medium' => __('Medium', 'jigoshop_chronous'),
			'full' => __('Full', 'jigoshop_chronous'),
		];
	}
	
	/**
	 * @return array
	 */
	public function selectPage()
	{
		return [
			'all' => __('All Pages', 'jigoshop-chronous'),
			'front' => __('Front Page', 'jigoshop-chronous'),
			'shop' => __('Shop', 'jigoshop-chronous'),
			'cart' => __('Cart', 'jigoshop-chronous'),
			'account' => __('Account', 'jigoshop-chronous'),
			'checkout' => __('Checkout', 'jigoshop-chronous'),
		];
	}
}