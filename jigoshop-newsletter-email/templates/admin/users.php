<?php
/**
 * @var $subscribers array
 * @var $countUsers string
 * @var $pagination array
 * @var $spin string, url to loader image
 */
?>
<div id="wpwrap">
    <div id="wpcontent">
        <div id="wpbody" role="main">
            <div id="wpbody-content" aria-label="Main content" tabindex="0">
                <div class="wrap">
                    <?php if(!empty($subscribers)) : ?>
                     <h1 class="wp-heading-inline"><?php _e('Subscribers', 'jne') ?>
                        (<?php echo esc_html($countUsers); ?>)</h1>
                        <div id="res-jne"></div>
                        <table class="wp-list-table widefat">
                            <thead>
                            <tr>
                                <th scope="col" id="status" class="manage-column">ID</th>
                                <th scope="col" id="status" class="manage-column">Name</th>
                                <th scope="col" id="title" class="manage-column">Email</th>
                                <th scope="col" id="customer" class="manage-column">Is Signed?</th>
                                <th scope="col" id="customer" class="manage-column">Delete User</th>
                            </tr>
                            </thead>
                            <tbody>
                           
	                            <?php foreach($subscribers as $subscriber) : ?>
                                    <tr>
                                        <td><?php echo esc_attr($subscriber['userID']); ?></td>
                                        <td><?php echo esc_attr($subscriber['userName']); ?></td>
                                        <td><?php echo esc_attr($subscriber['userEmail']); ?></td>
                                        <td><?php echo esc_attr($subscriber['isUserSigned']); ?></td>
                                        <td><a href="#" class="remove-jne-user"
                                               data-jne="<?php echo esc_attr($subscriber['userID']) ?>">
                                                <?php _e('Delete Subscriber', 'jne') ?>
                                        </td>
                                    <tr>
	                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php if(!empty($pagination)) : ?>
                            <?php if($pagination['countPages'] > 1) : ?>
                                <div class="pagination">
                                    <?php echo \Jigoshop\Extension\Admin\JNEPagination::getPagination($pagination['page'],
                                                                                        $pagination['countPages']) ?>
                                </div>
                            <?endif;?>
                        <?php endif; ?>
                        <table>
                            <tbody>
                                <tr>
                                    <td class="l-cont">
                                        <h3><label for="send-email"><?php _e('Send Email', 'jne') ?></label></h3>
                                        <div id="res"></div>
                                        <textarea id="send-email" name="send-email" cols="350" rows="25" class="widefat"></textarea>
                                        <button class="submit-btn button-secondary" id="jne-submit"><?php _e('Send Email', 'jne') ?></button>
                                        <span id="w-loader" style="display: none;"><img src="<?php echo esc_url($spin) ?>"/></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php else : ?>
                        <h3 class="heading"><?php _e('There are not any subscribers yet', 'jne') ?></h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
