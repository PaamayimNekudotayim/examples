<?php
/**
 * @var $spin string, url to loader image
 */
?>
<div id="res"></div>
<form action="" method="post" id="whole-submit">
    <div class="whole">
        <input type="text" id="name" name="name" class="shrined" placeholder="name" />
    </div>
    <div class="whole">
        <input type="text" id="email" name="email" class="shrined" placeholder="email" />
    </div>
    <div class="whole l-cont">
        <input type="submit" id="submit" class="sign-in" value="<?php _e('Sign In', 'jne') ?>"/>
        <span id="w-loader" style="display: none;"><img src="<?php echo esc_url($spin) ?>"/></span>
    </div>
</form>