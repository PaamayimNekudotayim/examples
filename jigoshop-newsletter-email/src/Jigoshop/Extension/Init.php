<?php

namespace Jigoshop\Extension;

use Jigoshop\Extension\Admin\JNEAjax;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;

class Init
{
	private static $instance;
	
	private function __construct()
	{
		Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
		Render::addLocation('jne', JIGOSHOP_NEWSLETTER_EMAIL_DIR);
		add_action('widgets_init', [$this, 'createWidget']);
		add_action('wp_footer', [$this, 'jneStyles']);
		
		JNEAjax::initAjaxRequest();
	}
	
	/**
	 * Register Widget
	 */
	public function createWidget()
	{
		register_widget('\Jigoshop\Extension\Frontend\JNEWidget');
	}
	
	public static function loadWidget()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function __clone(){}
	
	public function jneStyles()
	{
		Styles::add('jigoshop.jne.style', JIGOSHOP_NEWSLETTER_EMAIL_URL . '/assets/css/frontend/frontstyle.css');
		Styles::add('jigoshop.vendors.bs_switch', \JigoshopInit::getUrl() . '/assets/css/vendors/bs_switch.css');
		Scripts::add('jigoshop.jne.script', JIGOSHOP_NEWSLETTER_EMAIL_URL . '/assets/js/frontend/user.js');
		
		Scripts::localize('jigoshop.jne.script', 'jne', [
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('jne_nonce')
		]);
	}
}

Init::loadWidget();