<?php

namespace Jigoshop\Extension\Frontend;

use Jigoshop\Integration\Helper\Render;

class JNEWidget extends \WP_Widget
{
	function __construct()
	{
		parent::__construct(
			'jen_widget',
			esc_html__('Jigoshop Email Newsletter', 'jen'),
			[
				'classname' => 'jen-options',
				'description' => esc_html__('Email Newsletter subscriber Widget', 'jen'),
			]
		);
	}
	
	/**
	 * @param array $args
	 * @param array $instance
	 */
	public function widget($args, $instance)
	{
		echo $args['before_widget'];
		
		if (!empty($instance['title'])) {
			echo $args['before_title']  . apply_filters('widget_title', $instance['title']) . $args['after_title'];
		}
		Render::output('jne', 'frontend/form', [
			'spin' => JIGOSHOP_NEWSLETTER_EMAIL_URL . '/assets/images/loader.gif',
		]);
		
		echo $args['after_widget'];
	}
	
	/*
	 * @param array $instance
	 */
	public function form($instance)
	{
		$title = !empty($instance['title']) ? $instance['title'] : esc_html__('Title', 'jen');
		Render::output('jne', 'admin/form', [
			'title' => $title,
			'that' => $this,
		]);
	}
}