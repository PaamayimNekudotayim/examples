<?php

namespace Jigoshop\Extension;

use Jigoshop\Extension\Admin\AdminMenu;
use Jigoshop\Extension\Admin\UserPage;

class Admin
{
	public function __construct()
	{
		add_filter('plugin_action_links_' . plugin_basename(JIGOSHOP_NEWSLETTER_EMAIL_DIR . '/bootstrap.php'),
			[$this, 'actionLinks']);
		add_action('init', [$this, 'adminSettings']);
	}
	
	public function adminSettings()
	{
		new AdminMenu();
		new UserPage();
	}
	
	/**
	 * Show action links on plugins page.
	 *
	 * @param $links
	 *
	 * @return array
	 */
	public function actionLinks($links)
	{
		$links[] = '<a href="https://www.jigoshop.com/documentation/paypal-express-checkout-gateway/" target="_blank">' . __('Documentation') . '</a>';
		$links[] = '<a href="https://www.jigoshop.com/support/" target="_blank">' . __('Support') . '</a>';
		$links[] = '<a href="https://wordpress.org/support/view/plugin-reviews/jigoshop#$postform" target="_blank">' . __('Rate Us') . '</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">' . __('More plugins for Jigoshop') . '</a>';
		
		return $links;
	}
}
new Admin();