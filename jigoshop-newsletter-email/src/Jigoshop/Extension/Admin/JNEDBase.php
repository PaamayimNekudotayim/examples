<?php

namespace Jigoshop\Extension\Admin;

class JNEDBase
{
	private static $pagination;
	/**
	 * @Create User Table
	 */
	public static function registerUserTable()
	{
		global $wpdb;
		$table = $wpdb->prefix . 'jen_user';
		
		$q = "CREATE TABLE IF NOT EXISTS {$table}jen_user (
				userID INT UNSIGNED NOT NULL AUTO_INCREMENT,
				userName VARCHAR(100) NOT NULL,
				userEmail VARCHAR(150) NOT NULL,
				isUserSigned VARCHAR(10) DEFAULT 'no' NOT NULL,
				PRIMARY KEY (userID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		require_once (ABSPATH . '/wp-admin/includes/upgrade.php');
		
		dbDelta($q);
	}
	
	/**
	 * @param $name
	 * @param $email
	 * @param $isUserSigned
	 */
	public static function checkUser($name, $email, $isUserSigned)
	{
		global $wpdb;
		if($wpdb->get_var($wpdb->prepare(
			"SELECT userID FROM {$wpdb->prefix}jen_user WHERE userEmail = %s", esc_sql($email)))){
			exit('
				<div class="error error-message hide">
					<p>' . __('User Already Exists', 'jne') . '</p>
				</div>');
		}else{
			if($wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}jen_user (userName, userEmail, isUserSigned)
				VALUES (%s, %s, %s)", esc_sql($name), esc_sql($email), esc_sql($isUserSigned)))){
				exit('
				<div class="error error-message hide-success">
					<p>' . __('Thank you for the subscription', 'jne') . '</p>
				</div>');
			}else{
				exit('
				<div class="error error-message hide">
					<p>' . __('Something went wrong.', 'jne') . '</p>
				</div>');
			}
		}
	}
	
	/**
	 * @param string $all
	 * @return array|null|object
	 */
	public static function getSubscribers($all = 'no')
	{
		global $wpdb;
		
		$pagination = JNEPagination::jnePagination();
		if('yes' == $all){
			$query = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}jen_user", ARRAY_A);
		}else{
			$query = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}jen_user LIMIT {$pagination['startPage']},
																			{$pagination['perPage']}", ARRAY_A);
		}
		
		return $query;
	}
	
	
	/**
	 * @return null|string
	 */
	public static function countUsers()
	{
		global $wpdb;
		
		$query = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}jen_user");
		
		return $query;
	}
}