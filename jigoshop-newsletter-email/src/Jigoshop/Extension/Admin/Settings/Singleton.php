<?php
/**
 * Created by PhpStorm.
 * User: Jack
 * Date: 6/27/2018
 * Time: 1:24 PM
 */

namespace Jigoshop\Extension\Admin\Settings;


abstract class Singleton
{
	private static $instance;
	
	/**
	 * @return static
	 */
	public static function create()
	{
		if(null == self::$instance){
			self::$instance = new static();
		}
		
		return self::$instance;
	}
}