<?php

namespace Jigoshop\Extension\Admin\Settings;


class Tab extends \Jigoshop\Extension\Admin\Settings\Singleton
{
	/**
	 * @return array
	 */
	public static function renderTabs()
	{
		$tabs = [
			[
				'tab' => 'jne-plugin-options',
				'href' => 'admin.php?page=',
				'class' => 'nav-tab',
				'active' => (isset($_GET['page']) && $_GET['page'] == 'jne-plugin-options' && !isset($_GET['tab']) ?
					'nav-tab-active' : ''),
			]
		];
		
		return $tabs;
	}
}