<?php

namespace Jigoshop\Extension\Admin;


class AdminMenu
{
	public function __construct()
	{
		add_action('admin_menu', [$this, 'jneAdminMenu']);
	}
	
	public function jneAdminMenu()
	{
		add_menu_page('Plugin Options', 'JNE Plugin Options', 'manage_options',
			'jne-plugin-options', [$this, 'jneMenuOptions'], 'dashicons-admin-tools');
	}
	
	public function jneMenuOptions()
	{
		echo 'OK';
	}
}