<?php

namespace Jigoshop\Extension\Admin;


class JNEAjax
{
	private static $instance;
	
	/**
	 * JNEAjax constructor.
	 */
	private function __construct()
	{
		// Ajax
		add_action('wp_ajax_jneHandler', [$this, 'jneClientHandler']);
		add_action('wp_ajax_nopriv_jneHandler', [$this, 'jneClientHandler']);
		add_action('wp_ajax_jneSendEmail', [$this, 'jneSendEmail']);
		add_action('wp_ajax_jneDeleteUser', [$this, 'jneDeleteUser']);
	}
	
	/**
	 * @return JNEAjax
	 */
	public static function initAjaxRequest()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	/**
	 * Email Subscriber Handler
	 */
	public function jneClientHandler()
	{
		if (!wp_verify_nonce($_POST['jneNonce'], 'jne_nonce')) {
			die('No actions allowed!');
		}
		parse_str($_POST['formData'], $posted);
		if (empty($posted['email']) || empty($posted['name'])) {
			
			exit('
				<div class="error error-message hide">
					<p>' . __('Please fill out all inputs', 'jne') . '</p>
				</div>'
			);
		}
		
		$name = sanitize_text_field($posted['name']);
		if (function_exists('is_email')) {
			$email = is_email($posted['email']);
		} else {
			$email = filter_var($posted['email'], FILTER_VALIDATE_EMAIL);
		}
		if (!$email) {
			exit('
				<div class="error error-message hide">
					<p>' . __('Email has wrong format', 'jne') . '</p>
				</div>');
		}
		
		JNEDBase::checkUser($name, $email, 'yes');
		
		die();
	}
	
	public function jneSendEmail()
	{
		if (!wp_verify_nonce($_POST['emailNonce'], 'jne-admin-nonce')) {
			die('No actions allowed!');
		}
		if(empty($_POST['data']) || '' == $_POST['data']){
			die(
				'Please fill out email field'
			);
		}
		$subscribers = JNEDBase::getSubscribers('yes');
		$i = 0;
		foreach ($subscribers as $subscriber) {
			$data = nl2br(str_replace('%name%', $subscriber['userName'], $_POST['data']));
			if (wp_mail($subscriber['userEmail'], 'Email sent', $data)) $i++;
		}
		die('
				<div class="hide-success-admin">
					<p>' . __('Email has been sent to ' . $i . ' users', 'jne') . '</p>
				</div>'
		);
	}
	
	public function jneDeleteUser()
	{
		$response = null;
		if($_POST['type'] == 'removeUser'){
			
			global $wpdb;
			$table = $wpdb->prefix . 'jen_user';
			$userID = strip_tags($_POST['id']);
			
			$result = $wpdb->delete($table,
				[
					'userID' => $userID
				], ['%d']
			);
			if($result == 1){
				$response = [
					'response' => 'success',
					'id' => $userID,
					'message' => '
						<div class="hide-success-admin">
							<p>' . __('User has been deleted', 'jne') . '</p>
						</div>'
				];
			}else{
				$response = [
					'response' => 'error',
				];
			}
			
		}
		die(json_encode($response));
	}
}