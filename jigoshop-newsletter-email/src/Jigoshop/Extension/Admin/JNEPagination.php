<?php

namespace Jigoshop\Extension\Admin;

class JNEPagination
{
	public static $perPage = 5;
	
	protected static $back;
	protected static $forward;
	protected static $startPage;
	protected static $endPage;
	protected static $page2Left;
	protected static $page2Right;
	protected static $page1Left;
	protected static $page1Right;
	protected static $uri = '?';
	
	/**
	 * @return array
	 */
	public static function jnePagination()
	{
		$count = JNEDBase::countUsers();
		
		$countPages = ceil($count / self::$perPage);
		
		if(false == $countPages){
			$countPages = 1;
		}
		if(isset($_GET['jne-page']) && !empty($_GET['jne-page'])){
			$page = (int)$_GET['jne-page'];
			if($page < 1){
				$page = 1;
			}
		}else{
			$page = 1;
		}
		
		if ($page > $countPages){
			$page = $countPages;
		}
		$startPage = ($page - 1) * self::$perPage;
		
		$params = [
			'page' => $page,
			'count' => $count,
			'countPages' => $countPages,
			'perPage' => self::$perPage,
			'startPage' => $startPage
		];
		
		return $params;
	}
	
	/**
	 * @param $page
	 * @param $countPages
	 * @return string
	 */
	public static function getPagination($page, $countPages)
	{
		if($_SERVER['QUERY_STRING']){
			foreach($_GET as $key => $value){
				if($key != 'jne-page'){
					self::$uri .= "{$key}=$value&amp;";
				}
			}
		}
		
		if($page > 1){
			self::$back = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page - 1) . '">'
				. __('Back', 'jne') . '</a>';
		}
		
		if($page < $countPages){
			self::$forward = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page + 1) . '">'
				. __('Next', 'jne') . '</a>';
		}
		
		if($page > 3){
			self::$startPage = '<a class="nav-links" href="' . self::$uri . 'jne-page=1">'
				. __('First', 'jne')
				. '</a>';
		}
		
		if($page > ($countPages - 2 )){
			self::$endPage = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . $countPages . '">'
				. __('End', 'jne') . '</a>';
		}
		
		if($page - 2 > 0){
			self::$page2Left = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page - 2) . '">'
				. ($page - 2) . '</a>';
		}
		
		if($page - 1 > 0){
			self::$page1Right = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page - 1) . '">'
				. ($page - 1) . '</a>';
		}
		
		if($page + 2 <= $countPages){
			self::$page2Right = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page + 2) . '">' . ($page + 2)
				. '</a>';
		}
		
		if($page + 1 <= $countPages){
			self::$page1Right = '<a class="nav-links" href="' . self::$uri . 'jne-page=' . ($page + 1) . '">' . ($page + 1)
				. '</a>';
		}
		
		$html = self::$startPage . self::$back . self::$page2Left . self::$page1Left . '<a class="active-page">'
			. $page . '</a>' . self::$page1Right . self::$page2Right . self::$forward . self::$endPage;
		
		return $html;
	}
}