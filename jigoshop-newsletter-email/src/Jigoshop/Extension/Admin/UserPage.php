<?php

namespace Jigoshop\Extension\Admin;


use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration\Render;

class UserPage
{
	public function __construct()
	{
		add_action('admin_menu', [$this, 'createUserPage']);
	}
	
	/**
	 * User Page
	 */
	public function createUserPage()
	{
		add_menu_page('JNE Email Users', 'JNE Email Users', 'manage_options',
			'jne-subscribers', [$this, 'jneUsers'], 'dashicons-admin-users');
		add_action('admin_enqueue_scripts', [$this, 'jneAdminScripts']);
		add_action('admin_enqueue_scripts', [$this, 'jneAdminStyle']);
	}
	
	/**
	 *
	 */
	public function jneUsers()
	{
		Render::output('jne', 'admin/users', [
			'subscribers' => JNEDBase::getSubscribers(),
			'countUsers' => JNEDBase::countUsers(),
			'pagination' => JNEPagination::jnePagination(),
			'spin' => JIGOSHOP_NEWSLETTER_EMAIL_URL . '/assets/images/loader.gif',
		]);
	}
	
	/**
	 * @param $page
	 */
	public function jneAdminScripts($page)
	{
		if($page != 'toplevel_page_jne-subscribers'){
			return;
		}
		Scripts::add('jigoshop.jne_admin.script', JIGOSHOP_NEWSLETTER_EMAIL_URL .
																		'/assets/js/admin/adminScript.js', ['jquery']);
		
		Scripts::localize('jigoshop.jne_admin.script', 'jneAdmin', [
			'emailIsEmpty' => __('Please fill out the email message before send.', 'jne'),
			'nonce' => wp_create_nonce('jne-admin-nonce'),
		]);
	}
	
	/**
	 * @param $page
	 */
	public function jneAdminStyle($page)
	{
		if($page != 'toplevel_page_jne-subscribers'){
			return;
		}
		Styles::add('jigoshop.jne_admin.style', JIGOSHOP_NEWSLETTER_EMAIL_URL .
																					'/assets/css/admin/adminStyle.css');
	}
}