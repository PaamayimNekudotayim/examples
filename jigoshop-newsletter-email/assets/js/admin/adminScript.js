
jQuery(document).ready(function($){
    $('#jne-submit').click(function(){
        var jneEmail = $.trim($('#send-email').val());
        if(jneEmail  == ''){
            alert(jneAdmin.emailIsEmpty);
            return;
        }

        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                data: jneEmail,
                action: 'jneSendEmail',
                emailNonce: jneAdmin.nonce
            },
            beforeSend: function(){
                $('#jne-submit').attr('disabled', 'disabled');
                $('#res').empty();
                $('#w-loader').fadeIn();
            },
            success: function(res){
                $('#w-loader').fadeOut(300, function(){
                    $('#res').html(res);
                    $('#whole-submit').find('input:not(#submit)').val('');
                    setTimeout(function () {
                        $('#res').empty();
                    }, 5000);
                });
                $('#jne-submit').removeAttr('disabled', 'disabled');
            }

        });
    });

    $('.remove-jne-user').on('click', function(e){
        e.preventDefault();
        var ID = $(this).attr(
            'data-jne'
        );
        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                'action': 'jneDeleteUser',
                'id': ID,
                'type': 'removeUser'
            },
            beforeSend: function(){
                $('#res-jne').empty();
            },
            success: function(res){
                var result = JSON.parse(res);
                if(result.response == 'success'){
                    jQuery("[data-jne='" + result.id + "']").parent().parent().remove();
                    $('#res-jne').html(result.message);
                    setTimeout(function () {
                        $('#res-jne').empty();
                    }, 5000);
                }
            }
        });
    });
});