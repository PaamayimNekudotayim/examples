jQuery(document).ready(function($){
    $('#whole-submit').submit(function(){
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: jne.url,
            data: {
                formData: data,
                jneNonce: jne.nonce,
                action: 'jneHandler'
            },
            beforeSend: function(){
                $('#res').empty();
                $('#w-loader').fadeIn();
            },
            success: function(res){
                $('#w-loader').fadeOut(300, function(){
                    $('#res').html(res);
                    $('#whole-submit').find('input:not(#submit)').val('');
                    setTimeout(function () {
                        $('#res').empty();
                    }, 5000);
                });
            }
        });
        return false;
    });
});