<?php
/**
 * Plugin Name: Jigoshop Newsletter Email
 * Plugin URI: https://jigoshop.com/product/jigoshopo-newsletter-email/
 * Description: Allows to send bulk Email Newsletter to customers. (Plugin is under developing)
 * Author: @vasilguruli | JigoLtd
 * Version: 1.0
 */
// Define plugin name

define('JIGOSHOP_NEWSLETTER_EMAIL_NAME', 'Jigoshop Newsletter Email');
// Define plugin directory for inclusions
define('JIGOSHOP_NEWSLETTER_EMAIL_DIR', dirname(__FILE__));
register_activation_hook(__FILE__, function(){
	include JIGOSHOP_NEWSLETTER_EMAIL_DIR . '/src/Jigoshop/Extension/Admin/JNEDBase.php';
	Jigoshop\Extension\Admin\JNEDBase::registerUserTable();
});
add_action('plugins_loaded', function () {
	load_plugin_textdomain('jne', false, dirname(plugin_basename(__FILE__))
		. '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		// Define plugin URL for assets
		define('JIGOSHOP_NEWSLETTER_EMAIL_URL', plugins_url('', __FILE__));
		//Check version.
		if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_NEWSLETTER_EMAIL_NAME, '2.0')) {
			return;
		}
		//Check license.
		/*$licence = new \Jigoshop\Licence(__FILE__, '', 'http://www.jigoshop.com');
		if (!$licence->isActive()) {
			return;
		}*/
		/**
		 * Code is missing as it is not necessary here at this point
		 */
		//Init components.
		require_once(JIGOSHOP_NEWSLETTER_EMAIL_DIR . '/src/Jigoshop/Extension/Init.php');
		if (is_admin()) {
			//require_once(JIGOSHOP_NEWSLETTER_EMAIL_DIR . '/src/Jigoshop/Extension/Admin/JNEDBase.php');
			require_once(JIGOSHOP_NEWSLETTER_EMAIL_DIR . '/src/Jigoshop/Extension/Admin.php');
		}
	}else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.', 'jne'),
				JIGOSHOP_NEWSLETTER_EMAIL_NAME, JIGOSHOP_NEWSLETTER_EMAIL_NAME);
			echo '</p></div>';
		});
	}
}, 99);