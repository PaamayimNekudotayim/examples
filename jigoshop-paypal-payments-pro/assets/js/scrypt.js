jQuery(document).ready(function ($) {
    //$('#payment-paypal_payments_pro').on('click', function () {});
    $('#payment-methods input[type="radio"]').on('change', function (event) {
        $.ajax({
            type: 'post',
            data: {
                'action': 'checkFee',
                'method': $(event.target).val()
            },

            url: paypal.ajaxUrl,
            success: function (data) {
                var result = JSON.parse(data);
                if (result.response == 'success') {
                    jQuery('#cart-total td strong').html(result.amount);
                    if ($('#payment-paypal_payments_pro input[type="radio"]').is(':checked')) {
                        jQuery('table.table tr#tax-standard').after(
                            '<tr id="fee-ad"><th scope="paypal">Payment processing fee</th><td>' + paypal.fee + '</td></tr>'
                        )
                    }else{
                        jQuery('#fee-ad').remove();
                    }
                }
            }
        });
    });


    // Thank you page
    /*
    * Read $_GET value
    * @url http://papermashup.com/read-url-get-variables-withjavascript/
    *
    */
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
    if(getUrlVars()['ppp_fee'] == 'true'){
        var attribute = $('tr[data-product="0"]');
        // var id = $(this).attr('data-product');
        $.ajax({
            type: 'post',
            data: {
                'action': 'addFee',
                'paypalFee': 'add',
                'orderID': getUrlVars()['order']
            },

            url: paypal.macarena,
            success: function (data) {
                // Somehow "data" takes fucking script tag with
                // weird string inside (ssl.dev.jigoshop.com) so I found this way to fix it,
                // if yo find another and I am sure you do, prosze bardzo!
                var splitWeirdString = data.split('}', 1)[0];
                var toConcat = '}';
                var finalData = splitWeirdString.concat(toConcat);
                var result = JSON.parse(finalData);

                if (result.response == 'success') {
                    jQuery(attribute).remove();
                    jQuery('table.table tr#tax-standard').after(
                        '<tr id="fee-ad"><th scope="paypal">Payment processing fee</th><td>' + result.amount + '</td></tr>'
                    )
                }
            }
        });
    }
});