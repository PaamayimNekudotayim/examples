Jigoshop New Product Badge
==========================

A simple plugin that displays a 'New' badge on Jigoshop products that were published in a specified period of time.

### Installation

1. Upload `jigoshop-new-product-badge` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Define the amount of days you want it to show on new products on the New Badge tab of the Jigoshop settings screen.
3. Done!