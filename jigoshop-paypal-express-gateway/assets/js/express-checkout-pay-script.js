jQuery(document).ready(function(){
    setTimeout(function() {
        jQuery(".paypal-button").trigger('click');
    }, 5);

    window.paypalCheckoutReady = function () {
        paypal.checkout.setup(jigoshop_paypal_express_gateway.merchantID, {
            container: 'ppContainer',
            environment: jigoshop_paypal_express_gateway.env
        });
    };
});