jQuery(document).ready(function(){
     setTimeout(function() {
     jQuery('input[value="paypal_express"]').trigger('click');
     }, 1);

     setTimeout(function() {
     jQuery(".btn").trigger('click');
     }, 1);
    var docHeight = jQuery(document).height();
    jQuery('body').prepend('<div id="overlay"><div class="spinner loading"><p id="text">Thank you for your order, we are now redirecting you to Paypal!</p></div><a class="boxclose" id="boxclose">&times;</a></div>');

    jQuery("#overlay")
        .height(docHeight)
        .css({
            'opacity' : 0.4,
            'position': 'fixed',
            'top': 0,
            'left': 0,
            'background-color': 'black',
            'width': '100%',
            'z-index': 5000
        });

    jQuery("#boxclose").css({
        'position' : 'absolute',
        'width' : '26px',
        'height' : '26px',
        'cursor' : 'pointer',
        'top' : '100px',
        'right' : '50px',
        'font-size' : '35px'
    }).click(function(){
        jQuery('#overlay').fadeOut('fast');
    });
    jQuery("#text").css({
       'font-size' : '25px',
        'margin-bottom' : '65px'
    });
});
