<?php

namespace Jigoshop\Extension\PaypalExpressGateway\Common;


use Jigoshop\Core\Messages;
use Jigoshop\Core\Options;
use Jigoshop\Entity\Cart;
use Jigoshop\Entity\Customer\CompanyAddress;
use Jigoshop\Entity\Order;
use Jigoshop\Exception;
use Jigoshop\Frontend\Pages;
use Jigoshop\Helper\Api;
use Jigoshop\Helper\Currency;
use Jigoshop\Helper\Options as OptionsHelper;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration\Helper\Render;
use Jigoshop\Payment\Method3;
use Jigoshop\Service\CartService;
use Jigoshop\Service\CartServiceInterface;
use Jigoshop\Service\OrderServiceInterface;
use Jigoshop\Extension\PaypalExpressGateway\Common\Method\Helper as PaypalLanguage;
use Jigoshop\Extension\PaypalExpressGateway\Common\Method\PaypalExpressCheckoutClass;

class Method implements Method3
{
	const ID = 'paypal_express';

	/**@var Options $options */
	private $options;
	/**@var OrderService $orderService */
	private $orderService;
	/**@var Messages $messages */
	private $messages;
	/**@var CartService $cartService */
	private $cartService;
	/**@var $settings array */
	private $settings;
	/**@var $paypalLanguage array*/
	private $paypalLanguage;
	/**@var $paypalButtonLanguage array*/
	private $paypalButtonLanguage;


	/**
	 * Method constructor.
	 * @param Options $options
	 * @param CartServiceInterface $cartService
	 * @param OrderServiceInterface $orderService
	 * @param Messages $messages
	 */
	public function __construct(Options $options, CartServiceInterface $cartService, OrderServiceInterface $orderService, Messages $messages)
	{
		$this->options = $options;
		$this->messages = $messages;
		$this->cartService = $cartService;
		$this->orderService = $orderService;
		$this->paypalLanguage = PaypalLanguage::getLanguage('paypal');
		$this->paypalButtonLanguage = PaypalLanguage::getLanguage('button');

		OptionsHelper::setDefaults('payment.' . self::ID, array(
			'enabled' => false,
			'title' => __('Paypal Advanced', 'jigoshop_paypal_advanced'),
			'description' => __('Paypal Advanced', 'jigoshop_paypal_advanced'),
			'api_username' => '',
			'api_password' => '',
			'api_signature' => '',
			'receiver_email' => '',
			'transaction_type' => '',
			'force_free_product_payment' => false,
			'force_free_order_payment' => false,
			'paypal_language' => '',
			'paypal_button_language' => '',
			'image_name' => '',
			'page_style_name' => '',
			'paypal_ipn_fallback' => false,
			'debug' => false,
			'receiver_error_logs' => '',
			'debug_email' => '',
			'testMode' => false,
			'adminOnly' => false,
		));

		$this->settings = OptionsHelper::getOptions('payment.' . self::ID);


		add_action('cart-collaterals', array($this, 'singleProductPaypalCheckout'));
		add_action('init', array($this, 'paypalExpressRedirect'));
		add_action('init', array($this, 'successfulPayment'));
		add_action('wp_enqueue_scripts', array($this, 'paypalExpressRedirect'));
	}

	public function successfulPayment()
	{
		if(isset($_GET['paypal_express_gateway']) && $_GET['paypal_express_gateway'] == 'success'){
			$orderId = $_GET['order'];
			/**@var Order $order*/
			$order = $this->orderService->find((int)$orderId);
			if(!empty($_GET['token']) && !empty($_GET['PayerID'])){

				$status = \Jigoshop\Helper\Order::getStatusAfterCompletePayment($order);
				/**@var Order $order */
				$order->setStatus($status, sprintf('Purchase has been completed. PayerID: ' . $_GET['PayerID']));
				$this->orderService->save($order);
			}
		}
	}

	public function paypalButtonLanguage()
	{
		$paypalButton = array(
			'au' => 'https://www.paypal.com/en_AU/i/btn/btn_xpressCheckout.gif',
			'at' => 'https://www.paypal.com/de_DE/AT/i/btn/btn_xpressCheckout.gif',
			'cn' => 'https://www.paypal.com/zh_CN/i/btn/btn_xpressCheckout.gif',
			'de' => 'https://www.paypal.com/de_DE/i/btn/btn_xpressCheckout.gif',
			'de_CH' => 'https://www.paypal.com/de_DE/CH/i/btn/btn_xpressCheckout.gif',
			'es' => 'https://www.paypal.com/es_ES/i/btn/btn_xpressCheckout.gif',
			'fr' => 'https://www.paypal.com/fr_FR/i/btn/btn_xpressCheckout.gif',
			'fr_BE' => 'https://www.paypal.com/fr_FR/BE/i/btn/btn_xpressCheckout.gif',
			'fr_CA' => 'https://www.paypal.com/fr_CA/i/btn/btn_xpressCheckout.gif',
			'it' => 'https://www.paypal.com/it_IT/i/btn/btn_xpressCheckout.gif',
			'j1' => 'https://www.paypal.com/ja_JP/i/btn/btn_xpressCheckout.gif',
			'nl' => 'https://www.paypal.com/nl_NL/i/btn/btn_xpressCheckout.gif',
			'pl' => 'https://www.paypal.com/pl_PL/i/btn/btn_xpressCheckout.gif',
			'pt' => 'https://www.paypal.com/pt_PT/i/btn/btn_xpressCheckout.gif',
			'ru' => 'https://www.paypal.com/ru_RU/i/btn/btn_xpressCheckout.gif',
			'sv_SE' => 'https://www.paypal.com/sv_SE/i/btn/btn_xpressCheckout.gif',
			'tr_TR' => 'https://www.paypal.com/tr_TR/i/btn/btn_xpressCheckout.gif',
			'uk' => 'https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif',
			'us' => 'https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif',
			'zh_HK' => 'https://www.paypal.com/zh_HK/i/btn/btn_xpressCheckout.gif',
			'zh_TW' => 'https://www.paypal.com/zh_TW/i/btn/btn_xpressCheckout.gif',
		);

		return $paypalButton[$this->settings['paypal_button_language']];
	}

	public function singleProductPaypalCheckout()
	{
		if($this->settings['enabled'] == true){
			try{
				$checkout = add_query_arg('action', '_express_checkout', get_permalink($this->options->getPageId(Pages::CHECKOUT)));
				$button = $this->paypalButtonLanguage();
				Render::output('paypal_express', 'buttonInCart', array(
					'checkout' => $checkout,
					'button' => $button,
				));

			}catch(\Jigoshop\Service\Exception $e){
				$e->getMessage();
			}
		}
	}


	/**
	 * @return string ID of payment method.
	 */
	public function getId()
	{
		return self::ID;
	}

	/**
	 * @return string Human readable name of method.
	 */
	public function getName()
	{
		return is_admin() ? $this->getLogoImage() . ' ' . __('Paypal Express', 'jigoshop_paypal_express') : $this->settings['title'];
	}

	private function getLogoImage()
	{
		return '<img src="' . JIGOSHOP_PAYPAL_EXPRESS_URL . '/assets/images/PayPal_mark_37x23.gif' . '" alt="paypal" width="" height="" style="border:none !important;" class="shipping-logo" />';
	}

	/**
	 * @return bool Whether current method is enabled and able to work.
	 */
	public function isEnabled()
	{
		return $this->settings['enabled'];
	}

	/**
	 * @return array List of options to display on Payment settings page.
	 */
	public function getOptions()
	{
		return array(
			array(
				'name' => sprintf('[%s][enabled]', self::ID),
				'title' => __('Enable PayPal Express', 'jigoshop_paypal_express'),
				'id' => 'jigoshop_paypal_advanced_enabled',
				'type' => 'checkbox',
				'checked' => $this->settings['enabled'],
				'classes' => array('switch-medium'),
			),
			array(
				'name' => sprintf('[%s][title]', self::ID),
				'title' => __('Method Title', 'jigoshop_paypal_express'),
				'tip' => __('This controls the title which the user sees during checkout.', 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['title'],
			),
			array(
				'name' => sprintf('[%s][description]', self::ID),
				'title' => __('Description', 'jigoshop_paypal_express'),
				'tip' => __('This controls the description which the user sees during checkout.', 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['description'],
			),
			array(
				'name' => sprintf('[%s][api_username]', self::ID),
				'title' => __('API Username', 'jigoshop_paypal_express'),
				'tip' => __('Your PayPal API Username.', 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['api_username'],
			),
			array(
				'name' => sprintf('[%s][api_password]', self::ID),
				'title' => __('API Password', 'jigoshop_paypal_express'),
				'tip' => __('Your PayPal API Password.', 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['api_password'],
			),
			array(
				'name' => sprintf('[%s][api_signature]', self::ID),
				'title' => __('API Signature', 'jigoshop_paypal_express'),
				'tip' => __("Your PayPal API Signature.", 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['api_signature'],
			),
			array(
				'name' => sprintf('[%s][receiver_email]', self::ID),
				'title' => __('Receiver E-mail', 'jigoshop_paypal_express'),
				'tip' => __("The email of the PayPal account that will receive the payments.", 'jigoshop_paypal_express'),
				'type' => 'text',
				'value' => $this->settings['receiver_email'],
			),
			array(
				'name' => sprintf('[%s][transaction_type]', self::ID),
				'title' => __('Transaction Type', 'jigoshop_paypal_express'),
				'tip' => __('Choose what type of transaction you want to process. <br/><strong>1. Sale</strong> - represents a single payment that completes a purchase for a specified amount. Use Sale, if the transaction, including shipping, can be completed immediately.<br/><strong>2. Authorization</strong> - represents an agreement to pay and places the buyer\'s funds on hold for up to three days. Using Authorization you can manually capture the customer funds up to 115% or USD $75 more than its specified total.', 'jigoshop_paypal_express'),
				'type' => 'select',
				'options' => array(
					'sale' => __('Sale', 'jigoshop_paypal_express'),
					'authorization' => __('Authorization', 'jigoshop_paypal_express')
				),
				'value' => $this->settings['transaction_type']
			),
			/*	array(
					'name' => sprintf('[%s][force_free_product_payment]', self::ID),
					'title' => __('Force Free Product Payment', 'jigoshop_paypal_express'),
					'tip' => __('PayPal Express product total cannot be 0. If the total of all products with or without discount is 0 and you still require payment (exp. you have shipping or tax before discount) for the order, you would have to enable this option. Enabling the option will force an additional product with 0.01 price to be send to PayPal. <strong>For more info see the plugin readme.txt file</strong> <br/> <strong>DISCLAIMER:</strong> You are responsible for letting your customers know what is happening and refunding the 0.01 amount to them, if refund is necessary.', 'jigoshop_paypal_express'),
					'type' => 'checkbox',
					'checked' => $this->settings['force_free_product_payment'],
					'classes' => array('switch-medium'),
				),*/
			array(
				'name' => sprintf('[%s][force_free_order_payment]', self::ID),
				'title' => __('Force Free Order Payment', 'jigoshop_paypal_express'),
				'tip' => __('PayPal Express total cannot be 0. If the total of the order is 0 and you still need the customer to register a payment, you would have to enable this option. Enabling the option will force a product with 0.01 price to be send to PayPal making the total 0.01. <strong>For more info see the plugin readme.txt file</strong> <br/> <strong>DISCLAIMER:</strong> You are responsible for letting your customers know what is happening and refunding the 0.01 amount to them, if refund is necessary.', 'jigoshop_paypal_express'),
				'type' => 'checkbox',
				'checked' => $this->settings['force_free_order_payment'],
				'classes' => array('switch-medium'),
			),
			/*	array(
					'name' => sprintf('[%s][paypal_language]', self::ID),
					'title' => __('PayPal Language', 'jigoshop_paypal_express'),
					'tip' => __('You can change the language of your PayPal pages to match the language on your website. Choose one of the supported languages.', 'jigoshop_paypal_express'),
					'type' => 'select',
					'options' => $this->paypalLanguage,
					'value' => $this->settings['paypal_language'],
				),*/
			array(
				'name' => sprintf('[%s][paypal_button_language]', self::ID),
				'title' => __('PayPal Button Language', 'jigoshop_paypal_express'),
				'tip' => __('PayPal Express uses dynamic button image for the "Checkout with PayPal" button. Choose your country from the list and PayPal Express will use your country specific button image. If your country is not present in the list, just leave the country to United States for the default button image.', 'jigoshop_paypal_express'),
				'type' => 'select',
				'options' =>  $this->paypalButtonLanguage,
				'value' => $this->settings['paypal_button_language'],
			),
			array(
				'name' => sprintf('[%s][image_name]', self::ID),
				'title' => __('Custom logo name', 'jigoshop_paypal_express'),
				'tip' => __('Upload your logo image to the your-plugin-directory/jigoshop-paypal-express-gateway/assets/images/ and enter the logo name.', 'jigoshop_paypal_express'),
				'type' => 'text',
				'description' => __('Enter you image logo name. For example : <strong>myLogo.jpg</strong>', 'jigoshop_paypal_express'),
				'value' => $this->settings['image_name'],
			),
			/*array(
					'name' => sprintf('[%s][page_style_name]', self::ID),
					'title' => __('Page Style Name', 'jigoshop_paypal_express'),
					'tip' => __('When your buyer logs on to PayPal to checkout, you can make the PayPal pages the buyer sees, appear to have a similar look and feel to those on your website. You can customize any of these page characteristics and save the results as a Page Style Name.<br />Write the name of the style you want to use. Leave empty to use default.', 'jigoshop_paypal_express'),
					'type' => 'text',
					'value' => $this->settings['page_style_name'],
				),*/
			/*	array(
					'name' => sprintf('[%s][paypal_ipn_fallback]', self::ID),
					'title' => __('Enable PayPal IPN Fallback', 'jigoshop_paypal_express'),
					'tip' => __('If your \'Transaction Type\' is:<br/> <strong>Authorization</strong>, you would want to enable this option, in order to update the order status automatically after the Authorized payment is Captured.<br/><strong>Sale</strong>, then enable this, if you want to have a fallback in case the main transaction handling fails.', 'jigoshop_paypal_express'),
					'type' => 'checkbox',
					'checked' => $this->settings['paypal_ipn_fallback'],
					'classes' => array('switch-medium'),
				),*/
			/*	array(
					'name' => sprintf('[%s][debug]', self::ID),
					'title' => __('Enable Debug', 'jigoshop_paypal_express'),
					'tip' => __('Debug log will provide you with most of the data and events generated by the payment process.', 'jigoshop_paypal_express'),
					'type' => 'checkbox',
					'checked' => $this->settings['debug'],
					'classes' => array('switch-medium'),
				),*/
			/*array(
					'name' => sprintf('[%s][receiver_error_logs]', self::ID),
					'title' => __('Receive Error Logs', 'jigoshop_paypal_express'),
					'tip' => __('Do you want to receive the error logs from jigoshop security/fraud checks. The error logs are only generated during the final stages of the payment processing and can be used in live mode to receive any payment errors or potential fraud tempering with the data.', 'jigoshop_paypal_express'),
					'type' => 'checkbox',
					'checked' => $this->settings['receiver_error_logs'],
					'classes' => array('switch-medium'),
				),*/
			/*array(
					'name' => sprintf('[%s][debug_email]', self::ID),
					'title' => __('Debug Email', 'jigoshop_paypal_express'),
					'tip' => __('Email you want to receive all error logs and the debug data to. If email field is empty, the admin email will be used.', 'jigoshop_paypal_express'),
					'type' => 'text',
					'value' => $this->settings['debug_email'],
				),*/
			array(
				'name' => sprintf('[%s][testMode]', self::ID),
				'title' => __('Sandbox/Testmode', 'jigoshop_paypal_express'),
				'tip' => __('Enable PayPal Express Sandbox', 'jigoshop_paypal_express'),
				'type' => 'checkbox',
				'checked' => $this->settings['testMode'],
				'classes' => array('switch-medium'),
			),
			array(
				'name' => sprintf('[%s][adminOnly]', self::ID),
				'title' => __('Admin Only', 'jigoshop_paypal_express'),
				'tip' => __('Enable PayPal Express Sandbox only for admin', 'jigoshop_paypal_express'),
				'type' => 'checkbox',
				'checked' => $this->settings['adminOnly'],
				'classes' => array('switch-medium'),
			),
		);
	}

	private function pathToImage($fileName)
	{
		$path = JIGOSHOP_PAYPAL_EXPRESS_URL . '/assets/images/'.$fileName;

		return $path;
	}

	/*
	 * Sanitize data*/
	private function validateArData($data)
	{
		if (is_array($data)) {
			filter_var_array($data, FILTER_SANITIZE_STRING);
		}

		return $data;
	}

	private function isEmail($email)
	{
		if (!empty($email)) {
			return preg_match('/^[A-Za-z0-9!#$%&\'*+-\/=?^_`{|}~]+@[A-Za-z0-9-]+(\.[AZa-z0-9-]+)+[A-Za-z]$/', $email);
		}

		return true;
	}

	/**
	 * Validates and returns properly sanitized options.
	 *
	 * @param $settings array Input options.
	 *
	 * @return array Sanitized result.
	 */
	public function validateOptions($settings)
	{
		$disabled = null;
		$settings['enabled'] = $settings['enabled'] == 'on';
		//$settings['debug'] = $settings['debug'] == 'on';
		$settings['testMode'] = $settings['testMode'] == 'on';
		$settings['adminOnly'] = $settings['adminOnly'] == 'on';
		$settings['force_free_order_payment'] = $settings['force_free_order_payment'] == 'on';
	//	$settings['force_free_product_payment'] = $settings['force_free_product_payment'] == 'on';
	//	$settings['paypal_ipn_fallback'] = $settings['paypal_ipn_fallback'] == 'on';
	//	$settings['receiver_error_logs'] = $settings['receiver_error_logs'] == 'on';

		$settings['title'] = trim(htmlspecialchars(strip_tags($settings['title'])));
		$settings['description'] = trim(htmlspecialchars(strip_tags($settings['description'],
			'<p><a><strong><em><b><i>')));

		$settings['transaction_type'] = $this->validateArData($settings['transaction_type']);
		//$settings['paypal_language'] = $this->validateArData($settings['paypal_language']);
		$settings['paypal_button_language'] = $this->validateArData($settings['paypal_button_language']);

		//$settings['debug_email'] = trim(strip_tags($settings['debug_email']));
		$settings['receiver_email'] = trim(strip_tags($settings['receiver_email']));
		$settings['image_name'] = trim(strip_tags($settings['image_name']));
		$settings['api_username'] = trim(strip_tags(esc_attr($settings['api_username'])));
		$settings['api_password'] = trim(strip_tags(esc_attr($settings['api_password'])));
		$settings['api_signature'] = trim(strip_tags(esc_attr($settings['api_signature'])));
	//	$settings['page_style_name'] = trim(strip_tags(esc_attr($settings['page_style_name'])));

		/*if($settings['enabled'] == 'on'){
			if (!$this->isEmail($settings['debug_email']) && !empty($settings['debug_email'])) {
				$this->messages->addError('Please enter a valid debug email. <strong>Gateway is disabled</strong>', 'jigoshop_paypal_express');
				$settings['enabled'] = $disabled;
			}
		}*/
		if($settings['enabled'] == 'on'){
			if (!$this->isEmail($settings['receiver_email']) && !empty($settings['receiver_email'])) {
				$this->messages->addError('Please enter a valid receiver email. <strong>Gateway is disabled</strong>', 'jigoshop_paypal_express');
				$settings['enabled'] = $disabled;
			}
		}


		return $settings;
	}

	/**
	 * Renders method fields and data in Checkout page.
	 */
	public function render()
	{
		if($this->settings['description']){
			echo wpautop($this->settings['description']);
		}
	}

	public function paypalExpressRedirect()
	{
		if(isset($_GET['action']) && $_GET['action'] == '_express_checkout'){
			Scripts::add('jigoshop.paypal_express.frontend.checkout', JIGOSHOP_PAYPAL_EXPRESS_URL . '/assets/js/express-checkout-script.js', array('jquery'));
			Styles::add('jigoshop.paypal_express.frontend.style', JIGOSHOP_PAYPAL_EXPRESS_URL . '/assets/css/paypal.css');
		}
		if(isset($_GET['payment_action']) && $_GET['payment_action'] == 'paypal_express'){
			Scripts::add('jigoshop.paypal_express.frontend.checkout.pay', JIGOSHOP_PAYPAL_EXPRESS_URL . '/assets/js/express-checkout-pay-script.js', array('jquery'));
			/*Scripts::localize('jigoshop.paypal_express.frontend.checkout.pay', 'jigoshop_paypal_express_gateway', array(
				'merchantID' => $merchantID,
				'env' => $env,
			));*/
			$orderId = $_GET['order'];
			/**@var Order $order*/
			$order = $this->orderService->find($orderId);
			try{
				$paypal = new PaypalExpressCheckoutClass();
				$currency = Currency::code();
				$cancelURL = \Jigoshop\Helper\Order::getCancelLink($order);
				$cancelURL = str_replace('&', '%26', $cancelURL);
				$returnURL = add_query_arg('paypal_express_gateway', 'success', add_query_arg('key', $order->getKey(),
					add_query_arg('order', $order->getId(),
						get_permalink($this->options->getPageId(Pages::THANK_YOU)))));
				$returnURL = str_replace('&', '%26', $returnURL);

				$checkoutPay = get_permalink($this->options->getPageId(Pages::CHECKOUT));
				$errorRedirect = \Jigoshop\Helper\Order::getPayLink($order);

				$checkoutPay .= $errorRedirect;

				$logoImage = $this->pathToImage($this->settings['image_name']);
				$logoImage = esc_url($logoImage);

				$paypalExpressArgs = [
					'PAYMENTREQUEST_0_TAXAMT' => number_format($order->getTotalTax(), 2),
					'L_PAYMENTREQUEST_0_DESC0' => $order->getCustomerNote(),
					'L_PAYMENTREQUEST_0_NUMBER0' => $order->getId(),
					'PAYMENTREQUEST_0_SHIPPINGAMT' => $order->getShippingPrice(),
					'PAYMENTREQUEST_0_AMT' => $order->getTotal(),
					'LOGOIMG' => $logoImage,
					'currencyCodeType' => $currency,
					'PAYMENTREQUEST_0_PAYMENTACTION' => $this->settings['transaction_type'],
				];

				$count = 1;
				if(sizeof($order->getItems()) > 0){
					foreach($order->getItems() as $item){
						if($item->getQuantity()){
							$title = $item->getName();
							$qty = $item->getQuantity();
							$paypalExpressArgs['L_PAYMENTREQUEST_0_NAME0' . $count] = $title;
							$paypalExpressArgs['L_PAYMENTREQUEST_0_QTY0' . $count] = $qty;
							$paypalExpressArgs['PAYMENTREQUEST_0_ITEMAMT' . $count] = $item->getPrice();

							$count++;
						}
					}
				}

				if($order->getTotal() == 0){
					$this->addAmountToZeroOrderTotal($order, $paypalExpressArgs);
				}

				$paypalExpressArgs['L_PAYMENTREQUEST_0_AMT0'] =  $order->getTotal();

				$resArray = $paypal->CallShortcutExpressCheckout($paypalExpressArgs, $returnURL, $cancelURL);

				$ack = strtoupper($resArray["ACK"]);

				if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING"){
					$paypal->RedirectToPayPal($resArray["TOKEN"]);
				}else{
					wp_safe_redirect($checkoutPay);
					$this->messages->addError('We are sorry, something went wrong and payment was rejected from Paypal system.');
					exit;
				}
			}catch(\Jigoshop\Service\Exception $e){
				$checkoutPay = get_permalink($this->options->getPageId(Pages::CHECKOUT));
				$errorRedirect = \Jigoshop\Helper\Order::getPayLink($order);

				$checkoutPay .= $errorRedirect;
				wp_safe_redirect($checkoutPay);
				exit;
			}
		}
	}

	public function addAmountToZeroOrderTotal($order,$paypalOrderTotal)
	{
		$paypalExpressArgs = array();
		/**@var Order $order*/
		if($order->getTotal() == 0){
			$paypalOrderTotal['PAYMENTREQUEST_0_AMT'] = 0.01;
			$paypalOrderTotal['L_PAYMENTREQUEST_0_AMT0'] = $paypalOrderTotal['PAYMENTREQUEST_0_AMT'];
			$paypalExpressArgs[] = $paypalOrderTotal;
		}

		return $paypalExpressArgs;
	}

	/**
	 * @var Order $order
	 */
	public function sendOrder($order)
	{
		try{
			$paypal = new PaypalExpressCheckoutClass();
			$currency = Currency::code();
			$cancelURL = \Jigoshop\Helper\Order::getCancelLink($order);
			$cancelURL = str_replace('&', '%26', $cancelURL);
			$returnURL = add_query_arg('paypal_express_gateway', 'success', add_query_arg('key', $order->getKey(),
				add_query_arg('order', $order->getId(),
					get_permalink($this->options->getPageId(Pages::THANK_YOU)))));
			$returnURL = str_replace('&', '%26', $returnURL);
			$checkoutPay = \Jigoshop\Helper\Order::getPayLink($order);
			$errorRedirect = \Jigoshop\Helper\Order::getPayLink($order);

			$checkoutPay .= $errorRedirect;

			$shipping = $order->getCustomer()->getShippingAddress();
			$logoImage = $this->pathToImage($this->settings['image_name']);
			$logoImage = esc_url($logoImage);

			$paypalExpressArgs = [
				'PAYMENTREQUEST_0_AMT' => $order->getTotal(),
				'paymentType' => $this->settings['transaction_type'],
				'RETURN_URL' => $returnURL,
				'CANCEL_URL' => $cancelURL,
				'currencyCodeType' => $currency,
				'PAYMENTREQUEST_0_TAXAMT' => number_format($order->getTotalTax(), 2),
				'L_PAYMENTREQUEST_0_DESC0' => $order->getCustomerNote(),
				'L_PAYMENTREQUEST_0_NUMBER0' => $order->getId(),
				'PAYMENTREQUEST_0_SHIPPINGAMT' => $order->getShippingPrice(),
				'LOGOIMG' => $logoImage,
			];

			$shippingDetails = [
				'L_PAYMENTREQUEST_FIRSTNAME' => $shipping->getFirstName(),
				'L_PAYMENTREQUEST_LASTNAME', $shipping->getLastName(),
				'PAYMENTREQUEST_0_SHIPTOSTREET' => $shipping->getAddress(),
				'PAYMENTREQUEST_0_SHIPTOSTREET2' => $shipping->getAddress(),
				'PAYMENTREQUEST_0_SHIPTOCITY' => $shipping->getCity(),
				'PAYMENTREQUEST_0_SHIPTOSTATE' => $shipping->getState(),
				'PAYMENTREQUEST_0_SHIPTOZIP' => $shipping->getPostcode(),
				'PAYMENTREQUEST_0_SHIPTOCOUNTRY' => $shipping->getCountry(),
			];

			$count = 1;
			if(sizeof($order->getItems()) > 0){
				foreach($order->getItems() as $item){
					if($item->getQuantity()){
						$title = $item->getName();
						$qty = $item->getQuantity();
						$paypalExpressArgs['L_PAYMENTREQUEST_0_NAME0' . $count] = $title;
						$paypalExpressArgs['L_PAYMENTREQUEST_0_QTY0' . $count] = $qty;
						$paypalExpressArgs['PAYMENTREQUEST_0_ITEMAMT' . $count] = $item->getPrice();

						$count++;
					}
				}
			}

			if($order->getTotal() == 0){
				$this->addAmountToZeroOrderTotal($order, $paypalExpressArgs);
			}

			$paypalExpressArgs['L_PAYMENTREQUEST_0_AMT0'] =  $order->getTotal();
			$total = $paypalExpressArgs['L_PAYMENTREQUEST_0_AMT0'];

			$resArray = $paypal->CallMarkExpressCheckout($total, $shippingDetails, $paypalExpressArgs);
			$ack = strtoupper($resArray["ACK"]);

			if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING"){
				$paypal->RedirectToPayPal($resArray["TOKEN"]);
			}else{
				wp_safe_redirect($checkoutPay);
				$this->messages->addError('We are sorry, something went wrong and payment was rejected from Paypal system.');
				exit;
			}
		}catch(\Jigoshop\Service\Exception $e){
			$checkoutPay = get_permalink($this->options->getPageId(Pages::CHECKOUT));
			$errorRedirect = \Jigoshop\Helper\Order::getPayLink($order);

			$checkoutPay .= $errorRedirect;
			wp_safe_redirect($checkoutPay);
			$this->messages->addError('We are sorry, something went wrong and payment was rejected from Paypal system.');
			exit;
		}
	}

	/**
	 * @param Order $order Order to process payment for.
	 *
	 * @return string URL to redirect to.
	 * @throws Exception On any payment error.
	 */
	public function process($order)
	{
		if(isset($_GET['action']) && $_GET['action'] == '_express_checkout'){
			return add_query_arg(array('pay' => $order->getId(), 'key' => $order->getKey()),add_query_arg(array('payment_action' => 'paypal_express', 'order' => $order->getId())), Api::getEndpointUrl('pay'));
		}else{
			$this->sendOrder($order);
			
			return \Jigoshop\Helper\Order::getPayLink($order);
		}
	}
	
	/**
	 * Whenever method was enabled by the user.
	 *
	 * @return boolean Method enable state.
	 */
	public function isActive()
	{
		if ($this->settings['enabled']) {
			$enabled = true;
		} else {
			$enabled = false;
		}
		return $enabled;
	}
	
	/**
	 * Set method enable state.
	 *
	 * @param boolean $state Method enable state.
	 *
	 * @return array Method current settings (after enable state change).
	 */
	public function setActive($state)
	{
		$this->settings['enabled'] = $state;
		
		return $this->settings;
	}
	
	/**
	 * Whenever method was configured by the user (all required data was filled for current scenario).
	 *
	 * @return boolean Method config state.
	 */
	public function isConfigured()
	{
		if ($this->settings['enabled']) {
			if (isset($this->settings['api_username']) && $this->settings['api_username']
				&& isset($this->settings['api_password']) && $this->settings['api_password'] &&
				isset($this->settings['api_signature']) && $this->settings['api_signature']
			) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Whenever method has some sort of test mode.
	 *
	 * @return boolean Method test mode presence.
	 */
	public function hasTestMode()
	{
		return true;
	}
	
	/**
	 * Whenever method test mode was enabled by the user.
	 *
	 * @return boolean Method test mode state.
	 */
	public function isTestModeEnabled()
	{
		$testModeState = false;
		if ($this->settings['testMode']) {
			$testModeState = true;
		}
		
		return $testModeState;
	}
	
	/**
	 * Set Method test mode state.
	 *
	 * @param boolean $state Method test mode state.
	 *
	 * @return array Method current settings (after test mode state change).
	 */
	public function setTestMode($state)
	{
		$this->settings['testMode'] = $state;
		
		return $this->settings;
	}
	
	/**
	 * Whenever method requires SSL to be enabled to function properly.
	 *
	 * @return boolean Method SSL requirment.
	 */
	public function isSSLRequired()
	{
		return false;
	}
	
	/**
	 * Whenever method is set to enabled for admin only.
	 *
	 * @return boolean Method admin only state.
	 */
	public function isAdminOnly()
	{
		if (true == $this->settings['adminOnly']) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Sets admin only state for the method and returns complete method options.
	 *
	 * @param boolean $state Method admin only state.
	 *
	 * @return array Complete method options after change was applied.
	 */
	public function setAdminOnly($state)
	{
		$this->settings['adminOnly'] = $state;
		
		return $this->settings;
	}
}