<?php

namespace Jigoshop\Extension\PaypalExpressGateway\Common\Method;

use Jigoshop\Extension\PaypalExpressGateway\Common\Method as PaypalExpressMethod;
use Jigoshop\Helper\Options as OptionsHelper;

class PaypalExpressCheckoutClass
{
	private $options;
	
	private $merchantId;
	private $merchantPassword;
	private $merchantSignature;
	private $env;
	private $paypalUrl;
	private $paypalEndpointUrl;
	private $sBNCode;
	private $version;
	private $ADDRESS_OVERRIDE = true;
	
	const PAYPAL_ID = PaypalExpressMethod::ID;
	const API_VERSION = '109.0';
	const SBN_CODE = 'PP-DemoPortal-EC-IC-php';
	const EXPRESS_MARK = true;
	const USERACTION_FLAG = false;

	public static $_options;
	
	public function __construct()
	{
		$this->options = OptionsHelper::getOptions('payment.' . self::PAYPAL_ID);
		
		if ($this->options['test_mode'] == true) {
			$this->env = 'sandbox';
		} else {
			$this->env = 'production';
		}

		$this->ADDRESS_OVERRIDE = true;
		
		$this->sBNCode = self::SBN_CODE;
		$this->version = self::API_VERSION;
	}

	private function getPaypalUrl()
	{
		if ($this->options['test_mode'] == true) {
			$this->paypalUrl = 'https://www.sandbox.paypal.com/checkoutnow?token=';
		} else {
			$this->paypalUrl = 'https://api-3t.sandbox.paypal.com/nvp';
		}
		
		return $this->paypalUrl;
	}
	
	private function getPaypalApiEndpointUrl()
	{
		if ($this->options['test_mode'] == true) {
			$this->paypalEndpointUrl = 'https://api-3t.sandbox.paypal.com/nvp';
		} else {
			$this->paypalEndpointUrl = 'https://api-3t.paypal.com/nvp';
		}
		
		return $this->paypalEndpointUrl;
	}
	
	/**
	 * @param $FinalPaymentAmt
	 * @return array
	 */
	public function ConfirmPayment($FinalPaymentAmt)
	{
		$nvpstr = '';
		if (isset($_SESSION['TOKEN']))
			$nvpstr = '&TOKEN=' . urlencode($_SESSION['TOKEN']);
		
		if (isset($_SESSION['payer_id']))
			$nvpstr .= '&PAYERID=' . urlencode($_SESSION['payer_id']);
		
		if (isset($_SESSION['PaymentType']))
			$nvpstr .= '&PAYMENTREQUEST_0_PAYMENTACTION=' . urlencode($_SESSION['PaymentType']);
		
		if (isset($_SERVER['SERVER_NAME']))
			$nvpstr .= '&IPADDRESS=' . urlencode($_SERVER['SERVER_NAME']);
		
		$nvpstr .= '&PAYMENTREQUEST_0_AMT=' . $FinalPaymentAmt;
		
		if (isset($_SESSION['currencyCodeType']))
			$nvpstr .= '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($_SESSION['currencyCodeType']);
		
		if (isset($_SESSION['itemAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($_SESSION['itemAmt']);
		
		if (isset($_SESSION['taxAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($_SESSION['taxAmt']);
		
		if (isset($_SESSION['shippingAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($_SESSION['shippingAmt']);
		
		if (isset($_SESSION['handlingAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($_SESSION['handlingAmt']);
		
		if (isset($_SESSION['shippingDiscAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($_SESSION['shippingDiscAmt']);
		
		if (isset($_SESSION['insuranceAmt']))
			$nvpstr = $nvpstr . '&PAYMENTREQUEST_0_INSURANCEAMT=' . urlencode($_SESSION['insuranceAmt']);
		
		$resArray = $this->hashCall("DoExpressCheckoutPayment", $nvpstr);
		
		$ack = strtoupper($resArray["ACK"]);
		
		return $resArray;
	}
	
	
	/**
	 * @param $paramsArray
	 * @param $returnURL
	 * @param $cancelURL
	 * @return array
	 */
	public function CallShortcutExpressCheckout($paramsArray, $returnURL, $cancelURL)
	{
		
		$nvpstr = '';
		//Mandatory parameters for SetExpressCheckout API call
		if (isset($paramsArray["PAYMENTREQUEST_0_AMT"])) {
			$nvpstr = "&PAYMENTREQUEST_0_AMT=" . $paramsArray["PAYMENTREQUEST_0_AMT"];
			$_SESSION["Payment_Amount"] = $paramsArray["PAYMENTREQUEST_0_AMT"];
		}
		
		if (isset($paramsArray["paymentType"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $paramsArray["paymentType"];
			$_SESSION["PaymentType"] = $paramsArray["paymentType"];
		}
		
		if (isset($returnURL))
			$nvpstr = $nvpstr . "&RETURNURL=" . $returnURL;
		
		if (isset($cancelURL))
			$nvpstr = $nvpstr . "&CANCELURL=" . $cancelURL;
		
		if (isset($paramsArray["currencyCodeType"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $paramsArray["currencyCodeType"];
			$_SESSION["currencyCodeType"] = $paramsArray["currencyCodeType"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_ITEMAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_ITEMAMT=" . $paramsArray["PAYMENTREQUEST_0_ITEMAMT"];
			$_SESSION['itemAmt'] = $paramsArray["PAYMENTREQUEST_0_ITEMAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_TAXAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_TAXAMT=" . $paramsArray["PAYMENTREQUEST_0_TAXAMT"];
			$_SESSION['taxAmt'] = $paramsArray["PAYMENTREQUEST_0_TAXAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPPINGAMT=" . $paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"];
			$_SESSION['shippingAmt'] = $paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_HANDLINGAMT=" . $paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"];
			$_SESSION['handlingAmt'] = $paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPDISCAMT=" . $paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"];
			$_SESSION['shippingDiscAmt'] = $paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_INSURANCEAMT=" . $paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"];
			$_SESSION['insuranceAmt'] = $paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"];
		}
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_NAME0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NAME0=" . $paramsArray["L_PAYMENTREQUEST_0_NAME0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_NUMBER0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NUMBER0=" . $paramsArray["L_PAYMENTREQUEST_0_NUMBER0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_DESC0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_DESC0=" . $paramsArray["L_PAYMENTREQUEST_0_DESC0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_AMT0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_AMT0=" . $paramsArray["L_PAYMENTREQUEST_0_AMT0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_QTY0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_QTY0=" . $paramsArray["L_PAYMENTREQUEST_0_QTY0"];
		
		if (isset($paramsArray["LOGOIMG"]))
			$nvpstr = $nvpstr . "&LOGOIMG=" . $paramsArray["LOGOIMG"];
		
		$resArray = $this->hashCall("SetExpressCheckout", $nvpstr);
		
		$ack = strtoupper($resArray["ACK"]);

		if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
			$token = urldecode($resArray["TOKEN"]);
			$_SESSION['TOKEN'] = $token;
		}


		return $resArray;
	}
	
	/**
	 * @param $paymentAmount
	 * @param $shippingDetail
	 * @param $paramsArray
	 * @return array
	 */
	public function CallMarkExpressCheckout($paymentAmount, $shippingDetail, $paramsArray)
	{
		$nvpstr = '';
		
		if (isset($paramsArray["PAYMENTREQUEST_0_AMT"])) {
			$nvpstr = "&PAYMENTREQUEST_0_AMT=" . $paramsArray["PAYMENTREQUEST_0_AMT"];
			$_SESSION["Payment_Amount"] = $paramsArray["PAYMENTREQUEST_0_AMT"];
		}
		
		if (isset($paramsArray["paymentType"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $paramsArray["paymentType"];
			$_SESSION["PaymentType"] = $paramsArray["paymentType"];
		}
		
		if (isset($paramsArray["RETURN_URL"]))
			$nvpstr = $nvpstr . "&RETURNURL=" . $paramsArray["RETURN_URL"];
		
		if (isset($paramsArray["CANCEL_URL"]))
			$nvpstr = $nvpstr . "&CANCELURL=" . $paramsArray["CANCEL_URL"];
		
		if (isset($paramsArray["currencyCodeType"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $paramsArray["currencyCodeType"];
			$_SESSION["currencyCodeType"] = $paramsArray["currencyCodeType"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_ITEMAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_ITEMAMT=" . $paramsArray["PAYMENTREQUEST_0_ITEMAMT"];
			$_SESSION['itemAmt'] = $paramsArray["PAYMENTREQUEST_0_ITEMAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_TAXAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_TAXAMT=" . $paramsArray["PAYMENTREQUEST_0_TAXAMT"];
			$_SESSION['taxAmt'] = $paramsArray["PAYMENTREQUEST_0_TAXAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPPINGAMT=" . $paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"];
			$_SESSION['shippingAmt'] = $paramsArray["PAYMENTREQUEST_0_SHIPPINGAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_HANDLINGAMT=" . $paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"];
			$_SESSION['handlingAmt'] = $paramsArray["PAYMENTREQUEST_0_HANDLINGAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPDISCAMT=" . $paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"];
			$_SESSION['shippingDiscAmt'] = $paramsArray["PAYMENTREQUEST_0_SHIPDISCAMT"];
		}
		
		if (isset($paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_INSURANCEAMT=" . $paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"];
			$_SESSION['insuranceAmt'] = $paramsArray["PAYMENTREQUEST_0_INSURANCEAMT"];
		}
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_NAME0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NAME0=" . $paramsArray["L_PAYMENTREQUEST_0_NAME0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_NUMBER0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NUMBER0=" . $paramsArray["L_PAYMENTREQUEST_0_NUMBER0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_DESC0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_DESC0=" . $paramsArray["L_PAYMENTREQUEST_0_DESC0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_AMT0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_AMT0=" . $paramsArray["L_PAYMENTREQUEST_0_AMT0"];
		
		if (isset($paramsArray["L_PAYMENTREQUEST_0_QTY0"]))
			$nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_QTY0=" . $paramsArray["L_PAYMENTREQUEST_0_QTY0"];
		
		if (isset($paramsArray["LOGOIMG"]))
			$nvpstr = $nvpstr . "&LOGOIMG=" . $paramsArray["LOGOIMG"];
		
		if ($this->ADDRESS_OVERRIDE)
			$nvpstr = $nvpstr . "&ADDROVERRIDE=1";
		
		if (isset($shippingDetail["L_PAYMENTREQUEST_FIRSTNAME"])) {
			$fullname = $shippingDetail["L_PAYMENTREQUEST_FIRSTNAME"];
			if (isset($shippingDetail["L_PAYMENTREQUEST_LASTNAME"]))
				$fullname = $fullname . " " . $shippingDetail["L_PAYMENTREQUEST_LASTNAME"];
			
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTONAME=" . $fullname;
			$_SESSION["shipToName"] = $fullname;
		}
		
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET"];
			$_SESSION['shipToAddress'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET"];
		}
		
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET2"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET2=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET2"];
			$_SESSION['shipToAddress2'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTREET2"];
		}
		
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOCITY"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCITY=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOCITY"];
			$_SESSION['shipToCity'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOCITY"];
		}
		
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOSTATE"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTATE=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTATE"];
			$_SESSION['shipToState'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOSTATE"];
		}
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOZIP"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOZIP=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOZIP"];
			$_SESSION['shipToZip'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOZIP"];
		}
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOCOUNTRY"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCOUNTRY=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOCOUNTRY"];
			$_SESSION['shipToCountry'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOCOUNTRY"];
		}
		if (isset($shippingDetail["PAYMENTREQUEST_0_SHIPTOPHONENUM"])) {
			$nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOPHONENUM=" . $shippingDetail["PAYMENTREQUEST_0_SHIPTOPHONENUM"];
			$_SESSION['shipToPhone'] = $shippingDetail["PAYMENTREQUEST_0_SHIPTOPHONENUM"];
		}
		
		$resArray = $this->hashCall("SetExpressCheckout", $nvpstr);
		$ack = strtoupper($resArray["ACK"]);
		if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
			$token = urldecode($resArray["TOKEN"]);
			$_SESSION['TOKEN'] = $token;
		}
		return $resArray;
	}
	
	/**
	 * @param $token
	 * @return array
	 */
	public function GetShippingDetails($token)
	{
		$nvpstr = "&TOKEN=" . $token;
		
		$resArray = $this->hashCall("GetExpressCheckoutDetails", $nvpstr);
		$ack = strtoupper($resArray["ACK"]);
		if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
			$_SESSION['payer_id'] = $resArray['PAYERID'];
		}
		
		return $resArray;
	}
	
	/**
	 * @param $methodName
	 * @param $nvpStr
	 * @return array
	 */
	public function hashCall($methodName, $nvpStr)
	{
		$API_Password = $this->options['api_password'];
		$API_UserName = $this->options['api_username'];
		$API_Signature = $this->options['api_signature'];

		$endpoint = esc_url($this->getPaypalApiEndpointUrl());
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$nvpreq="METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($this->version) . "&PWD=" .
									urlencode($API_Password) . "&USER=" . urlencode($API_UserName) . "&SIGNATURE=" .
								urlencode($API_Signature) . $nvpStr . "&BUTTONSOURCE=" . urlencode($this->sBNCode);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
		
		$response = curl_exec($ch);
		
		$nvpResArray = $this->deformatNVP($response);
		$nvpReqArray = $this->deformatNVP($nvpreq);
		$_SESSION['nvpReqArray'] = $nvpReqArray;

		if (curl_errno($ch)) {
			$_SESSION['curl_error_no'] = curl_errno($ch);
			$_SESSION['curl_error_msg'] = curl_error($ch);
			
		} else {
			curl_close($ch);
		}

		return $nvpResArray;
	}
	
	/**
	 * @param $nvpstr
	 * @return array
	 */
	private function deformatNVP($nvpstr)
	{
		$intial = 0;
		$nvpArray = array();

		while (strlen($nvpstr)) {
			
			$keypos = strpos($nvpstr, '=');
			$valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);
			
			$keyval = substr($nvpstr, $intial, $keypos);
			$valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
			
			$nvpArray[urldecode($keyval)] = urldecode($valval);
			$nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
		}

		return $nvpArray;
	}
	
	/**
	 * @param $token
	 */
	public function RedirectToPayPal($token)
	{
		$url = esc_url($this->getPaypalUrl());
		$payPalURL = $url . $token;
		if ($_SESSION['EXPRESS_MARK'] == 'ECMark') {
			$payPalURL = $payPalURL . '&useraction=commit';
		}
		else {
			$payPalURL = $payPalURL. '&useraction=commit';
		}
		header("Location:".$payPalURL);
		exit;
	}
}