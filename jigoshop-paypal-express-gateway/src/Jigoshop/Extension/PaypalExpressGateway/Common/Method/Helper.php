<?php

namespace Jigoshop\Extension\PaypalExpressGateway\Common\Method;


use Jigoshop\Service\Exception;

class Helper
{
	protected static function paypalLanguage()
	{
		return array(
			'US' => __('English', 'jigoshop_paypal_express'),
			'DE' => __('Deutsch', 'jigoshop_paypal_express'),
			'ES' => __('Spanish', 'jigoshop_paypal_express'),
			'FR' => __('French', 'jigoshop_paypal_express'),
			'IT' => __('Italian', 'jigoshop_paypal_express'),
			'NL' => __('Dutch', 'jigoshop_paypal_express'),
			'PT' => __('Portuguese', 'jigoshop_paypal_express'),
			'CN' => __('Chinese', 'jigoshop_paypal_express'),
			'PL' => __('Polish', 'jigoshop_paypal_express'),
			'RU' => __('Russian', 'jigoshop_paypal_express'),
			'sv_SE' => __('Swedish (for Sweden only)', 'jigoshop_paypal_express'),
			'tr_TR' => __('Turkish (for Turkey only)', 'jigoshop_paypal_express'),
			'zh_HK' => __('Traditional Chinese (for Hong Kong only)', 'jigoshop_paypal_express'),
			'zh_TW' => __('Traditional Chinese (for Taiwan only)', 'jigoshop_paypal_express'),
			'no_NO' => __('Norwegian (for Norway only)', 'jigoshop_paypal_express'),
			'jp_JP' => __('Japanese (for Japan only)', 'jigoshop_paypal_express'),
			'da_DK' => __('Danish (for Denmark only)', 'jigoshop_paypal_express'),
		);
	}
	
	protected static function paypalButtonLanguage()
	{
		return array(
			'au' => __('Australia', 'jigoshop_paypal_express'),
			'at' => __('Austria', 'jigoshop_paypal_express'),
			'fr_BE' => __('Belgium (French)', 'jigoshop_paypal_express'),
			'fr_CA' => __('Canada (French)', 'jigoshop_paypal_express'),
			'cn' => __('China', 'jigoshop_paypal_express'),
			'fr' => __('France', 'jigoshop_paypal_express'),
			'de' => __('Germany', 'jigoshop_paypal_express'),
			'it' => __('Italy', 'jigoshop_paypal_express'),
			'j1' => __('Japan', 'jigoshop_paypal_express'),
			'nl' => __('Netherlands', 'jigoshop_paypal_express'),
			'pl' => __('Poland', 'jigoshop_paypal_express'),
			'pt' => __('Portugal', 'jigoshop_paypal_express'),
			'ru' => __('Russia', 'jigoshop_paypal_express'),
			'es' => __('Spain', 'jigoshop_paypal_express'),
			'sv_SE' => __('Sweden', 'jigoshop_paypal_express'),
			'de_CH' => __('Switzerland (German)', 'jigoshop_paypal_express'),
			'tr_TR' => __('Turkey', 'jigoshop_paypal_express'),
			'zh_HK' => __('Hong Kong (Chinese)', 'jigoshop_paypal_express'),
			'zh_TW' => __('Taiwan (Chinese)', 'jigoshop_paypal_express'),
			'uk' => __('United Kingdom', 'jigoshop_paypal_express'),
			'us' => __('United States', 'jigoshop_paypal_express'),
		);
	}
	
	public static function getLanguage($language)
	{
		switch($language){
			case 'paypal' :
				return self::paypalLanguage();
			break;
			
			case 'button' :
				return self::paypalButtonLanguage();
			break;
			default :
				throw new Exception('Language type: ' . $language . ' does not exists!');
			break;
		}
	}
}