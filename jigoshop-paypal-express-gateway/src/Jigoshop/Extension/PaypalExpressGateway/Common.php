<?php

namespace Jigoshop\Extension\PaypalExpressGateway;

use Jigoshop\Integration;
use Jigoshop\Container;

class Common
{
	/**
	 * Common constructor.
	 */
	public function __construct()
	{
		Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
		Integration\Helper\Render::addLocation('paypal_express', JIGOSHOP_PAYPAL_EXPRESS_DIR);
		/**@var Container $di*/
		$di = Integration::getService('di');
		$di->services->setDetails('jigoshop.payment.paypal_express', __NAMESPACE__ . '\\Common\\Method', array(
			'jigoshop.options',
			'jigoshop.service.cart',
			'jigoshop.service.order',
			'jigoshop.messages',
		));

		$di->triggers->add('jigoshop.service.payment', 'addMethod', array('jigoshop.payment.paypal_express'));
	}
}

new Common();