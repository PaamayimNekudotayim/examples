Jigoshop Paypal Express Gateway
===============================

This plugin allows you to use PayPal Express Checkout within your Jigoshop powered WordPress.

###Installation:

1. Upload the plugin folder to the '/wp-content/plugins/' directory
2. Activate plugin through the 'Plugins' menu in WordPress
3. Go to 'Jigoshop/Manage Licenses' to enter license key for your plugin
