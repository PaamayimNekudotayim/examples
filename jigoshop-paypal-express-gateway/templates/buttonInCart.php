<?php
/**
 * @var $checkout string
 * @var $button array
 */
?>
<a href="#" onclick="return false;">
    <object data="<?php echo esc_url($button); ?>" onclick="window.location='<?php echo esc_url($checkout); ?>'; return false;"></object>
</a>
