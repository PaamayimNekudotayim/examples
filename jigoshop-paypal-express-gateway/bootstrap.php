<?php
/**
 * Plugin Name: Jigoshop PayPal Express Checkout Payment Gateway
 * Plugin URI: https://www.jigoshop.com/product/jigoshop-paypal-express-gateway/
 * Description: Allows you to use <a href="https://www.paypal.com/webapps/mpp/express-checkout">PayPal Express Checkout</a> payment gateway with the Jigoshop plugin.
 * Version: 3.0.1
 * Author: @vasilguruli | JigoLtd
 */
// Define plugin name
define('JIGOSHOP_PAYPAL_EXPRESS_NAME', 'Jigoshop PayPal Express Checkout Payment Gateway');
add_action('plugins_loaded', function () {
    load_plugin_textdomain('jigoshop_paypal_express', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    if (class_exists('\Jigoshop\Core')) {
        //Check version.
        //if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_PAYPAL_EXPRESS_NAME, '2.0')) {
        //    return;
        //}
        //Check license.
        //$licence = new \Jigoshop\Licence(__FILE__, 'add_licence_number', 'http://www.jigoshop.com');
        //if (!$licence->isActive()) {
        //   return;
        //}
        // Define plugin directory for inclusions
        define('JIGOSHOP_PAYPAL_EXPRESS_DIR', dirname(__FILE__));
        // Define plugin URL for assets
        define('JIGOSHOP_PAYPAL_EXPRESS_URL', plugins_url('', __FILE__));
        //Init components.
        require_once(JIGOSHOP_PAYPAL_EXPRESS_DIR . '/src/Jigoshop/Extension/PaypalExpressGateway/Common.php');
        if (is_admin()) {
            require_once(JIGOSHOP_PAYPAL_EXPRESS_DIR . '/src/Jigoshop/Extension/PaypalExpressGateway/Admin.php');
        } //else {
        //require_once(JIGOSHOP_PAYPAL_EXPRESS_DIR . '/src/Jigoshop/Extension/PaypalExpressGateway/Frontend.php');
        //}
    }else {
        add_action('admin_notices', function () {
            echo '<div class="error"><p>';
            printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.',
                'jigoshop_paypal_express'), JIGOSHOP_PAYPAL_EXPRESS_NAME, JIGOSHOP_PAYPAL_EXPRESS_NAME);
            echo '</p></div>';
        });
    }
});
