<?php

namespace Jigoshop\Extension\ImageWatermark;

use Jigoshop\Extension\ImageWatermark\Admin\Image;
use \Jigoshop\Integration;
use \Jigoshop\Container;
use Jigoshop\Integration\Helper\Render;

class Init
{
	private static $class;
	
	private function __construct()
	{
		Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
		Render::addLocation('image_watermark', JIGOSHOP_IMAGE_WATERMARK_DIR);
	}
	private function __clone(){}
	
	public static function loadPlugin()
	{
		if(null == self::$class){
			self::$class = new self();
		}
		
		return self::$class;
	}
	
	public function image()
	{
		
		new Image();
	}
}

Init::loadPlugin()->image();