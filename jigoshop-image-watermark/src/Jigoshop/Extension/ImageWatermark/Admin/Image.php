<?php

namespace Jigoshop\Extension\ImageWatermark\Admin;

use Jigoshop\Extension\ImageWatermark\Template\Output\Output;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration;
use Jigoshop\Integration\Render;
use Slim\Router;

class Image
{
	const WATERMARK_NONCE = 'watermark-nonce';
	const WATERMARK_OPTIONS = 'watermark_options';
	
	/**
	 * Image constructor.
	 */
	public function __construct()
	{
		add_action('admin_menu', [$this, 'jiwMenu']);
		add_action('admin_init', [$this, 'settings']);
		
		//add_action('wp_ajax_watermarkAction', [$this, 'saveSettings']);
		add_action('wp_ajax_removeImage', [$this, 'removeImage']);
		add_action('wp_ajax_applyToAll', [$this, 'applyToAll']);
		add_filter('wp_generate_attachment_metadata', array($this, 'applyWatermark'), 10, 2);
	}
	
	/**
	 *
	 */
	public function applyToAll()
	{
		if(isset($_POST['applyToAllImages']) && $_POST['applyToAllImages'] == 'yes'){
			$this->applyWatermarkToAllImagesInMediaLibrary();
		}
		
		die();
	}
	
	/**
	 *
	 */
	public function removeImage()
	{
		if (!empty($_POST) && isset($_POST['imageOption']) && '' != $_POST['imageOption']) {
			if (!wp_verify_nonce($_POST['nonce'], self::WATERMARK_NONCE)) {
				$response = [
					'response' => 'error',
					'message' => '
									<div class="error error-message">
									<p>' . __('Something went wrong and image could not be deleted dynamically',
							'watermark') . '</p>
									</div>'
				];
				die(json_encode($response));
			} else {
				$response = [
					'response' => 'success',
					'message' => '
										<div class="jigoshop updated">
											<p>' . __('Image was deleted', 'watermark') . '</p>
										</div>'
				];
				
				update_option(esc_attr($_POST['imageOption']), esc_attr($_POST['watermarkImage']));
				die(json_encode($response));
			}
		}
		
		die();
	}
	
	/**
	 *
	 */
	public function saveSettings()
	{
		$response = null;
		if (isset($_POST['formData']) && !empty($_POST['formData']) && $_POST['formData']) {
			parse_str($_POST['formData'], $postData);
			$watermarkOptions = $this->getOption(self::WATERMARK_OPTIONS);
			if (!wp_verify_nonce($postData['watermark-nonce'], self::WATERMARK_NONCE)) {
				$response = [
					'response' => 'error',
					'message' => '
									<div class="error error-message">
									<p>' . __('Something went wrong and settings could not be saved, please try again
																							later', 'watermark') . '</p>
									</div>'
				];
				
				die(json_encode($response));
			}
			if (!empty($watermarkOptions) && is_array($watermarkOptions)) {
				foreach ($watermarkOptions as $options => $values) {
					foreach ($values as $value) {
						foreach ($postData as $data => $_value) {
							if ($value['type'] == 'checkbox') {
								// Do Something
							}
							if ($value['id'] == $data) {
								update_option($value['id'], $_value);
								$response = [
									'response' => 'success',
									'id' => [
										'val' => $values
									],
									'message' => '
										<div class="jigoshop updated">
											<p>' . __('Settings have been saved', 'watermark') . '</p>
										</div>'
								];
							}
						}
					}
				}
			}
			
			die(json_encode($response));
		}
	}
	
	/**
	 * @link to icon //https://www.iconfinder.com/webalys
	 */
	public function jiwMenu()
	{
		$page = add_menu_page(
			__('Image Watermark', 'image_watermark'),
			__('Image Watermark', 'image_watermark'),
			'manage_options', 'watermark-settings',
			[$this, 'display'], JIGOSHOP_IMAGE_WATERMARK_URL . '/assets/images/stamp.png',
			20
		);
		
		add_action('admin_print_scripts-' . $page, [$this, 'jiwScripts']);
	}
	
	/**
	 * @output options page
	 */
	public function display()
	{
		if (isset($_GET['settings-updated'])) {
			add_settings_error('watermark', 'watermarkMessage', __('Settings Saved', 'watermark'),
				'updated');
		}
		Output::output('admin/options', [
		//	'gif' => JIGOSHOP_IMAGE_WATERMARK_URL . '/assets/images/loader.gif',
			'saved' => settings_errors('watermark'),
		]);
	}
	
	/**
	 * Options
	 */
	public function settings()
	{
		add_settings_section(
			'watermark-settings',
			__('', 'watermark'),
			[$this, 'watermarkSettingsTitle'],
			'watermark-settings'
		);
		add_settings_field(
			'main-watermark-settings',
			__('Watermark', 'watermark'),
			[$this, 'watermarkSettings'],
			'watermark-settings',
			'watermark-settings',
			[
				'sections' => [
					[
						'id' => 'enable-watermark',
						'name' => 'enable-watermark',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('enable-watermark') ? 'yes' : 'no'),
						'title' => __('Enable', 'watermark')
					],
					[
						'id' => 'watermark-image',
						'name' => 'watermark-image',
						'type' => 'text',
						'value' => (!empty($this->getOption('watermark-image')) ?
							esc_url($this->getOption('watermark-image')) : ''),
						'description' => __('Enter IMG url or Upload it', 'watermark')
					],
					[
						'id' => 'position-title',
						'name' => 'position-title',
						'type' => 'position-title',
						'title' => __('Choose the Watermark Position', 'watermark')
					],
					[
						'id' => 'top-left',
						'name' => 'top-left',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('top-left') ? 'yes' : 'no'),
						'title' => __('Top Left', 'watermark'),
					],
					[
						'id' => 'top-right',
						'name' => 'top-right',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('top-right') ? 'yes' : 'no'),
						'title' => __('Top Right', 'watermark'),
					],
					[
						'id' => 'bottom-left',
						'name' => 'bottom-left',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('bottom-left') ? 'yes' : 'no'),
						'title' => __('Bottom Left', 'watermark'),
					],
					[
						'id' => 'bottom-right',
						'name' => 'bottom-right',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('bottom-right') ? 'yes' : 'no'),
						'title' => __('Bottom Right', 'watermark'),
					],
					[
						'id' => 'custom-margin-x',
						'name' => 'custom-margin-x',
						'type' => 'number',
						'value' => (!empty($this->getOption('custom-margin-x')) ?
							(int)esc_attr($this->getOption('custom-margin-x')) : ''),
						'description' => __('Enter custom value for margin X (result depends on selected positions)',
							'watermark')
					],
					[
						'id' => 'custom-margin-y',
						'name' => 'custom-margin-y',
						'type' => 'number',
						'value' => (!empty($this->getOption('custom-margin-y')) ?
							(int)esc_attr($this->getOption('custom-margin-y')) : ''),
						'description' => __('Enter custom value for margin Y (result depends on selected positions)',
							'watermark')
					],
					/*[
						'id' => 'enable-for-all-images',
						'name' => 'enable-for-all-images',
						'type' => 'checkbox',
						'value' => 'on',
						'checked' => ('' != $this->getOption('enable-for-all-images') ? 'yes' : 'no'),
						'title' => __('Enable Watermark for all images in WP Media Library', 'watermark')
					],*/
				]
			]
		);
		
		register_setting('watermark-settings', 'enable-watermark');
		register_setting('watermark-settings', 'watermark-image');
		register_setting('watermark-settings', 'top-left');
		register_setting('watermark-settings', 'top-right');
		register_setting('watermark-settings', 'bottom-left');
		register_setting('watermark-settings', 'bottom-right');
		register_setting('watermark-settings', 'custom-margin-x');
		register_setting('watermark-settings', 'custom-margin-y');
	//	register_setting('watermark-settings', 'enable-for-all-images');
	}
	
	public function watermarkSettingsTitle()
	{
		echo '<h1>' . __('Watermark Settings', 'watermark') . '</h1>';
	}
	
	/**
	 * @param $args
	 */
	public function watermarkSettings($args)
	{
		$html = '';
		if (!empty($args['sections'])) {
			// Cza tu zrobic w inny sposob, a jak pozycje tablicy sie zmieny to co wtedy a?
			$selectedPositions = [
				0 => $args['sections'][3]['checked'],
				1 => $args['sections'][4]['checked'],
				2 => $args['sections'][5]['checked'],
				3 => $args['sections'][6]['checked'],
			];
			$checked = array_count_values($selectedPositions);
			$nonSelected = false;
			$wrongFormat = false;
			$isChecked = 'no';
			if (isset($checked['yes'])) {
				$isChecked = $checked['yes'];
			}
			if ($isChecked > 1) {
				$nonSelected = true;
				add_settings_error('watermark-position', 'watermarkPositionErrorMessage',
					__('You can not select more then one position at same time!', 'watermark'),
					'error');
				esc_html(settings_errors('watermark-position'));
			}
			$supportedFormat = ['png'];
			$png = pathinfo(esc_url($this->getOption('watermark-image')), PATHINFO_EXTENSION);
			
			if (!empty($this->getOption('watermark-image')) && '' != $this->getOption('watermark-image')) {
				if (!in_array(strtolower($png), $supportedFormat)) {
					add_settings_error('png-format', 'pngFormat',
						__('The image should be in "png" format', 'watermark'),
						'error');
					esc_html(settings_errors('png-format'));
					$wrongFormat = true;
				}
			}
			if (!empty($this->getOption('custom-margin-x')) || !empty($this->getOption('custom-margin-y'))) {
				if (!ctype_digit($this->getOption('custom-margin-x')) ||
					!ctype_digit($this->getOption('custom-margin-y'))) {
					add_settings_error('watermark-not-digit', 'watermarkNotDigitErrorMessage',
						__('Custom value is invalid! It has set to 0', 'watermark'),
						'error');
					esc_html(settings_errors('watermark-not-digit'));
				}
			}
			
			foreach ($args['sections'] as $section => $value) {
				$html .= Output::get('admin/formFields', [
					'value' => $value,
					'that' => $this,
					'selected' => $nonSelected,
					'wrongFormat' => $wrongFormat,
					'gif' => JIGOSHOP_IMAGE_WATERMARK_URL . '/assets/images/loader.gif',
				]);
			}
			$wOptions = [
				'positions' => [
					'top-left' => $this->getOption('top-left'),
					'top-right' => $this->getOption('top-right'),
					'bottom-left' => $this->getOption('bottom-left'),
					'bottom-right' => $this->getOption('bottom-right'),
					'values' => $selectedPositions,
					'enable' => $args['sections'][0]['checked']
				],
			];
			
			$merge = array_merge($wOptions, $args);
			$this->updateOption($merge);
		}
		echo $html;
	}
	
	/**
	 * @param $data
	 * @param $attachmentId
	 * @return mixed
	 */
	public function applyWatermark($data, $attachmentId)
	{
		if (!empty($this->getOption('watermark_options'))) {
			$enabled = $this->getOption('watermark_options')['sections'][0]['checked'];
			if ($enabled == 'no') {
				return;
			}
			
			if (!empty($this->getOption('watermark-image'))) {
				$valueX = (!empty($this->getOption('custom-margin-x')) && $this->getOption('custom-margin-x') > 0
					? $this->getOption('custom-margin-x') : 0);
				$valueY = (!empty($this->getOption('custom-margin-y')) && $this->getOption('custom-margin-y') > 0
					? $this->getOption('custom-margin-y') : 0);
				$watermark = pathinfo($this->getOption('watermark-image'), PATHINFO_BASENAME);
				$supportedJpgFormat = ['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG'];
				
				$jpg = pathinfo($data['file'], PATHINFO_EXTENSION);
				
				if (in_array($jpg, $supportedJpgFormat)) {
					$dir = wp_upload_dir();
					
					$watermarkImageToApply = imagecreatefrompng($this->getOption('watermark-image'));
					if (is_resource($watermarkImageToApply)) {
						$wImageWidth = imagesx($watermarkImageToApply);
						$wImageHeight = imagesy($watermarkImageToApply);
					}
					$images = array_diff(scandir($dir['path']), ['..', '.']);
					
					$watermarkName = pathinfo($data['file'], PATHINFO_BASENAME);
					
					foreach ($images as $image) {
						// To chyba zatrzyma watermark na watermak? Czy nie?
						if ($image == $watermark) {
							continue;
						}
						if ($watermarkName == $image) {
							$imToApplyWatermark = $this->getImage($dir['path'] . '/' . $watermarkName);
							if (is_resource($imToApplyWatermark)) {
								$imageWidth = imagesx($imToApplyWatermark);
								$imageHeight = imagesy($imToApplyWatermark);
							}
							$merge = $this->getOption('watermark_options');
							$checked = array_count_values($merge['positions']['values']);
							$nonSelected = false;
							
							$isChecked = 'no';
							if (isset($checked['yes'])) {
								$isChecked = $checked['yes'];
							}
							if ($isChecked > 1) {
								$nonSelected = true;
							}
							
							foreach ($merge['positions'] as $selectedPosition => $on) {
								if ($on == 'on' && true !== $nonSelected) {
									$this->calculateAndAddWatermarkToSingleImage(
										$selectedPosition, $imToApplyWatermark, $watermarkImageToApply, $wImageWidth,
										$wImageHeight, $dir, $watermarkName, $imageWidth, $imageHeight, $valueX, $valueY);
								}
							}
							imagedestroy($imToApplyWatermark);
							imagedestroy($watermarkImageToApply);
						}
					}
				}
				
			}
		}
		
		return $data;
	}
	
	public function applyWatermarkToAllImagesInMediaLibrary()
	{
		
		$images = [];
		if (!empty($this->getOption('watermark_options'))) {
			$enabled = $this->getOption('watermark_options')['sections'][0]['checked'];
			if ($enabled == 'no') {
				return;
			}
			if (!empty($this->getOption('watermark-image'))) {
				$valueX = (!empty($this->getOption('custom-margin-x')) && $this->getOption('custom-margin-x') > 0
					? $this->getOption('custom-margin-x') : 0);
				$valueY = (!empty($this->getOption('custom-margin-y')) && $this->getOption('custom-margin-y') > 0
					? $this->getOption('custom-margin-y') : 0);
				$watermark = pathinfo($this->getOption('watermark-image'), PATHINFO_BASENAME);
				
				$dir = wp_upload_dir();
				$images = $this->getAllImgs($this->getAllDirs($dir['basedir'], DIRECTORY_SEPARATOR));
				$dirs = $this->getAllDirs($dir['basedir'], DIRECTORY_SEPARATOR);
				$supportedJpgFormat = ['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG'];
				foreach ($images as $image) {
					$jpgPng = pathinfo($image, PATHINFO_EXTENSION);
					$watermarkName = pathinfo($image, PATHINFO_BASENAME);
					
					if (in_array($jpgPng, $supportedJpgFormat)) {
						
						$watermarkImageToApply = imagecreatefrompng($this->getOption('watermark-image'));
						
						if (is_resource($watermarkImageToApply)) {
							$wImageWidth = imagesx($watermarkImageToApply);
							$wImageHeight = imagesy($watermarkImageToApply);
						}
						
						
						// To chyba zatrzyma watermark na watermak? Czy nie?
						if ($watermarkName == $watermark) {
							continue;
						}
						
						$imToApplyWatermark = $this->getImage($image);
						
						if (is_resource($imToApplyWatermark)) {
							$imageWidth = imagesx($imToApplyWatermark);
							$imageHeight = imagesy($imToApplyWatermark);
						}
						
						$merge = $this->getOption('watermark_options');
						$checked = array_count_values($merge['positions']['values']);
						$nonSelected = false;
						
						$isChecked = 'no';
						if (isset($checked['yes'])) {
							$isChecked = $checked['yes'];
						}
						if ($isChecked > 1) {
							$nonSelected = true;
						}
						
						foreach ($merge['positions'] as $selectedPosition => $on) {
							if ($on == 'on' && true !== $nonSelected) {
								foreach ($dirs as $imageDir) {
									$this->calculateAndAddWatermarkToAllImages(
										$selectedPosition, $imToApplyWatermark, $watermarkImageToApply, $wImageWidth,
										$wImageHeight, $imageDir, $watermarkName, $imageWidth, $imageHeight, $valueX, $valueY);
									
								}
							}
						}
						if (is_resource($imToApplyWatermark)) {
							imagedestroy($imToApplyWatermark);
						}
						if (is_resource($watermarkImageToApply)) {
							imagedestroy($watermarkImageToApply);
						}
					}
				}
				
			}
		}
		
		return $images;
	}
	
	public function getImage($path)
	{
		switch (mime_content_type($path)) {
			case 'image/png'    :
			case 'image/PNG'    :
				$img = imagecreatefrompng($path);
				break;
			case 'image/gif':
			case 'image/GIF':
				$img = imagecreatefromgif($path);
				break;
			case 'image/jpeg'   :
			case 'image/jpg'    :
			case 'image/JPEG'   :
			case 'image/JPG'    :
				$img = imagecreatefromjpeg($path);
				break;
			case 'image/bmp':
				$img = imagecreatefromwbmp($path);
				break;
			default:
				$img = null;
		}
		return $img;
	}
	
	/**
	 * @param $directory
	 * @param $dirSep
	 * @return array
	 */
	public function getAllDirs($directory, $dirSep)
	{
		
		$dirs = array_map(function ($item) use ($dirSep) {
			return $item . $dirSep;
		}, array_filter(glob($directory . '*'), 'is_dir'));
		
		foreach ($dirs as $dir) {
			$dirs = array_merge($dirs, $this->getAllDirs($dir, $dirSep));
		}
		return $dirs;
	}
	
	
	/**
	 * @param $directory
	 * @return array
	 */
	public function getAllImgs($directory)
	{
		$resizedFilePath = [];
		foreach ($directory AS $dir) {
			
			foreach (glob($dir . '*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE) as $filename) {
				
				array_push($resizedFilePath, $filename);
				
			}
			
		}
		return $resizedFilePath;
	}
	
	/**
	 * @param $position
	 * @param $imToApplyWatermark
	 * @param $watermarkImageToApply
	 * @param $wImageWidth
	 * @param $wImageHeight
	 * @param $dir wp_upload_dir()
	 * @param $watermarkName
	 * @param $imageWidth
	 * @param $imageHeight
	 * @param $valueX
	 * @param $valueY
	 */
	private function calculateAndAddWatermarkToSingleImage($position, $imToApplyWatermark, $watermarkImageToApply, $wImageWidth,
	                                            $wImageHeight, $dir, $watermarkName, $imageWidth, $imageHeight, $valueX,
	                                            $valueY)
	{
		switch ($position) {
			case 'top-left' :
				$x = $valueX;
				$y = $valueY;
				break;
			case 'top-right' :
				$x = $imageWidth - $wImageWidth - $valueX;
				$y = $valueY;
				break;
			case 'bottom-left' :
				$x = $valueX;
				$y = $imageHeight - $wImageHeight - $valueY;
				break;
			case 'bottom-right' :
			default :
				$x = $imageWidth - $wImageWidth - $valueX;
				$y = $imageHeight - $wImageHeight - $valueY;
				break;
		}
		
		if (is_resource($imToApplyWatermark)) {
			if (is_resource($watermarkImageToApply)) {
				imagecopy($imToApplyWatermark, $watermarkImageToApply, $x, $y, 0, 0,
					$wImageWidth, $wImageHeight);
			}
			
		}
		if (is_resource($imToApplyWatermark)) {
			imagejpeg($imToApplyWatermark, $dir['path'] . DIRECTORY_SEPARATOR . $watermarkName, 100);
		}
	}
	
	private function calculateAndAddWatermarkToAllImages($position, $imToApplyWatermark, $watermarkImageToApply, $wImageWidth,
	                                             $wImageHeight, $dir, $watermarkName, $imageWidth, $imageHeight, $valueX,
	                                             $valueY)
	{
		switch ($position) {
			case 'top-left' :
				$x = $valueX;
				$y = $valueY;
				break;
			case 'top-right' :
				$x = $imageWidth - $wImageWidth - $valueX;
				$y = $valueY;
				break;
			case 'bottom-left' :
				$x = $valueX;
				$y = $imageHeight - $wImageHeight - $valueY;
				break;
			case 'bottom-right' :
			default :
				$x = $imageWidth - $wImageWidth - $valueX;
				$y = $imageHeight - $wImageHeight - $valueY;
				break;
		}
		if (is_resource($imToApplyWatermark)) {
			if (is_resource($watermarkImageToApply)) {
				
				imagecopy($imToApplyWatermark, $watermarkImageToApply, $x, $y, 0, 0,
					$wImageWidth, $wImageHeight);
			}
			
		}
		
		if (is_resource($imToApplyWatermark)) {
			imagejpeg($imToApplyWatermark, $dir . DIRECTORY_SEPARATOR . $watermarkName, 100);
		}
		
	}
	
	/**
	 * @style unt @Script
	 *
	 */
	public function jiwScripts()
	{
		if (!is_admin()) {
			return;
		}
		Styles::add('jigoshop.styles.new', JIGOSHOP_IMAGE_WATERMARK_URL . '/assets/css/admin-style.css');
		Styles::add('jigoshop.vendors.bs_switch', \JigoshopInit::getUrl() . '/assets/css/vendors/bs_switch.css');
		Scripts::add('jigoshop.vendors.bs_switch', \JigoshopInit::getUrl() . '/assets/js/vendors/bs_switch.js');
		
		Scripts::add('image.watermark.script', JIGOSHOP_IMAGE_WATERMARK_URL . '/assets/js/admin-script.js');
		$uploadDir = wp_upload_dir();
		Scripts::localize('image.watermark.script', 'watermark', [
			'nonce' => wp_create_nonce(self::WATERMARK_NONCE),
			'img' => $uploadDir['baseurl'],
			'image' => 'watermark-image',
			// Apply to all
			'confirm' => __('This will apply Watermark to all images in Media Library, do you want to do this?', 'watermark'),
			'hasApplied' => __('Watermark has applied to all images', 'watermark')
		]);
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');
		
		if (!did_action('wp_enqueue_media')) {
			wp_enqueue_media();
		}
	}
	
	/**
	 * @param $name
	 * @return mixed|string
	 */
	public function getOption($name)
	{
		$option = '';
		if (!empty($name)) {
			$option = get_option($name);
		}
		
		return $option;
	}
	
	/**
	 * @param $args
	 * @return array|bool
	 */
	public function updateOption($args)
	{
		$options = [];
		
		if (!empty($args) && is_array($args)) {
			$options = update_option(self::WATERMARK_OPTIONS, $args);
		}
		
		return $options;
	}
}