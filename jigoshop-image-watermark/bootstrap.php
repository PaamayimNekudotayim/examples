<?php
/**
 * Plugin Name: Jigoshop Image Watermark
 * Plugin URI: https://jigoshop.com/product/jigoshop-image-watermark
 * Description: Not Yet Available
 * Version: 1.0
 * Author: @vasilguruli | JigoLtd
 */
// Define plugin name
define('JIGOSHOP_IMAGE_WATERMARK_NAME', 'Jigoshop Image Watermark');
add_action('plugins_loaded', function () {
	load_plugin_textdomain('image_watermark', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		// Define plugin directory for inclusions
		define('JIGOSHOP_IMAGE_WATERMARK_DIR', dirname(__FILE__));
		// Define plugin URL for assets
		define('JIGOSHOP_IMAGE_WATERMARK_URL', plugins_url('', __FILE__));
		//Check version.
		if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_IMAGE_WATERMARK_NAME, '2.0')) {
			return;
		}
		//Check license.
		/**
		 * Code is missing as it is not necessary here at this point
		 */
		//Init components.
		require_once(JIGOSHOP_IMAGE_WATERMARK_DIR . '/src/Jigoshop/Extension/ImageWatermark/Init.php');
		if (is_admin()) {
			require_once(JIGOSHOP_IMAGE_WATERMARK_DIR . '/src/Jigoshop/Extension/ImageWatermark/Admin.php');
		}
	}else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.', 'jigoshop_new_product_badge'),
				JIGOSHOP_IMAGE_WATERMARK_NAME, JIGOSHOP_IMAGE_WATERMARK_NAME);
			echo '</p></div>';
		});
	}
});
