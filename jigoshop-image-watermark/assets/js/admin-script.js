$ = jQuery.noConflict();
$(function($){
    return $("input[type=checkbox].switch-medium").bootstrapSwitch({
        size: "small",
        onText: "Yes",
        offText: "No"
    }), $("input[type=checkbox].switch-mini").bootstrapSwitch({
        size: "mini",
        onText: "Yes",
        offText: "No"
    }),
        $("input[type=checkbox].switch-normal").bootstrapSwitch({size: "normal", onText: "Yes", offText: "No"});
});

$(document).ready(function($){
    var uploadID = '';
    $('#upload-image-button').click(function() {
        uploadID = $(this).prev('input');
        formfield = $('.shinned').attr('name');
        uploadWatermark();
        return false;
    });
    function uploadWatermark() {
        wpMedia = wp.media({
            multiple: false,
            library: {
                type: 'image'
            }
        }).on('close', function(e) {
            image = wpMedia.state().get('selection').first();
            if(image === undefined) {
                return;
            }
            uploadID.val(image.changed.url);
        }).open();
    }
    $('.remove-image').on('click', function (event) {
        event.preventDefault();
        $('.shinned').attr('value','');
        $.ajax({
            type: 'POST',
            data: {
                watermarkImageValue : $('.empty').val(),
                nonce : watermark.nonce,
                imageOption : watermark.image,
                action: 'removeImage'
            },
            url: ajaxurl,
            success: function(res){
                var result = jQuery.parseJSON(res);
                if (result.response === 'success') {
                    $('#w-text').html(result.message);
                    setTimeout(function () {
                        $('#w-text').fadeOut(3000);
                    });
                    $('.remove-image').hide();
                    $('.apply-to-all').hide();
                }else{
                    $('#w-text').html(result.message);
                    setTimeout(function () {
                        $('#w-text').fadeOut(3000);
                    });
                }
            }
        });
        return false;
    });
    $('.apply-to-all').on('click', function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            data: {
                applyToAllImages: 'yes',
                action: 'applyToAll'
            },
            url: ajaxurl,
            beforeSend: function(){
                var conf = confirm(watermark.confirm);
                if(conf == true){
                    $('.apply-to-all').attr('disabled', 'disabled');
                    $('#w-loader').show();
                }else{
                    return false;
                }
            },
            success: function(res){
                $('.apply-to-all').removeAttr('disabled', 'disabled');
                $('#w-loader').hide();
                alert(watermark.hasApplied);
            }
        });
        return false;
    });
});
// Na razie nie chce mi sie bawic z zapisiwaniem checkbox'ow z Ajaxem bo jak dodajesz jako Menu Page a nie theme page
// to inaczej cza zrobic.
// Czyli mysle ze trzeba wyjebac "register_settings" i dodac to jako osobny page bez tych register'ow i settings_section'ow.
// Dokladnie tak jak maja zrobione w aggregat'u
// Cza sie przyznac ze Ajax'em to duzo lepiej wyglada though!
/*$(document).ready(function ($) {
    $('#jiw-form').submit(function () {
        var _form = $(this).serialize();
        var nonce = '&watermark-nonce=' + watermark.nonce;
        _form += nonce;
        $.ajax({
            type: "POST",
            data: {
                formData: _form,
                action: 'watermarkAction'
            },
            url: ajaxurl,
            beforeSend: function (res) {
                $('#w-text').fadeIn(3000);
                $('#w-loader').fadeIn()
            },
            success: function (res) {
                var result = jQuery.parseJSON(res);
                console.log(res);
                $('#w-loader').fadeOut(300, function () {
                    if (result.response === 'success') {
                        $('#w-text').html(result.message);
                        setTimeout(function () {
                            $('#w-text').fadeOut(3000);
                        });
                    }else{
                        $('#w-text').html(result.message);
                        setTimeout(function () {
                            $('#w-text').fadeOut(3000);
                        });
                    }
                });
            },
            error: function (err) {
                console.log(err)
            }


        });
        return false;
    });
});*/
