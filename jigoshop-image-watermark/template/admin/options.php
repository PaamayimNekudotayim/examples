<?php
/**
 * @var $gif string
 * @var $saved string
 */
?>
<?php esc_html($saved); ?>
<div id="w-text"></div>
<div class="wrap jigoshop">
	<form id="jiw-form" method="post" action="options.php" enctype="multipart/form-data" role="form" class="clearfix">
        <table class="form-table">
            <tbody>
                <?php
                    do_settings_sections('watermark-settings');
                    settings_fields('watermark-settings');
                ?>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="submit" id="submit ol" class="button pla button-primary" value="Save Changes">
           <!-- <span id="w-loader"><img src="<?php /*echo esc_url($gif) */?>" /></span>-->
        </p>
	</form>
</div>