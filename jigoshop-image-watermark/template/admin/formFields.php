<?php
/**
 * @var $value array
 * @var $that \Jigoshop\Extension\ImageWatermark\Admin\Image
 * @var $selected bool
 * @var $wrongFormat bool
 * @var $notDigit bool
 * @var $gif string
 */
?>
<div id="w-text"></div>
<?php
switch ($value['type']) {
	case 'checkbox' :
		$on = esc_attr($that->getOption($value['id']));
		$checked = ('on' == $on ? 'checked="checked"' : '');
		if (true == $selected && $value['id'] != 'enable-watermark') {
			$checked = '';
        }
		?>
        <tr>
            <th scope="row">
                <label class=""
                       for="<?php echo esc_attr($value['id']) ?>"><?php echo esc_attr($value['title']); ?></label>
            </th>
            <td>
                <div class="form-group odstep">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-xs-12 col-sm-12 clearfix">
                                <div class="tooltip-inline-input">
                                    <input class="switch-medium <?php echo esc_attr($value['name']) ?>" type="<?php echo esc_attr($value['type']); ?>"
                                           id="<?php echo esc_attr($value['id']); ?>"
                                           name="<?php echo esc_attr($value['id']); ?>"
                                           value="<?php echo esc_attr($value['value']); ?>"<?php echo esc_attr($checked); ?>/>
                                    <div class="tooltip-inline-badge"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
		<?php
		break;
	case 'text' :
	    $img = true == $wrongFormat ? '' : esc_url($that->getOption($value['name']));
		?>
        <tr>
            <th scope="row">
                <label class=""
                       for="<?php echo $value['id']; ?> upload-image"><?php echo $value['description']; ?></label>
            </th>
            <td>
                <div class="form-group odstep">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-xs-12 col-sm-12 clearfix">
                                <div class="tooltip-inline-badge"></div>
                                <div class="tooltip-inline-input">
                                    <input class="form-control shinned empty"
                                           type="<?php echo esc_attr($value['type']); ?>"
                                           id="<?php echo esc_attr($value['id']); ?> upload-image"
                                           name="<?php echo esc_attr($value['id']); ?>"
                                           value="<?php echo esc_url($img); ?>"/>
                                    <input type="button" id="upload-image-button" class="upload-image-button button"
                                           name="upload_watermark"
                                           value="<?php echo __('Upload Image', 'watermark'); ?>"/>
                                    <input class="remove-image button" type="button" value="Remove Image"
                                           style="display:<?php echo(empty(esc_attr(get_option($value['name']))) ?
                                               'none' : 'block'); ?>"/>
                                    <input class="apply-to-all button" type="button" value="Apply to all images"
                                           style="display:<?php echo(empty(esc_attr(get_option($value['name']))) ?
                                               'none' : 'block'); ?>"/>
                                    <div class=" loader-cont">
                                        <span id="w-loader"><img src="<?php echo esc_url($gif) ?>" /></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
		<?php
		break;
	case 'position-title' :
		?>
        <tr>
            <th scope="row">
                <h3><?php echo __('Choose watermark position', 'watermark') ?></h3>
            </th>
        </tr>
		<?php
		break;
    case 'number' :
        if(!empty($that->getOption($value['name'])) && '' != $that->getOption($value['name'])){
	        $custom = !ctype_digit($that->getOption($value['name'])) ? '' : $that->getOption($value['name']);
        }
        ?>
        <tr>
            <th scope="row">
                <label class=""
                       for="<?php echo $value['id']; ?>"><?php echo $value['description']; ?></label>
            </th>
            <td>
                <div class="form-group odstep">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-xs-12 col-sm-12 clearfix">
                                <div class="tooltip-inline-badge"></div>
                                <div class="tooltip-inline-input">
                                    <input class="form-control w-number"
                                           type="<?php echo esc_attr($value['type']); ?>"
                                           id="<?php echo esc_attr($value['id']); ?>"
                                           name="<?php echo esc_attr($value['id']); ?>"
                                           value="<?php echo (int)esc_attr($custom) ; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
<?php
        break;
	default :
		echo '';
		break;
}
?>
