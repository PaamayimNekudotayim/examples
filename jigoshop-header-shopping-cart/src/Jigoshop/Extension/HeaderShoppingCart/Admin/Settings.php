<?php

namespace Jigoshop\Extension\HeaderShoppingCart\Admin;


use Jigoshop\Admin\Helper\Forms;
use Jigoshop\Admin\Settings\TabInterface;
use Jigoshop\Admin\Settings\ValidationException;
use Jigoshop\Extension\HeaderShoppingCart\Init;
use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;

class Settings implements TabInterface
{
	const SLUG = 'jigoshop_hsc';
	/**@var $hscOptions array */
	private $hscOptions;
	/**@var $instance Settings */
	private static $instance;
	
	private $init;
	
	private function __construct()
	{
		$this->init = new Init();
		$this->init->setOptions($this->hscOptions);
		add_filter('jigoshop\admin\settings', [$this, 'addSettingsTab']);
		add_action('init', function () {
			$this->hscOptions = $this->defaultOptions();
		}, 99);
	}
	
	public function addSettingsTab($tabs)
	{
		$tabs[] = $this;
		
		return $tabs;
	}
	
	public static function getSettings()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	/**
	 * @return array
	 */
	public function defaultOptions()
	{
		return array_merge($this->init->defaultSettings(), Integration::getOptions()->get($this->getSlug(), []));
	}
	
	
	/**
	 * @return string Title of the tab.
	 */
	public function getTitle()
	{
		return __('Shopping Cart', 'jigoshop_hsc');
	}
	
	/**
	 * @return string Tab slug.
	 */
	public function getSlug()
	{
		return self::SLUG;
	}
	
	/**
	 * @return array List of items to display.
	 */
	public function getSections()
	{
		return [
			[
				'title' => __('Shopping Cart', 'jigoshop_hsc'),
				'id' => 'hsc_enabled',
				'description' => __('Select the options for Shopping Cart', 'jigoshop_hsc'),
				'fields' => [
					[
						'name' => '[enable]',
						'title' => __('Enable', 'jigoshop_hsc'),
						'type' => 'checkbox',
						'checked' => $this->hscOptions['enable'],
						'classes' => ['switch-medium'],
					],
					[
						'name' => '[selectMenu]',
						'title' => __('Select menu', 'jigoshop_hsc'),
						'description' => __('Select the menu in which you want to display the cart', 'jigoshop_hsc'),
						'type' => 'select',
						'options' => $this->selectMenu(),
						'value' => $this->hscOptions['selectMenu'],
					],
					[
						'name' => '[displayCartIcon]',
						'title' => __('Display Cart Icon', 'jigoshop_hsc'),
						'description' => __('Set it to "Yes" if you would like to display cart icon', 'jigoshop_hsc'),
						'type' => 'checkbox',
						'checked' => $this->hscOptions['displayCartIcon'],
						'classes' => ['switch-medium'],
					],
					[
						'name' => '[showEmpty]',
						'title' => __('Display Cart even if it\'s empty', 'jigoshop_hsc'),
						'type' => 'checkbox',
						'checked' => $this->hscOptions['showEmpty'],
						'classes' => ['switch-medium'],
					],
					[
						'name' => '[cartText]',
						'title' => __('Text on cart', 'jigoshop_hsc'),
						'type' => 'text',
						'description' => __('Set the text on the cart when cart icon is not set', 'jigoshop_hsc'),
						'value' => $this->hscOptions['cartText'],
					],
					[
						'name' => '[cartIcon]',
						'id' => '[cartIcon]',
						'title' => __('Choose cart icon', 'jigoshop_hsc'),
						'tip' => '',
						'type' => 'user_defined',
						'display' => [$this, 'renderIcon'],
						'value' => $this->hscOptions['cartIcon'],
					],
				],
			],
		];
	}
	
	/**
	 * Validate and sanitize input values.
	 *
	 * @param array $settings Input fields.
	 *
	 * @return array Sanitized and validated output.
	 * @throws ValidationException When some items are not valid.
	 */
	public function validate($settings)
	{
		return $this->init->validateSettings($settings);
	}
	
	/**
	 * @return array
	 */
	private function selectMenu()
	{
		$adminMenus = get_terms(
			'nav_menu',
			[
				'hide_empty' => false,
			]
		);
		
		$menus = [];
		if (!empty($adminMenus)) {
			foreach ($adminMenus as $navMenu) {
				$menus[$navMenu->slug] = $navMenu->name;
			}
		}
		
		return $menus;
	}
	
	private function cartIcon($cartIcons)
	{
		$icons = array();
		
		if (is_array($cartIcons)) {
			foreach ($cartIcons as $key => $card) {
				$icons[] = JIGOSHOP_SHOPPING_CART_HEADER_URL . '/assets/images/cartIcons/' . $card . '.png';
			}
		}
		
		return $icons;
	}
	
	private function icons()
	{
		return [
			0 => 'cartIcon1',
			1 => 'cartIcon2',
			2 => 'cartIcon3',
			3 => 'cartIcon4',
		];
	}
	
	/**
	 * @param $field
	 */
	public function renderIcon($field)
	{
		$icons = $this->cartIcon($this->icons());
		$checked = false;
		$img = null;
		add_option('cartIconOptions');
		for ($i = 0; $i < count($icons); $i++) {
			if (isset($this->hscOptions['cartIcon']) || $this->hscOptions['cartIcon'] == '0' && $this->hscOptions['cartIcon']) {
				if (isset($field['value'][$i])) {
					foreach ($icons as $key => $icon) {
						if ($field['value'][$i] == $key) {
							$img = $icon;
							$checked = true;
						}
					}
				} else {
					$checked = false;
				}
				if ($checked == true) {
					$args = [
						'id' => $field['value'][$i],
						'value' => $checked,
						'imgUrl' => esc_url($img),
					];
					
					update_option('cartIconOptions', $args);
				}
				
			}
		}
		
		Render::output('jigoshop_hsc', 'admin/icons', [
			'icons' => $icons,
			'checked' => $checked,
			'name' => 'jigoshop[cartIcon]',
			'value' => $field['value'],
		]);
	}
	
	/**
	 * @return array|bool
	 */
	public function getHscOptions()
	{
		if (!is_array($this->hscOptions) && $this->isEmpty($this->hscOptions)) {
			return false;
		}
		
		return $this->hscOptions;
	}
	
	private function isEmpty($options)
	{
		if (!empty($options)) {
			return true;
		}
		
		return false;
	}
}