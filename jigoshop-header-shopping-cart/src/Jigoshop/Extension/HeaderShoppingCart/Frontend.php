<?php

namespace Jigoshop\Extension\HeaderShoppingCart;


use Jigoshop\Core\Options;
use Jigoshop\Entity\Product\Variable;
use Jigoshop\Extension\HeaderShoppingCart\Admin\Settings;
use Jigoshop\Frontend\Pages;
use Jigoshop\Helper\Product;
use Jigoshop\Helper\Scripts;
use Jigoshop\Helper\Styles;
use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;

class Frontend
{
	private $settings;
	private $cartService;
	
	public function __construct()
	{
		$this->settings = get_option('jigoshop')[Settings::SLUG] ?? [];
		add_action('init', [$this, 'initHscMenu'], 9999);
		add_action('wp_enqueue_scripts', [$this, 'frontStyle']);
	}
	
	
	public function frontStyle()
	{
		Styles::add(
			'jigoshop.frontend.hsc_style', JIGOSHOP_SHOPPING_CART_HEADER_URL . '/assets/css/fontend.css'
		);
	}
	
	public function initHscMenu()
	{
		$menu = $this->settings['selectMenu'][0];
		
		if (!empty($menu)) {
			add_filter('wp_nav_menu_' . $menu . '_items', [$this, 'hscMenu'], 10, 2);
		}
	}
	
	/**
	 * @param $items
	 * @return string
	 */
	public function hscMenu($items)
	{
		$liItems = $this->renderMenu();
		
		$menu = '';
		if (null !== $liItems) {
			$menu .= $items . $liItems;
		} else {
			$menu .= $items;
		}
		
		return $menu;
	}
	
	/**
	 * @return array
	 */
	public function cartItems()
	{
		$qty = 0;
		$price = 0;
		$id = null;
		$name = '';
		$items = [];
		$attachment = null;
		if (!empty(\Jigoshop\Integration::getCart()->getItems())) {
			foreach (\Jigoshop\Integration::getCart()->getItems() as $key => $item) {
				$product = $item->getProduct();
				$attachment = Product::getFeaturedImage($product, 'shop_tiny');
				if ($product instanceof Variable) {
					$variation = $product->getVariation($item->getMeta('variation_id')->getValue());
					foreach ($variation->getAttributes() as $attribute) {
						$price = $variation->getProduct()->getPrice();
						$qty = $item->getQuantity();
						$name = $variation->getProduct()->getName();
					}
				} else {
					$qty = $item->getQuantity();
					$price = $item->getPrice();
					$name = $item->getName();
				}
				$menuItem = [
					'qty' => $qty,
					'price' =>  Product::formatPrice($price),
					'name' => $name,
					'attachment' => $attachment,
					'ID' => $product->getId(),
					'key' => $key,
				];
				$items[] = $menuItem;
			}
		}
		
		return $items;
	}
	
	/**
	 * @return array
	 */
	private function displayHscContent()
	{
		return [
			'cartUrl' => get_permalink(\Jigoshop\Integration::getOptions()->getPageId('cart')),
			'cartContent' => count(\Jigoshop\Integration::getCart()->getItems()),
			'cartTotal' => Product::formatPrice(\Jigoshop\Integration::getCartService()->getCurrent()->getProductSubtotal()),
			'total' => Product::formatPrice(\Jigoshop\Integration::getCartService()->getCurrent()->getTotal()),
		];
	}
	
	/**
	 * @return null|string
	 *
	 * ["ID"]=>
	string(1) "1"
	["imgUrl"]=>
	string(99) "http://dev-jigoshop.php/wp-content/themes/jigoshop-jigoshop-chronous/images/cartIcons/cartIcon1.png"
	["enable"]=>
	string(2) "on"
	["displayCartIcon"]=>
	string(2) "on"
	["displayCartEvenEmpty"]=>
	string(2) "on"
	["selectMenu"]=>
	string(12) "socials-menu"
	["textOnCart"]=>
	string(13) "Items In Cart"
	 */
	public function renderMenu()
	{
		$items = $this->cartItems();
		$hsc = $this->displayHscContent();
		$item = null;
		$productSubtotal = 0;
		$total = 0;
		$text = '';
		$cartUrl = '';
		$enable = false;
		$showEmpty = false;
		$displayCartIcon = false;
		$result = get_option('cartIconOptions');
		$value = false;
		$iconUrl = '';
		
		if (!empty($result)) {
			if (true == $result['value']) {
				$value = $result['value'];
			}
			if (!empty($result['imgUrl'])) {
				$iconUrl = $result['imgUrl'];
			}
		}
		if (isset($hsc['cartUrl'])) {
			$cartUrl = $hsc['cartUrl'];
		}
		if (true == $this->settings['enable']) {
			$enable = $this->settings['enable'];
		}
		if (true == $this->settings['showEmpty']) {
			$showEmpty = $this->settings['showEmpty'];
		}
		if (true == $this->settings['displayCartIcon']) {
			$displayCartIcon = $this->settings['displayCartIcon'];
		}
		if (isset($hsc['cartContent'])) {
			$itemsTotal = $hsc['cartContent'];
		} else {
			$itemsTotal = 0;
		}
		if (isset($hsc['cartTotal'])) {
			if($hsc['cartTotal'] == 'Free'){
				$productSubtotal = Product::formatPrice($productSubtotal);
			}else{
				$productSubtotal = $hsc['cartTotal'];
			}
		} else {
			$productSubtotal = Product::formatPrice($productSubtotal);
		}
		if(isset($hsc['total'])){
			$total = $hsc['total'];
		}else{
			$total = Product::formatPrice($total);
		}
		
		if (!empty($this->settings['cartText'])) {
			$text = $this->settings['cartText'];
		}
		if ($this->settings['enable']) {
			// Dropdown
			$item = Render::get('jigoshop_hsc', 'frontend/items', [
				'items' => $items,
				'enable' => $enable,
				'showEmpty' => $showEmpty,
				'displayCartIcon' => $displayCartIcon,
				'cartUrl' => $cartUrl,
				'value' => $value,
				'iconUrl' => $iconUrl,
				'text' => $text,
				'productSubtotal' => $productSubtotal,
				'itemsTotal' => $itemsTotal,
				'result' => $result,
				'total' => $total,
			]);
		}
		return $item;
	}
}

new Frontend();