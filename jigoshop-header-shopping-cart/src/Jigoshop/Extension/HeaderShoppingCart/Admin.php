<?php

namespace Jigoshop\Extension\HeaderShoppingCart;


use Jigoshop\Extension\HeaderShoppingCart\Admin\Settings;

class Admin
{
	public function __construct()
	{
		add_filter('plugin_action_links_' . plugin_basename(JIGOSHOP_SHOPPING_CART_HEADER_DIR . '/bootstrap.php'), array($this, 'actionLinks'));
		add_action('init', [$this, 'initSettings']);
	}
	
	/**
	 * Settings
	 */
	public function initSettings()
	{
		Settings::getSettings();
	}
	
	/**
	 * Show action links on plugins page.
	 *
	 * @param $links
	 *
	 * @return array
	 */
	public function actionLinks($links)
	{
		$links[] = '<a href="https://wordpress.org/support/plugin/jigoshop-ecommerce" target="_blank">Support</a>';
		$links[] = '<a href="https://wordpress.org/support/view/plugin-reviews/jigoshop-ecommerce#$postform" target="_blank">Rate Us</a>';
		$links[] = '<a href="https://www.jigoshop.com/product-category/extensions/" target="_blank">More plugins for Jigoshop</a>';
		
		return $links;
	}
}
new Admin();