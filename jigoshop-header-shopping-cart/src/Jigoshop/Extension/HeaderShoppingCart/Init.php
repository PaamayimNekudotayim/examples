<?php

namespace Jigoshop\Extension\HeaderShoppingCart;

use Jigoshop\Integration;
use Jigoshop\Integration\Helper\Render;

class Init
{
	private $options = [];
	public function __construct()
	{
		Integration::addPsr4Autoload(__NAMESPACE__ . '\\', __DIR__);
		add_action('init', function(){
			Render::addLocation('jigoshop_hsc', JIGOSHOP_SHOPPING_CART_HEADER_DIR);
		});
		
	}
	
	public function setOptions($options)
	{
		if(!is_array($options))
			$options = [];
		
		$this->options = $options;
	}
	
	/**
	 * @return array
	 */
	public static function defaultSettings()
	{
		return [
			'enable' => false,
			'selectMenu' => [],
			'displayCartIcon' => false,
			'showEmpty' => false,
			'cartIcon' => false,
			'showPrice' => false,
			'cartText' => __('Items', 'jigoshop_hsc')
		];
	}
	
	public function validateSettings($settings)
	{
		$settings['enable'] = $settings['enable'] == 'on';
		$settings['displayCartIcon'] = $settings['displayCartIcon'] == 'on';
		$settings['showEmpty'] = $settings['showEmpty'] == 'on';
		$settings['showPrice'] = $settings['showPrice'] == 'on';
		
		$settings['cartText'] = trim(strip_tags(esc_attr($settings['cartText'])));
		//	$settings['cartIcon'] = $this->validateArray($settings['cartIcon']);
		$settings['selectMenu'] = $this->validateArray($settings['selectMenu']);
		
		return $settings;
	}
	
	/**
	 * @param $array
	 * @return mixed
	 */
	public function validateArray($array)
	{
		if(!is_array($array)){
			$array = (array)$array;
		}
		return filter_var_array($array, FILTER_SANITIZE_STRING);
	}
}

new Init();