<?php
/**
 * @var $icons array
 * @var $name string
 * @var $value string
 */

?>
<form method="post">
	<?php if(!empty($icons)): ?>
        <ul class="cart-icons">
			<?php foreach($icons as $cartIcon => $icon): ?>
                <li class="icon-site">
                    <img src="<?php echo esc_url($icon); ?>" id="<?php echo esc_attr('cartIcon_') . esc_attr($cartIcon) ?>" />
                    <input type="hidden" name="<?php echo esc_attr('jigoshop[cartIcon_' . $cartIcon .']')?>" />
                    <input type="radio" name="<?php echo esc_attr($name); ?>"
                           id="<?php echo esc_attr('jigoshop[cartIcon_') . esc_attr($cartIcon) . ']' ?>"
                           value="<?php echo esc_attr($cartIcon) ?>"<?php $cartIcon == $value and print " checked"; ?> />
                </li>
			<?php endforeach; ?>
        </ul>
	<?php endif; ?>
</form>
