### Jigoshop Header Shopping Cart

### Installation

1. Upload the plugin folder to the '/wp-content/plugins/' directory
2. Activate plugin through the 'Plugins' menu in WordPress
3. Go to 'Jigoshop/Manage Licenses' to enter license key for your plugin
4. Navigate to `Jigoshop > Settings > Shopping Cart` to configure the settings.