<?php
/**
 * Plugin Name: Jigoshop Shopping Cart Header
 * Plugin URI: https://www.jigoshop.com/product/jigoshop-header-shopping-cart/
 * Description: This plugin adds shopping cart on your header menu
 * Version: 1.1
 * Author: @vasilguruli | JigoLtd
 */
// Define plugin name
define('JIGOSHOP_SHOPPING_CART_HEADER_NAME', 'Jigoshop Mollie Payment Gateway');
add_action('plugins_loaded', function () {
	load_plugin_textdomain('jigoshop_hsc', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	if (class_exists('\Jigoshop\Core')) {
		//Check version.
		//if (\Jigoshop\addRequiredVersionNotice(JIGOSHOP_SHOPPING_CART_HEADER_NAME, '2.0')) {
		//    return;
		//}
		//Check license.
		//$licence = new \Jigoshop\Licence(__FILE__, 'add_licence_number', 'http://www.jigoshop.com');
		//if (!$licence->isActive()) {
		//   return;
		//}
		// Define plugin directory for inclusions
		define('JIGOSHOP_SHOPPING_CART_HEADER_DIR', dirname(__FILE__));
		// Define plugin URL for assets
		define('JIGOSHOP_SHOPPING_CART_HEADER_URL', plugins_url('', __FILE__));
		//Init components.
		require_once(JIGOSHOP_SHOPPING_CART_HEADER_DIR . '/src/Jigoshop/Extension/HeaderShoppingCart/Init.php');
		if (is_admin()) {
			require_once(JIGOSHOP_SHOPPING_CART_HEADER_DIR . '/src/Jigoshop/Extension/HeaderShoppingCart/Admin.php');
		}else{
			require_once (JIGOSHOP_SHOPPING_CART_HEADER_DIR . '/src/Jigoshop/Extension/HeaderShoppingCart/Frontend.php');
		}
	}else {
		add_action('admin_notices', function () {
			echo '<div class="error"><p>';
			printf(__('%s requires Jigoshop plugin to be active. Code for plugin %s was not loaded.',
				'jigoshop_hsc'), JIGOSHOP_SHOPPING_CART_HEADER_NAME, JIGOSHOP_SHOPPING_CART_HEADER_NAME);
			echo '</p></div>';
		});
	}
});